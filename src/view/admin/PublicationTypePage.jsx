import React from 'react';
import { connect } from 'react-redux';
import { getAllPublicationTypes, createPublicationType, updatePublicationType, deletePublicationType } from '../redux/user.jsx';
import { Link } from 'react-router-dom';

class PublicationTypeModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#publicationViType').focus());
        });
    }

    show = (item) => {
        let { _id, type } = item ? item : { type: '' };
        type = T.language.parse(type, true);
        $('#publicationViType').val(type.vi);
        $('#publicationEnType').val(type.en);

        $(this.modal.current).modal('show');
        this.setState({ _id });
    }

    save = (e) => {
        e.preventDefault();
        const _id = this.state._id,
            changes = { type: JSON.stringify({ vi: $('#publicationViType').val().trim(), en: $('#publicationEnType').val().trim() }) };

        if (changes.type == '') {
            T.notify('Loại xuất bản bị trống!', 'danger');
            $('#publicationViType').focus();
        } else {
            if (_id) {
                this.props.update(_id, changes)
            } else {
                this.props.create(changes)
            }
            $(this.modal.current).modal('hide');
        }
    }

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Loại xuất bản</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='publicationViType'>Loại xuất bản</label>
                                <input className='form-control' id='publicationViType' type='text' placeholder='Loại xuất bản' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='publicationEnType'>Publication type</label>
                                <input className='form-control' id='publicationEnType' type='text' placeholder='Publication type' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button> </div>
                    </div>
                </form>
            </div>
        );
    }
}

class PublicationTypePage extends React.Component {
    constructor(props) {
        super(props);
        this.publicationTypeModal = React.createRef();
    }

    componentDidMount() {
        this.props.getAllPublicationTypes();
        T.ready('/user/publication-type');
    }

    create = (e) => {
        this.publicationTypeModal.current.show(null);
        e.preventDefault();
    }

    edit = (e, item) => {
        this.publicationTypeModal.current.show(item);
        e.preventDefault();
    }

    delete = (e, item) => {
        T.confirm('Xóa thông tin đề tài', 'Bạn có chắc bạn muốn xóa Cấp quản lý này?', true, isConfirm =>
            isConfirm && this.props.deletePublicationType(item._id, () => this.props.getAllPublicationTypes()));
        e.preventDefault();
    }

    render() {
        let table = null;
        if (this.props.user && this.props.user.publicationTypes && this.props.user.publicationTypes.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '100%' }}>Cấp quản lý</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.user.publicationTypes.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{T.language.parse(item.type, true).vi}</a>
                                </td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có thông tin!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-newspaper-o' /> Loại xuất bản</h1>
                </div>

                <div className='row tile'>{table}</div>

                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <PublicationTypeModal ref={this.publicationTypeModal} create={this.props.createPublicationType} update={this.props.updatePublicationType} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ user: state.user });
const mapActionsToProps = { getAllPublicationTypes, createPublicationType, updatePublicationType, deletePublicationType };
export default connect(mapStateToProps, mapActionsToProps)(PublicationTypePage);