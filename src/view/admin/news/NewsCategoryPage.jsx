import React from 'react';
import Category from '../Category.jsx';

export default class NewsCategoryPage extends React.Component {
    componentDidMount() {
        T.ready();
    }

    render() {
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-file' /> Tin tức: Danh mục</h1>
                </div>
                <Category type='news' uploadType='newsCategoryImage' />
            </main>
        );
    }
}