import '../common/scss/bootstrap/bootstrap.scss';
import '../common/scss/admin/main.scss';
import './admin.scss';

import T from '../common/js/common';
import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { connect, Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import system, { getSystemState, updateSystemState } from '../redux/system.jsx';
import menu from '../redux/menu.jsx';
import user, { changeUser } from '../redux/user.jsx';
import carousel, { changeCarouselItem } from '../redux/carousel.jsx';
import content from '../redux/content.jsx';
import category, { changeCategory } from '../redux/category.jsx';
import division from '../redux/division.jsx';
import slogan from '../redux/slogan.jsx';
import staffGroup from '../redux/staffGroup.jsx';
import video, { changeVideo } from '../redux/video.jsx';
import news from '../redux/news.jsx';
import event from '../redux/event.jsx';
import job from '../redux/job.jsx';
import statistic from '../redux/statistic.jsx';
import logo from '../redux/logo.jsx';
import testimony from '../redux/testimony.jsx';
import answer from '../redux/answer.jsx';
import role from '../redux/role.jsx';
import question from '../redux/question.jsx';
import staff from '../redux/staff.jsx';
import yearPoint from '../redux/yearPoint.jsx';
import ds_nv2_pkhcn from '../redux/dsNhiemVu2PhongKhoaHocCongNghe.jsx';
import dsPgsGsKhtnv from '../redux/dsPgsGsKhtnv.jsx';
import categoryOffice from '../redux/categoryOffice.jsx';
import dsViPhamKyLuat from '../redux/dsViPhamKyLuat.jsx';
import dsCanBoLamViec06Thang from '../redux/dsCanBoLamViec06Thang.jsx';
import dsCanBoLamViec712Thang from '../redux/dsCanBoLamViec712Thang.jsx';
import dsNv2Tv from '../redux/dsNhiemVu2ThuVien.jsx';
import dsChuaDGCBQL from '../redux/dsChuaDGCBQL.jsx';
import kqDgNgoaile from '../redux/kq_dg_ngoaile.jsx';
import dsGiamDM from '../redux/dsGiamDinhMuc.jsx';
import dsDiemThuong from '../redux/dsDiemThuong.jsx';
import dsDiemTru from '../redux/dsDiemTru.jsx';
import dsNv1Pdt from '../redux/dsNhiemVu1PhongDaoTao.jsx';
import dsNghiThaiSan from '../redux/dsNghiThaiSan.jsx';
import dsKQDGNgoaiLeCBQL from '../redux/dsKQDGNgoaiLeCBQL.jsx';
import dsKQDGNgoaiLeHCPV from '../redux/dsKQDGNgoaiLeHCPV.jsx';
import dsNv1Psdh from '../redux/dsNhiemVu1PhongSauDaiHoc.jsx';
import dsCoithiNv1 from '../redux/dsNhiemVu1CoiThi.jsx';
import dsTapSu from '../redux/dsTapSu.jsx';
import dsSauDaiHocTrongNuoc from '../redux/dsSauDaiHocTrongNuoc.jsx';
import qtTapSu from '../redux/qtTapSu.jsx';
import dsPhieuTinNhiem from '../redux/dsPhieuTinNhiem.jsx';
import dsCanboCongdoan from '../redux/dsCanboCongdoan.jsx';
import dmTyLeDinhMuc from '../redux/dmTyLeDinhMuc.jsx';
import dmChucVu from '../redux/dmChucVu.jsx';

import Loadable from 'react-loadable';
import Loading from '../common/Loading.jsx';
import AdminHeader from './AdminHeader.jsx';
import AdminMenu from './AdminMenu.jsx';
import ContactModal from './ContactModal.jsx';

// Load modules -------------------------------------------------------------------------------------------------------------------------------------
import contact, { initContact } from '../../module/contact/index.jsx';
import { addContact, changeContact } from '../../module/contact/redux.jsx';
import subscriber from '../../module/subscriber/index.jsx';
import photoBooth from '../../module/photoBooth/index.jsx';
import email from '../../module/email/index.jsx';
const modules = [contact, subscriber, email, photoBooth];

// Initialize Redux ---------------------------------------------------------------------------------------------------------------------------------
const reducers = {
    system, menu, user, staffGroup, carousel, content, division, slogan, video, statistic, testimony, answer, category, news, event, job, logo, role, question, staff,
    yearPoint, categoryOffice, dsViPhamKyLuat, dsNv2Tv, dsChuaDGCBQL, dsPgsGsKhtnv, dsSauDaiHocTrongNuoc, dsPhieuTinNhiem, dsCanboCongdoan, dsNv1Pdt, ds_nv2_pkhcn, dsCoithiNv1, dsTapSu,
    dsCanBoLamViec06Thang, dsCanBoLamViec712Thang, dsGiamDM, dsDiemThuong, dsDiemTru, dsNghiThaiSan, dsKQDGNgoaiLeCBQL, dsKQDGNgoaiLeHCPV, dsNv1Psdh,
    kqDgNgoaile, qtTapSu, dmTyLeDinhMuc, dmChucVu
};
modules.forEach(item =>
    Object.keys(item.redux).forEach(key => reducers[key] = item.redux[key]));

const store = createStore(combineReducers(reducers), {}, composeWithDevTools(applyMiddleware(thunk)));
store.dispatch(getSystemState());
window.T = T;

// Main DOM render ----------------------------------------------------------------------------------------------------------------------------------
class App extends React.Component {
    constructor(props) {
        super(props);
        this.contactModal = React.createRef();

        const ContactPageTag = Loadable({ loading: Loading, loader: () => import('./ContactPage.jsx') });
        initContact(this.showContactModal);

        this.routeData = [];
        modules.forEach(item => this.routeData.push(...item.routes));
        this.routeData.push(
            { path: '/user/settings', component: Loadable({ loading: Loading, loader: () => import('./SettingsPage.jsx') }) },
            // { path: '/user/contact', component: () => (<ContactPageTag showContactModal={this.showContactModal} />) },
            { path: '/user/division', component: Loadable({ loading: Loading, loader: () => import('./DivisionPage.jsx') }) },
            { path: '/user/staff/item/:staffId', component: Loadable({ loading: Loading, loader: () => import('./UserStaffEditPage.jsx') }) },
            { path: '/user/user', component: Loadable({ loading: Loading, loader: () => import('./UserPage.jsx') }) },
            { path: '/user/project/:userId', component: Loadable({ loading: Loading, loader: () => import('./ProjectPage.jsx') }) },
            { path: '/user/project-level', component: Loadable({ loading: Loading, loader: () => import('./ProjectLevelPage.jsx') }) },
            { path: '/user/project', component: Loadable({ loading: Loading, loader: () => import('./StaffProjectPage.jsx') }) },
            { path: '/user/publication/:userId', component: Loadable({ loading: Loading, loader: () => import('./PublicationPage.jsx') }) },
            { path: '/user/publication-type', component: Loadable({ loading: Loading, loader: () => import('./PublicationTypePage.jsx') }) },
            { path: '/user/publication', component: Loadable({ loading: Loading, loader: () => import('./StaffPublicationPage.jsx') }) },

            { path: '/user/role', component: Loadable({ loading: Loading, loader: () => import('./RolePage.jsx') }) },
            { path: '/user/menu/edit/:menuId', component: Loadable({ loading: Loading, loader: () => import('./MenuEditPage.jsx') }) },
            { path: '/user/menu', component: Loadable({ loading: Loading, loader: () => import('./MenuPage.jsx') }) },
            { path: '/user/component', component: Loadable({ loading: Loading, loader: () => import('./ComponentPage.jsx') }) },

            { path: '/user/carousel/edit/:carouselId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/CarouselEditPage.jsx') }) },
            { path: '/user/content/edit/:contentId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/ContentEditPage.jsx') }) },
            { path: '/user/slogan/edit/:sloganId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/SloganEditPage.jsx') }) },
            { path: '/user/staff-group/edit/:staffGroupId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/StaffGroupEditPage.jsx') }) },
            { path: '/user/statistic/edit/:statisticId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/StatisticEditPage.jsx') }) },
            { path: '/user/logo/edit/:logoId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/LogoEditPage.jsx') }) },
            { path: '/user/testimony/edit/:testimonyId', component: Loadable({ loading: Loading, loader: () => import('./componentPage/TestimonyEditPage.jsx') }) },

            { path: '/user/news/category', component: Loadable({ loading: Loading, loader: () => import('./news/NewsCategoryPage.jsx') }) },
            { path: '/user/news/list', component: Loadable({ loading: Loading, loader: () => import('./news/NewsPage.jsx') }) },
            { path: '/user/news/edit/:newsId', component: Loadable({ loading: Loading, loader: () => import('./news/NewsEditPage.jsx') }) },
            { path: '/user/news/draft/edit/:draftId', component: Loadable({ loading: Loading, loader: () => import('./news/NewsDraftEditPage.jsx') }) },
            { path: '/user/news/draft', component: Loadable({ loading: Loading, loader: () => import('./news/NewsWaitApprovalPage.jsx') }) },

            { path: '/user/event/category', component: Loadable({ loading: Loading, loader: () => import('./event/EventCategoryPage.jsx') }) },
            { path: '/user/event/list', component: Loadable({ loading: Loading, loader: () => import('./event/EventPage.jsx') }) },
            { path: '/user/event/edit/:eventId', component: Loadable({ loading: Loading, loader: () => import('./event/EventEditPage.jsx') }) },
            { path: '/user/event/draft/edit/:draftId', component: Loadable({ loading: Loading, loader: () => import('./event/EventDraftEditPage.jsx') }) },
            { path: '/user/event/draft', component: Loadable({ loading: Loading, loader: () => import('./event/EventWaitApprovalPage.jsx') }) },
            { path: '/user/event/registration/:eventId', component: Loadable({ loading: Loading, loader: () => import('./event/EventRegistrationPage.jsx') }) },

            { path: '/user/job/category', component: Loadable({ loading: Loading, loader: () => import('./job/JobCategoryPage.jsx') }) },
            { path: '/user/job/list', component: Loadable({ loading: Loading, loader: () => import('./job/JobPage.jsx') }) },
            { path: '/user/job/edit/:newsId', component: Loadable({ loading: Loading, loader: () => import('./job/JobEditPage.jsx') }) },
            { path: '/user/job/draft/edit/:draftId', component: Loadable({ loading: Loading, loader: () => import('./job/JobDraftEditPage.jsx') }) },
            { path: '/user/job/draft', component: Loadable({ loading: Loading, loader: () => import('./job/JobWaitApprovalPage.jsx') }) },
            { path: '/user/job/registration/:jobId', component: Loadable({ loading: Loading, loader: () => import('./job/JobRegistrationPage.jsx') }) },

            { path: '/user/category-office/upload-point', component: Loadable({ loading: Loading, loader: () => import('./tchc/CategoryOfficeImportPage.jsx') }) },
            { path: '/user/category-office', component: Loadable({ loading: Loading, loader: () => import('./tchc/CategoryOfficePage.jsx') }) },
            { path: '/user/summary/year/upload-point', component: Loadable({ loading: Loading, loader: () => import('./tchc/YearPointImportPage.jsx') }) },
            { path: '/user/summary/year', component: Loadable({ loading: Loading, loader: () => import('./tchc/YearPointPage.jsx') }) },
            { path: '/user/summary/pgsgs_khtnv/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsPgsGsKhongHoanThanhNhiemVuImportPage.jsx') }) },
            { path: '/user/summary/pgsgs_khtnv', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsPgsGsKhongHoanThanhNhiemVuPage.jsx') }) },
            { path: '/user/summary/kq-dg-ngoaile/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/KqDgNgoaileImportPage.jsx') }) },
            { path: '/user/summary/kq-dg-ngoaile', component: Loadable({ loading: Loading, loader: () => import('./tchc/KqDgNgoaile.jsx') }) },
            { path: '/user/summary/ds-nhiem-vu-1-phong-dao-tao-sdh/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu1PhongSauDaiHocImportPage.jsx') }) },
            { path: '/user/summary/ds-nhiem-vu-1-phong-dao-tao-sdh', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu1PhongSauDaiHocPage.jsx') }) },
            { path: '/user/summary/ds_vi_pham_ky_luat/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsViPhamKyLuatImportPage.jsx') }) },
            { path: '/user/summary/ds_vi_pham_ky_luat', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsViPhamKyLuatPage.jsx') }) },
            { path: '/user/summary/ds_can_bo_lam_viec_06_thang/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsCanBoLamViec06ThangImportPage.jsx') }) },
            { path: '/user/summary/ds_can_bo_lam_viec_06_thang', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsCanBoLamViec06ThangPage.jsx') }) },
            { path: '/user/summary/ds_can_bo_lam_viec_712_thang/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsCanBoLamViec712ThangImportPage.jsx') }) },
            { path: '/user/summary/ds_can_bo_lam_viec_712_thang', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsCanBoLamViec712ThangPage.jsx') }) },
            { path: '/user/summary/ds_nhiem_vu_2_thu_vien/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu2ThuVienImportPage.jsx') }) },
            { path: '/user/summary/ds_nhiem_vu_2_thu_vien', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu2ThuVienPage.jsx') }) },
            { path: '/user/summary/ds-chua-dg-cbql/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsChuaDanhGiaCanBoQuanLyImportPage.jsx') }) },
            { path: '/user/summary/ds-chua-dg-cbql', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsChuaDanhGiaCanBoQuanLyPage.jsx') }) },
            { path: '/user/summary/ds-giam-dinh-muc/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsGiamDinhMucImportPage.jsx') }) },
            { path: '/user/summary/ds-giam-dinh-muc', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsGiamDinhMucPage.jsx') }) },
            { path: '/user/summary/ds-diem-thuong/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsDiemThuongImportPage.jsx') }) },
            { path: '/user/summary/ds-diem-thuong', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsDiemThuongPage.jsx') }) },
            { path: '/user/summary/ds-diem-tru/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsDiemTruImportPage.jsx') }) },
            { path: '/user/summary/ds-diem-tru', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsDiemTruPage.jsx') }) },
            { path: '/user/summary/ds-nhiem-vu-1-phong-dao-tao/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu1PhongDaoTaoImportPage.jsx') }) },
            { path: '/user/summary/ds-nhiem-vu-1-phong-dao-tao', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu1PhongDaoTaoPage.jsx') }) },
            { path: '/user/summary/ds-nghi-thai-san/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNghiThaiSanImportPage.jsx') }) },
            { path: '/user/summary/ds-nghi-thai-san', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNghiThaiSanPage.jsx') }) },
            { path: '/user/summary/ds-kqdg-ngoai-le-hcpv/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsKQDGNgoaiLeHCPVImportPage.jsx') }) },
            { path: '/user/summary/ds-kqdg-ngoai-le-hcpv', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsKQDGNgoaiLeHCPVPage.jsx') }) },
            { path: '/user/summary/ds-kqdg-ngoai-le-cbql/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsKQDGNgoaiLeCBQLImportPage.jsx') }) },
            { path: '/user/summary/ds-kqdg-ngoai-le-cbql', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsKQDGNgoaiLeCBQLPage.jsx') }) },
            { path: '/user/summary/ds-tap-su/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsTapSuImportPage.jsx') }) },
            { path: '/user/summary/ds-tap-su', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsTapSuPage.jsx') }) },
            { path: '/user/summary/qt-tap-su/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/QtTapSuImportPage.jsx') }) },
            { path: '/user/summary/qt-tap-su', component: Loadable({ loading: Loading, loader: () => import('./tchc/QtTapSuPage.jsx') }) },
            { path: '/user/summary/ds-sdh-trong-nuoc/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsSauDaiHocTrongNuocImportPage.jsx') }) },
            { path: '/user/summary/ds-sdh-trong-nuoc', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsSauDaiHocTrongNuocPage.jsx') }) },
            { path: '/user/summary/ds-phieu-tin-nhiem/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsPhieuTinNhiemImportPage.jsx') }) },
            { path: '/user/summary/ds-phieu-tin-nhiem', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsPhieuTinNhiemPage.jsx') }) },
            { path: '/user/summary/danh-muc-ty-le-dinh-muc/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DmTyLeDinhMucImportPage.jsx') }) },
            { path: '/user/summary/danh-muc-ty-le-dinh-muc', component: Loadable({ loading: Loading, loader: () => import('./tchc/DmTyLeDinhMucPage.jsx') }) },
            { path: '/user/summary/nv2-pkhcn/upload-point', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu2PhongKhoaHocCongNgheImportPage.jsx') }) },
            { path: '/user/summary/nv2-pkhcn', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu2PhongKhoaHocCongNghePage.jsx') }) },
            { path: '/user/summary/ds-coithi-nv1/upload-point', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu1CoiThiImportFile.jsx') }) },
            { path: '/user/summary/ds-coithi-nv1', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsNhiemVu1CoiThiPage.jsx') }) },
            { path: '/user/summary/ds-canbo-congdoan/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsCanboCongdoanImportPage.jsx') }) },
            { path: '/user/summary/ds-canbo-congdoan', component: Loadable({ loading: Loading, loader: () => import('./tchc/DsCanboCongdoanPage.jsx') }) },
            { path: '/user/summary/dm-chucvu/upload', component: Loadable({ loading: Loading, loader: () => import('./tchc/DmChucVuImportPage.jsx') }) },
            { path: '/user/summary/dm-chucvu', component: Loadable({ loading: Loading, loader: () => import('./tchc/DmChucVuPage.jsx') }) },

            { path: '/user/dashboard', component: Loadable({ loading: Loading, loader: () => import('./DashboardPage.jsx') }) },
            { path: '/user', component: Loadable({ loading: Loading, loader: () => import('./ProfilePage.jsx') }) },
        );
        this.routes = this.routeData.map((route, index) => (<Route key={index} {...route} />));
    }

    componentDidMount() {
        T.socket.on('category-changed', item => store.dispatch(changeCategory(item)));
        T.socket.on('carouselItem-changed', item => store.dispatch(changeCarouselItem(item)));
        T.socket.on('video-changed', item => store.dispatch(changeVideo(item)));

        T.socket.on('contact-added', item => store.dispatch(addContact(item)));
        T.socket.on('contact-changed', item => store.dispatch(changeContact(item)));

        T.socket.on('user-changed', user => {
            if (this.props.system && this.props.system.user && this.props.system.user._id == user._id) {
                store.dispatch(updateSystemState({ user: Object.assign({}, this.props.system.user, user) }));
            }
            store.dispatch(changeUser(user));
        });

        T.socket.on('debug-user-changed', user => {
            store.dispatch(updateSystemState({ user }));
        });

        T.socket.on('debug-role-changed', roles => {
            if (this.props.system && this.props.system.isDebug) {
                this.props.updateSystemState({ roles });
            }
        });
    }

    showContactModal = item => this.contactModal.current.show(item);

    render() {
        for (let i = 0, pathname = window.location.pathname; i < this.routeData.length; i++) {
            const route = T.routeMatcher(this.routeData[i].path);
            if (route.parse(pathname)) {
                return (
                    <BrowserRouter>
                        <div className='app sidebar-mini rtl'>
                            <AdminHeader showContactModal={this.showContactModal} />
                            <AdminMenu />
                            <div className='site-content'>
                                <Switch>{this.routes}</Switch>
                            </div>
                            <ContactModal ref={this.contactModal} />
                        </div >
                    </BrowserRouter>
                );
            }
        }

        return (
            <BrowserRouter>
                <main className='app'>
                    <Route component={Loadable({ loading: Loading, loader: () => import('../common/MessagePage.jsx') })} />
                </main >
            </BrowserRouter>
        );
    }
}

const Main = connect(state => ({ system: state.system }), { updateSystemState })(App);
ReactDOM.render(<Provider store={store}><Main /></Provider>, document.getElementById('app'));
