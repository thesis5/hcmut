import React from 'react';
import { connect } from 'react-redux';
import { getProjectInPage, createProject, updateProject, deleteProject, getAllProjectLevels, createProjectList } from '../redux/user.jsx';
import { Link } from 'react-router-dom';
import FileBox from '../common/FileBox.jsx';
import Pagination from '../common/Pagination.jsx';
import Select from 'react-select';

class UploadProjectModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
        this.fileBox = React.createRef();
        this.btnSave = React.createRef();
    }

    show = () => {
        this.setState({ list: null });
        $(this.modal.current).modal('show');
    }

    save = (e) => {
        e.preventDefault();
        if (this.state.list) {
            this.props.createProjectList(this.state.list, () => $(this.modal.current).modal('hide'));
        }
    }

    render() {
        let numberOfNull = 0,
            table = this.state.list == null ? null : (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '100%' }}>Tiêu đề</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Thời gian</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Vai trò</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Cấp quản lý</th>
                        </tr>
                    </thead>
                    <tbody>{
                        this.state.list.map((project, index) => {
                            const invalidValue = project.title == null || project.startYear == null || project.endYear == null || project.isLeader == null || project.level == null,
                                style = {};
                            if (invalidValue) {
                                numberOfNull++;
                                style.backgroundColor = 'red';
                                style.color = 'white';
                            }
                            return (
                                <tr key={index} style={style}>
                                    <td style={{ width: 'auto', textAlign: 'right' }}>{index + 1}</td>
                                    <td style={{ width: '100%' }}>{project.title}</td>
                                    <td style={{ width: 'auto' }} nowrap='true'>{project.startYear} - {project.endYear}</td>
                                    <td style={{ width: 'auto' }} nowrap='true'>{project.isLeader ? 'Chủ nhiệm' : 'Tham gia'}</td>
                                    <td style={{ width: 'auto' }} nowrap='true'>{project.levelText}</td>
                                </tr>
                            );
                        })
                    }</tbody>
                </table>
            );

        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Upload danh sách dự án</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='ProjectFile' userData='projectImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }}
                                success={({ error, list }) => list && this.setState({ list })} />
                            {table ? (numberOfNull > 0 ? <p style={{ color: 'red' }}>Số ô không có giá trị: {numberOfNull}.</p> : '') : ''}
                            {table}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' style={{ display: table ? 'block' : 'none' }} ref={this.btnSave}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class ProjectModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#projectTitle').focus());
        });
    }

    show = (item) => {
        const { _id, title, startYear, endYear, isLeader, level, finished } = item ? item : { title: '', startYear: '', endYear: '', isLeader: false, finished: false };
        $('#projectTitle').val(title);
        $('#projectStartYear').val(startYear);
        $('#projectEndYear').val(endYear);
        $('#projectIsLeader').prop('checked', isLeader);
        $('#projectFinished').prop('checked', finished);

        $(this.modal.current).modal('show');
        this.setState({ _id, projectLevel: level });
    }

    save = (e) => {
        e.preventDefault();
        const _id = this.state._id,
            changes = {
                title: $('#projectTitle').val().trim(),
                startYear: $('#projectStartYear').val().trim(),
                endYear: $('#projectEndYear').val().trim(),
                isLeader: $('#projectIsLeader').prop('checked'),
                level: this.state.projectLevel,
                finished: $('#projectFinished').prop('checked'),
            };

        if (changes.title == '') {
            T.notify('Tiêu đề đề tài bị trống!', 'danger');
            $('#projectTitle').focus();
        } else if (changes.title == null) {
            T.notify('Cấp quản lý đề tài bị trống!', 'danger');
        } else {
            if (_id) {
                this.props.update(_id, changes)
            } else {
                this.props.create(changes)
            }
            $(this.modal.current).modal('hide');
        }
    }

    render() {
        let projectLevel = this.state.projectLevel;
        if (projectLevel && this.props.projectLevelOptions && this.props.projectLevels[projectLevel]) {
            projectLevel = { value: projectLevel, label: this.props.projectLevels[projectLevel] };
        } else {
            projectLevel = null;
        }

        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin đề tài</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='projectTitle'>Tiêu đề</label>
                                <input className='form-control' id='projectTitle' type='text' placeholder='Tiêu đề' />
                            </div>

                            <div className='row'>
                                <div className='col-md-6 form-group'>
                                    <label htmlFor='projectStartYear'>Năm đề tài bắt đầu</label>
                                    <input className='form-control' id='projectStartYear' type='number' placeholder='Năm đề tài bắt đầu' />
                                </div>
                                <div className='col-md-6 form-group'>
                                    <label htmlFor='projectEndYear'>Năm đề tài kết thúc</label>
                                    <input className='form-control' id='projectEndYear' type='number' placeholder='Năm đề tài kết thúc' />
                                </div>

                                <div className='col-md-6 form-group' style={{ display: 'inline-flex', width: '100%' }}>
                                    <label htmlFor='projectIsLeader'>Là chủ nhiệm đề tài: </label>&nbsp;&nbsp;
                                    <div className='toggle'>
                                        <label>
                                            <input type='checkbox' id='projectIsLeader' onChange={() => { }} /><span className='button-indecator' />
                                        </label>
                                    </div>
                                </div>
                                <div className='col-md-6 form-group' style={{ display: 'inline-flex', width: '100%' }}>
                                    <label htmlFor='projectFinished'>Đã nghiệm thu: </label>&nbsp;&nbsp;
                                    <div className='toggle'>
                                        <label>
                                            <input type='checkbox' id='projectFinished' onChange={() => { }} /><span className='button-indecator' />
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div className='form-group'>
                                <label>Cấp quản lý</label>
                                <Select value={projectLevel} options={this.props.projectLevelOptions} placeholder='Cấp quản lý'
                                    onChange={selectedItem => this.setState({ projectLevel: selectedItem ? selectedItem.value : null })} />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class ProjectPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { projectLevels: {}, projectLevelOptions: [] };
        this.projectModal = React.createRef();
        this.uploadProjectModal = React.createRef();
    }

    componentDidMount() {
        this.props.getProjectInPage();
        this.props.getAllProjectLevels(levels => {
            const projectLevels = {},
                projectLevelOptions = (levels ? levels : []).map(item => {
                    projectLevels[item._id] = T.language.parse(item.level, true).vi;
                    return { value: item._id, label: T.language.parse(item.level, true).vi };
                });
            this.setState({ projectLevels, projectLevelOptions });
        });

        T.ready('/user/user');
    }

    create = (e) => {
        this.projectModal.current.show(null);
        e.preventDefault();
    }

    edit = (e, item) => {
        this.projectModal.current.show(item);
        e.preventDefault();
    }

    changeFinished = (item) => {
        this.props.updateProject(item._id, { finished: !item.finished }, () => this.props.getProjectInPage());
    }

    delete = (e, item) => {
        T.confirm('Xóa thông tin đề tài', 'Bạn có chắc bạn muốn xóa thông tin đề tài này?', true, isConfirm =>
            isConfirm && this.props.deleteProject(item._id, () => this.props.getProjectInPage()));
        e.preventDefault();
    }

    showUploadProjectModal = (e) => {
        this.uploadProjectModal.current.show();
        e.preventDefault();
    }

    render() {
        const { projectLevels, projectLevelOptions } = this.state;
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.user && this.props.user.projectPage ?
            this.props.user.projectPage : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.user && this.props.user.projectPage && this.props.user.projectPage.list && this.props.user.projectPage.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '100%' }}>Tiêu đề</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Năm</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Chủ nhiệm</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Cấp quản lý</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Nghiệm thu</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.user.projectPage.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.title}</a>
                                </td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.startYear + ' - ' + item.endYear}</td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.isLeader ? 'Chủ nhiệm' : 'Tham gia'}</td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.level && projectLevels[item.level] ? T.language.parse(projectLevels[item.level], true).vi : '<no value>'}</td>
                                <td className='toggle' style={{ textAlign: 'center' }}>
                                    <label>
                                        <input type='checkbox' checked={item.finished} onChange={() => this.changeFinished(item)} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có thông tin!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-cog' /> Đề tài</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/user'><i className='fa fa-home fa-lg' /></Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/user/user'>Nhân viên</Link>
                        </li>
                        <li className='breadcrumb-item'>Đề tài</li>
                    </ul>
                </div>

                <div className='row tile'>{table}</div>
                <Pagination name='adminProject' style={{ marginLeft: '10px' }}
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} getPage={this.props.getProjectInPage} />

                <a href='#' className='btn btn-circle' style={{ position: 'fixed', right: '190px', bottom: '10px', backgroundColor: '#0F0' }} onClick={this.showUploadProjectModal}>
                    <i className='fa fa-lg fa-upload' />
                </a>
                <a href='/download/project.xlsx' className='btn btn-secondary btn-circle' style={{ position: 'fixed', right: '130px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-download' />
                </a>

                <Link to='/user/user' className='btn btn-info btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-user' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <ProjectModal ref={this.projectModal} projectLevelOptions={projectLevelOptions} projectLevels={projectLevels}
                    create={this.props.createProject} update={this.props.updateProject} />
                <UploadProjectModal ref={this.uploadProjectModal} createProjectList={this.props.createProjectList} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ user: state.user });
const mapActionsToProps = { getProjectInPage, createProject, updateProject, deleteProject, getAllProjectLevels, createProjectList };
export default connect(mapStateToProps, mapActionsToProps)(ProjectPage);