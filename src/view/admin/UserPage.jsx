import React from 'react';
import { connect } from 'react-redux';
import { getAllDivisions } from '../redux/division.jsx';
import { getUserInPage, createUser, updateUser, deleteUser } from '../redux/user.jsx'
import { Link } from 'react-router-dom';
import Pagination from '../common/Pagination.jsx';
import { UserPasswordModal, RolesModal, UserModal, MoreInfoModal, UploadStaffModal } from './UserModal.jsx';

class UserPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { personType: 'Tất cả', searchText: '', isSearching: false };
        this.userModal = React.createRef();
        this.passwordModal = React.createRef();
        this.rolesModal = React.createRef();
        this.moreInfoModal = React.createRef();
        this.uploadStaffModal = React.createRef();
    }

    componentDidMount() {
        this.props.getAllDivisions();
        this.props.getUserInPage(1, 50, {});
        T.tooltip();
        T.ready();
    }

    create = (e) => {
        this.userModal.current.show();
        e.preventDefault();
    };

    edit = (e, item) => {
        this.userModal.current.show(item);
        e.preventDefault();
    };

    editRoles = (e, item) => {
        this.rolesModal.current.show(item);
        e.preventDefault();
    };

    changePassword = (e, item) => {
        this.passwordModal.current.show(item);
        e.preventDefault();
    };

    changeActive = (item) => {
        this.props.updateUser(item._id, { active: !item.active });
    };

    delete = (e, item) => {
        T.confirm('Người dùng: Xóa người dùng', 'Bạn có chắc bạn muốn xóa người dùng này?', true, isConfirm => isConfirm && this.props.deleteUser(item._id));
        e.preventDefault();
    };

    moreInfo = (e, item) => {
        this.moreInfoModal.current.show(item);
        e.preventDefault();
    };

    search = (e, personType) => {
        e.preventDefault();
        let condition = {},
            searchText = $('#searchTextBox').val();
        if (personType == undefined) personType = this.state.personType;
        if (searchText) condition.searchText = searchText;
        if (personType == 'Nhân sự') condition.isStaff = true;
        if (personType == 'Sinh viên') condition.isStudent = true;

        this.props.getUserInPage(undefined, undefined, condition, () => {
            const isSearching = Object.keys(condition).length > 0;
            this.setState({ personType, searchText, isSearching });
        });
    };

    uploadStaff = (e) => {
        e.preventDefault();
        this.uploadStaffModal.current.show();
    };

    render() {
        let { pageNumber, pageSize, pageTotal, pageCondition, totalItem } = this.props.user && this.props.user.page ?
            this.props.user.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, pageCondition: {}, totalItem: 0 };
        let table = null;
        if (this.props.user && this.props.user.page && this.props.user.page.list && this.props.user.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '70%' }} colSpan={2}>Tên</th>
                            <th style={{ width: '30%' }}>Email</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Hình ảnh</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Vai trò</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Kích hoạt</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.user.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                {item.isStaff ? [
                                    <td key={0} style={{ borderRight: '0px solid #fff' }}>
                                        <a href='#'
                                            onClick={e => this.edit(e, item)}>{item.lastname + ' ' + item.firstname}</a>
                                        <br />
                                        <span>Nhân sự{item.isStudent ? ', Sinh viên' : null}</span>
                                    </td>,
                                    <td key={1} style={{ borderLeft: '0px solid #fff', textAlign: 'right' }}>
                                        <Link to={'/user/staff/item/' + item._id} className='text-info'>
                                            <i className='fa fa-lg fa-pencil' />
                                        </Link>
                                        &nbsp; &nbsp;
                                    <a className='text-success' href='#' onClick={e => this.moreInfo(e, item)}>
                                            <i className='fa fa-lg fa-graduation-cap' />
                                        </a>
                                        &nbsp; &nbsp;
                                    <Link to={'/user/project/' + item._id} className='text-danger'>
                                            <i className='fa fa-lg fa-cog' />
                                        </Link>
                                        &nbsp; &nbsp;
                                    <Link to={'/user/publication/' + item._id} className='text-info'>
                                            <i className='fa fa-lg fa-newspaper-o' />
                                        </Link>
                                    </td>
                                ] : (
                                        <td colSpan={2}>
                                            <a href='#' onClick={e => this.edit(e, item)}>{item.lastname + ' ' + item.firstname}</a>
                                            {item.isStudent ? [<br key={0} />, <span key={1}>Sinh viên</span>] : null}
                                        </td>
                                    )}
                                <td>{item.email}</td>
                                <td style={{ textAlign: 'center' }}>
                                    <img src={item.image ? item.image : '/img/avatar.png'} alt='avatar' style={{ height: '32px' }} />
                                </td>
                                <td style={{ textAlign: 'left' }}>
                                    <a href='#' onClick={e => this.editRoles(e, item)}>
                                        {item.roles ? item.roles.map((role, index) => <label style={{ whiteSpace: 'nowrap' }} key={index}>_ {role.name}</label>) : ''}
                                    </a>
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }}>
                                    <label>
                                        <input type='checkbox' checked={item.active} onChange={() => this.changeActive(item, index)} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td>
                                    <div className={item.default ? '' : 'btn-group'}>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        {item.default == false && !item.email.endsWith('@hcmut.edu.vn') && !item.email.endsWith('@oisp.edu.vn') ?
                                            <a className='btn btn-info' href='#' onClick={e => this.changePassword(e, item)}><i className='fa fa-lg fa-key' /></a> : ''}
                                        {item.default ? null :
                                            <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}><i className='fa fa-lg fa-trash' /></a>}
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-user' /> Người dùng</h1>
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <li style={{ padding: '.5rem' }}>
                            Loại người dùng:
                        </li>
                        <li className='nav-item dropdown'>
                            <a className='nav-link dropdown-toggle' data-toggle='dropdown' href='#' role='button' aria-haspopup='true' aria-expanded='false'>
                                {this.state.personType}
                            </a>
                            <div className='dropdown-menu' x-placement='bottom-start'
                                style={{ position: 'absolute', transform: `translate3d(0px, 35px, 0px)`, top: '0px', left: '0px', willChange: 'transform' }}>
                                <a className='dropdown-item' href='#' onClick={e => this.search(e, 'Tất cả')}>Tất cả</a>
                                <a className='dropdown-item' href='#' onClick={e => this.search(e, 'Sinh viên')}>Sinh viên</a>
                                <a className='dropdown-item' href='#' onClick={e => this.search(e, 'Nhân sự')}>Nhân sự</a>
                            </div>
                        </li>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}><i className='fa fa-search' /></a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>

                <div className='row tile'>{table}</div>
                <Pagination name='adminUser' pageCondition={pageCondition}
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getUserInPage} />

                <button type='button' className='btn btn-success btn-circle' data-toggle='tooltip' data-placement='top'
                    title='Tải lên danh sách nhân viên'
                    style={{ position: 'fixed', right: '70px', bottom: '10px' }} onClick={this.uploadStaff}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </button>

                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <UserModal ref={this.userModal}
                    allRoles={this.props.system && this.props.system.roles ? this.props.system.roles : []}
                    allDivisions={this.props.division ? this.props.division : []}
                    updateUser={this.props.updateUser} createUser={this.props.createUser} getPage={this.props.getUserInPage} />
                <RolesModal ref={this.rolesModal}
                    allRoles={this.props.system && this.props.system.roles ? this.props.system.roles : []}
                    updateUser={this.props.updateUser} getPage={this.props.getUserInPage} />
                <UserPasswordModal updateUser={this.props.updateUser} ref={this.passwordModal} />
                <MoreInfoModal ref={this.moreInfoModal} updateUser={this.props.updateUser} />
                <UploadStaffModal ref={this.uploadStaffModal} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, division: state.division, user: state.user });
const mapActionsToProps = { getAllDivisions, getUserInPage, createUser, updateUser, deleteUser };
export default connect(mapStateToProps, mapActionsToProps)(UserPage);
