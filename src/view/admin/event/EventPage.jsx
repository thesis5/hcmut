import React from 'react';
import { connect } from 'react-redux';
import { getEventInPage, createEvent, updateEvent, swapEvent, deleteEvent } from '../../redux/event.jsx';
import { deleteManyAnswerByAdmin } from '../../redux/answer.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

class EventPage extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getEventInPage();
        T.ready();
    }

    create = (e) => {
        this.props.createEvent(data => this.props.history.push('/user/event/edit/' + data.item._id));
        e.preventDefault();
    }

    swap = (e, item, isMoveUp) => {
        this.props.swapEvent(item._id, isMoveUp);
        e.preventDefault();
    }

    changeActive = (item) => this.props.updateEvent(item._id, { active: !item.active })
    
    changeisInternal = (item) => this.props.updateEvent(item._id, { isInternal: !item.isInternal })

    delete = (e, item) => {
        T.confirm('Sự kiện', 'Bạn có chắc bạn muốn xóa sự kiện này?', 'warning', true, isConfirm =>
            isConfirm && this.props.deleteEvent(item._id));
        e.preventDefault();
    }

    render() {
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.event && this.props.event.page ?
            this.props.event.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.event && this.props.event.page && this.props.event.page.list && this.props.event.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '80%' }}>Tiêu đề</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Hình ảnh</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Kích hoạt</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Tin nội bộ</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.event.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    <Link to={'/user/event/edit/' + item._id} data-id={item._id}>
                                        {T.language.parse(item.title)}
                                    </Link>
                                </td>
                                <td style={{ width: '20%', textAlign: 'center' }}>
                                    <img src={item.image} alt='avatar' style={{ height: '32px' }} />
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <label>
                                        <input type='checkbox' checked={item.active} onChange={() => this.changeActive(item, index)} disabled={!currentPermission.contains('event:write')} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <label>
                                        <input type='checkbox' checked={item.isInternal} onChange={() => this.changeisInternal(item, index)} disabled={!currentPermission.contains('event:write')} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td className='btn-group'>
                                    <Link to={'/user/event/registration/' + item._id} data-id={item._id} className='btn btn-warning'>
                                        <i className='fa fa-lg fa-list-alt' />
                                    </Link>
                                    {currentPermission.contains('event:write') ? [
                                        <a key={0} className='btn btn-success' href='#' onClick={e => this.swap(e, item, true)}>
                                            <i className='fa fa-lg fa-arrow-up' />
                                        </a>,
                                        <a key={1} className='btn btn-success' href='#' onClick={e => this.swap(e, item, false)}>
                                            <i className='fa fa-lg fa-arrow-down' />
                                        </a>
                                    ] : null}
                                    <Link to={'/user/event/edit/' + item._id} data-id={item._id} className='btn btn-primary'>
                                        <i className='fa fa-lg fa-edit' />
                                    </Link>
                                    {currentPermission.contains('event:write') ?
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a> : null}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có bài viết!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-star' /> Sự kiện: Danh sách</h1>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pageEvent'
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getEventInPage} />
                {currentPermission.contains('event:write') ?
                    <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}
                        onClick={this.create}>
                        <i className='fa fa-lg fa-plus' />
                    </button> : null}
            </main>
        );
    }
}

const mapStateToProps = state => ({ event: state.event, system: state.system });
const mapActionsToProps = { getEventInPage, createEvent, updateEvent, swapEvent, deleteEvent, deleteManyAnswerByAdmin };
export default connect(mapStateToProps, mapActionsToProps)(EventPage);