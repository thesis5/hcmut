import React from 'react';
import { Link } from 'react-router-dom';
import Category from '../Category.jsx';

export default class EventCategoryPage extends React.Component {
    componentDidMount() {
        T.ready();
    }

    render() {
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-star' /> Sự kiện: Danh mục</h1>
                </div>
                <Category type='event' uploadType='eventCategoryImage' />
            </main>
        );
    }
}