import React from 'react';
import { connect } from 'react-redux';
import { getEventWithQuestion } from '../../redux/event.jsx';
import { adminGetEventRegistrationInPage } from '../../redux/answer.jsx';
import { Link } from 'react-router-dom';
import RegistrationResultComponent from '../../common/RegistrationResultComponent.jsx';

class EventRegistrationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { item: null };
    }

    componentDidMount() {
        T.ready('/user/event/list');
        const route = T.routeMatcher('/user/event/registration/:eventId'),
            params = route.parse(window.location.pathname);
        this.props.getEventWithQuestion(params.eventId, data => {
            if (data.error) {
                T.notify('Lấy sự kiện bị lỗi!', 'danger');
                this.props.history.push('/user/event/list');
            } else if (data.item) {
                this.setState(data);
            } else {
                this.props.history.push('/user/event/list');
            }
        });
    }

    render() {
        const item = this.props.event && this.props.event.event ? this.props.event.event : { _id: null, title: '', maxRegisterUsers: -1, questions: [] };
        const title = item.title != '' ? `Tiêu đề: <b>${item.title.viText()}</b> - ${T.dateToText(item.createdDate)}` : '';
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-edit' /> Sự kiện: Danh sách đăng ký tham gia</h1>
                        <p dangerouslySetInnerHTML={{ __html: title }} />
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <Link to='/user'><i className='fa fa-home fa-lg' /></Link>
                        &nbsp;/&nbsp;
                        <Link to='/user/event/list'>Danh sách sự kiện</Link>
                        &nbsp;/&nbsp;
                        <a href='#'>Danh sách đăng ký tham gia</a>
                    </ul>
                </div>
                <RegistrationResultComponent questions={item.questions} postId={item._id} field='questions' getPageType='eventQuestions'
                    getPage={this.props.adminGetEventRegistrationInPage} paginationName='pageEventRegistration' maxRegisterUsers={item.maxRegisterUsers}
                    permission={{ manager: 'event:registration', roller: 'event:roller', import: 'event:import', export: 'event:export' }}
                />

                <Link to='/user/event/list' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
            </main>
        );
    }
}

const mapStateToProps = state => ({ event: state.event });
const mapActionsToProps = { getEventWithQuestion, adminGetEventRegistrationInPage };
export default connect(mapStateToProps, mapActionsToProps)(EventRegistrationPage);