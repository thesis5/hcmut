import React from 'react';
import { connect } from 'react-redux';
import { getCarousel, updateCarousel, createCarouselItem, updateCarouselItem, swapCarouselItem, deleteCarouselItem } from '../../redux/carousel.jsx'
import { Link } from 'react-router-dom';
import ImageBox from '../../common/ImageBox.jsx';

class CarouselItemModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.modal = React.createRef();
        this.imageBox = React.createRef();
        this.btnSave = React.createRef();
    }

    componentDidMount() {
        $(this.modal.current).on('shown.bs.modal', () => $('#carViName').focus());
    }

    show = (item, carouselId) => {
        let { _id, title, description, image, link } = item ? item : { _id: null, title: '', description: '', image: '/img/avatar.png', link: '' };
        title = T.language.parse(title, true);
        description = T.language.parse(description, true);

        $(this.btnSave.current).data('id', _id).data('carouselId', carouselId);
        $('#carViName').data('title', title.vi).val(title.vi);
        $('#carEnName').data('title', title.en).val(title.en);
        $('#carLink').data('link', link).val(link);
        $('#carViDes').data('description', description.vi).val(description.vi);
        $('#carEnDes').data('description', description.en).val(description.en);

        this.setState({ image });
        this.imageBox.current.setData('CarouselItem:' + (_id ? _id : 'new'));

        $(this.modal.current).modal('show');
    }

    save = (e) => {
        e.preventDefault();
        const _id = $(e.target).data('id'),
            carouselId = $(e.target).data('carouselId'),
            changes = {
                title: JSON.stringify({ vi: $('#carViName').val().trim(), en: $('#carEnName').val().trim() }),
                link: $('#carLink').val().trim(),
                description: JSON.stringify({ vi: $('#carViDes').val().trim(), en: $('#carEnDes').val().trim() })
            };
        if (changes.title.vi == '') {
            T.notify('Tên hình ảnh bị trống!', 'danger');
            $('#carViName').focus();
        } else if (changes.title.en == '') {
            T.notify('Tên hình ảnh bị trống!', 'danger');
            $('#carEnName').focus();
        } else {
            if (_id) { // Update
                this.props.updateCarouselItem(_id, changes, error => {
                    if (error == undefined || error == null) {
                        $(this.modal.current).modal('hide');
                    }
                });
            } else { // Create
                changes.carouselId = carouselId;
                this.props.createCarouselItem(changes, () => $(this.modal.current).modal('hide'));
            }
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Hình ảnh</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <ul className='nav nav-tabs'>
                                <li className='nav-item'>
                                    <a className='nav-link active show' data-toggle='tab' href='#carouselViTab'>Việt Nam</a>
                                </li>
                                <li className='nav-item'>
                                    <a className='nav-link' data-toggle='tab' href='#carouselEnTab'>English</a>
                                </li>
                            </ul>
                            <div className='tab-content'>
                                <div id='carouselViTab' className='tab-pane fade show active'>
                                    <div className='form-group'>
                                        <label htmlFor='carViName'>Tên hình ảnh</label>
                                        <input className='form-control' id='carViName' type='text' placeholder='Tên hình ảnh' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='carViDes'>Mô tả hình ảnh</label>
                                        <textarea className='form-control' id='carViDes' rows='5' placeholder='Mô tả hình ảnh' />
                                    </div>
                                </div>
                                <div id='carouselEnTab' className='tab-pane fade'>
                                    <div className='form-group'>
                                        <label htmlFor='carEnName'>Image name</label>
                                        <input className='form-control' id='carEnName' type='text' placeholder='Image name' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='carEnDes'>Image description</label>
                                        <textarea className='form-control' id='carEnDes' rows='5' placeholder='Image description' />
                                    </div>
                                </div>
                            </div>

                            <div className='form-group'>
                                <label htmlFor='carLink'>Link liên kết</label>
                                <input className='form-control' id='carLink' type='text' placeholder='Link liên kết' />
                            </div>
                            <div className='form-group'>
                                <label>Hình đại diện</label>
                                <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='CarouselItemImage' userData='CarouselItem' image={this.state.image} />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='button' className='btn btn-primary' ref={this.btnSave} onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class CarouselEditPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.table = React.createRef();
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/component', () => {
            $('#crsTitle').focus();

            const route = T.routeMatcher('/user/carousel/edit/:carouselId'),
                params = route.parse(window.location.pathname);
            this.props.getCarousel(params.carouselId, data => {
                $('#crsTitle').val(data.title).focus();
                $('#crsHeight').val(data.height);
                this.setState(data);
            });
        });
    }

    save = () => {
        const changes = {
            title: $('#crsTitle').val(),
            height: parseInt($('#crsHeight').val()),
            single: this.state.single,
            active: this.state.active,
        };

        this.props.updateCarousel(this.state._id, changes);
    }

    createItem = (e) => {
        this.modal.current.show(null, this.state._id);
        e.preventDefault();
    }

    editItem = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    }

    swapItem = (e, item, isMoveUp) => {
        this.props.swapCarouselItem(item._id, isMoveUp);
        e.preventDefault();
    }

    changeItemActive = (item) => this.props.updateCarouselItem(item._id, { active: !item.active });

    deleteItem = (e, item) => {
        T.confirm('Xóa hình ảnh', 'Bạn có chắc bạn muốn xóa hình ảnh này?', true, isConfirm =>
            isConfirm && this.props.deleteCarouselItem(item._id));
        e.preventDefault();
    }

    render() {
        const currentPermissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            readOnly = !currentPermissions.includes('component:write');
        let items = this.props.carousel && this.props.carousel.selectedItem && this.props.carousel.selectedItem.items ? this.props.carousel.selectedItem.items : [],
            table = null;

        if (items.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: '80%' }}>Tiêu đề</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Hình ảnh</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Kích hoạt</th>
                            {!readOnly ? <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th> : null}
                        </tr>
                    </thead>
                    <tbody>
                        {items.map((item, index) => (
                            <tr key={index}>
                                <td>
                                    {readOnly ? T.language.parse(item.title) : <a href='#' onClick={e => this.editItem(e, item, index)}>
                                        {T.language.parse(item.title)}
                                    </a>}
                                </td>
                                <td style={{ width: '20%', textAlign: 'center' }}>
                                    <img src={T.url(item.image)} alt='avatar' style={{ height: '32px' }} />
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <label>
                                        <input type='checkbox' checked={item.active} onChange={() => !readOnly && this.changeItemActive(item, index)} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                {!readOnly ? <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-success' href='#' onClick={e => this.swapItem(e, item, true)}>
                                            <i className='fa fa-lg fa-arrow-up' />
                                        </a>
                                        <a className='btn btn-success' href='#' onClick={e => this.swapItem(e, item, false)}>
                                            <i className='fa fa-lg fa-arrow-down' />
                                        </a>
                                        <a className='btn btn-primary' href='#' onClick={e => this.editItem(e, item, index)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.deleteItem(e, item, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td> : null}
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có hình ảnh!</p>;
        }

        const { title, height } = this.props.carousel && this.props.carousel.selectedItem ? this.props.carousel.selectedItem : { title: '', height: 0 };
        const carouselTitle = title != '' ? 'Tên: <b>' + title + '</b>' : '';
        return (
            <main className='app-content' >
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-image' /> Tập hình ảnh: Chỉnh sửa</h1>
                        <p dangerouslySetInnerHTML={{ __html: carouselTitle }} />
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <Link to='/user'><i className='fa fa-home fa-lg' /></Link>
                        &nbsp;/&nbsp;
                        <Link to='/user/component'>Thành phần giao diện</Link>
                        &nbsp;/&nbsp;Chỉnh sửa
                    </ul>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='tile'>
                            <h3 className='tile-title'>Thông tin chung</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Tiêu đề tập hình ảnh</label>
                                    <input className='form-control' type='text' placeholder='Tiêu đề tập hình ảnh' id='crsTitle' defaultValue={title} readOnly={readOnly} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Chiều cao</label>
                                    <input className='form-control' type='number' placeholder='Chiều cao' id='crsHeight' defaultValue={height} style={{ textAlign: 'right' }} readOnly={readOnly} />
                                </div>
                                <div className='form-group row'>
                                    <label className='control-label col-3 col-sm-3'>Đơn ảnh</label>
                                    <div className='col-8 col-sm-8 toggle'>
                                        <label>
                                            <input type='checkbox' checked={this.state.single} onChange={e => !readOnly && this.setState({ single: e.target.checked })} /><span className='button-indecator' />
                                        </label>
                                    </div>
                                </div>
                                <div className='form-group row'>
                                    <label className='control-label col-3 col-sm-3'>Kích hoạt</label>
                                    <div className='col-8 col-sm-8 toggle'>
                                        <label>
                                            <input type='checkbox' checked={this.state.active} onChange={e => !readOnly && this.setState({ active: e.target.checked })} /><span className='button-indecator' />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {!readOnly ? <div className='tile-footer'>
                                <div className='row'>
                                    <div className='col-md-12' style={{ textAlign: 'right' }}>
                                        <button className='btn btn-primary' type='button' onClick={this.save}>
                                            <i className='fa fa-fw fa-lg fa-check-circle'></i>Lưu
                                        </button>
                                    </div>
                                </div>
                            </div> : null}
                        </div>
                    </div>

                    <div className='col-md-12'>
                        <div className='tile'>
                            <h3 className='tile-title'>Danh sách hình ảnh</h3>
                            <div className='tile-body'>
                                {table}
                            </div>
                        </div>
                    </div>
                </div>
                <Link to='/user/component' className='btn btn-secondary btn-circle' style={{ position: 'fixed', lefft: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                {!readOnly ? <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}
                    onClick={this.createItem}>
                    <i className='fa fa-lg fa-plus' />
                </button> : null}

                <CarouselItemModal ref={this.modal} createCarouselItem={this.props.createCarouselItem} updateCarouselItem={this.props.updateCarouselItem} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, carousel: state.carousel });
const mapActionsToProps = { getCarousel, updateCarousel, createCarouselItem, updateCarouselItem, swapCarouselItem, deleteCarouselItem };
export default connect(mapStateToProps, mapActionsToProps)(CarouselEditPage);
