import React from 'react';
import { connect } from 'react-redux';
import { getAllProjectLevels, createProjectLevel, updateProjectLevel, deleteProjectLevel } from '../redux/user.jsx';
import { Link } from 'react-router-dom';

class ProjectLevelModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#projectViLevel').focus());
        });
    }

    show = (item) => {
        let { _id, level } = item ? item : { level: '' };
        level = T.language.parse(level, true);
        $('#projectViLevel').val(level.vi);
        $('#projectEnLevel').val(level.en);

        $(this.modal.current).modal('show');
        this.setState({ _id });
    }

    save = (e) => {
        e.preventDefault();
        const _id = this.state._id,
            changes = { level: JSON.stringify({ vi: $('#projectViLevel').val().trim(), en: $('#projectEnLevel').val().trim() }) };

        if (changes.level == '') {
            T.notify('Cấp quản lý đề tài bị trống!', 'danger');
            $('#projectViLevel').focus();
        } else {
            if (_id) {
                this.props.update(_id, changes)
            } else {
                this.props.create(changes)
            }
            $(this.modal.current).modal('hide');
        }
    }

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cấp quản lý đề tài</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='projectViLevel'>Cấp quản lý đề tài</label>
                                <input className='form-control' id='projectViLevel' type='text' placeholder='Cấp quản lý đề tài' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='projectEnLevel'>Project type</label>
                                <input className='form-control' id='projectEnLevel' type='text' placeholder='Project type' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button> </div>
                    </div>
                </form>
            </div>
        );
    }
}

class ProjectLevelPage extends React.Component {
    constructor(props) {
        super(props);
        this.projectLevelModal = React.createRef();
    }

    componentDidMount() {
        this.props.getAllProjectLevels();
        T.ready('/user/project-level');
    }

    create = (e) => {
        this.projectLevelModal.current.show(null);
        e.preventDefault();
    }

    edit = (e, item) => {
        this.projectLevelModal.current.show(item);
        e.preventDefault();
    }

    delete = (e, item) => {
        T.confirm('Xóa thông tin đề tài', 'Bạn có chắc bạn muốn xóa Cấp quản lý này?', true, isConfirm =>
            isConfirm && this.props.deleteProjectLevel(item._id, () => this.props.getAllProjectLevels()));
        e.preventDefault();
    }

    render() {
        let table = null;
        if (this.props.user && this.props.user.projectLevels && this.props.user.projectLevels.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '100%' }}>Cấp quản lý</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.user.projectLevels.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{T.language.parse(item.level, true).vi}</a>
                                </td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-university' /> Cấp quản lý dự án</h1>
                </div>

                <div className='row tile'>{table}</div>

                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <ProjectLevelModal ref={this.projectLevelModal} create={this.props.createProjectLevel} update={this.props.updateProjectLevel} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ user: state.user });
const mapActionsToProps = { getAllProjectLevels, createProjectLevel, updateProjectLevel, deleteProjectLevel };
export default connect(mapStateToProps, mapActionsToProps)(ProjectLevelPage);