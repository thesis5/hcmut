import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getStaffInfo, updateStaffInfo } from '../redux/user.jsx';

const schema = {
    NGHI: { type: 'text' },
    IS_NNGOAI: { type: 'number' },
    LOAI: { type: 'text' },
    shcc: { type: 'text' },
    HO: { type: 'text' },
    TEN: { type: 'text' },
    PHAI: { type: 'text' },
    NGAY_SINH: { type: 'text', isDate: true },
    NGND: { type: 'text' },
    NGUT: { type: 'text' },
    NGAY_BD_CT: { type: 'text', isDate: true },
    NGAY_VAO: { type: 'text', isDate: true },
    NGAY_CBGD: { type: 'text', isDate: true },
    NGAY_BC: { type: 'text', isDate: true },
    NGAY_NGHI: { type: 'text', isDate: true },
    CHUCDANH_TRINHDO: { type: 'text' },
    NGACH: { type: 'text' },
    NGACH_MOI: { type: 'text' },
    CHUC_DANH: { type: 'text' },
    TRINH_DO: { type: 'text' },
    NUOC_NGOAI: { type: 'text' },
    GHI_CHU_IN: { type: 'text' },
    HESO_LG: { type: 'number' },
    BAC_LG: { type: 'text' },
    MOC_NANG_LG: { type: 'text', isDate: true },
    NGAY_HUONG_LG: { type: 'text', isDate: true },
    TY_LE_VUOT_KHUNG: { type: 'number' },
    PCCV: { type: 'number' },
    NGAY_PCCV: { type: 'text', isDate: true },
    MS_CVU: { type: 'text' },
    CHUC_VU_BCH_DANG_BO: { type: 'text' },
    CHUC_VU_BCH_CONG_DOAN: { type: 'text' },
    CHUC_VU_BCH_DOAN_TN: { type: 'text' },
    CHUYEN_NGANH: { type: 'text' },
    TDO_LLCT: { type: 'text' },
    NOI_DKHK: { type: 'text' },
    DC_HIENTAI: { type: 'text' },
    D_THOAI: { type: 'text' },
    E_mail: { type: 'text' },
    Dan_toc: { type: 'text' },
    Ton_giao: { type: 'text' },
    DANG_VIEN: { type: 'number' },
    MS_BM: { type: 'text' },
    TEN_BM: { type: 'text' },
    MS_KHOA: { type: 'text' },
    TEN_KHOA: { type: 'text' },
    PHUC_LOI: { type: 'text' },
    GHI_CHU: String
};

class UserStaffEditPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { staffId: null };
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        T.ready('/user/user');
        Object.keys(schema).forEach(key => {
            if (schema[key].isDate) $(this[key].current).datepicker({ format: 'dd/mm/yyyy', autoclose: true });
        });
        const route = T.routeMatcher('/user/staff/item/:staffId'), staffId = route.parse(window.location.pathname).staffId;
        this.props.getStaffInfo(staffId, (staff) => {
            this.setState({ staffId });
            $('#titleStaffName').html(staff.lastname + ' ' + staff.firstname);
            if (staff.staffInfo != {}) {
                const info = staff.staffInfo;
                Object.keys(schema).forEach(key => {
                    if (schema[key].isDate) {
                        info[key] && info[key] != '' ? $(this[key].current).datepicker('setDate', new Date(info[key])) : $(this[key].current).val('');
                        // $(this[key].current).val(info[key] ? T.dateToText(info[key], 'dd/mm/yyyy') : '');
                    } else {
                        $(this[key].current).val(info[key] ? info[key] : null);
                    }
                })
            }
        });
    }

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => {
            if (schema[key].isDate) {
                changes[key] = $(this[key].current).val() != '' ? T.formatDate($(this[key].current).val()) : null;
            } else {
                changes[key] = $(this[key].current).val();
            }
        });

        this.props.updateStaffInfo(this.state.staffId, changes, () => {
            T.notify('Cập nhật thông tin nhân viên thành công', 'success');
        });
    };

    render() {
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Nhân viên: <span id='titleStaffName' /></h1>
                </div>

                <div className='row'>
                    <div className='tile col-12'>
                        <h3 className='tile-title'>Thông tin nhân viên</h3>
                        <div className='tile-body row'>
                            {Object.keys(schema).map((key, index) => {
                                return (
                                    <div key={index} className='form-group col-12 col-md-6'>
                                        <label className='control-label'>{key}</label>
                                        <input className='form-control' ref={this[key]} type={schema[key].type} placeholder={key} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>

                <Link to='/user/user' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { getStaffInfo, updateStaffInfo };
export default connect(mapStateToProps, mapActionsToProps)(UserStaffEditPage);
