import React from 'react';
import { connect } from 'react-redux';
import { updateProfile } from '../redux/system.jsx'
import { getAllDivisionsByStaff } from '../redux/division.jsx';
import { Link } from 'react-router-dom';
import Dropdown from '../common/Dropdown.jsx';
import ImageBox from '../common/ImageBox.jsx';
import Select from 'react-select';
import Editor from '../common/CkEditor4.jsx';

import { getNames, getCode, getName } from 'country-list';

const academicTitles = T.academicTitles.map(label => ({ value: label, label }));
const academicDistinctions = T.academicDistinctions.map(label => ({ value: label, label }));
const countryList = getNames().map(label => ({ value: getCode(label), label }));

class ProfilePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { user: null };
        this.imageBox = React.createRef();

        this.saveCommon = this.saveCommon.bind(this);
        this.saveDetail = this.saveDetail.bind(this);
        this.savePassword = this.savePassword.bind(this);
        this.renderData = this.renderData.bind(this);

        this.firstname = React.createRef();
        this.lastname = React.createRef();
        this.email = React.createRef();
        this.phoneNumber = React.createRef();
        this.password1 = React.createRef();
        this.password2 = React.createRef();

        this.editorViBiography = React.createRef();
        this.editorEnBiography = React.createRef();
        this.editorViResearchFields = React.createRef();
        this.editorEnResearchFields = React.createRef();
        this.editorViPostgraduateCourses = React.createRef();
        this.editorEnPostgraduateCourses = React.createRef();
        this.editorViPrize = React.createRef();
        this.editorEnPrize = React.createRef();

        this.sex = React.createRef();
    }

    componentDidMount() {
        T.ready(() => {
            if (this.props.system && this.props.system.user) {
                const image = this.props.system.user.image ? this.props.system.user.image : '/img/avatar.png';
                this.setState({ image });
                if (this.props.system.user.isStaff) {
                    this.props.getAllDivisionsByStaff((data) => {
                        let allDivisions = (data.items ? data.items : []).map(item => ({
                            id: item._id,
                            text: item.name.getText()
                        }));
                        this.renderData(this.props.system.user, allDivisions, () => {
                            setTimeout(() => {
                                $('#divisions').select2();
                                $('#birthday').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
                            }, 250);
                        });
                    });
                } else {
                    this.renderData(this.props.system.user, [], () => {
                        setTimeout(() => {
                            $('#birthday').datepicker({ autoclose: true, format: 'dd/mm/yyyy' });
                        }, 250);
                    });
                }
            }
        });
    }

    renderData(user, allDivisions, callback) {
        const { isStaff } = this.props.system && this.props.system.user ? this.props.system.user : { isStaff: false };
        let {
            firstname, lastname, email, phoneNumber, birthday, sex, facebook, website, academicTitle,
            academicDistinction, graduatedCountry, academicTitleYear, divisions, publicationName, image,
            masterThesis, goingDoctorThesis, finishedDoctorThesis, trainingExpertise, currentExpertise,
            researchFields, postgraduateCourses, biography, prize
        } = user ?
                user : {
                    firstname: '', lastname: '', phoneNumber: '', birthday: '', sex: '', facebook: '', website: '', academicTitle: '', academicDistinction: '',
                    graduatedCountry: '', academicTitleYear: '', divisions: [], publicationName: '', image: '/img/avatar.png', masterThesis: 0, goingDoctorThesis: 0, finishedDoctorThesis: 0, trainingExpertise: '', currentExpertise: '',
                    researchFields: '', postgraduateCourses: '', biography: '', prize: ''
                };

        divisions = divisions.map(division => division._id);
        trainingExpertise = T.language.parse(trainingExpertise, true);
        currentExpertise = T.language.parse(currentExpertise, true);
        biography = T.language.parse(biography, true);
        prize = T.language.parse(prize, true);
        researchFields = T.language.parse(researchFields, true);
        postgraduateCourses = T.language.parse(postgraduateCourses, true);

        $('#userLastname').val(lastname);
        $('#userFirstname').val(firstname);
        $('#email').html(email);
        $('#birthday').val(birthday ? T.dateToText(birthday, 'dd/mm/yyyy') : '');
        $('#phoneNumber').val(phoneNumber);
        this.sex.current.setText(sex ? sex : '');
        this.imageBox.current.setData('profile', image ? image : '/img/avatar.png');

        if (isStaff) {
            $('#publicationName').val(publicationName);

            $('#facebook').val(facebook);
            $('#website').val(website);
            $('#academicTitleYear').val(academicTitleYear);
            $('#divisions').select2({
                placeholder: 'Đơn vị công tác',
                data: allDivisions
            }).val(divisions ? divisions : []).trigger('change');

            $('#masterThesis').val(masterThesis);
            $('#finishedDoctorThesis').val(finishedDoctorThesis);
            $('#goingDoctorThesis').val(goingDoctorThesis);

            $('#viTrainingExpertise').val(trainingExpertise.vi);
            $('#enTrainingExpertise').val(trainingExpertise.en);
            $('#viCurrentExpertise').val(currentExpertise.vi);
            $('#enCurrentExpertise').val(currentExpertise.en);

            this.editorViBiography.current.html(biography.vi);
            this.editorEnBiography.current.html(biography.en);
            this.editorViResearchFields.current.html(researchFields.vi);
            this.editorEnResearchFields.current.html(researchFields.en);
            this.editorViPostgraduateCourses.current.html(postgraduateCourses.vi);
            this.editorEnPostgraduateCourses.current.html(postgraduateCourses.en);
            this.editorViPrize.current.html(prize.vi);
            this.editorEnPrize.current.html(prize.en);

            this.setState({ academicTitle, academicDistinction, graduatedCountry });
        }
        callback && callback();
    }

    saveCommon(e) {
        const { isStaff } = this.props.system && this.props.system.user ? this.props.system.user : { isStaff: false };
        let sex = this.sex.current.getSelectedItem().toLowerCase(),
            birthday = $('#birthday').val() ? T.formatDate($('#birthday').val()) : null,
            changes = {
                firstname: $('#userFirstname').val(),
                lastname: $('#userLastname').val(),
                phoneNumber: $('#phoneNumber').val(),
                birthday: birthday ? birthday : 'empty',
            };
        if (T.sexes.indexOf(sex) != -1) {
            changes.sex = sex;
        }
        if (isStaff) {
            changes = Object.assign({}, changes, {
                facebook: $('#facebook').val(),
                website: $('#website').val(),
                academicTitle: this.state.academicTitle ? this.state.academicTitle : '',
                academicDistinction: this.state.academicDistinction ? this.state.academicDistinction : '',
                graduatedCountry: this.state.graduatedCountry ? this.state.graduatedCountry : null,
                academicTitleYear: $('#academicTitleYear').val(),
                divisions: $('#divisions').val(),
                publicationName: $('#publicationName').val(),
            });
        }
        if (changes.firstname == '') {
            T.notify('Tên bị trống!', 'danger');
            $('#userFirstName').focus();
        } else if (changes.lastname == '') {
            T.notify('Họ và tên lót bị trống!', 'danger');
            $('#userLastName').focus();
        } else {
            if (changes.divisions && changes.divisions.length == 0) changes.divisions = 'empty';
            this.props.updateProfile(changes);
        }
        e.preventDefault();
    }

    saveDetail(e) {
        const trainingExpertise = {
            vi: $('#viTrainingExpertise').val(),
            en: $('#enTrainingExpertise').val(),
        },
            currentExpertise = {
                vi: $('#viCurrentExpertise').val(),
                en: $('#enCurrentExpertise').val(),
            },
            biography = {
                vi: this.editorViBiography.current.html(),
                en: this.editorEnBiography.current.html(),
            },
            prize = {
                vi: this.editorViPrize.current.html(),
                en: this.editorEnPrize.current.html()
            },
            researchFields = {
                vi: this.editorViResearchFields.current.html(),
                en: this.editorEnResearchFields.current.html(),
            },
            postgraduateCourses = {
                vi: this.editorViPostgraduateCourses.current.html(),
                en: this.editorEnPostgraduateCourses.current.html(),
            },
            masterThesis = parseInt($('#masterThesis').val()),
            goingDoctorThesis = parseInt($('#goingDoctorThesis').val()),
            finishedDoctorThesis = parseInt($('#finishedDoctorThesis').val()),
            changes = {
                masterThesis: isNaN(masterThesis) ? 0 : masterThesis,
                goingDoctorThesis: isNaN(goingDoctorThesis) ? 0 : goingDoctorThesis,
                finishedDoctorThesis: isNaN(finishedDoctorThesis) ? 0 : finishedDoctorThesis,
                trainingExpertise: JSON.stringify(trainingExpertise),
                currentExpertise: JSON.stringify(currentExpertise),
                biography: JSON.stringify(biography),
                prize: JSON.stringify(prize),
                researchFields: JSON.stringify(researchFields),
                postgraduateCourses: JSON.stringify(postgraduateCourses),
            };
        console.log(changes)
        this.props.updateProfile(changes);
        e.preventDefault();
    }

    savePassword() {
        const password1 = $(this.password1.current).val(),
            password2 = $(this.password2.current).val(),
            email = $(this.email.current).val().trim();
        if (!T.isBKer(email)) {
            if (password1 == '') {
                T.notify('Mật khẩu mới của bạn bị trống!', 'danger');
                $(this.password1.current).focus();
            } else if (password2 == '') {
                T.notify('Bạn vui lòng nhập lại mật khẩu!', 'danger');
                $(this.password2.current).focus();
            } else if (password1 != password2) {
                T.notify('Mật khẩu không trùng nhau!', 'danger');
                $(this.password1.current).focus();
            } else {
                this.props.updateProfile({ password: password1 });
            }
        }
    }

    render() {
        const { isStaff, email } = this.props.system && this.props.system.user ? this.props.system.user : { isStaff: false, email: '' };
        const academicTitle = this.state.academicTitle ? { value: this.state.academicTitle, label: this.state.academicTitle } : null;
        const academicDistinction = this.state.academicDistinction ? { value: this.state.academicDistinction, label: this.state.academicDistinction } : null;
        const graduatedCountry = this.state.graduatedCountry ? { value: this.state.graduatedCountry, label: getName(this.state.graduatedCountry) } : null;
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Thông tin cá nhân</h1>
                </div>
                <div className='row'>
                    {isStaff ? (
                        <div className='col-12'>
                            <div className='tile'>
                                <h3 className='tile-title'>Thông tin cá nhân</h3>
                                <div className='tile-body'>
                                    <div className='row'>
                                        <div className='col-8'>
                                            <div className='row'>
                                                <div className='col-6'>
                                                    <div className='form-group'>
                                                        <label className='control-label'
                                                            htmlFor='userLastname'>Họ và tên lót</label>
                                                        <input className='form-control' type='text'
                                                            placeholder='Họ và tên lót' id='userLastname' />
                                                    </div>
                                                </div>
                                                <div className='col-6'>
                                                    <div className='form-group'>
                                                        <label className='control-label' htmlFor='userFirstname'>Tên</label>
                                                        <input className='form-control' type='text' id='userFirstname' placeholder='Tên' />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='form-group'>
                                                <label className='control-label' htmlFor='publicationName'>Tên viết tắt (trong bài báo khoa học)</label>
                                                <input className='form-control' type='text' placeholder='Tên viết tắt (trong bài báo khoa học)' id='publicationName' />
                                            </div>
                                            <div className='row'>
                                                <div className='col-6'>
                                                    <div className='form-group'>
                                                        <label className='control-label' htmlFor='birthday'>Ngày
                                                            sinh</label>
                                                        <input className='form-control' type='text'
                                                            placeholder='Ngày sinh' id='birthday' />
                                                    </div>
                                                </div>
                                                <div className='col-6'>
                                                    <div className='form-group'>
                                                        <label className='control-label' htmlFor='phoneNumber'>Số điện thoại</label>
                                                        <input className='form-control' type='text' placeholder='Số điện thoại' id='phoneNumber' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-4 form-group'>
                                            <div className='form-group'>
                                                <label className='control-label'>Hình đại diện</label>
                                                <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='ProfileImage' userData='profile' image={this.state.image} />
                                            </div>
                                            <div className='form-group'
                                                style={{ display: 'inline-flex', width: '100%' }}>
                                                <label>Giới tính: </label>&nbsp;&nbsp;
                                                <Dropdown ref={this.sex} text='' items={T.sexes} />
                                            </div>
                                            <div className='form-group'
                                                style={{ display: 'inline-flex', width: '100%' }}>
                                                <label className='control-label'>Email: <span
                                                    id='email' /></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='col-12 col-md-6 form-group'>
                                            <label className='control-label' htmlFor='facebook'>Facebook</label>
                                            <input className='form-control' type='text' placeholder='Facebook'
                                                id='facebook' />
                                        </div>
                                        <div className='col-12 col-md-6 form-group'>
                                            <label className='control-label' htmlFor='website'>Website</label>
                                            <input className='form-control' type='text' placeholder='Website'
                                                id='website' />
                                        </div>
                                        <div className='col-12 col-md-6 form-group'>
                                            <label className='control-label'>Học vị</label>
                                            <div>
                                                <Select
                                                    value={academicDistinction} options={academicDistinctions} placeholder='Học vị' isClearable={true}
                                                    onChange={selectedItem => this.setState({ academicDistinction: selectedItem ? selectedItem.value : null })} />
                                            </div>
                                        </div>
                                        <div className='col-12 col-md-6 form-group'>
                                            <label className='control-label'>Quốc gia tốt nghiệp</label>
                                            <div>
                                                <Select
                                                    value={graduatedCountry} options={countryList} placeholder='Quốc gia tốt nghiệp' isClearable={true}
                                                    onChange={selectedItem => this.setState({ graduatedCountry: selectedItem ? selectedItem.value : null })} />
                                            </div>
                                        </div>
                                        <div className='col-12 col-md-6 form-group'>
                                            <label className='control-label'>Học hàm</label>
                                            <div>
                                                <Select
                                                    value={academicTitle} options={academicTitles} placeholder='Học hàm' isClearable={true}
                                                    onChange={selectedItem => this.setState({ academicTitle: selectedItem ? selectedItem.value : null })} />
                                            </div>
                                        </div>
                                        <div className='col-12 col-md-6 form-group'>
                                            <label className='control-label' htmlFor='academicTitleYear'>Năm phong học hàm</label>
                                            <input className='form-control' type='number' placeholder='Năm phong học hàm' id='academicTitleYear' />
                                        </div>
                                        <div className='col-12 form-group'>
                                            <label className='control-label'>Đơn vị công tác</label>
                                            <select className='form-control' id='divisions' multiple={true}
                                                defaultValue={[]}>
                                                <optgroup label='Lựa chọn Đơn vị công tác' />
                                            </select>
                                        </div>
                                    </div>
                                    <div className='tile-footer' style={{ textAlign: 'right' }}>
                                        <button className='btn btn-primary' onClick={e => this.saveCommon(e)}>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : (
                            <div className='col-12 col-md-8'>
                                <div className='tile'>
                                    <h3 className='tile-title'>Thông tin cá nhân</h3>
                                    <div className='tile-body'>
                                        <div className='row'>
                                            <div className='col-12 col-lg-8 order-2 order-lg-1'>
                                                <div className='form-group'>
                                                    <label className='control-label' htmlFor='userLastname'>Họ và tên lót</label>
                                                    <input className='form-control' id='userLastname' type='text' placeholder='Họ và tên lót' />
                                                </div>
                                                <div className='form-group'>
                                                    <label className='control-label' htmlFor='userFirstname'>Tên</label>
                                                    <input className='form-control' type='text' id='userFirstname' placeholder='Tên' />
                                                </div>
                                            </div>
                                            <div className='col-12 col-lg-4 order-1 order-lg-2'>
                                                <div className='form-group'>
                                                    <label className='control-label'>Hình đại diện</label>
                                                    <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='ProfileImage' userData='profile' image={this.state.image} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className='row'>
                                            <div className='col-12 col-lg-8'>
                                                <div className='form-group'
                                                    style={{ display: 'inline-flex', width: '100%' }}>
                                                    <label className='control-label'>Email: <span
                                                        id='email' /></label>
                                                </div>
                                            </div>
                                            <div className='col-12 col-lg-4'>
                                                <div className='form-group'
                                                    style={{ display: 'inline-flex', width: '100%' }}>
                                                    <label>Giới tính: </label>&nbsp;&nbsp;
                                                <Dropdown ref={this.sex} text='' items={T.sexes} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className='row'>
                                            <div className='col-12 col-sm-6'>
                                                <div className='form-group'>
                                                    <label className='control-label' htmlFor='phoneNumber'>Số điện thoại</label>
                                                    <input className='form-control' type='text' placeholder='Số điện thoại' id='phoneNumber' />
                                                </div>
                                            </div>
                                            <div className='col-12 col-sm-6'>
                                                <div className='form-group'>
                                                    <label className='control-label' htmlFor='birthday'>Ngày
                                                    sinh</label>
                                                    <input className='form-control' type='text'
                                                        placeholder='Ngày sinh' id='birthday' />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='tile-footer' style={{ textAlign: 'right' }}>
                                        <button className='btn btn-primary' type='button' onClick={this.saveCommon}>Lưu</button>
                                    </div>
                                </div>
                            </div>
                        )}
                    {isStaff ? (
                        <div className={'col-12 ' + (!T.isBKer(email) ? 'col-md-8' : '')}>
                            <div className='tile'>
                                <h3 className='tile-title'>Thông tin chi tiết</h3>
                                <div className='tile-body'>
                                    <ul className='nav nav-tabs'>
                                        <li className='nav-item'>
                                            <a className='nav-link active show' data-toggle='tab'
                                                href='#staffViTab'>Việt Nam</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab'
                                                href='#staffEnTab'>English</a>
                                        </li>
                                    </ul>

                                    <div className='row mt-3'>
                                        <div className='col-md-4 form-group'>
                                            <label htmlFor='staffMasterThesis'>LVThS đã hướng dẫn thành
                                                công</label>
                                            <input className='form-control' id='masterThesis' type='number'
                                                placeholder='LVThS đã hướng dẫn thành công' />
                                        </div>
                                        <div className='col-md-4 form-group'>
                                            <label htmlFor='staffFinishedDoctorThesis'>LVTS đã hướng dẫn thành
                                                công</label>
                                            <input className='form-control' id='finishedDoctorThesis'
                                                type='number' placeholder='LVTS đã hướng dẫn thành công' />
                                        </div>
                                        <div className='col-md-4 form-group'>
                                            <label htmlFor='staffGoingDoctorThesis'>LVTS đang hướng dẫn</label>
                                            <input className='form-control' id='goingDoctorThesis' type='number'
                                                placeholder='LVTS đang hướng dẫn' />
                                        </div>
                                    </div>

                                    <div className='tab-content'>
                                        <div id='staffViTab' className='tab-pane fade show active'>
                                            <div className='row'>
                                                <div className='col-md-6 form-group'>
                                                    <label htmlFor='staffViTrainingExpertise'>Chuyên ngành
                                                        (Tiếng Việt)</label>
                                                    <input className='form-control' id='viTrainingExpertise'
                                                        type='text' placeholder='Chuyên ngành' />
                                                </div>
                                                <div className='col-md-6 form-group'>
                                                    <label htmlFor='staffViCurrentExpertise'>Lĩnh vực chuyên môn
                                                        hiện tại (Tiếng Việt)</label>
                                                    <input className='form-control' id='viCurrentExpertise'
                                                        type='text'
                                                        placeholder='Lĩnh vực chuyên môn hiện tại' />
                                                </div>
                                            </div>

                                            <ul className='nav nav-tabs'>
                                                <li className='nav-item'>
                                                    <a className='nav-link active show' data-toggle='tab'
                                                        href='#staffBiographyViTab'>Tiểu sử</a>
                                                </li>
                                                <li className='nav-item'>
                                                    <a className='nav-link' data-toggle='tab'
                                                        href='#staffResearchFieldsViTab'>Các hướng nghiên cứu
                                                        chính</a>
                                                </li>
                                                <li className='nav-item'>
                                                    <a className='nav-link' data-toggle='tab'
                                                        href='#staffPostgraduateCoursesViTab'>Giảng dạy các môn
                                                        học Sau đại học</a>
                                                </li>
                                                <li className='nav-item'>
                                                    <a className='nav-link' data-toggle='tab'
                                                        href='#staffPrizeViTab'>Giải thưởng
                                                    </a>
                                                </li>
                                            </ul>
                                            <div className='tab-content'>
                                                <div id='staffBiographyViTab'
                                                    className='tab-pane fade show active'>
                                                    <Editor ref={this.editorViBiography} placeholder='Tiểu sử'
                                                        height={400} />
                                                </div>
                                                <div id='staffResearchFieldsViTab' className='tab-pane fade'>
                                                    <Editor ref={this.editorViResearchFields}
                                                        placeholder='Các hướng nghiên cứu chính'
                                                        height={400} />
                                                </div>
                                                <div id='staffPostgraduateCoursesViTab'
                                                    className='tab-pane fade'>
                                                    <Editor ref={this.editorViPostgraduateCourses}
                                                        placeholder='Giảng dạy các môn học Sau đại học'
                                                        height={400} />
                                                </div>
                                                <div id='staffPrizeViTab'
                                                    className='tab-pane fade'>
                                                    <Editor ref={this.editorViPrize}
                                                        placeholder='Giải thưởng'
                                                        height={400} />
                                                </div>
                                            </div>
                                        </div>

                                        <div id='staffEnTab' className='tab-pane fade'>
                                            <div className='row'>
                                                <div className='col-md-6 form-group'>
                                                    <label htmlFor='staffEnTrainingExpertise'>Chuyên ngành
                                                        (English)</label>
                                                    <input className='form-control' id='enTrainingExpertise'
                                                        type='text' placeholder='Chuyên ngành' />
                                                </div>
                                                <div className='col-md-6 form-group'>
                                                    <label htmlFor='staffEnCurrentExpertise'>Lĩnh vực chuyên môn
                                                        hiện tại (English)</label>
                                                    <input className='form-control' id='enCurrentExpertise'
                                                        type='text'
                                                        placeholder='Lĩnh vực chuyên môn hiện tại' />
                                                </div>
                                            </div>

                                            <ul className='nav nav-tabs'>
                                                <li className='nav-item'>
                                                    <a className='nav-link active show' data-toggle='tab'
                                                        href='#staffBiographyEnTab'>Tiểu sử</a>
                                                </li>
                                                <li className='nav-item'>
                                                    <a className='nav-link' data-toggle='tab'
                                                        href='#staffResearchFieldsEnTab'>Các hướng nghiên cứu
                                                        chính</a>
                                                </li>
                                                <li className='nav-item'>
                                                    <a className='nav-link' data-toggle='tab'
                                                        href='#staffPostgraduateCoursesEnTab'>Giảng dạy các môn
                                                        học Sau đại học</a>
                                                </li>
                                                <li className='nav-item'>
                                                    <a className='nav-link' data-toggle='tab'
                                                        href='#staffPrizeEnTab'>Awards
                                                    </a>
                                                </li>
                                            </ul>
                                            <div className='tab-content'>
                                                <div id='staffBiographyEnTab'
                                                    className='tab-pane fade show active'>
                                                    <Editor ref={this.editorEnBiography} placeholder='Tiểu sử'
                                                        height={400} />
                                                </div>
                                                <div id='staffResearchFieldsEnTab' className='tab-pane fade'>
                                                    <Editor ref={this.editorEnResearchFields}
                                                        placeholder='Các hướng nghiên cứu chính'
                                                        height={400} />
                                                </div>
                                                <div id='staffPostgraduateCoursesEnTab'
                                                    className='tab-pane fade'>
                                                    <Editor ref={this.editorEnPostgraduateCourses}
                                                        placeholder='Giảng dạy các môn học Sau đại học'
                                                        height={400} />
                                                </div>
                                                <div id='staffPrizeEnTab'
                                                    className='tab-pane fade'>
                                                    <Editor ref={this.editorEnPrize}
                                                        placeholder='Awards'
                                                        height={400} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='tile-footer' style={{ textAlign: 'right' }}>
                                        <button className='btn btn-primary' onClick={e => this.saveDetail(e)}>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : null}
                    {!T.isBKer(email) ? (
                        <div className='col-12 col-md-4'>
                            <div className='tile'>
                                <h3 className='tile-title'>Mật khẩu</h3>
                                <div className='tile-body'>
                                    <div className='form-group'>
                                        <label className='control-label'>Mật khẩu mới</label>
                                        <input className='form-control' type='password' placeholder='Mật khẩu mới' ref={this.password1} defaultValue='' />
                                    </div>
                                    <div className='form-group'>
                                        <label className='control-label'>Nhập lại mật khẩu</label>
                                        <input className='form-control' type='password' placeholder='Nhập lại mật khẩu' ref={this.password2} defaultValue='' />
                                    </div>
                                </div>
                                <div className='tile-footer' style={{ textAlign: 'right' }}>
                                    <button className='btn btn-primary' type='button' onClick={this.savePassword}>Lưu</button>
                                </div>
                            </div>
                        </div>
                    ) : null}
                </div>
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, division: state.division });
const mapActionsToProps = { updateProfile, getAllDivisionsByStaff };
export default connect(mapStateToProps, mapActionsToProps)(ProfilePage);
