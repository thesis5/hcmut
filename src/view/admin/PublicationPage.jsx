import React from 'react';
import { connect } from 'react-redux';
import { getPublicationInPage, createPublication, updatePublication, deletePublication, getAllPublicationTypes, createPublicationList } from '../redux/user.jsx';
import { Link } from 'react-router-dom';
import FileBox from '../common/FileBox.jsx';
import Pagination from '../common/Pagination.jsx';
import Select from 'react-select';

class UploadPublicationModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { list: null };
        this.modal = React.createRef();
        this.fileBox = React.createRef();
        this.btnSave = React.createRef();

        this.show = this.show.bind(this);
        this.save = this.save.bind(this);
    }

    show() {
        this.setState({ list: null });
        $(this.modal.current).modal('show');
    }

    save(e) {
        e.preventDefault();
        const { list } = this.state;
        this.props.createPublicationList(list, () => $(this.modal.current).modal('hide'));
    }

    render() {
        const list = this.state.list;
        const table = list && list.length ? (
            <table className='table table-hover table-bordered'>
                <thead>
                    <tr>
                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                        <th style={{ width: '100%' }}>Tiêu đề</th>
                        <th style={{ width: 'auto' }} nowrap='true'>Năm</th>
                        <th style={{ width: 'auto' }} nowrap='true'>Loại công bố</th>
                    </tr>
                </thead>
                <tbody>{
                    list.map((publication, index) => {
                        return (
                            <tr key={index}>
                                <td style={{ width: 'auto', textAlign: 'right' }}>{index + 1}</td>
                                <td style={{ width: '100%' }}>{publication.title}</td>
                                <td style={{ width: 'auto' }}>{publication.year}</td>
                                <td style={{ width: 'auto' }} nowrap='true'>{publication.typeText}</td>
                            </tr>
                        );
                    })
                }</tbody>
            </table>
        ) : null;
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Upload danh sách dự án</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='PublicationFile' userData='publicationImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }}
                                success={({ error, list }) => this.setState({ list })} />
                            {table}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class PublicationModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.modal = React.createRef();
        this.show = this.show.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount() {
        T.ready(() => {
            setTimeout(() => $(this.modal.current).on('shown.bs.modal', () => $('#publicationTitle').focus()), 250);
        });
    }

    show(item) {
        const { _id, title, year, type, active } = item ? item : { title: '', year: '', type: null, active: false };
        $('#publicationTitle').val(title);
        $('#publicationYear').val(year);
        $('#publicationActive').prop('checked', active);

        $(this.modal.current).modal('show');
        this.setState({ _id, publicationType: type });
    }

    save(e) {
        e.preventDefault();
        const _id = this.state._id,
            changes = {
                title: $('#publicationTitle').val().trim(),
                year: $('#publicationYear').val().trim(),
                active: $('#publicationActive').prop('checked'),
                type: this.state.publicationType ? this.state.publicationType : null,
            };

        if (changes.title == '') {
            T.notify('Tiêu đề xuất bản bị trống!', 'danger');
            $('#publicationTitle').focus();
        } else if (changes.year == '') {
            T.notify('Năm xuất bản bị trống!', 'danger');
            $('#publicationYear').focus();
        } else if (changes.type == null) {
            T.notify('Loại xuất bản bị trống!', 'danger');
        } else {
            if (_id) {
                this.props.update(_id, changes)
            } else {
                this.props.create(changes)
            }
            $(this.modal.current).modal('hide');
        }
    }

    render() {
        let publicationType = this.state.publicationType;
        if (publicationType && this.props.publicationTypeOptions && this.props.publicationTypes[publicationType]) {
            publicationType = { value: publicationType, label: this.props.publicationTypes[publicationType] };
        }

        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin xuất bản</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='publicationTitle'>Tiêu đề</label>
                                <input className='form-control' id='publicationTitle' type='text' placeholder='Tiêu đề' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='publicationYear'>Năm xuất bản</label>
                                <input className='form-control' id='publicationYear' type='number' placeholder='Năm xuất bản' />
                            </div>
                            <div className='form-group row'>
                                <div className='col-md-8'>
                                    <label htmlFor='publicationActive'>Loại xuất bản</label>
                                    <Select value={publicationType} options={this.props.publicationTypeOptions} placeholder='Loại xuất bản'
                                        onChange={selectedItem => this.setState({ publicationType: selectedItem ? selectedItem.value : null })} />
                                </div>
                                <div className='col-md-4' style={{ display: 'inline-flex' }}>
                                    <label htmlFor='publicationActive'>Kích hoạt: </label>&nbsp;&nbsp;
                                    <div className='toggle'>
                                        <label>
                                            <input type='checkbox' id='publicationActive' onChange={() => { }} /><span className='button-indecator' />
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button> </div>
                    </div>
                </form>
            </div>
        );
    }
}

class PublicationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { publicationTypes: {}, publicationTypeOptions: [] };
        this.publicationModal = React.createRef();
        this.uploadPublicationModal = React.createRef();

        this.create = this.create.bind(this);
        this.edit = this.edit.bind(this);
        this.changeActive = this.changeActive.bind(this);
        this.delete = this.delete.bind(this);
        this.showUploadPublicationModal = this.showUploadPublicationModal.bind(this);
    }

    componentDidMount() {
        this.props.getPublicationInPage();
        this.props.getAllPublicationTypes(types => {
            const publicationTypes = {},
                publicationTypeOptions = (types ? types : []).map(item => {
                    publicationTypes[item._id] = T.language.parse(item.type, true).vi;
                    return { value: item._id, label: T.language.parse(item.type, true).vi };
                });
            this.setState({ publicationTypes, publicationTypeOptions });
        });

        T.ready('/user/user');
    }

    create(e) {
        this.publicationModal.current.show(null);
        e.preventDefault();
    }

    edit(e, item) {
        this.publicationModal.current.show(item);
        e.preventDefault();
    }

    changeActive(item) {
        this.props.updatePublication(item._id, { active: !item.active }, () => this.props.getPublicationInPage());
    }

    delete(e, item) {
        T.confirm('Xóa thông tin xuất bản', 'Bạn có chắc bạn muốn xóa thông tin xuất bản này?', true, isConfirm =>
            isConfirm && this.props.deletePublication(item._id, () => this.props.getPublicationInPage()));
        e.preventDefault();
    }

    showUploadPublicationModal(e) {
        this.uploadPublicationModal.current.show();
        e.preventDefault();
    }

    render() {
        const { publicationTypes, publicationTypeOptions } = this.state;
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.user && this.props.user.publicationPage ?
            this.props.user.publicationPage : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.user && this.props.user.publicationPage && this.props.user.publicationPage.list && this.props.user.publicationPage.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>Năm</th>
                            <th style={{ width: '100%' }}>Tiêu đề</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Loại</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Kích hoạt</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.user.publicationPage.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td style={{ textAlign: 'right' }}>{item.year}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.title}</a>
                                </td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.type && publicationTypes[item.type] ? T.language.parse(publicationTypes[item.type], true).vi : '<no value>'}</td>
                                <td className='toggle' style={{ textAlign: 'center' }}>
                                    <label>
                                        <input type='checkbox' checked={item.active} onChange={() => this.changeActive(item)} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có thông tin!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-newspaper-o' /> Xuất bản</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/user'><i className='fa fa-home fa-lg' /></Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/user/user'>Nhân sự</Link>
                        </li>
                        <li className='breadcrumb-item'>Xuất bản</li>
                    </ul>
                </div>

                <div className='row tile'>{table}</div>
                <Pagination name='adminPublication' style={{ marginLeft: '10px' }}
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} getPage={this.props.getPublicationInPage} />

                <a href='#' className='btn btn-circle' style={{ position: 'fixed', right: '190px', bottom: '10px', backgroundColor: '#0F0' }} onClick={this.showUploadPublicationModal}>
                    <i className='fa fa-lg fa-upload' />
                </a>
                <a href='/download/publication.xlsx' className='btn btn-secondary btn-circle' style={{ position: 'fixed', right: '130px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-download' />
                </a>

                <Link to='/user/user' className='btn btn-info btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-user' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <PublicationModal ref={this.publicationModal} publicationTypeOptions={publicationTypeOptions} publicationTypes={publicationTypes}
                    create={this.props.createPublication} update={this.props.updatePublication} />
                <UploadPublicationModal ref={this.uploadPublicationModal} createPublicationList={this.props.createPublicationList} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ user: state.user });
const mapActionsToProps = { getPublicationInPage, createPublication, updatePublication, deletePublication, getAllPublicationTypes, createPublicationList };
export default connect(mapStateToProps, mapActionsToProps)(PublicationPage);
