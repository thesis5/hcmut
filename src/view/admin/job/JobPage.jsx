import React from 'react';
import { connect } from 'react-redux';
import { getJobInPage, createJob, updateJob, swapJob, deleteJob } from '../../redux/job.jsx'
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

class JobPage extends React.Component {
    componentDidMount() {
        this.props.getJobInPage();
        T.ready();
    }

    create = (e) => {
        this.props.createJob(data => this.props.history.push('/user/job/edit/' + data.item._id));
        e.preventDefault();
    }

    swap = (e, item, isMoveUp) => {
        this.props.swapJob(item._id, isMoveUp);
        e.preventDefault();
    }

    changeActive = (item, index) => {
        this.props.updateJob(item._id, { active: !item.active });
    }

    changeisInternal = (item) => this.props.updateJob(item._id, { isInternal: !item.isInternal })

    delete = (e, item) => {
        T.confirm('Việc làm', 'Bạn có chắc bạn muốn xóa việc làm này?', 'warning', true, isConfirm => isConfirm && this.props.deleteJob(item._id));
        e.preventDefault();
    }

    render() {
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.job && this.props.job.page ?
            this.props.job.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.job && this.props.job.page && this.props.job.page.list && this.props.job.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '80%' }}>Tiêu đề</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Hình ảnh</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Kích hoạt</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Tin nội bộ</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.job.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    {
                                        <Link to={`/user/job/edit/${item._id}`} data-id={item._id}>
                                            {T.language.parse(item.title)}
                                        </Link>
                                    }

                                </td>
                                <td style={{ width: '20%', textAlign: 'center' }}>
                                    <img src={item.image} alt='avatar' style={{ height: '32px' }} />
                                </td>

                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <label>
                                        <input type='checkbox'
                                            checked={item.active}
                                            onChange={() => this.changeActive(item, index)}
                                            disabled={!currentPermission.contains('job:write')}
                                        />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <label>
                                        <input type='checkbox' checked={item.isInternal} onChange={() => this.changeisInternal(item, index)} disabled={!currentPermission.contains('job:write')} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>

                                {
                                    <td>
                                        <div className='btn-group'>
                                            <Link to={'/user/job/registration/' + item._id} data-id={item._id} className='btn btn-warning'>
                                                <i className='fa fa-lg fa-list-alt' />
                                            </Link>
                                            {
                                                currentPermission.contains('job:write') ? [
                                                    <a key={0} className='btn btn-success' href='#' onClick={e => this.swap(e, item, true)}>
                                                        <i className='fa fa-lg fa-arrow-up' />
                                                    </a>,
                                                    <a key={1} className='btn btn-success' href='#' onClick={e => this.swap(e, item, false)}>
                                                        <i className='fa fa-lg fa-arrow-down' />
                                                    </a>,
                                                ] : null
                                            }
                                            < Link to={'/user/job/edit/' + item._id} data-id={item._id} className='btn btn-primary'>
                                                <i className='fa fa-lg fa-edit' />
                                            </Link>
                                            {
                                                currentPermission.contains('job:write') &&
                                                <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                                    <i className='fa fa-lg fa-trash' />
                                                </a>
                                            }
                                        </div>
                                    </td>
                                }
                            </tr>
                        ))}
                    </tbody>
                </table >
            );
        } else {
            table = <p>Không có việc làm!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-pagelines' /> Việc làm: Danh sách</h1>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pageJob'
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getJobInPage} />
                {
                    currentPermission.contains('job:write') &&
                    <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}
                        onClick={this.create}>
                        <i className='fa fa-lg fa-plus' />
                    </button>
                }
            </main>
        );
    }
}

const mapStateToProps = state => ({ job: state.job, system: state.system });
const mapActionsToProps = { getJobInPage, createJob, updateJob, swapJob, deleteJob };
export default connect(mapStateToProps, mapActionsToProps)(JobPage);