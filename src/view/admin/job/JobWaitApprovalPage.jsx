import React from 'react';
import { connect } from 'react-redux';
import { getDraftJobInPage, createDraftJobDefault, updateJob, swapJob, deleteDraftJob, draftToJob } from '../../redux/job.jsx'
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

class JobWaitApprovalPage extends React.Component {
    componentDidMount() {
        this.props.getDraftJobInPage();
        T.ready();
    }

    create = (e) => {
        this.props.createDraftJobDefault(data => this.props.history.push('/user/job/draft/edit/' + data.item._id));
        e.preventDefault();
    }


    changeActive = (item, index) => {
        this.props.draftToJob(item._id);
    }

    delete = (e, item) => {
        T.confirm('Việc làm', 'Bạn có chắc bạn muốn xóa mẫu việc làm này?', 'warning', true, isConfirm => isConfirm && this.props.deleteDraftJob(item._id));
        e.preventDefault();
    }

    render() {
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            viewerType = currentPermission.includes('job:write') ? 2 : (currentPermission.includes('job:draft') ? 1 : 0);
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.job && this.props.job.page ?
            this.props.job.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.job && this.props.job.draft && this.props.job.draft.list && this.props.job.draft.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '80%' }}>Tiêu đề</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Hình ảnh</th>
                            {viewerType == 2 ? <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Người tạo</th> : null}
                            {viewerType == 1 ? <th style={{ width: 'auto' }} nowrap='true'>Được duyệt</th> : null}
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.job.draft.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    {
                                        <Link to={`/user/job/draft/edit/${item._id}`} data-id={item._id}>
                                            {T.language.parse(item.title)}
                                        </Link>
                                    }
                                </td>
                                <td style={{ width: '20%', textAlign: 'center' }}>
                                    <img src={item.image} alt='avatar' style={{ height: '32px' }} />
                                </td>
                                {viewerType == 2 ? (<td style={{ textAlign: 'center' }}>{item.editorName}</td>) : null}
                                {viewerType == 1 ? (
                                    <td className='toggle' style={{ textAlign: 'center' }} >
                                        <label>
                                            <input type='checkbox' checked={item.active} disabled={viewerType != 2} onChange={() => { }} /><span className='button-indecator' />
                                        </label>
                                    </td>
                                ) : null}
                                <td>
                                    <div className='btn-group'>
                                        {viewerType == 2 ? (<a href='#' className='btn btn-success' onClick={() => this.changeActive(item, index)} title='Duyệt bản nháp này'> <i className='fa fa-check' /></a>) : null}
                                        <Link to={'/user/job/draft/edit/' + item._id} data-id={item._id} className='btn btn-primary'>
                                            <i className='fa fa-lg fa-edit' />
                                        </Link>
                                        {currentPermission.contains('job:draft') ?
                                            <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                                <i className='fa fa-lg fa-trash' />
                                            </a> : null}
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table >
            );
        } else {
            table = <p>Không có bản nháp việc làm!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-pagelines' /> Việc làm: Chờ duyệt</h1>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pageJob'
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getDraftJobInPage} />
                {
                    !currentPermission.contains('job:write') &&
                    <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}
                        onClick={this.create}>
                        <i className='fa fa-lg fa-plus' />
                    </button>
                }
            </main>
        );
    }
}

const mapStateToProps = state => ({ job: state.job, system: state.system });
const mapActionsToProps = { getDraftJobInPage, createDraftJobDefault, updateJob, swapJob, deleteDraftJob, draftToJob };
export default connect(mapStateToProps, mapActionsToProps)(JobWaitApprovalPage);
