import React from 'react';
import { connect } from 'react-redux';
import { updateJob, getJob, checkLink, getDraftJob, createDraftJob, updateDraftJob } from '../../redux/job.jsx'
import { countAnswer } from '../../redux/answer.jsx';
import { Link } from 'react-router-dom';
import ImageBox from '../../common/ImageBox.jsx';
import Editor from '../../common/CkEditor4.jsx';
import QuestionFormComponent from '../../common/QuestionFormComponent.jsx';

class JobEditPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { item: null, numOfRegisterUsers: 0, };
        this.jobLink = React.createRef();
        this.imageBox = React.createRef();
        this.viEditor = React.createRef();
        this.enEditor = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/job/list', () => {
            this.getData();
            $('#jbJobTitle').focus();
            $('#jbJobCategories').select2();
            $('#jbJobStartPost').datetimepicker(T.dateFormat);
            $('#jbJobStopPost').datetimepicker(T.dateFormat);
        });
    }

    getData = () => {
        const route = T.routeMatcher('/user/job/edit/:jobId'),
            IdJob = route.parse(window.location.pathname).jobId;
        this.props.getJob(IdJob, data => {
            if (data.error) {
                T.notify('Lấy việc làm bị lỗi!', 'danger');
                this.props.history.push('/user/job/list');
            } else if (data.categories && data.item) {
                let categories = data.categories.map(item => ({ id: item.id, text: T.language.parse(item.text) }));
                $('#jbJobCategories').select2({ data: categories }).val(data.item.categories).trigger('change');

                const jbJobStartPost = $('#jbJobStartPost').datetimepicker(T.dateFormat);
                const jbJobStopPost = $('#jbJobStopPost').datetimepicker(T.dateFormat);
                if (data.item.startPost)
                    jbJobStartPost.val(T.dateToText(data.item.startPost, 'dd/mm/yyyy HH:MM')).datetimepicker('update');
                if (data.item.stopPost)
                    jbJobStopPost.val(T.dateToText(data.item.stopPost, 'dd/mm/yyyy HH:MM')).datetimepicker('update');

                if (data.item.link) {
                    $(this.jobLink.current).html(T.rootUrl + '/vieclam/' + data.item.link)
                        .attr('href', '/vieclam/' + data.item.link);
                } else {
                    $(this.jobLink.current).html('').attr('href', '#');
                }

                data.image = data.item.image ? data.item.image : '/image/avatar.jpg';
                this.imageBox.current.setData('job:' + (data.item._id ? data.item._id : 'new'));

                let title = T.language.parse(data.item.title, true),
                    abstract = T.language.parse(data.item.abstract, true),
                    content = T.language.parse(data.item.content, true);

                $('#jbJobViTitle').val(title.vi);
                $('#jbJobEnTitle').val(title.en);
                $('#neJobsViAbstract').val(abstract.vi);
                $('#neJobsEnAbstract').val(abstract.en);
                this.viEditor.current.html(content.vi);
                this.enEditor.current.html(content.en);

                $('#jbMaxRegisterUsers').val(data.item.maxRegisterUsers);
                this.props.countAnswer(data.item._id, 'questions', (numOfRegisterUsers) => {
                    this.setState(Object.assign({}, data, { numOfRegisterUsers }));
                });
            } else {
                this.props.history.push('/user/job/list');
            }
        });

    }

    changeActive = (job) => {
        this.setState({ item: Object.assign({}, this.state.item, { active: job.target.checked }) });
    }
    changeisInternal = (event) => {
        this.setState({ item: Object.assign({}, this.state.item, { isInternal: event.target.checked }) });
    }
    checkLink = (item) => {
        this.props.checkLink(item._id, $('#jbJobLink').val().trim());
    }

    jobLinkChange = (e) => {
        if (e.target.value) {
            $(this.jobLink.current).html(T.rootUrl + '/vieclam/' + e.target.value)
                .attr('href', '/vieclam/' + e.target.value);
        } else {
            $(this.jobLink.current).html('').attr('href', '#');
        }
    }

    save = () => {
        const startPost = $('#jbJobStartPost').val(),
            stopPost = $('#jbJobStopPost').val(),
            changes = {
                categories: $('#jbJobCategories').val(),
                title: JSON.stringify({ vi: $('#jbJobViTitle').val(), en: $('#jbJobEnTitle').val() }),
                link: $('#jbJobLink').val().trim(),
                active: this.state.item.active,
                isInternal:this.state.item.isInternal,
                abstract: JSON.stringify({ vi: $('#neJobsViAbstract').val(), en: $('#neJobsEnAbstract').val() }),
                content: JSON.stringify({ vi: this.viEditor.current.html(), en: this.enEditor.current.html() }),
                maxRegisterUsers: $('#jbMaxRegisterUsers').val(),
            };
        if (startPost) changes.startPost = T.formatDate(startPost);
        if (stopPost) changes.stopPost = T.formatDate(stopPost);
        let newDraft = {
            title: JSON.stringify({ vi: $('#jbJobViTitle').val(), en: $('#jbJobEnTitle').val() }),
            editorId: this.props.system.user._id,
            documentId: this.state.item._id,
            editorName: this.props.system.user.firstname,
            isInternal:this.state.item.isInternal,
            documentType: 'job',
            documentJson: JSON.stringify(changes),
        }
        if (this.props.system.user.permissions.includes('job:write')) {
            this.props.updateJob(this.state.item._id, changes, () => {
                $('#jbJobLink').val(changes.link)
            })
        }
        else {
            newDraft.image = T.url('/image/avatar.jpg');
            this.props.createDraftJob(newDraft, result => { this.getData() });
        }
    }

    render() {
        const currentPermissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        let readOnly = true;
        const item = this.state.item ? this.state.item : {
            _id: '',
            priority: 1, maxRegisterUsers: -1,
            categories: [],
            title: '',
            content: '',
            image: T.url('/image/avatar.jpg'),
            createdDate: new Date(),
            active: false,isInternal:false,
            view: 0
        };
        const title = T.language.parse(item.title, true),
            linkDefaultJob = `${T.rootUrl}/job/item/${item._id}`;
        const route = T.routeMatcher('/user/job/edit/:jobId'),
            jobId = route.parse(window.location.pathname).jobId;
        const docDraftUser = this.props.job && this.props.job.docDraftUser ? this.props.job.docDraftUser : [];
        const docMapper = {};
        docDraftUser.forEach(user => docMapper[user.documentId] = user._id);
        if (!docMapper[jobId]) readOnly = false;
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-edit' /> Việc làm: Chỉnh sửa</h1>
                        <p dangerouslySetInnerHTML={{ __html: title.vi != '' ? 'Tiêu đề: <b>' + title.vi + '</b> - ' + T.dateToText(item.createdDate) : '' }} />
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <Link to='/user'><i className='fa fa-home fa-lg' /></Link>
                        &nbsp;/&nbsp;
                        <Link to='/user/job/list'>Danh sách việc làm</Link>
                        &nbsp;/&nbsp;Chỉnh sửa
                    </ul>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='tile'>
                            <h3 className='tile-title'>Thông tin chung</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Tên việc làm</label>
                                    <input className='form-control' type='text' placeholder='Tên việc làm' id='jbJobViTitle' readOnly={readOnly} defaultValue={item.title} />
                                </div>

                                <div className='form-group'>
                                    <label className='control-label'>Job title</label>
                                    <input className='form-control' type='text' placeholder='Job title' id='jbJobEnTitle' readOnly={readOnly} defaultValue={title.en} />
                                </div>

                                <div className='row'>
                                    <div className='col-md-6'>
                                        <div className='form-group'>
                                            <label className='control-label'>Hình ảnh</label>
                                            <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='JobImage' userData='job' readOnly={!currentPermissions.includes('job:write')} image={this.state.image} />
                                        </div>
                                    </div>
                                    <div className='col-md-6'>
                                    {currentPermissions.includes('job:write') ? 
                                        <div className='form-group' style={{ display: 'inline-flex' }}>
                                            <label className='control-label'>Kích hoạt:&nbsp;</label>
                                            <div className='toggle'>
                                                <label>
                                                    <input type='checkbox' checked={item.active} onChange={this.changeActive} disabled={!currentPermissions.contains('job:write')} />
                                                    <span className='button-indecator' />
                                                </label>
                                            </div>
                                        </div>
                                    : null}
                                        <div className='form-group' >
                                                <label className='control-label'>Tin nội bộ:&nbsp;</label>
                                                <span className='toggle'>
                                                    <label>
                                                        <input type='checkbox' checked={item.isInternal} onChange={this.changeisInternal} disabled={readOnly} />
                                                        <span className='button-indecator' />
                                                    </label>
                                                </span>
                                        </div>
                                        <div className='form-group row'>
                                                <label className='control-label col-12'>Lượt xem: {item.view}</label>
                                        </div>
                                    </div> 
                                    
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Số lượng người đăng ký tối đa</label><br />
                                    <input className='form-control' id='jbMaxRegisterUsers' type='number' placeholder='Số lượng người đăng ký tối đa' readOnly={readOnly} defaultValue={item.maxRegisterUsers}
                                        aria-describedby='jbMaxRegisterUsersHelp' />
                                    <small className='form-text text-success' id='jbMaxRegisterUsersHelp'>Điền -1 nếu không giới hạn số lượng người đăng ký tối đa.</small>
                                </div>
                                <div className='form-group row'>
                                    <label className='control-label col-12'>Số lượng người đăng ký tham gia: {this.state.numOfRegisterUsers}</label>
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Danh mục bài viết</label>
                                    <select className='form-control' id='jbJobCategories' multiple={true} defaultValue={[]} disabled={readOnly}>
                                        <optgroup label='Lựa chọn danh mục' />
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-6'>
                        <div className='tile'>
                            <h3 className='tile-title'>Link</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Link mặc định</label><br />
                                    <a href={linkDefaultJob} style={{ fontWeight: 'bold', wordWrap: 'break-word' }} target='_blank'>{linkDefaultJob}</a>
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Link truyền thông</label><br />
                                    <a href='#' ref={this.jobLink} style={{ fontWeight: 'bold' }} target='_blank' />
                                    <input className='form-control' id='jbJobLink' type='text' placeholder='Link truyền thông' readOnly={readOnly} defaultValue={item.link} onChange={this.jobLinkChange} />
                                </div>
                            </div>
                            <div className='tile-footer'>
                                <button className='btn btn-danger' type='button' onClick={() => this.checkLink(item)}>
                                    <i className='fa fa-fw fa-lg fa-check-circle' />Kiểm tra link
                                </button>
                            </div>
                        </div>
                        <div className='tile'>
                            <h3 className='tile-title'>Ngày tháng</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Ngày tạo: {T.dateToText(item.createdDate)}</label>
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Ngày bắt đầu đăng bài viết</label>
                                    <input className='form-control' id='jbJobStartPost' type='text' placeholder='Ngày bắt đầu đăng bài viết' autoComplete='off' readOnly={readOnly} defaultValue={item.startPost} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Ngày kết thúc đăng bài viết</label>
                                    <input className='form-control' id='jbJobStopPost' type='text' placeholder='Ngày kết thúc đăng bài viết' autoComplete='off' readOnly={readOnly} defaultValue={item.stopPost} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-12'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <ul className='nav nav-tabs'>
                                    <li className='nav-item'>
                                        <a className='nav-link active show' data-toggle='tab' href='#jobsViTab'>Việt Nam</a>
                                    </li>
                                    <li className='nav-item'>
                                        <a className='nav-link' data-toggle='tab' href='#jobsEnTab'>English</a>
                                    </li>
                                </ul>
                                <div className='tab-content' style={{ paddingTop: '12px' }}>
                                    <div id='jobsViTab' className='tab-pane fade show active'>
                                        <label className='control-label'>Tóm tắt công việc</label>
                                        <textarea defaultValue='' className='form-control' id='neJobsViAbstract' placeholder='Tóm tắt công việc'
                                            style={{ minHeight: '100px', marginBottom: '12px' }} readOnly={readOnly} />
                                        <label className='control-label'>Nội dung công việc</label>
                                        <Editor ref={this.viEditor} placeholder='Nội dung công việc' height={600} readOnly={readOnly} uploadUrl='/user/upload?category=job' />
                                    </div>
                                    <div id='jobsEnTab' className='tab-pane fade'>
                                        <label className='control-label'>Jobs abstract</label>
                                        <textarea defaultValue='' className='form-control' id='neJobsEnAbstract' placeholder='Jobs abstracts'
                                            style={{ minHeight: '100px', marginBottom: '12px' }} readOnly={readOnly} />
                                        <label className='control-label'>Jobs content</label>
                                        <Editor ref={this.enEditor} placeholder='Jobs content' height={600} readOnly={readOnly} uploadUrl='/user/upload?category=job' />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {currentPermissions.includes('job:write') ?
                        <div className='col-md-12'>
                            <QuestionFormComponent title='Form đăng ký việc làm' model='job' postId={item._id} field='questions' permission={'job:write'} currentPermission={currentPermissions} />
                        </div> : null}
                </div>
                <Link to={'/user/job/list'} className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                {
                    readOnly ? '' :
                        <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}
                            onClick={this.save}>
                            <i className='fa fa-lg fa-save' />
                        </button>
                }
            </main>
        );
    }
}

const mapStateToProps = state => ({ job: state.job, system: state.system });
const mapActionsToProps = { updateJob, getJob, getDraftJob, checkLink, countAnswer, createDraftJob, updateDraftJob };
export default connect(mapStateToProps, mapActionsToProps)(JobEditPage);
