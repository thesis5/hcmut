import React from 'react';
import { connect } from 'react-redux';
import { getJobWithQuestion } from '../../redux/job.jsx';
import { adminGetJobRegistrationInPage } from '../../redux/answer.jsx';
import { Link } from 'react-router-dom';
import RegistrationResultComponent from '../../common/RegistrationResultComponent.jsx';

class JobRegistrationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { item: null };
    }

    componentDidMount() {
        T.ready('/user/job/list');
        const route = T.routeMatcher('/user/job/registration/:jobId'),
            params = route.parse(window.location.pathname);
        this.props.getJobWithQuestion(params.jobId, data => {
            if (data.error) {
                T.notify('Lấy việc làm bị lỗi!', 'danger');
                this.props.history.push('/user/job/list');
            } else if (data.item) {
                this.setState(data);
            } else {
                this.props.history.push('/user/job/list');
            }
        });
    }

    render() {
        const item = this.props.job && this.props.job.job ? this.props.job.job : { _id: null, title: '', maxRegisterUsers: -1, questions: [] };
        const title = item.title != '' ? `Tiêu đề: <b>${item.title.viText()}</b> - ${T.dateToText(item.createdDate)}` : '';
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-edit' /> Việc làm: Danh sách đăng ký việc làm</h1>
                        <p dangerouslySetInnerHTML={{ __html: title }} />
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <Link to='/user'><i className='fa fa-home fa-lg' /></Link>
                        &nbsp;/&nbsp;
                        <Link to='/user/event/list'>Danh sách sự kiện</Link>
                        &nbsp;/&nbsp;
                        <a href='#'>Danh sách đăng ký việc làm</a>
                    </ul>
                </div>
                <RegistrationResultComponent questions={item.questions} postId={item._id} field='questions' getPageType='jobQuestions'
                    getPage={this.props.adminGetJobRegistrationInPage} paginationName='pageJobRegistration' maxRegisterUsers={item.maxRegisterUsers}
                    permission={{ manager: 'job:registration', roller: 'job:roller', import: 'job:import', export: 'job:export' }}
                />

                <Link to='/user/event/list' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
            </main>
        );
    }
}

const mapStateToProps = state => ({ job: state.job });
const mapActionsToProps = { getJobWithQuestion, adminGetJobRegistrationInPage };
export default connect(mapStateToProps, mapActionsToProps)(JobRegistrationPage);