import React from 'react';
import Category from '../Category.jsx';

export default class JobCategoryPage extends React.Component {
    componentDidMount() {
        T.ready();
    }

    render() {
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-pagelines' /> Việc làm: Danh mục</h1>
                </div>
                <Category type='job' uploadType='jobCategoryImage' />
            </main>
        );
    }
}