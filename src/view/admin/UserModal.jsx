import React from 'react';
import { connect } from 'react-redux';
import ImageBox from '../common/ImageBox.jsx';
import Dropdown from '../common/Dropdown.jsx';
import FileBox from '../common/FileBox.jsx';
import Select from 'react-select';
import { getNames, getCode, getName } from 'country-list';
import Editor from '../common/CkEditor4.jsx';
import { clearSession } from '../redux/system.jsx';
import { createStaffsFromFile } from '../redux/user.jsx';

const academicTitles = T.academicTitles.map(label => ({ value: label, label }));
const academicDistinctions = T.academicDistinctions.map(label => ({ value: label, label }));
const countryList = getNames().map(label => ({ value: getCode(label), label }));

export class UserPasswordModal extends React.Component {
    modal = React.createRef();
    state = {};

    componentDidMount() {
        T.ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#userPassword1').focus());
        });
    }

    show = (item) => {
        $('#userPassword1').val('');
        $('#userPassword2').val('');
        this.setState({ _id: item._id });
        $(this.modal.current).modal('show');
    };

    save = (event) => {
        const _id = $(event.target).data('id'),
            password1 = $('#userPassword1').val().trim(),
            password2 = $('#userPassword2').val().trim();
        if (password1 == '') {
            T.notify('Mật khẩu bị trống!', 'danger');
            $('#userPassword1').focus();
        } else if (password2 == '') {
            T.notify('Vui lòng nhập lại mật khẩu!', 'danger');
            $('#userPassword2').focus();
        } else if (password1 != password2) {
            T.notify('Mật khẩu không trùng nhau!', 'danger');
            $('#userPassword2').focus();
        } else {
            this.props.updateUser(_id, { password: password1 }, error => {
                if (error == undefined || error == null) {
                    $(this.modal.current).modal('hide');
                }
            });
        }
        event.preventDefault();
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Đổi mật khẩu</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='userPassword1'>Mật khẩu</label>
                                <input className='form-control' id='userPassword1' type='password'
                                    placeholder='Mật khẩu' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='userPassword2'>Nhập lại mật khẩu</label>
                                <input className='form-control' id='userPassword2' type='password'
                                    placeholder='Mật khẩu' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export class RolesModal extends React.Component {
    state = {};
    modal = React.createRef();

    componentDidMount() {
        T.ready(() => {
            $('#userRoles').select2();
            $(this.modal.current).on('shown.bs.modal', () => $('#userRoles').focus())
        });
    }

    show = (item) => {
        const { _id, firstname, roles } = item ?
            item : { roles: [] };
        this.setState(_id);

        T.ready(() => {
            let userRoles = roles.map(item => item._id),
                allRoles = this.props.allRoles.map(item => ({ id: item._id, text: item.name }));
            $('#userName').val(firstname);
            $('#roles').select2({ placeholder: 'Lựa chọn Vai trò', data: allRoles }).val(userRoles).trigger('change');
        });

        $(this.modal.current).modal('show');
    };

    save = (event) => {
        this.props.updateUser(this.state._id, { roles: $('#roles').val() });
        $(this.modal.current).modal('hide');
        event.preventDefault();
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật vai trò</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='roles' className='control-label'>Vai trò</label><br />
                                <select className='form-control' id='roles' multiple={true} defaultValue={[]}>
                                    <optgroup label='Lựa chọn Vai trò' />
                                </select>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary'>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    };

}

export class UserModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isStaff: false };

        this.modal = React.createRef();
        this.sex = React.createRef();
        this.imageBox = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => setTimeout(() => {
            $('#userDivisions').select2();
            $('#userRoles').select2();
            $('#userBirthday').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
            $(this.modal.current).on('shown.bs.modal', () => $('#userLastname').focus())
        }, 250));
    }

    componentDidUpdate() {
        $(document).ready(() => {
            if (this.state.divisions) {
                const allDivisions = (this.props.allDivisions ? this.props.allDivisions : []).map(item => ({
                    id: item._id,
                    text: item.name.getText()
                }));
                $('#userDivisions').select2({
                    placeholder: 'Lựa chọn Đơn vị công tác',
                    data: allDivisions
                }).val(this.state.divisions).trigger('change');
            }
        });
    }

    show = (_item) => {
        const item = _item ?
            _item : { _id: null, divisions: [], roles: [] };
        $('#userFirstname').val(item.firstname);
        $('#userLastname').val(item.lastname);
        $('#userBirthday').val(item.birthday ? T.dateToText(item.birthday, 'dd/mm/yyyy') : '');
        $('#userEmail').val(item.email);
        $('#userPhoneNumber').val(item.phoneNumber);
        $('#userFacebook').val(item.facebook);
        $('#userWebsite').val(item.website);
        $('#userAcademicTitleYear').val(item.academicTitleYear);
        $('#userPublicationName').val(item.publicationName);
        $('#userOrganizationId').val(item.organizationId);
        $('#userActive').prop('checked', item.active);
        $('#userStudent').prop('checked', item.isStudent);
        $('#userStaff').prop('checked', item.isStaff);
        this.sex.current.setText(item.sex ? item.sex : '');

        T.ready(() => {
            if (item.divisions) {
                const allDivisions = (this.props.allDivisions ? this.props.allDivisions : []).map(item => ({
                    id: item._id,
                    text: item.name.getText()
                }));
                $('#userDivisions').select2({
                    placeholder: 'Lựa chọn Đơn vị công tác',
                    data: allDivisions
                }).val(item.divisions).trigger('change');
            }

            let userRoles = item.roles.map(item => item._id),
                allRoles = this.props.allRoles.map(item => ({ id: item._id, text: item.name }));
            $('#userRoles').select2({
                placeholder: 'Lựa chọn Vai trò',
                data: allRoles
            }).val(userRoles).trigger('change');
        });

        this.setState({
            _id: item._id,
            academicTitle: item.academicTitle,
            academicDistinction: item.academicDistinction,
            graduatedCountry: item.graduatedCountry,
            image: item.image,
            isStaff: item.isStaff,
            divisions: item.divisions
        });
        this.imageBox.current.setData('user:' + (item._id ? item._id : 'new'));
        $(this.modal.current).modal('show');
    };

    save = (event) => {
        const sex = this.sex.current.getSelectedItem().toLowerCase(),
            birthday = $('#userBirthday').val() ? T.formatDate($('#userBirthday').val()) : null;
        let changes = {
            firstname: $('#userFirstname').val().trim(),
            lastname: $('#userLastname').val().trim(),
            email: $('#userEmail').val().trim(),
            phoneNumber: $('#userPhoneNumber').val().trim(),
            active: $('#userActive').prop('checked'),
            isStudent: $('#userStudent').prop('checked'),
            isStaff: $('#userStaff').prop('checked'),
            roles: $('#userRoles').val(),
            birthday,
            organizationId: $('#userOrganizationId').val(),
        };

        if (T.sexes.indexOf(sex) != -1) {
            changes.sex = sex;
        }
        if (changes.isStaff) {
            changes = Object.assign({}, changes, {
                publicationName: $('#userPublicationName').val().trim(),
                facebook: $('#userFacebook').val().trim(),
                website: $('#userWebsite').val().trim(),

                academicDistinction: this.state.academicDistinction ? this.state.academicDistinction : '',
                graduatedCountry: this.state.graduatedCountry ? this.state.graduatedCountry : null,

                academicTitle: this.state.academicTitle ? this.state.academicTitle : '',
                academicTitleYear: $('#userAcademicTitleYear').val().trim(),

                divisions: $('#userDivisions').val(),
            })
        }
        if (changes.firstname == '') {
            T.notify('Tên người dùng bị trống!', 'danger');
            $('#userFirstname').focus();
        } else if (changes.lastname == '') {
            T.notify('Họ người dùng bị trống!', 'danger');
            $('#userLastname').focus();
        } else if (changes.email == '') {
            T.notify('Email người dùng bị trống!', 'danger');
            $('#userEmail').focus();
        } else {
            if (changes.divisions && changes.divisions.length == 0) changes.divisions = 'empty';
            if (changes.roles.length == 0) changes.roles = 'empty';
            if (this.state._id) {
                this.props.updateUser(this.state._id, changes);
            } else {
                this.props.createUser(changes);
            }
            $(this.modal.current).modal('hide');
        }
        event.preventDefault();
    };

    render() {
        const academicTitle = this.state.academicTitle ? {
            value: this.state.academicTitle,
            label: this.state.academicTitle
        } : null;
        const academicDistinction = this.state.academicDistinction ? {
            value: this.state.academicDistinction,
            label: this.state.academicDistinction
        } : null;
        const graduatedCountry = this.state.graduatedCountry ? {
            value: this.state.graduatedCountry,
            label: getName(this.state.graduatedCountry)
        } : null;

        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin người dùng</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-md-8 col-12'>
                                    <div className='form-group row'>
                                        <div className='col-12 col-sm-8'>
                                            <label htmlFor='userLastname'>Họ người dùng</label>
                                            <input className='form-control' id='userLastname' type='text'
                                                placeholder='Họ người dùng' />
                                        </div>
                                        <div className='col-12 col-sm-4'>
                                            <label htmlFor='userFirstname'>Tên người dùng</label>
                                            <input className='form-control' id='userFirstname' type='text'
                                                placeholder='Tên người dùng' />
                                        </div>
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='userEmail'>Email người dùng</label>
                                        <input className='form-control' id='userEmail' type='email'
                                            placeholder='Email người dùng' />
                                    </div>
                                    <div className='form-group row'>
                                        <div className='col-12 col-sm-6'>
                                            <label htmlFor='userBirthday'>Ngày sinh</label>
                                            <input className='form-control' id='userBirthday' type='text'
                                                placeholder='Ngày sinh' />
                                        </div>
                                        <div className='col-12 col-sm-6'>
                                            <label htmlFor='userPhoneNumber'>Số điện thoại</label>
                                            <input className='form-control' id='userPhoneNumber' type='text'
                                                placeholder='Số điện thoại' />
                                        </div>
                                    </div>
                                </div>
                                <div className='col-md-4 col-12'>
                                    <div className='form-group'>
                                        <label>Hình đại diện</label>
                                        <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='UserImage'
                                            userData='user' image={this.state.image} />
                                    </div>
                                    <div className='form-group' style={{ display: 'inline-flex', width: '100%' }}>
                                        <label>Giới tính: </label>&nbsp;&nbsp;
                                            <Dropdown ref={this.sex} text='' items={T.sexes} />
                                    </div>
                                </div>
                                <div className='col-12 row'>
                                    <div className='col-12 col-md-8 form-group'>
                                        <label htmlFor='userRoles' className='control-label'>Vai trò</label><br />
                                        <select className='form-control' id='userRoles' multiple={true}
                                            defaultValue={[]}>
                                            <optgroup label='Lựa chọn Vai trò' />
                                        </select>
                                    </div>
                                    <div className='col-12 col-md-4'>
                                        <label htmlFor='userOrganizationId'>MSSV/MSCB</label>
                                        <input className='form-control' id='userOrganizationId' type='text' placeholder='MSSV/MSCB' />
                                    </div>
                                </div>
                                <div className='col-12 row'>
                                    <div className='col-4'>
                                        <div className='form-group' style={{ display: 'inline-flex', width: '100%' }}>
                                            <label htmlFor='userActive'>Kích hoạt: </label>&nbsp;&nbsp;
                                                <div className='toggle'>
                                                <label>
                                                    <input type='checkbox' id='userActive' onChange={() => {
                                                    }} /><span className='button-indecator' />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-4'>
                                        <div className='form-group' style={{ display: 'inline-flex', width: '100%' }}>
                                            <label htmlFor='userStudent'>Sinh viên: </label>&nbsp;&nbsp;
                                                <div className='toggle'>
                                                <label>
                                                    <input type='checkbox' id='userStudent' onChange={() => {
                                                    }} /><span className='button-indecator' />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-4'>
                                        <div className='form-group' style={{ display: 'inline-flex', width: '100%' }}>
                                            <label htmlFor='userStaff'>Nhân sự: </label>&nbsp;&nbsp;
                                                <div className='toggle'>
                                                <label>
                                                    <input type='checkbox' id='userStaff' onChange={(event) => {
                                                        this.setState({ isStaff: event.target.checked })
                                                    }} value={this.state.isStaff} /><span className='button-indecator' />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {this.state.isStaff ? (
                                    <div className='col-12'>
                                        <div className='form-group row'>
                                            <div className='col-md-4 col-12'>
                                                <label htmlFor='userPublicationName'>Tên viết tắt (trong bài báo khoa
                                                            học)</label>
                                                <input className='form-control' id='userPublicationName' type='text'
                                                    placeholder='Tên viết tắt (trong bài báo khoa học)' />
                                            </div>
                                            <div className='col-md-4 col-12'>
                                                <label htmlFor='userFacebook'>Facebook</label>
                                                <input className='form-control' id='userFacebook' type='text'
                                                    placeholder='Facebook' />
                                            </div>
                                            <div className='col-md-4 col-12'>
                                                <label htmlFor='userWebsite'>Website</label>
                                                <input className='form-control' id='userWebsite' type='text'
                                                    placeholder='Website' />
                                            </div>
                                        </div>
                                        <div className='form-group row'>
                                            <div className='col-md-6 col-12'>
                                                <label>Học vị</label>
                                                <div>
                                                    <Select value={academicDistinction} options={academicDistinctions}
                                                        placeholder='Học vị' isClearable={true}
                                                        onChange={selectedItem => this.setState({ academicDistinction: selectedItem ? selectedItem.value : null })} />
                                                </div>
                                            </div>
                                            <div className='col-md-6 col-12'>
                                                <label>Quốc gia tốt nghiệp</label>
                                                <div>
                                                    <Select value={graduatedCountry} options={countryList}
                                                        placeholder='Quốc gia tốt nghiệp' isClearable={true}
                                                        onChange={selectedItem => this.setState({ graduatedCountry: selectedItem ? selectedItem.value : null })} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className='form-group row'>
                                            <div className='col-md-6 col-12'>
                                                <label>Học hàm</label>
                                                <div>
                                                    <Select value={academicTitle} options={academicTitles}
                                                        placeholder='Học hàm' isClearable={true}
                                                        onChange={selectedItem => this.setState({ academicTitle: selectedItem ? selectedItem.value : null })} />
                                                </div>
                                            </div>
                                            <div className='col-md-6 col-12'>
                                                <label htmlFor='userAcademicTitleYear'>Năm phong học hàm</label>
                                                <input className='form-control' id='userAcademicTitleYear' type='number'
                                                    placeholder='Năm phong học hàm' />
                                            </div>
                                        </div>
                                        <div className='form-group'>
                                            <label htmlFor='userDivisions' className='control-label'>Đơn vị công tác</label><br />
                                            <select className='form-control' id='userDivisions' multiple={true} defaultValue={[]}>
                                                <optgroup label='Lựa chọn Đơn vị công tác' />
                                            </select>
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary'>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export class MoreInfoModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
        this.editorViBiography = React.createRef();
        this.editorEnBiography = React.createRef();
        this.editorViResearchFields = React.createRef();
        this.editorEnResearchFields = React.createRef();
        this.editorViPostgraduateCourses = React.createRef();
        this.editorEnPostgraduateCourses = React.createRef();

        this.show = this.show.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount() {
        T.ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => {
                $(this.modal.current).find('li:first-child a').tab('show');
                $('#staffMasterThesis').focus();
            });
        });
    }

    show(item) {
        let { _id, masterThesis, goingDoctorThesis, finishedDoctorThesis, trainingExpertise, currentExpertise, biography, researchFields, postgraduateCourses } = item ? item : {};
        masterThesis = masterThesis ? masterThesis : 0;
        goingDoctorThesis = goingDoctorThesis ? goingDoctorThesis : 0;
        finishedDoctorThesis = finishedDoctorThesis ? finishedDoctorThesis : 0;

        trainingExpertise = T.language.parse(trainingExpertise, true);
        currentExpertise = T.language.parse(currentExpertise, true);
        biography = T.language.parse(biography, true);
        researchFields = T.language.parse(researchFields, true);
        postgraduateCourses = T.language.parse(postgraduateCourses, true);

        $('#staffMasterThesis').val(masterThesis);
        $('#staffGoingDoctorThesis').val(goingDoctorThesis);
        $('#staffFinishedDoctorThesis').val(finishedDoctorThesis);

        $('#staffViTrainingExpertise').val(trainingExpertise.vi);
        $('#staffEnTrainingExpertise').val(trainingExpertise.en);
        $('#staffViCurrentExpertise').val(currentExpertise.vi);
        $('#staffEnCurrentExpertise').val(currentExpertise.en);

        this.editorViBiography.current.html(biography.vi);
        this.editorEnBiography.current.html(biography.en);
        this.editorViResearchFields.current.html(researchFields.vi);
        this.editorEnResearchFields.current.html(researchFields.en);
        this.editorViPostgraduateCourses.current.html(postgraduateCourses.vi);
        this.editorEnPostgraduateCourses.current.html(postgraduateCourses.en);

        $(this.modal.current).modal('show');
        this.setState({ _id });
    }

    save(e) {
        e.preventDefault();
        const _id = this.state._id,
            trainingExpertise = {
                vi: $('#staffViTrainingExpertise').val(),
                en: $('#staffEnTrainingExpertise').val(),
            },
            currentExpertise = {
                vi: $('#staffViCurrentExpertise').val(),
                en: $('#staffEnCurrentExpertise').val(),
            },
            biography = {
                vi: this.editorViBiography.current.html(),
                en: this.editorEnBiography.current.html(),
            },
            researchFields = {
                vi: this.editorViResearchFields.current.html(),
                en: this.editorEnResearchFields.current.html(),
            },
            postgraduateCourses = {
                vi: this.editorViPostgraduateCourses.current.html(),
                en: this.editorEnPostgraduateCourses.current.html(),
            },
            changes = {
                masterThesis: $('#staffMasterThesis').val(),
                goingDoctorThesis: $('#staffGoingDoctorThesis').val(),
                finishedDoctorThesis: $('#staffFinishedDoctorThesis').val(),
                trainingExpertise: JSON.stringify(trainingExpertise),
                currentExpertise: JSON.stringify(currentExpertise),
                biography: JSON.stringify(biography),
                researchFields: JSON.stringify(researchFields),
                postgraduateCourses: JSON.stringify(postgraduateCourses),
            };

        this.props.updateUser(_id, changes);
        $(this.modal.current).modal('hide');
    }

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin chi tiết nhân viên</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <ul className='nav nav-tabs'>
                                <li className='nav-item'>
                                    <a className='nav-link active show' data-toggle='tab' href='#staffViTab'>Việt
                                            Nam</a>
                                </li>
                                <li className='nav-item'>
                                    <a className='nav-link' data-toggle='tab' href='#staffEnTab'>English</a>
                                </li>
                            </ul>

                            <div className='row mt-3'>
                                <div className='col-md-4 form-group'>
                                    <label htmlFor='staffMasterThesis'>LVThS đã hướng dẫn thành công</label>
                                    <input className='form-control' id='staffMasterThesis' type='number'
                                        placeholder='LVThS đã hướng dẫn thành công' />
                                </div>
                                <div className='col-md-4 form-group'>
                                    <label htmlFor='staffFinishedDoctorThesis'>LVTS đã hướng dẫn thành công</label>
                                    <input className='form-control' id='staffFinishedDoctorThesis' type='number'
                                        placeholder='LVTS đã hướng dẫn thành công' />
                                </div>
                                <div className='col-md-4 form-group'>
                                    <label htmlFor='staffGoingDoctorThesis'>LVTS đang hướng dẫn</label>
                                    <input className='form-control' id='staffGoingDoctorThesis' type='number'
                                        placeholder='LVTS đang hướng dẫn' />
                                </div>
                            </div>

                            <div className='tab-content'>
                                <div id='staffViTab' className='tab-pane fade show active'>
                                    <div className='row'>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffViTrainingExpertise'>Chuyên ngành (Tiếng Việt)</label>
                                            <input className='form-control' id='staffViTrainingExpertise' type='text'
                                                placeholder='Chuyên ngành' />
                                        </div>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffViCurrentExpertise'>Lĩnh vực chuyên môn hiện tại (Tiếng
                                                    Việt)</label>
                                            <input className='form-control' id='staffViCurrentExpertise' type='text'
                                                placeholder='Lĩnh vực chuyên môn hiện tại' />
                                        </div>
                                    </div>

                                    <ul className='nav nav-tabs'>
                                        <li className='nav-item'>
                                            <a className='nav-link active show' data-toggle='tab'
                                                href='#staffBiographyViTab'>Tiểu sử</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab' href='#staffResearchFieldsViTab'>Các
                                                    hướng nghiên cứu chính</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab'
                                                href='#staffPostgraduateCoursesViTab'>Giảng dạy các môn học Sau đại
                                                    học</a>
                                        </li>
                                    </ul>
                                    <div className='tab-content'>
                                        <div id='staffBiographyViTab' className='tab-pane fade show active'>
                                            <Editor ref={this.editorViBiography} placeholder='Tiểu sử' height={400} />
                                        </div>
                                        <div id='staffResearchFieldsViTab' className='tab-pane fade'>
                                            <Editor ref={this.editorViResearchFields}
                                                placeholder='Các hướng nghiên cứu chính' height={400} />
                                        </div>
                                        <div id='staffPostgraduateCoursesViTab' className='tab-pane fade'>
                                            <Editor ref={this.editorViPostgraduateCourses}
                                                placeholder='Giảng dạy các môn học Sau đại học' height={400} />
                                        </div>
                                    </div>
                                </div>

                                <div id='staffEnTab' className='tab-pane fade'>
                                    <div className='row'>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffEnTrainingExpertise'>Chuyên ngành (English)</label>
                                            <input className='form-control' id='staffEnTrainingExpertise' type='text'
                                                placeholder='Chuyên ngành' />
                                        </div>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffEnCurrentExpertise'>Lĩnh vực chuyên môn hiện tại
                                                    (English)</label>
                                            <input className='form-control' id='staffEnCurrentExpertise' type='text'
                                                placeholder='Lĩnh vực chuyên môn hiện tại' />
                                        </div>
                                    </div>

                                    <ul className='nav nav-tabs'>
                                        <li className='nav-item'>
                                            <a className='nav-link active show' data-toggle='tab'
                                                href='#staffBiographyEnTab'>Tiểu sử</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab' href='#staffResearchFieldsEnTab'>Các
                                                    hướng nghiên cứu chính</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab'
                                                href='#staffPostgraduateCoursesEnTab'>Giảng dạy các môn học Sau đại
                                                    học</a>
                                        </li>
                                    </ul>
                                    <div className='tab-content'>
                                        <div id='staffBiographyEnTab' className='tab-pane fade show active'>
                                            <Editor ref={this.editorEnBiography} placeholder='Tiểu sử' height={400} />
                                        </div>
                                        <div id='staffResearchFieldsEnTab' className='tab-pane fade'>
                                            <Editor ref={this.editorEnResearchFields}
                                                placeholder='Các hướng nghiên cứu chính' height={400} />
                                        </div>
                                        <div id='staffPostgraduateCoursesEnTab' className='tab-pane fade'>
                                            <Editor ref={this.editorEnPostgraduateCourses}
                                                placeholder='Giảng dạy các môn học Sau đại học' height={400} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary'>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class ChildUploadStaffModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: null };
        this.modal = React.createRef();
        this.fileBox = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ message: null });
                this.props.clearSession('staffs');
            });
        });
    }

    show = () => {
        this.setState({ message: null });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        this.props.createStaffsFromFile(() => {
            $(this.modal.current).modal('hide');
        });
    };

    onSuccess = (response) => {
        this.setState({ message: <p className='text-center' style={{ color: 'green' }}>{response.number} nhân viên được tải lên thành công</p> });
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Tải lên danh sách nhân viên</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='StaffFile' userData='staffImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                            {this.state.message}
                        </div>
                        <div className='modal-footer'>
                            <a href='/download/SampleUploadStaffsVer2.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' style={{ display: this.state.message ? 'block' : 'none' }}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToUploadStaff = state => ({});
const mapActionsToUploadStaff = { clearSession, createStaffsFromFile };
const ConnectUploadStaffModal = connect(mapStateToUploadStaff, mapActionsToUploadStaff, null, { forwardRef: true })(ChildUploadStaffModal);

export class UploadStaffModal extends React.Component {
    constructor(props) {
        super(props);

        this.childUpload = React.createRef();
    }

    show = () => {
        this.childUpload.current.show();
    };

    render() {
        return (<ConnectUploadStaffModal ref={this.childUpload} />);
    }
}
