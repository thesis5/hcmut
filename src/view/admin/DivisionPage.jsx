import React from 'react';
import { connect } from 'react-redux';
import { getAllDivisions, getDivision, createDivision, swapDivision, updateDivision, deleteDivision } from '../redux/division.jsx'
import { Link } from 'react-router-dom';
import Select from 'react-select';
import Editor from '../common/CkEditor4.jsx';
import ImageBox from '../common/ImageBox.jsx';
import Dropdown from '../common/Dropdown.jsx';

class DivisionModal extends React.Component {
    constructor(props) {
        super(props);
        let types = Object.keys(T.divisionTypes).map(key => ({ value: key, label: T.divisionTypes[key] }));
        this.state = { divisionTypes: types, selectedDivision: null };

        this.modal = React.createRef();
        this.imageBox = React.createRef();
        this.viEditor = React.createRef();
        this.enEditor = React.createRef();
        this.btnSave = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => setTimeout(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#divisionViName').focus())
        }, 250));
    }

    show = (_id) => {
        if (_id) {
            this.props.getDivision(_id, item => {
                let { _id, name, image, type, active, abstract, content } = item ? item : { image: '/img/avatar.png' };
                name = T.language.parse(name, true);
                abstract = T.language.parse(abstract, true);
                content = T.language.parse(content, true);
                $('#divisionViName').val(name.vi);
                $('#divisionEnName').val(name.en);
                $('#divisionActive').prop('checked', active != null && active.toString().toLowerCase() == 'true');
                $('#divisionViAbstract').val(abstract.vi);
                $('#divisionEnAbstract').val(abstract.en);
                this.viEditor.current.html(content.vi);
                this.enEditor.current.html(content.en);
                this.imageBox.current.setData('Division:' + _id);
                this.setState({ selectedItem: { value: type, label: T.divisionTypes[type] }, image });

                $(this.btnSave.current).data('_id', _id);
                $(this.modal.current).modal('show');
            });
        } else {
            $('#divisionViName').val('');
            $('#divisionEnName').val('');
            $('#divisionActive').prop('checked', false);
            $('#divisionViAbstract').val('');
            $('#divisionEnAbstract').val('');
            this.viEditor.current.html('');
            this.enEditor.current.html('');
            this.imageBox.current.setData('division:new');
            this.setState({ selectedItem: null, image: null });

            $(this.btnSave.current).data('_id', null);
            $(this.modal.current).modal('show');
        }
    }

    onSelectType = (selectedItem) => this.setState({ selectedItem })

    imageChanged = (data) => this.setState({ image: data.image });

    save = (event) => {
        const _id = $(this.btnSave.current).data('_id'),
            name = JSON.stringify({ vi: $('#divisionViName').val().trim(), en: $('#divisionEnName').val().trim() }),
            type = this.state.selectedItem ? this.state.selectedItem.value : null,
            image = this.state.image,
            active = $('#divisionActive').prop('checked'),
            abstract = JSON.stringify({ vi: $('#divisionViAbstract').val().trim(), en: $('#divisionEnAbstract').val().trim() }),
            content = JSON.stringify({ vi: this.viEditor.current.html(), en: this.enEditor.current.html() });
        if (name.vi == '') {
            T.notify('Tên bộ phận bị trống!', 'danger');
            $('#divisionViName').focus();
        } else if (name.en == '') {
            T.notify('The division name is empty now!', 'danger');
            $('#divisionEnName').focus();
        } else if (type == null) {
            T.notify('Loại bộ phận bị trống!', 'danger');
        } else if (_id) {
            this.props.updateDivision(_id, { name, type, image, active, abstract, content }, data => {
                if (data.error == undefined || data.error == null) {
                    $(this.modal.current).modal('hide');
                }
            });
        } else {
            this.props.createDivision({ name, type, image, active, abstract, content }, data => {
                if (data.error == undefined || data.error == null) {
                    $(this.modal.current).modal('hide');
                }
            });
        }
        event.preventDefault();
    }

    render() {
        const { currentPermission } = this.props
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin bộ phận</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <ul className='nav nav-tabs'>
                                <li className='nav-item'>
                                    <a className='nav-link active show' data-toggle='tab' href='#divisionViTab'>Việt Nam</a>
                                </li>
                                <li className='nav-item'>
                                    <a className='nav-link' data-toggle='tab' href='#divisionEnTab'>English</a>
                                </li>
                            </ul>
                            <div className='tab-content'>
                                <div className='form-group row'>
                                    <div className='col-8'>
                                        <div className='form-group'>
                                            <label htmlFor='divisionType'>Loại</label><br />
                                            <Select options={this.state.divisionTypes} isClearable={true} onChange={this.onSelectType} value={this.state.selectedItem} />
                                        </div>
                                        <div style={{ display: 'inline-flex' }}>
                                            <label htmlFor='divisionActive'>Kích hoạt: </label>&nbsp;&nbsp;
                                                <div className='toggle'>
                                                <label>
                                                    <input type='checkbox' id='divisionActive' onChange={() => { }} /><span className='button-indecator' />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-4'>
                                        <label>Hình đại diện</label>
                                        <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='DivisionImage' userData='Division' image={this.state.image} success={this.imageChanged} />
                                    </div>
                                </div>

                                <div id='divisionViTab' className='tab-pane fade show active'>
                                    <div className='form-group'>
                                        <label htmlFor='divisionViName'>Tên</label>
                                        <input className='form-control' id='divisionViName' type='text' placeholder='Tên' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='divisionViAbstract'>Giới thiệu ngắn</label>
                                        <textarea className='form-control' id='divisionViAbstract' type='text' placeholder='Giới thiệu ngắn' defaultValue=''
                                            style={{ border: 'solid 1px #eee', width: '100%', minHeight: '100px', padding: '0 3px' }} />
                                    </div>
                                    <div className='form-group'>
                                        <label>Giới thiệu chi tiết</label>
                                        <Editor ref={this.viEditor} className='form-control' placeholder='Giới thiệu chi tiết' />
                                    </div>
                                </div>

                                <div id='divisionEnTab' className='tab-pane fade'>
                                    <div className='form-group'>
                                        <label htmlFor='divisionEnName'>Name</label>
                                        <input className='form-control' id='divisionEnName' type='text' placeholder='Name' />
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='divisionEnAbstract'>Short description</label>
                                        <textarea className='form-control' id='divisionEnAbstract' type='text' placeholder='Short description' defaultValue=''
                                            style={{ border: 'solid 1px #eee', width: '100%', minHeight: '100px', padding: '0 3px' }} />
                                    </div>
                                    <div className='form-group'>
                                        <label>Detail</label>
                                        <Editor ref={this.enEditor} className='form-control' placeholder='Detail' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            {
                                currentPermission.contains('division:write') &&
                                <button type='button' className='btn btn-primary' ref={this.btnSave} onClick={this.save}>Lưu</button>
                            }
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class DivisionPage extends React.Component {
    constructor(props) {
        super(props);
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getAllDivisions();
        T.ready();
    }

    create = (e) => {
        this.props.createDivision();
        e.preventDefault();
    }

    swap = (e, item, isMoveUp) => {
        this.props.swapDivision(item._id, isMoveUp);
        e.preventDefault();
    }

    changeType = (item, selectedType) => {
        this.props.updateDivision(item._id, { type: selectedType.value });
    }

    changeActive = (item) => {
        this.props.updateDivision(item._id, { active: !item.active });
    }

    delete = (e, item) => {
        T.confirm('Xóa division', 'Bạn có chắc bạn muốn xóa bộ phận này?', true, isConfirm => isConfirm && this.props.deleteDivision(item._id));
        e.preventDefault();
    }

    edit = (e, item) => {
        this.modal.current.show(item._id);
        e.preventDefault();
    }

    render() {
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];

        let table = null,
            items = this.props.division ? this.props.division : null,
            divisionTypeItems = Object.keys(T.divisionTypes).map(key => ({ value: key, text: T.divisionTypes[key] }));
        if (items && items.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '80%' }}>Tên</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Phân loại</th>
                            <th style={{ width: '20%', textAlign: 'center', whiteSpace: 'nowrap' }}>Hình ảnh</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Kích hoạt</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {items.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>
                                        {T.language.parse(item.name, true).vi}
                                    </a>
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <Dropdown text={T.divisionTypes[item.type]} items={divisionTypeItems} onSelected={selectedItem => this.changeType(item, selectedItem)} />
                                </td>
                                <td style={{ textAlign: 'center' }}>
                                    <img src={item.image ? item.image : '/img/avatar.png'} alt='avatar' style={{ height: '32px' }} />
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }} >
                                    <label>
                                        <input type='checkbox' checked={item.active} onChange={() => this.changeActive(item, index)} disabled={!currentPermission.contains('division:write')} /><span className='button-indecator' />
                                    </label>
                                </td>
                                <td>
                                    <div className='btn-group'>
                                        {
                                            currentPermission.contains('division:write') ? [
                                                <a key={0} className='btn btn-success' href='#' onClick={e => this.swap(e, item, true)}>
                                                    <i className='fa fa-lg fa-arrow-up' />
                                                </a>,
                                                <a key={1} className='btn btn-success' href='#' onClick={e => this.swap(e, item, false)}>
                                                    <i className='fa fa-lg fa-arrow-down' />
                                                </a>
                                            ] : null
                                        }
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        {
                                            currentPermission.contains('division:write') &&
                                            <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                                <i className='fa fa-lg fa-trash' />
                                            </a>
                                        }
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có bộ phận nào cả!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-bullseye' /> Các bộ phận của trường</h1>
                </div>

                <div className='row tile'>{table}</div>
                <DivisionModal currentPermission={currentPermission} createDivision={this.props.createDivision} updateDivision={this.props.updateDivision} getDivision={this.props.getDivision} ref={this.modal} />
                {
                    currentPermission.contains('division:write') &&
                    <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}
                        onClick={() => this.modal.current.show()}>
                        <i className='fa fa-lg fa-plus' />
                    </button>
                }
            </main>
        );
    }
}

const mapStateToProps = state => ({ division: state.division, system: state.system });
const mapActionsToProps = { getAllDivisions, getDivision, createDivision, swapDivision, updateDivision, deleteDivision };
export default connect(mapStateToProps, mapActionsToProps)(DivisionPage);