import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';
import { getPgsGsKhtnvInPage, updatePgsGsKhtnv, deletePgsGsKhtnv, createPgsGsKhtnv } from '../../redux/dsPgsGsKhtnv.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { shcc: null, isUpdate: true };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#shcc').focus());
            $(this.modal.current).on('hidden.bs.modal', () => this.setState({ shcc: null, isUpdate: true }));
        })
    }

    show = (officer) => {
        const { shcc, year, note } = officer ?
            officer : { shcc: '', year: '', note: '' };
        $('#shcc').val(shcc);
        $('#year').val(year);
        $('#note').val(note);
        if (shcc === '') {
            this.setState({ shcc, isUpdate: false });
        } else {
            this.setState({ shcc, isUpdate: true });
        }

        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            shcc: ($('#shcc').val()),
            year: ($('#year').val()),
            note: ($('#note').val()),
        };

        if (changes.shcc === '') {
            T.notify('Số hiệu công chức bị trống', 'danger');
            $('#shcc').focus();
        } else {
            if (this.state.isUpdate) {
                this.props.updatePgsGsKhtnv(this.state.shcc, changes, () => {
                    T.notify('Cập nhật thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }
            else {
                this.props.createPgsGsKhtnv(changes, () => {
                    T.notify('Tạo mới thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{this.state.isUpdate ? 'Cập nhật' : 'Tạo mới'}</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='shcc'>SHCC</label>
                                        <input className='form-control' id='shcc' type='text' placeholder='Số hiệu công chức' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='year'>Năm</label>
                                        <input className='form-control' id='year' type='number' placeholder='Năm' />
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <div className='form-group'>
                                        <label htmlFor='note'>Ghi chú</label>
                                        <textarea className='form-control' id='note' placeholder='Ghi chú' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DSPgsGsKhongHoanThanhNhiemVuPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isSearching: false };
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getPgsGsKhtnvInPage(1, 50, {});
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        T.confirm('Xóa', 'Bạn có chắc bạn muốn xóa hàng này?', true, isConfirm => isConfirm && this.props.deletePgsGsKhtnv(item._id));
        e.preventDefault();
    };

    create = (e) => {
        this.modal.current.show();
        e.preventDefault();
    };

    search = (e) => {
        e.preventDefault();
        const condition = {};
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value }
            ]
        }
        this.props.getPgsGsKhtnvInPage(undefined, undefined, (condition), () => this.setState({ isSearching: value != '' }));
    };

    render() {
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        const { pageNumber, pageSize, pageTotal, totalItem, pageCondition } = this.props.dsPgsGsKhtnv && this.props.dsPgsGsKhtnv.page ?
            this.props.dsPgsGsKhtnv.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, pageCondition: {} };
        let table = null;

        if (this.props.dsPgsGsKhtnv && this.props.dsPgsGsKhtnv.page && this.props.dsPgsGsKhtnv.page.list && this.props.dsPgsGsKhtnv.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Năm</th>
                            <th style={{ width: '100%' }} nowrap='true'>Ghi chú</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.dsPgsGsKhtnv.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a>
                                </td>
                                <td>{item.year}</td>
                                <td style={{ width: '100%' }}>{item.note}</td>
                                {currentPermission.contains('dsPgsGsKhongHoanThanhNhiemVu:write') ?
                                    <td className='btn-group' style={{ display: 'flex' }}>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </td>
                                    : null}
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Danh sách trống!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-star' /> DS PGS/GS không hoàn thành nhiệm vụ</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pagePgsGsKhtnv'
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} pageCondition={pageCondition}
                    getPage={this.props.getPgsGsKhtnvInPage} />
                <Link to='/user/summary/pgsgs_khtnv/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>
                <EditModal ref={this.modal} updatePgsGsKhtnv={this.props.updatePgsGsKhtnv} createPgsGsKhtnv={this.props.createPgsGsKhtnv} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsPgsGsKhtnv: state.dsPgsGsKhtnv, system: state.system });
const mapActionsToProps = { getPgsGsKhtnvInPage, updatePgsGsKhtnv, deletePgsGsKhtnv, createPgsGsKhtnv };
export default connect(mapStateToProps, mapActionsToProps)(DSPgsGsKhongHoanThanhNhiemVuPage);
