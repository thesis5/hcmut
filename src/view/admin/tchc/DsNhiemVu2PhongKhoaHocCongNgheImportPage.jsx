import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { updateMultiDs_nv2_pkhcn } from '../../redux/dsNhiemVu2PhongKhoaHocCongNghe.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#professionalPoint').focus());
            $(this.modal.current).on('hidden.bs.modal', () => this.setState({ index: -1 }));
        })
    }
    show = (index, item) => {
        const { fullName, shtt, cgcn, shcc, ldsx, articleOfUniversity, articleOutUniversity, reward, elseAction, article, point, totalHours } = item ?
            item : { fullName: '', shtt: '', cgcn: '', shcc: '', ldsx: '', articleOfUniversity: '', articleOutUniversity: '', reward: '', elseAction: '', article: '', point: 0, totalHours: '' };
        $('#shcc').val(shcc);
        $('#fullName').val(fullName);
        $('#shtt').val(shtt);
        $('#cgcn').val(cgcn);
        $('#ldsx').val(ldsx);
        $('#articleOfUniversity').val(articleOfUniversity);
        $('#articleOutUniversity').val(articleOutUniversity);
        $('#reward').val(reward);
        $('#elseAction').val(elseAction);
        $('#article').val(article);
        $('#point').val(point);
        $('#totalHours').val(totalHours);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            fullName: $('#fullName').val(),
            shcc: $('#shcc').val(),
            shtt: $('#shtt').val(),
            cgcn: $('#cgcn').val(),
            ldsx: $('#ldsx').val(),
            chcc: $('#chcc').val(),
            articleOfUniversity: $('#articleOfUniversity').val(),
            articleOutUniversity: $('#articleOutUniversity').val(),
            reward: $('#reward').val(),
            article: $('#article').val(),
            elseAction: $('#elseAction').val(),
            point: parseFloat($('#point').val()),
            totalHours: parseFloat($('#totalHours').val()),
        };

        if (isNaN(changes.point)) {
            T.notify('Điểm không chính xác!', 'danger');
            $('#point').focus();
        } else if (isNaN(changes.totalHours)) {
            T.notify('Tổng giờ làm không chính xác!', 'danger');
            $('#totalHours').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật dữ liệu</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='fullName'>Họ và tên</label>
                                        <input className='form-control' id='fullName' type='text' placeholder='Họ và tên' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='shcc'>SHCC</label>
                                        <input className='form-control' id='shcc' type='text' placeholder='SHCC' />
                                    </div>
                                </div>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='shtt'>SHTT</label>
                                <input className='form-control' id='shtt' type='text' placeholder='SHTT' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='ldsx'>LĐSX</label>
                                <input className='form-control' id='ldsx' type='text' placeholder='LĐSX' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='cgcn'>CGCN</label>
                                <input className='form-control' id='cgcn' type='text' placeholder='CGCN' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='articleOfUniversity'>Đề tài thuộc trường(VNU)</label>
                                <input className='form-control' id='articleOfUniversity' type='text' placeholder='Đề tài thuộc trường(VNU)' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='articleOutUniversity'>Đề tài bên ngoài </label>
                                <input className='form-control' id='articleOutUniversity' type='text' placeholder='Đề tài bên ngoài' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='reward'>Giải thưởng</label>
                                <input className='form-control' id='reward' type='text' placeholder='Giải thưởng' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='elseAction'>Hoạt động NCKH khác</label>
                                <input className='form-control' id='elseAction' type='text' placeholder='Hoạt động NCKH khác' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='article'>Bài báo</label>
                                <input className='form-control' id='article' type='text' placeholder='Bài báo' />
                            </div>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='point'>Điểm</label>
                                        <input className='form-control' id='point' type='number' placeholder='Điểm ' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='totalHours'>Tổng giờ nghiên cứu</label>
                                        <input className='form-control' id='totalHours' type='number' placeholder='Tổng giờ nghiên cứu' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsNhiemVu2PhongKhoaHocCongNgheImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/nv2-pkhcn');
    }

    onSuccess = (response) => {
        if (response.numberOfErrors > 0 || response.warnings.length > 0) {
            this.setState({ errorLog: true, numberOfErrors: response.numberOfErrors, warnings: response.warnings });
        }
        if (response.points.length > 0) {
            this.setState({ points: response.points, message: <p className='text-center' style={{ color: 'green' }}>{response.points.length} hàng được tải lên thành công</p> });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const points = this.state.points, currentValue = points[index];
        const updateValue = Object.assign({}, currentValue, {
            fullName: changes.fullName,
            shcc: changes.shcc,
            shtt: changes.shtt,
            cgcn: changes.cgcn,
            ldsx: changes.ldsx,
            articleOfUniversity: changes.articleOfUniversity,
            articleOutUniversity: changes.articleOutUniversity,
            reward: changes.reward,
            elseAction: changes.elseAction,
            article: changes.article,
            point: changes.point,
            totalHours: changes.totalHours,
        });
        points.splice(index, 1, updateValue);
        this.setState({ points });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const points = this.state.points;
        points.splice(index, 1);
        this.setState({ points });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiDs_nv2_pkhcn(this.state.points, () => {
            T.notify('Cập nhật dữ liệu thành công!', 'success');
            this.props.history.push('/user/summary/nv2-pkhcn');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, points } = this.state;
        let table = null;
        if (points && points.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: '100%', textAlign: 'center' }}>Họ và tên</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>CGCN</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>LĐSX</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Đề tài trường</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Đề tài bên ngoài</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Giải thưởng</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Tổng giờ NCKH</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {points.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td className='text-info text-right'>{item.shcc}</td>
                                <td className='text-info text-center'>{item.fullName}</td>
                                <td className='text-info text-right'>{item.cgcn}</td>
                                <td className='text-info text-right'>{item.ldsx}</td>
                                <td className='text-info text-right'>{item.articleOfUniversity}</td>
                                <td className='text-info text-right'>{item.articleOutUniversity}</td>
                                <td className='text-info text-right'>{item.reward}</td>
                                <td className='text-info text-right'>{item.point}</td>
                                <td className='text-info text-right'>{item.totalHours}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file Danh sách nhiệm vụ 2 - Phòng Khoa học công nghệ</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6 offset-md-3'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='Ds_nv2_pkhcnFile' userData='ds_nv2_pkhcnImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                                {errorLog ? (
                                    <div className='tile'>
                                        <div className='tile-body'>
                                            {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                            {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/03.ds_nv2_pkhcn.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    {points && points.length ? (
                        <div className='tile col-12'>
                            {table}
                        </div>
                    ) : null}
                </div>
                <Link to='/user/summary/year' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiDs_nv2_pkhcn };
export default connect(mapStateToProps, mapActionsToProps)(DsNhiemVu2PhongKhoaHocCongNgheImportPage);
