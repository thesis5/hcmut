import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';
import { getKqDgNgoaileInPage, updateKqDgNgoaile, deleteKqDgNgoaile, createKqDgNgoaile } from '../../redux/kq_dg_ngoaile.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = { officerId: null, isUpdate: true };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => {
                $('#officerId').focus();
            });

            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ officerId: null })
            });
        })
    }


    show = (officer) => {
        const { officerId, lastName, firstName, classification, kI } = officer ?
            officer : { officerId: '', lastName: '', firstName: '', classification: '', kI: 0 };
        $('#officerId').val(officerId);
        $('#lastName').val(lastName);
        $('#firstName').val(firstName);
        $('#classification').val(classification);
        $('#kI').val(kI);
        if (officerId === '') {
            this.setState({
                officerId: officerId,
                isUpdate: false
            });
        } else {
            this.setState({
                officerId: officerId,
                isUpdate: true
            });
        }

        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            officerId: ($('#officerId').val()),
            firstName: ($('#firstName').val()),
            lastName: ($('#lastName').val()),
            classification: ($('#classification').val()),
            kI: ($('#kI').val()),
        };

        if (changes.officerId === '') {
            T.notify('Số hiệu công chức bị trống', 'danger');
            $('#officerId').focus();
        } else {
            if (this.state.isUpdate) {
                this.props.updateKqDgNgoaile(this.state.officerId, changes, () => {
                    T.notify('Cập nhật thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }
            else {
                this.props.createKqDgNgoaile(changes, () => {
                    T.notify('Tạo mới thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }

        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật </h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='officerId'>Số hiệu công chức</label>
                                        <input className='form-control' id='officerId' type='text'
                                            placeholder='Số hiệu công chức' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='lastName'>Họ</label>
                                        <input className='form-control' id='lastName' type='text'
                                            placeholder='Ghi chú' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='firstName'>Tên</label>
                                        <input className='form-control' id='firstName' type='text'
                                            placeholder='Ghi chú' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='classification'>Xếp loại</label>
                                        <input className='form-control' id='classification' type='text'
                                            placeholder='Xếp loại' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='kI'>K_I</label>
                                        <input className='form-control' id='kI' type='number'
                                            placeholder='K_I' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class KqDgNgoaiLePage extends React.Component {
    constructor(props) {
        super(props);

        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getKqDgNgoaileInPage();
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        T.confirm('Xóa', 'Bạn có chắc bạn muốn xóa hàng này?', true, isConfirm => isConfirm && this.props.deleteKqDgNgoaile(item._id));
        e.preventDefault();
    };

    create = (e) => {
        this.modal.current.show();
        e.preventDefault();
    };

    render() {
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.kqDgNgoaile && this.props.kqDgNgoaile.page ?
            this.props.kqDgNgoaile.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;

        if (this.props.kqDgNgoaile && this.props.kqDgNgoaile.page && this.props.kqDgNgoaile.page.list && this.props.kqDgNgoaile.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '20%' }}>Số hiệu công chức</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Họ</th>
                            <th style={{ width: '20%' }} nowrap='true'>Tên</th>
                            <th style={{ width: '20%' }} nowrap='true'>Xếp loại</th>
                            <th style={{ width: '20%' }} nowrap='true'>K_I</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.kqDgNgoaile.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    {item.officerId}
                                </td>
                                <td>
                                    {item.firstName}
                                </td>
                                <td>
                                    {item.lastName}
                                </td>
                                <td>
                                    {item.classification}
                                </td>
                                <td>
                                    {item.kI}
                                </td>
                                {currentPermission.contains('point:read') ?
                                    <td className='btn-group' style={{ display: 'flex' }}>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </td>
                                    : null}
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Danh sách trống!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-star' /> Kết quả đánh giá ngoại lệ</h1>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pagekqDgNgoaile'
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getKqDgNgoaileInPage} />
                <Link to='/user/summary/kq-dg-ngoaile/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>
                <EditModal ref={this.modal} updateKqDgNgoaile={this.props.updateKqDgNgoaile} createKqDgNgoaile={this.props.createKqDgNgoaile} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ kqDgNgoaile: state.kqDgNgoaile, system: state.system });
const mapActionsToProps = { getKqDgNgoaileInPage, updateKqDgNgoaile, deleteKqDgNgoaile, createKqDgNgoaile };
export default connect(mapStateToProps, mapActionsToProps)(KqDgNgoaiLePage);
