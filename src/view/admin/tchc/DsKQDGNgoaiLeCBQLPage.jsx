import React from 'react';
import { connect } from 'react-redux';
import { getPointInPage, updatePoint } from '../../redux/dsKQDGNgoaiLeCBQL.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    'shcc': { type: 'text' },
    'tongDiemTN': { type: 'number' },
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null })
            });
        })
    }

    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val(item[key] ? item[key] : null);
            });
        }
        this.setState({ _id: item && item._id ? item._id : null });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        this.props.updatePoint(this.state._id, changes, () => {
            T.notify('Cập nhật thành công!', 'success');
            $(this.modal.current).modal('hide');
        });
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật điểm</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{key}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step}
                                        placeholder={key} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsKQDGNgoaiLeCBQLPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getPointInPage();
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    render() {
        let { pageNumber, pageSize, pageTotal, totalItem, list } = this.props.dsKQDGNgoaiLeCBQL && this.props.dsKQDGNgoaiLeCBQL.page ?
            this.props.dsKQDGNgoaiLeCBQL.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, list };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            {Object.keys(schema).map((key, index) => (
                                <th key={index} style={{ width: '50%' }}>{key}</th>
                            ))}
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                {Object.keys(schema).map((key, i) => {
                                    if (key != 'shcc') return <th key={i} style={{ width: '50%' }}>{item[key]}</th>
                                    else return <th key={i} style={{ width: '50%' }}><a href='#' onClick={e => this.edit(e, item)}>{item[key]}</a></th>
                                })}
                                <td>
                                    <div className=''>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Danh sách KQDG Ngoại lệ CBQL</h1>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='dsKQDGNgoaiLeCBQLPage' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getPointInPage} />
                <Link to='/user/summary/ds-kqdg-ngoai-le-cbql/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <EditModal ref={this.modal} updatePoint={this.props.updatePoint} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsKQDGNgoaiLeCBQL: state.dsKQDGNgoaiLeCBQL });
const mapActionsToProps = { getPointInPage, updatePoint };
export default connect(mapStateToProps, mapActionsToProps)(DsKQDGNgoaiLeCBQLPage);
