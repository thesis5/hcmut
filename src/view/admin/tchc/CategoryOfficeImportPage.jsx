import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { createMultipleCategories } from '../../redux/categoryOffice.jsx';

const schema = {
    'maQL': { type: 'text' },
    'tenChucVu': { type: 'text' },
    'heSoPCQL': { type: 'number', step: 1 },
    'phanNgachCBQL': { type: 'text' },
    'maNhomQuanLy': { type: 'text' },
    'maNhomQuanLyKhongGD': { type: 'text' },
    'tyLeDinhMuc': { type: 'number', step: 1 },
    'kCM': { type: 'number', step: 0.01 },
    'kLD': { type: 'number', step: 0.01 },
    'kI': { type: 'number', step: 0.01 },
    'cap': { type: 'number', step: 1 },
    'chucVuDoanThe': { type: 'text' },
    'note': { type: 'text' },
    'year': { type: 'number', readOnly: true }
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: -1 };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ index: -1 })
            });
        })
    }


    show = (index, item) => {
        Object.keys(schema).forEach(key => {
            if (item[key]) {
                $(this[key].current).val(item[key]);
            }
        });
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => {
            if (schema[key].type == 'text') {
                changes[key] = $(this[key].current).val();
            } else {
                changes[key] = (schema[key].step == 1 || schema[key].readOnly) ? parseInt($(this[key].current).val()) : parseFloat($(this[key].current).val());
            }
        });
        this.props.update(this.state.index, changes, () => {
            $(this.modal.current).modal('hide');
        });
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật danh mục</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label htmlFor={key}>{key}</label>
                                    <input className='form-control' type={schema[key].type} readOnly={schema[key].readOnly} step={schema[key].step} ref={this[key]}
                                        placeholder={key} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class CategoryOfficeImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { categories: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/category-office');
    }

    onSuccess = (response) => {
        const categories = response.categories.sort((a, b) => {
            return a.maQL - b.maQL;
        });
        this.setState({ categories });
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const categories = this.state.categories, currentValue = categories[index];
        const updateValue = Object.assign({}, currentValue, changes);
        categories.splice(index, 1, updateValue);
        this.setState({ categories });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const categories = this.state.categories;
        categories.splice(index, 1);
        this.setState({ categories });
    };

    save = (e) => {
        e.preventDefault();
        this.props.createMultipleCategories(this.state.categories, () => {
            T.notify('Thêm danh sách danh mục thành công!', 'success');
            this.props.history.push('/user/category-office');
        })
    };

    render() {
        const { categories } = this.state;
        let table = null;
        if (categories && categories.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' style={{ maxHeight: '600px', overFlow: 'scroll' }}>
                    <thead>
                        <tr>
                            {Object.keys(schema).map((key, index) => (
                                <th key={index} style={{ width: 'auto' }}>{key}</th>
                            ))}
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.map((item, index) => (
                            <tr key={index}>
                                {Object.keys(schema).map((key, index) => key == 'maQL' ? (
                                    <td key={index}><a href='#' onClick={e => this.showEdit(e, index, item)}>{item[key]}</a></td>
                                ) : (
                                        <td key={index}>{item[key]}</td>
                                    ))}
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên danh mục chức vụ</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6 offset-md-3'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='CategoryOfficeFile' userData='categoryOfficeImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/SampleCategoryOfficeUpload.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    {categories && categories.length ? <div className='tile col-12'>{table}</div> : null}
                </div>
                <Link to='/user/category-office' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { createMultipleCategories };
export default connect(mapStateToProps, mapActionsToProps)(CategoryOfficeImportPage);
