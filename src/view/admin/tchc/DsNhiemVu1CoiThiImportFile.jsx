import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { updateMultiDsNhiemVu1CoiThi } from '../../redux/dsNhiemVu1CoiThi.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => {
                $('#professionalPoint').focus();
            });

            $('#ngay').datepicker({ format: 'dd/mm/yyyy', autoclose: true });

            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ index: -1 })
            });
        })
    }
    show = (index, item) => {
        const { fullname, shcc, ngay, maMonHoc, monHoc, thoiLuong, kyThi, donViCongTac, vn1, ghiChu } = item ?
            item : {
                fullname: '', shcc: '', ngay: '', maMonHoc: '', monHoc: '', thoiLuong: '', kyThi: '', donViCongTac: '', vn1: '', ghiChu: ''
            };
        $('#fullname').val(fullname);
        $('#shcc').val(shcc);
        $('#ngay').datepicker('update', ngay ? T.dateToText(ngay, 'dd/mm/yyyy') : '').trigger('change');
        $('#maMonHoc').val(maMonHoc);
        $('#monHoc').val(monHoc);
        $('#thoiLuong').val(thoiLuong);
        $('#kyThi').val(kyThi);
        $('#donViCongTac').val(donViCongTac);
        $('#vn1').val(vn1);
        $('#ghiChu').val(ghiChu);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            fullname: $('#fullname').val(),
            shcc: $('#shcc').val(),
            cgcn: $('#cgcn').val(),
            ngay: T.formatDate($('#ngay').val()),
            monHoc: $('#monHoc').val(),
            maMonHoc: $('#maMonHoc').val(),
            thoiLuong: $('#thoiLuong').val(),
            kyThi: $('#kyThi').val(),
            donViCongTac: $('#donViCongTac').val(),
            vn1: parseFloat($('#vn1').val()),
            ghiChu: $('#ghiChu').val(),
        };
        if (isNaN(changes.vn1)) {
            T.notify('Thời gian không chính xác!', 'danger');
            $('#point').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật dữ liệu</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='shcc'>Số hiệu cán bộ</label>
                                <input className='form-control' id='shcc' type='text'
                                    placeholder='Số hiệu cán bộ' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='fullname'>Họ và tên</label>
                                <input className='form-control' id='fullname' type='text'
                                    placeholder='Họ và tên' />
                            </div>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='ngay'>Ngày</label>
                                        <input className='form-control' id='ngay' type='text'
                                            placeholder='Ngày' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='maMonHoc'>Mã môn học</label>
                                        <input className='form-control' id='maMonHoc' type='text'
                                            placeholder='Mã môn học' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='monHoc'>Môn học</label>
                                        <input className='form-control' id='monHoc' type='text'
                                            placeholder='Môn học' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='thoiLuong'>Thời lượng</label>
                                        <input className='form-control' id='thoiLuong' type='text'
                                            placeholder='Thời lượng' />
                                    </div>
                                </div>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='kyThi'>Kỳ thi</label>
                                <input className='form-control' id='kyThi' type='text'
                                    placeholder='Kỳ thi' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='donViCongTac'>Đơn vị công tác</label>
                                <input className='form-control' id='donViCongTac' type='text'
                                    placeholder='Kỳ thi' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='vn1'>Giờ quy đổi</label>
                                <input className='form-control' id='vn1' type='number'
                                    placeholder='Giờ quy đổi' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='ghiChu'>Ghi chú</label>
                                <input className='form-control' id='ghiChu' type='text'
                                    placeholder='Ghi chú' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsNhiemVu1CoiThiImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/ds-coithi-nv1');
    }

    onSuccess = (response) => {

        if (response.points.length > 0) {
            this.setState({ points: response.points, message: <p className='text-center' style={{ color: 'green' }}>{response.points.length} hàng được tải lên thành công</p> });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const points = this.state.points, currentValue = points[index];
        const updateValue = Object.assign({}, currentValue, {
            fullname: changes.fullname,
            shcc: changes.shcc,
            ngay: changes.ngay,
            maMonHoc: changes.maMonHoc,
            monHoc: changes.monHoc,
            thoiLuong: changes.thoiLuong,
            kyThi: changes.kyThi,
            donViCongTac: changes.donViCongTac,
            vn1: changes.vn1,
            ghiChu: changes.ghiChu,
        });
        points.splice(index, 1, updateValue);
        this.setState({ points });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const points = this.state.points;
        points.splice(index, 1);
        this.setState({ points });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiDsNhiemVu1CoiThi(this.state.points, () => {
            T.notify('Cập nhật dữ liệu thành công!', 'success');
            this.props.history.push('/user/summary/ds-coithi-nv1');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, points } = this.state;
        let table = null;
        if (points && points.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: '100%' }}>Họ và tên</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Ngày</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Mã MH</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Môn học</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Thời lượng</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Kỳ thi</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Ghi chú</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {points.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td className='text-info text-right'><a href='#' onClick={e => this.showEdit(e, index, item)}>{item.shcc}</a></td>
                                <td className=''>{item.fullname}</td>
                                <td style={{ width: 'auto', whiteSpace: 'nowrap' }}>{T.dateToText(item.ngay, 'dd/mm/yyyy')}</td>
                                <td style={{ width: 'auto', whiteSpace: 'nowrap' }}>{item.maMonHoc}</td>
                                <td style={{ width: 'auto', whiteSpace: 'nowrap' }}>{item.monHoc}</td>
                                <td className='text-right'>{item.thoiLuong}</td>
                                <td style={{ width: 'auto', whiteSpace: 'nowrap' }}>{item.kyThi}</td>
                                <td>{item.ghiChu}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file cập nhật</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6 offset-md-3'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='DsNhiemVu1CoiThiFile' userData='dsNhiemVu1CoiThiFile' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                                {errorLog ? (
                                    <div className='tile'>
                                        <div className='tile-body'>
                                            {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                            {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/09.ds_coithi_nv1.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    {points && points.length ? (
                        <div className='tile col-12'>
                            {table}
                        </div>
                    ) : null}
                </div>
                <Link to='/user/summary/ds-coithi-nv1' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiDsNhiemVu1CoiThi };
export default connect(mapStateToProps, mapActionsToProps)(DsNhiemVu1CoiThiImportPage);
