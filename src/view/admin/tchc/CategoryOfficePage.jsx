import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getCategoryOfficeInPage, updateCategoryOffice, createCategoryOffice } from '../../redux/categoryOffice.jsx';
import Pagination from '../../common/Pagination.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#publicationViType').focus());
        });
    }

    show = (item) => {
        if (item) {
            ['year', 'maQL', 'tenChucVu', 'heSoPCQL', 'phanNgachCBQL', 'maNhomQuanLy', 'maNhomQuanLyKhongGD', 'tyLeDinhMuc', 'kCM', 'kLD', 'kI', 'cap', 'chucVuDoanThe', 'note'].forEach(key => {
                $('#' + key).val(item[key] ? item[key] : null);
            });
        }
        $(this.modal.current).modal('show');
        this.setState({ _id: item && item._id ? item._id : null });
    };

    save = (e) => {
        e.preventDefault();
        const _id = this.state._id,
            changes = {};
        ['year', 'maQL', 'tenChucVu', 'heSoPCQL', 'phanNgachCBQL', 'maNhomQuanLy', 'maNhomQuanLyKhongGD', 'tyLeDinhMuc', 'kCM', 'kLD', 'kI', 'cap', 'chucVuDoanThe', 'note'].forEach(key => {
            changes[key] = $('#' + key).val() ? $('#' + key).val() : null;
        });
        if (_id) {
            this.props.update(_id, changes, () => {
                $(this.modal.current).modal('hide');
            })
        } else {
            this.props.create(changes, () => {
                $(this.modal.current).modal('hide');
            })
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'> Chỉnh sửa chức vụ</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='maQL'>Mã QL</label>
                                <input className='form-control' id='maQL' type='text' placeholder='Mã QL' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='tenChucVu'>Tên chức vụ</label>
                                <input className='form-control' id='tenChucVu' type='text' placeholder='Tên chức vụ' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='heSoPCQL'>Hệ số PCQL</label>
                                <input className='form-control' id='heSoPCQL' type='text' placeholder='Hệ số PCQL' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='phanNgachCBQL'>Phân ngạch CBQL</label>
                                <input className='form-control' id='phanNgachCBQL' type='text' placeholder='Phân ngạch CBQL' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='maNhomQuanLy'>Mã nhóm quản lý</label>
                                <input className='form-control' id='maNhomQuanLy' type='text' placeholder='Mã nhóm quản lý' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='maNhomQuanLyKhongGD'>Mã nhóm quản lý không GD</label>
                                <input className='form-control' id='maNhomQuanLyKhongGD' type='text' placeholder='Mã nhóm quản lý không GD' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='tyLeDinhMuc'>Tỷ lệ định mức</label>
                                <input className='form-control' id='tyLeDinhMuc' type='number' placeholder='Tỷ lệ định mức' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='kCM'>K<sub>CM</sub></label>
                                <input className='form-control' id='kCM' type='number' step='0.01' placeholder='kCM' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='kLD'>K<sub>LD</sub></label>
                                <input className='form-control' id='kLD' type='number' step='0.01' placeholder='kLD' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='kI'>K<sub>TN</sub></label>
                                <input className='form-control' id='kI' type='number' step='0.01' placeholder='kI' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='cap'>Cấp</label>
                                <input className='form-control' id='cap' type='text' placeholder='Cấp' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='chucVuDoanThe'>Chức vụ đoàn thể</label>
                                <input className='form-control' id='chucVuDoanThe' type='text' placeholder='Chức vụ đoàn thể' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='note'>Ghi chú</label>
                                <input className='form-control' id='note' type='text' placeholder='Ghi chú' />
                            </div>
                            <div className='form-group col-12 col-md-6'>
                                <label htmlFor='year'>Năm</label>
                                <input className='form-control' id='year' type='number' placeholder='Year' defaultValue={2019} readOnly={true} />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button> </div>
                    </div>
                </form>
            </div>
        );
    }
}

class CategoryOfficePage extends React.Component {
    constructor(props) {
        super(props);
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getCategoryOfficeInPage();
        T.ready('/user/category-office');
    }

    create = (e) => {
        this.modal.current.show(null);
        e.preventDefault();
    };

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        T.confirm('Xóa thông tin đề tài', 'Bạn có chắc bạn muốn xóa Cấp quản lý này?', true, isConfirm =>
            isConfirm && this.props.deletePublicationType(item._id, () => this.props.getAllPublicationTypes()));
        e.preventDefault();
    };

    render() {
        const { pageNumber, pageSize, pageTotal, totalItem, list } = this.props.categoryOffice && this.props.categoryOffice.page ?
            this.props.categoryOffice.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, list: [] };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Mã QL</th>
                            <th style={{ width: '100%' }}>Tên chức vụ</th>
                            <th style={{ width: 'auto' }}>K<sub>CM</sub></th>
                            <th style={{ width: 'auto' }}>K<sub>LĐ</sub></th>
                            <th style={{ width: 'auto' }}>K<sub>TN</sub></th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td>{item.maQL}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.tenChucVu}</a>
                                </td>
                                <td>{item.kCM}</td>
                                <td>{item.kLD}</td>
                                <td>{item.kI}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có thông tin!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-newspaper-o' /> Danh mục chức vụ</h1>
                </div>

                <div className='row tile'>{table}</div>

                <Pagination name='pageCategoryOffice' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} getPage={this.props.getCategoryOfficeInPage} />

                <Link to='/user/category-office/upload-point' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <EditModal ref={this.modal} create={this.props.createCategoryOffice} update={this.props.updateCategoryOffice} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ categoryOffice: state.categoryOffice });
const mapActionsToProps = { getCategoryOfficeInPage, updateCategoryOffice, createCategoryOffice };
export default connect(mapStateToProps, mapActionsToProps)(CategoryOfficePage);
