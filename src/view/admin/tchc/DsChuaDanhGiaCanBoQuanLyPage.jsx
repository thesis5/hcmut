import React from 'react';
import { connect } from 'react-redux';
import { getPointChuaDGCBQLInPage, createChuaDGCBQL, updatePoint, deleteChuaDGCBQL } from '../../redux/dsChuaDGCBQL.jsx'
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    nam: { type: 'number', title: 'Năm' },
    shcc: { type: 'text', title: 'SHCC' },
    hoten: { type: 'text', title: 'Họ và tên' },
    chucVu: { type: 'text', title: 'Chức vụ' },
    tenBoMon: { type: 'text', title: 'Tên bộ môn' },
    tenKhoa: { type: 'text', title: 'Tên khoa' },
    hoTenCanBoDuocDanhGia: { type: 'text', title: 'Họ tên cán bộ được đánh giá' },
    chucVuCanBoDuocDanhGia: { type: 'text', title: 'Chức vụ cán bộ được đánh giá' },
    ghiChu: { type: 'text', title: 'Ghi chú' },
    soLanChuaDanhGia: { type: 'number', title: 'Số lần chưa đánh giá' },
    diemTru: { type: 'number', title: 'Điểm trừ' },
    loai: { type: 'text', title: 'Loại' },
    nghi: { type: 'text', title: 'Nghỉ' },
    diNN: { type: 'text', title: 'Đi NN' }
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null, isUpdate: false };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null })
            });
        })
    }

    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val(item[key] ? item[key] : null);
            });
            this.state.isUpdate = true;
        }
        this.setState({ _id: item && item._id ? item._id : null });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        if (this.state.isUpdate) {
            this.props.updatePoint(this.state._id, changes, () => {
                T.notify('Cập nhật nội dung thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            this.props.createChuaDGCBQL(changes, () => {
                T.notify('Tạo nội dung thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật dữ liệu</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{schema[key].title}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step}
                                        placeholder={schema[key].title} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsChuaDanhGiaCanBoQuanLyPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getPointChuaDGCBQLInPage(1, 50, {});
        T.ready();
    }

    create = (e) => {
        this.modal.current.show();
        e.preventDefault();
    };

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };
    delete = (e, item) => {
        T.confirm('Chưa đánh giá cán bộ quản lý', 'Bạn có chắc bạn muốn xóa nội dung này?', 'warning', true, isConfirm =>
            isConfirm && this.props.deleteChuaDGCBQL(item._id));
        e.preventDefault();
    };

    search = (e) => {
        e.preventDefault();
        const condition = {};
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value }
            ]
        }
        this.props.getPointChuaDGCBQLInPage(undefined, undefined, (condition), () => this.setState({ isSearching: value != '' }));
    };
    render() {
        let { pageNumber, pageSize, pageTotal, totalItem, pageCondition, list } = this.props.dsChuaDGCBQL && this.props.dsChuaDGCBQL.page ?
            this.props.dsChuaDGCBQL.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, pageCondition: {}, list };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: 'auto' }}>Năm</th>
                            <th style={{ width: '100%' }}>Họ tên tác giả</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm trừ</th>
                            <th style={{ width: 'auto' }}>Loại</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td><a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a></td>
                                <td>{item.nam}</td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.hoten}</td>
                                <td style={{ textAlign: 'right', color: 'red' }}>{item.diemTru}</td>
                                <td>{item.loai}</td>
                                <td>
                                    <div className='' style={{ display: 'flex' }}>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> DS chưa đánh giá cán bộ quản lý</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='dsChuaDGCBQLPointPage' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} pageCondition={pageCondition}
                    getPage={this.props.getPointChuaDGCBQLInPage} />

                <Link to='/user/summary/ds-chua-dg-cbql/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <a href='#' className='btn btn-primary btn-circle' onClick={e => this.create(e, null)} style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-plus' />
                </a>
                <EditModal ref={this.modal} createChuaDGCBQL={this.props.createChuaDGCBQL} updatePoint={this.props.updatePoint} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsChuaDGCBQL: state.dsChuaDGCBQL });
const mapActionsToProps = { getPointChuaDGCBQLInPage, createChuaDGCBQL, updatePoint, deleteChuaDGCBQL };
export default connect(mapStateToProps, mapActionsToProps)(DsChuaDanhGiaCanBoQuanLyPage);
