import React from 'react';
import { connect } from 'react-redux';
import { dsDiemThuongInPage, updateDsDiemThuong, createDsDiemThuong, deleteDsDiemThuong } from '../../redux/dsDiemThuong.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    shcc: { type: 'text', title: 'SHCC' },
    hoTen: { type: 'text', title: 'Họ và Tên' },
    diemThuong: { type: 'number', title: 'Điểm thưởng' },
    boNhiemGsPgs: { type: 'text', title: 'Bổ nhiệm GS/PGS' },
    nhanBangTn: { type: 'text', title: 'Nhận bằng tốt nghiệp' },
    khenThuong: { type: 'text', title: 'Khen thưởng' },
    nam: { type: 'number', title: 'Năm' },
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null, isUpdate: true };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null, isUpdate: true })
            });
        })
    }

    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val(item[key] ? item[key] : null);
            });
            this.setState({
                _id: item && item._id ? item._id : null,
                isUpdate: true
            });
        } else {
            Object.keys(schema).forEach(key => $(this[key].current).val(''));
            this.setState({ _id: null, isUpdate: false });
        }

        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        if (this.state.isUpdate) {
            this.props.update(this.state._id, changes, () => {
                T.notify('Cập nhật điểm thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            this.props.create(changes, () => {
                T.notify('Tạo mới thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{this.state.isUpdate ? 'Cập nhật' : 'Tạo mới'}</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{schema[key].title}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step} placeholder={schema[key].title} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsDiemThuongPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isSearching: false };
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.dsDiemThuongInPage(1, 50, JSON.stringify({ nam: new Date().getFullYear() }));
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        T.confirm('Xóa', 'Bạn có chắc bạn muốn xóa điểm thưởng này?', true, isConfirm => isConfirm && this.props.deleteDsDiemThuong(item._id));
        e.preventDefault();
    };

    search = (e) => {
        e.preventDefault();
        const condition = { '$and': [{ nam: new Date().getFullYear() }] };
        let value = $('#searchTextBox').val();
        if (value) {
            value = { '$regex': `.*${value}.*`, '$options': 'i' };
            condition['$and'].push({
                '$or': [
                    { shcc: value },
                    { hoTen: value },
                ]
            });
        }
        this.props.dsDiemThuongInPage(undefined, undefined, JSON.stringify(condition), () => this.setState({ isSearching: value != '' }));
    };

    render() {
        let { pageNumber, pageSize, pageTotal, totalItem, list } = this.props.dsDiemThuong && this.props.dsDiemThuong.page ?
            this.props.dsDiemThuong.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, list };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: '100%' }}>Họ và Tên</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm thưởng</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td><a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a></td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.hoTen}</td>
                                <td style={{ whiteSpace: 'nowrap', textAlign: 'right' }}>{item.diemThuong}</td>
                                <td>
                                    <div className='btn-group' style={{ display: 'flex' }}>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> DS Điểm thưởng</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='dsDiemThuongPage' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.dsDiemThuongInPage} />
                <Link to='/user/summary/ds-diem-thuong/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={e => this.edit(e, null)}>
                    <i className='fa fa-lg fa-plus' />
                </button>
                <EditModal ref={this.modal} update={this.props.updateDsDiemThuong} create={this.props.createDsDiemThuong} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsDiemThuong: state.dsDiemThuong });
const mapActionsToProps = { dsDiemThuongInPage, updateDsDiemThuong, createDsDiemThuong, deleteDsDiemThuong };
export default connect(mapStateToProps, mapActionsToProps)(DsDiemThuongPage);
