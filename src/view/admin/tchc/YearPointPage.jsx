import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getYearPointInPage, updateYearPoint, calculateYearPoint } from '../../redux/yearPoint.jsx'
import Pagination from '../../common/Pagination.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null, quotaName: '' };
        this.modal = React.createRef();
    }

    show = (yearPoint) => {
        let { leadershipCoefficient, professionalCoefficient, leadershipPoint, professionalPoint, professionalPoint1, professionalPoint2, professionalPoint3, rewardPoint, penaltyPoint, ratedRatio, additionalIncomeCoefficient, round1Point, round2Point, quotaPoint1, quotaPoint2, quotaPoint3, point, active, note } = yearPoint && yearPoint ?
            yearPoint : { leadershipCoefficient: 0, professionalCoefficient: 0, leadershipPoint: 0, professionalPoint: 0, professionalPoint1: 0, professionalPoint2: 0, professionalPoint3: 0, rewardPoint: 0, penaltyPoint: 0, ratedRatio: 0, additionalIncomeCoefficient: 0, round1Point: 0, round2Point: 0, quotaPoint1: 0, quotaPoint2: 0, quotaPoint3: 0, point: 0, active: false, note: '' };

        professionalPoint1 = professionalPoint1 ? professionalPoint1 : 0;
        professionalPoint2 = professionalPoint2 ? professionalPoint2 : 0;
        professionalPoint3 = professionalPoint3 ? professionalPoint3 : 0;
        if (leadershipCoefficient == null || professionalCoefficient == null) {
            leadershipCoefficient = 0;
            professionalCoefficient = 1;
        }

        $('#leadershipCoefficient').val(leadershipCoefficient.toFixed(2));
        $('#professionalCoefficient').val(professionalCoefficient.toFixed(2));
        $('#leadershipPoint').val(leadershipPoint);
        $('#round1Point').val(round1Point ? round1Point.toFixed(2) : 0);
        $('#round2Point').val(round2Point ? round2Point.toFixed(2) : 0);
        $('#quotaPoint1').val(quotaPoint1.toFixed(2));
        $('#quotaPoint2').val(quotaPoint2.toFixed(2));
        $('#quotaPoint3').val(quotaPoint3.toFixed(2));
        $('#professionalPoint1').val(professionalPoint1.toFixed(2));
        $('#professionalPoint2').val(professionalPoint2.toFixed(2));
        $('#professionalPoint3').val(professionalPoint3.toFixed(2));
        $('#professionalPoint').val(professionalPoint.toFixed(2));
        $('#rewardPoint').val(rewardPoint);
        $('#penaltyPoint').val(penaltyPoint);
        $('#ratedRatio').val(ratedRatio);
        $('#additionalIncomeCoefficient').val(additionalIncomeCoefficient);
        $('#yearPointActive').prop('checked', active);
        $('#point').val(point && !isNaN(point) ? point.toFixed(2) : 0);
        $('#note').val(note);

        this.setState({ _id: yearPoint && yearPoint._id ? yearPoint._id : null, quotaName: yearPoint.quotaName });
        $(this.modal.current).modal('show');
    }

    hide = () => {
        $(this.modal.current).modal('hide');
    }

    save = (event) => {
        const changes = {
            leadershipCoefficient: Number($('#leadershipCoefficient').val()),
            professionalCoefficient: Number($('#professionalCoefficient').val()),
            leadershipPoint: Number($('#leadershipPoint').val()),
            round1Point: Number($('#round1Point').val()),
            round2Point: Number($('#round2Point').val()),
            quotaPoint1: Number($('#quotaPoint1').val()),
            quotaPoint2: Number($('#quotaPoint2').val()),
            quotaPoint3: Number($('#quotaPoint3').val()),
            professionalPoint1: Number($('#professionalPoint1').val()),
            professionalPoint2: Number($('#professionalPoint2').val()),
            professionalPoint3: Number($('#professionalPoint3').val()),
            professionalPoint: Number($('#professionalPoint').val()),
            rewardPoint: Number($('#rewardPoint').val()),
            penaltyPoint: Number($('#penaltyPoint').val()),
            ratedRatio: Number($('#ratedRatio').val()),
            additionalIncomeCoefficient: Number($('#additionalIncomeCoefficient').val()),
            active: $('#yearPointActive').prop('checked'),
            point: Number($('#point').val()),
            note: $('#note').val(),
        };
        if (isNaN(changes.rewardPoint)) {
            T.notify('Điểm thưởng không chính xác!', 'danger');
            $('#rewardPoint').focus();
        } else if (isNaN(changes.penaltyPoint)) {
            T.notify('Điểm trừ không chính xác!', 'danger');
            $('#penaltyPoint').focus();
        } else if (isNaN(changes.leadershipCoefficient)) {
            T.notify('Hệ số lãnh đạo không chính xác!', 'danger');
            $('#leadershipCoefficient').focus();
        } else if (isNaN(changes.professionalCoefficient)) {
            T.notify('Hệ số chuyên môn không chính xác!', 'danger');
            $('#professionalCoefficient').focus();
        } else if (isNaN(changes.leadershipPoint)) {
            T.notify('Điểm lãnh đạo không chính xác!', 'danger');
            $('#leadershipPoint').focus();
        } else if (isNaN(changes.professionalPoint)) {
            T.notify('Điểm chuyên môn không chính xác!', 'danger');
            $('#professionalPoint').focus();
        } else if (isNaN(changes.professionalPoint1)) {
            T.notify('Điểm NV1 không chính xác!', 'danger');
            $('#professionalPoint1').focus();
        } else if (isNaN(changes.professionalPoint2)) {
            T.notify('Điểm NV2 không chính xác!', 'danger');
            $('#professionalPoint2').focus();
        } else if (isNaN(changes.professionalPoint3)) {
            T.notify('Điểm NV3 không chính xác!', 'danger');
            $('#professionalPoint3').focus();
        } else if (isNaN(changes.ratedRatio)) {
            T.notify('Hệ số chuyện môn không chính xác!', 'danger');
            $('#ratedRatio').focus();
        } else if (isNaN(changes.additionalIncomeCoefficient)) {
            T.notify('Hệ số thu nhập tăng thêm không chính xác!', 'danger');
            $('#additionalIncomeCoefficient').focus();
        } else if (!this.props.readOnly) {
            this.props.updateYearPoint(this.state._id, changes, () => {
                T.notify('Cập nhật điểm thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
        event.preventDefault();
    }

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <div>
                                <h5 className='modal-title'>Chỉnh sửa điểm tổng kết năm</h5>
                            </div>
                            <div className='form-group' style={{ display: 'inline-flex', margin: 0, position: 'absolute', right: '20px' }}>
                                <label htmlFor='yearPointActive'>Kích hoạt: </label>&nbsp;&nbsp;
                                        <div className='toggle'>
                                    <label>
                                        <input type='checkbox' id='yearPointActive' onChange={() => { }} /><span className='button-indecator' />
                                    </label>
                                </div>
                            </div>
                            {/* <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button> */}
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-6'>
                                    <div className='form-group'>
                                        <label htmlFor='round1Point'>Điểm đánh giá đợt 1</label>
                                        <input className='form-control' id='round1Point' type='number' placeholder='Điểm đánh giá đợt 1' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6'>
                                    <div className='form-group'>
                                        <label htmlFor='round2Point'>Điểm đánh giá đợt 2</label>
                                        <input className='form-control' id='round2Point' type='number' placeholder='Điểm đánh giá đợt 2' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-4'>
                                    <div className='form-group'>
                                        <label htmlFor='quotaPoint1'>Định mức NV1</label>
                                        <input className='form-control' id='quotaPoint1' type='number' placeholder='Định mức NV1' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-4'>
                                    <div className='form-group'>
                                        <label htmlFor='quotaPoint2'>Định mức NV2</label>
                                        <input className='form-control' id='quotaPoint2' type='number' placeholder='Định mức NV2' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-4'>
                                    <div className='form-group'>
                                        <label htmlFor='quotaPoint3'>Định mức NV3</label>
                                        <input className='form-control' id='quotaPoint3' type='number' placeholder='Định mức NV3' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-4'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint1'>Điểm NV1</label>
                                        <input className='form-control' id='professionalPoint1' type='number' placeholder='Điểm NV1' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-4'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint2'>Điểm NV2</label>
                                        <input className='form-control' id='professionalPoint2' type='number' placeholder='Điểm NV2' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-4'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint3'>Điểm NV3</label>
                                        <input className='form-control' id='professionalPoint3' type='number' placeholder='Điểm NV3' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint'>Điểm chuyên môn</label>
                                        <input className='form-control' id='professionalPoint' type='number' placeholder='Điểm chuyên môn' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='leadershipPoint'>Điểm lãnh đạo</label>
                                        <input className='form-control' id='leadershipPoint' type='number' placeholder='Điểm lãnh đạo' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='rewardPoint' className='text-primary'>Điểm thưởng</label>
                                        <input className='form-control text-primary' id='rewardPoint' type='number' placeholder='Điểm thưởng' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='penaltyPoint' className='text-danger'>Điểm trừ</label>
                                        <input className='form-control text-danger' id='penaltyPoint' type='number' placeholder='Điểm trừ' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalCoefficient'>Hệ số chuyên môn</label>
                                        <input className='form-control' id='professionalCoefficient' type='number' placeholder='Hệ số chuyện môn' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='leadershipCoefficient'>Hệ số lãnh đạo</label>
                                        <input className='form-control' id='leadershipCoefficient' type='number' placeholder='Hệ số lãnh đạo' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='ratedRatio'>Tỷ số định mức</label>
                                        <input className='form-control' id='ratedRatio' type='number' placeholder='Tỷ số định mức' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-6 col-md-3'>
                                    <div className='form-group'>
                                        <label htmlFor='point' className='text-success'>Điểm tổng kết</label>
                                        <input className='form-control text-success' id='point' type='number' placeholder='Điểm tổng kết' />
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <div className='form-group'>
                                        <label htmlFor='additionalIncomeCoefficient' className='text-warning'>Hệ số thu nhập tăng thêm Ki</label>
                                        <input className='form-control text-warning' id='additionalIncomeCoefficient' type='number' placeholder='Hệ số thu nhập tăng thêm Ki' />
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <div className='form-group'>
                                        <label htmlFor='note'>Ghi chú</label>
                                        <textarea className='form-control col-12' id='note' placeholder='Ghi chú' rows={4} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            {this.props.readOnly ? null : <button type='button' className='btn btn-primary' onClick={(e) => this.save(e)}>Lưu</button>}
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class YearPointPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isSearching: false };
        this.modal = React.createRef();
    }

    componentDidMount() {
        const condition = { '$and': [{ year: new Date().getFullYear() }] };
        this.props.getYearPointInPage(null, null, JSON.stringify(condition));
        T.ready();
    }

    search = (e) => {
        e.preventDefault();
        let condition = {},
            searchText = $('#searchTextBox').val();
        if (searchText) condition.searchText = searchText;

        this.props.getYearPointInPage(undefined, undefined, condition, () => {
            const isSearching = Object.keys(condition).length > 0;
            this.setState({ searchText, isSearching });
        });
    };

    edit = (e, item) => {
        e.preventDefault();
        this.modal.current.show(item);
    }

    downloadPoint = e => {
        e.preventDefault();
        T.download('/api/year-point/export', 'Diem2019.xlsx');
    }

    calculatePoint = e => {
        e.preventDefault();
        this.props.calculateYearPoint();
    }

    getLevel = item => {
        if (item.discipline) {
            return item.result;
        } else if (item.isException) {
            return item.result;
        } else if (item.active == false) {
            return item.currentLeadershipName ? 'Chưa có điểm đánh giá' : 'Không đánh giá';
        } else if (item.point == null) {
            return '';
        } else if (95 <= item.point && (item.transformationType == null || item.transformationType == 'c')) {
            return 'A. Hoàn thành xuất sắc nhiệm vụ';
        } else if (80 <= item.point) {
            return 'B. Hoàn thành tốt nhiệm vụ';
        } else if (65 <= item.point) {
            return 'C. Hoàn thành nhiệm vụ';
        } else {
            return 'D. Không hoàn thành nhiệm vụ';
        }
    }

    render() {
        const currentPermissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            readOnly = !currentPermissions.includes('point:write');
        let { pageNumber, pageSize, pageTotal, pageCondition, totalItem } = this.props.yearPoint && this.props.yearPoint.page ?
            this.props.yearPoint.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, pageCondition: {}, totalItem: 0 };
        let table = null;
        if (this.props.yearPoint && this.props.yearPoint.page && this.props.yearPoint.page.list && this.props.yearPoint.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>MSCB</th>
                            <th style={{ width: '100%' }}>Họ tên</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm chuyên môn</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm lãnh đạo</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm thưởng </th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm trừ </th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm tổng kết </th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Mức</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.yearPoint.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a>
                                </td>
                                <td key={0} style={{ borderRight: '0px solid #fff' }}>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.user ? item.user.lastname + ' ' + item.user.firstname : null}</a><br />
                                    {item.user ? item.user.email : null}
                                </td>
                                <td className='text-right text-info'>{item && item.professionalPoint && !isNaN(item.professionalPoint) ? item.professionalPoint.toFixed(2) : 0}</td>
                                <td className='text-right '>{item && item.leadershipCoefficient ? item.leadershipPoint : null}</td>
                                <td className='text-right text-primary'>{item && item.rewardPoint ? item.rewardPoint : null}</td>
                                <td className='text-right text-danger'>{item && item.penaltyPoint ? item.penaltyPoint : null}</td>
                                <td className='text-right text-info'>{item && item.point && !isNaN(item.point) ? item.point.toFixed(2) : 0}</td>
                                <td style={{ whiteSpace: 'nowrap' }}>{item.resultBySystem ? item.resultBySystem : ''}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có đánh giá!</p>;
        }
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tổng kết điểm năm 2019</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>

                <div className='row tile'>{table}</div>
                <Pagination name='yearPoint' pageCondition={pageCondition} pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getYearPointInPage} />
                <a href='#' onClick={this.downloadPoint} className='btn btn-info btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-download' />
                </a>
                {readOnly ? null :
                    <a href='#' onClick={this.calculatePoint} className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                        <i className='fa fa-lg fa-gear' />
                    </a>}
                <EditModal ref={this.modal} updateYearPoint={this.props.updateYearPoint} readOnly={readOnly} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, user: state.user, yearPoint: state.yearPoint });
const mapActionsToProps = { getYearPointInPage: getYearPointInPage, updateYearPoint, calculateYearPoint };
export default connect(mapStateToProps, mapActionsToProps)(YearPointPage);
