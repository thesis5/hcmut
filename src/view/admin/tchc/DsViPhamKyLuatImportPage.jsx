import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { updateMultiValues } from '../../redux/dsViPhamKyLuat.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#dsvpklShcc').focus());
            $(this.modal.current).on('hidden.bs.modal', () => this.setState({ index: -1 }));
        })
    }

    show = (index, item) => {
        const { shcc, nam, ghiChu } = item ? item : { shcc: 0, nam: 2019, ghiChu: '' };
        $('#dsvpklShcc').val(shcc);
        $('#dsvpklNam').val(nam);
        $('#dsvpklGhiChu').val(ghiChu);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            shcc: $('#dsvpklShcc').val(),
            nam: parseInt($('#dsvpklNam').val()),
            ghiChu: $('#dsvpklGhiChu').val(),
        };

        if (changes.shcc == '') {
            T.notify('Số hiệu công chức trống!', 'danger');
            $('#dsvpklShcc').focus();
        } else if (changes.dsvpklNam == '') {
            T.notify('Điểm phạt không chính xác!', 'danger');
            $('#dsvpklNam').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật Vi phạm kỷ luật</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='dsvpklShcc'>SHCC</label>
                                <input className='form-control' id='dsvpklShcc' type='text' placeholder='SHCC' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dsvpklNam'>Năm</label>
                                <input className='form-control' id='dsvpklNam' type='number' placeholder='Năm' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dsvpklGhiChu'>Ghi chú</label>
                                <textarea className='form-control' id='dsvpklGhiChu' placeholder='Ghi chú' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsViPhamKyLuatImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/ds_vi_pham_ky_luat');
    }

    onSuccess = (response) => {
        if (response.numberOfErrors > 0 || response.warnings.length > 0) {
            this.setState({
                errorLog: true,
                numberOfErrors: response.numberOfErrors,
                warnings: response.warnings
            });
        }
        if (response.items.length > 0) {
            this.setState({
                items: response.items,
                message: <p className='text-center' style={{ color: 'green' }}>{response.items.length} hàng được tải lên thành công</p>
            });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const items = this.state.items,
            currentValue = items[index];
        const updateValue = Object.assign({}, currentValue, { shcc: changes.shcc, nam: changes.nam, ghiChu: changes.ghiChu });
        items.splice(index, 1, updateValue);
        this.setState({ items });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        T.confirm('Xóa phần tử', 'Bạn có chắc bạn muốn xóa phần tử này?', true, isConfirm => {
            if (isConfirm) {
                const items = this.state.items;
                items.splice(index, 1);
                this.setState({ items });
            }
        });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiValues(this.state.items, () => {
            T.notify('Cập nhật dữ liệu thành công!', 'success');
            this.props.history.push('/user/summary/ds_vi_pham_ky_luat');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, items } = this.state;
        let table = null;
        if (items && items.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: 'auto' }}>Năm</th>
                            <th style={{ width: '100%' }}>Ghi chú</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {items.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.showEdit(e, index, item)}>{item.shcc}</a>
                                </td>
                                <td style={{ textAlign: 'right' }}>{item.nam}</td>
                                <td>{item.ghiChu}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file cập nhật Vi phạm kỷ luật</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6 offset-md-3'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='DsViPhamKyLuatFile' userData='DsViPhamKyLuatImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                                {errorLog ? (
                                    <div>
                                        {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                        {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                    </div>
                                ) : null}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/10.ds_vi_pham_ky_luat.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    {items && items.length ? <div className='tile col-12'>{table}</div> : null}
                </div>
                <Link to='/user/summary/ds_vi_pham_ky_luat' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiValues };
export default connect(mapStateToProps, mapActionsToProps)(DsViPhamKyLuatImportPage);