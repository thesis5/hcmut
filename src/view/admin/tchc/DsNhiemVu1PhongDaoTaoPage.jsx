import React from 'react';
import { connect } from 'react-redux';
import { getDsNv1PdtInPage, updateDsNv1Pdt, deleteDsNv1Pdt, createDsNv1Pdt } from '../../redux/dsNhiemVu1PhongDaoTao.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    shcc: { type: 'text', title: 'SHCC' },
    ma_bm: { type: 'text', title: 'Mã BM' },
    ma_kh: { type: 'text', title: 'Mã KH' },
    cq182: { type: 'number', title: 'CQ 182' },
    cq183: { type: 'number', title: 'CQ 183' },
    cq191: { type: 'number', title: 'CQ 191' },
    gvcn182: { type: 'number', title: 'GVCN 182' },
    gvcn191: { type: 'number', title: 'GVCN 191' },
    dttx182: { type: 'number', title: 'DTTX 182' },
    dttx183: { type: 'number', title: 'DTTX 183' },
    dttx191: { type: 'number', title: 'DTTX 191' },
    qtcn182: { type: 'number', title: 'QTCN 182' },
    qtcn183: { type: 'number', title: 'QTCN 183' },
    qtcn191: { type: 'number', title: 'QTCN 191' },
    qt182: { type: 'number', title: 'QT 182' },
    qt183: { type: 'number', title: 'QT 183' },
    qt191: { type: 'number', title: 'QT 191' },
    tonggiochuan: { type: 'number', title: 'Tổng giờ chuẩn' },
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null, isUpdate: true };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null, isUpdate: true })
            });
        })
    }

    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => $(this[key].current).val(item[key] ? item[key] : null));
            this.setState({
                _id: item && item._id ? item._id : null,
                isUpdate: true
            });
        }
        else {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val('');
            });
            this.setState({
                _id: null,
                isUpdate: false
            });
        }
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        if (this.state.isUpdate) {
            this.props.updateDsNv1Pdt(this.state._id, changes, () => {
                T.notify('Cập nhật thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            this.props.createDsNv1Pdt(changes, () => {
                T.notify('Tạo mới thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{this.state.isUpdate ? 'Cập nhật' : 'Tạo mới'}</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{schema[key].title}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step} placeholder={schema[key].title} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsNhiemVu1PhongDaoTaoPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isSearching: false };
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getDsNv1PdtInPage(1, 50, {});
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        T.confirm('Xóa', 'Bạn có chắc bạn muốn xóa hàng này?', true, isConfirm => isConfirm && this.props.deleteDsNv1Pdt(item._id));
        e.preventDefault();
    };

    create = (e) => {
        this.modal.current.show();
        e.preventDefault();
    };

    search = (e) => {
        e.preventDefault();
        const condition = {};
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value }
            ]
        }
        this.props.getDsNv1PdtInPage(undefined, undefined, (condition), () => this.setState({ isSearching: value != '' }));
    };
    render() {
        let { pageNumber, pageSize, pageTotal, totalItem, pageCondition, list } = this.props.dsNv1Pdt && this.props.dsNv1Pdt.page ?
            this.props.dsNv1Pdt.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, pageCondition: {}, totalItem: 0, list };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>SHCC</th>
                            <th style={{ width: '25%', whiteSpace: 'nowrap', textAlign: 'center' }}>Chính quy 182</th>
                            <th style={{ width: '25%', whiteSpace: 'nowrap', textAlign: 'center' }}>Chính quy 183</th>
                            <th style={{ width: '25%', whiteSpace: 'nowrap', textAlign: 'center' }}>Chính quy 191</th>
                            <th style={{ width: '25%', whiteSpace: 'nowrap', textAlign: 'center' }}>Tổng giờ chuẩn</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td style={{ width: 'auto' }}>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a>
                                </td>
                                <td style={{ width: '25%', textAlign: 'right' }}>{item.cq182}</td>
                                <td style={{ width: '25%', textAlign: 'right' }}>{item.cq183}</td>
                                <td style={{ width: '25%', textAlign: 'right' }}>{item.cq191}</td>
                                <td style={{ width: '25%', textAlign: 'right' }}>{item.tonggiochuan}</td>
                                <td style={{ width: 'auto' }}>
                                    <div className='btn-group' style={{ display: 'flex' }}>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> DS Nhiệm vụ 1 - phòng Đào tạo</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='dsNv1PdtPage' pageCondition={pageCondition} pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getDsNv1PdtInPage} />
                <Link to='/user/summary/ds-nhiem-vu-1-phong-dao-tao/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>
                <EditModal ref={this.modal} updateDsNv1Pdt={this.props.updateDsNv1Pdt} createDsNv1Pdt={this.props.createDsNv1Pdt} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsNv1Pdt: state.dsNv1Pdt });
const mapActionsToProps = { getDsNv1PdtInPage, updateDsNv1Pdt, deleteDsNv1Pdt, createDsNv1Pdt };
export default connect(mapStateToProps, mapActionsToProps)(DsNhiemVu1PhongDaoTaoPage);