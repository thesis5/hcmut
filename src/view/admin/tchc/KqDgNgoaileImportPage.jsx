import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { updateMultiKqDgNgoaile } from '../../redux/kq_dg_ngoaile.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('show.bs.modal', () => {
                $('#officerId').focus();
            });

            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ index: -1 })
            });
        })
    }

    show = (index, item) => {
        const { officerId, lastName, firstName, classification, kI } = item ?
            item : { officerId: '', lastName: '', firstName: '', classification: '', kI: 0 };
        $('#officerId').val(officerId);
        $('#lastName').val(lastName);
        $('#firstName').val(firstName);
        $('#classification').val(classification);
        $('#kI').val(kI);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            officerId: ($('#officerId').val()),
            firstName: ($('#firstName').val()),
            lastName: ($('#lastName').val()),
            classification: ($('#classification').val()),
            kI: ($('#kI').val()),
        };

        if (changes.officerId === '') {
            T.notify('Số hiệu công chức bị trống!', 'danger');
            $('#officerId').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật </h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='officerId'>Số hiệu công chức</label>
                                        <input className='form-control' id='officerId' type='text'
                                            placeholder='Số hiệu công chức' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='lastName'>Họ</label>
                                        <input className='form-control' id='lastName' type='text'
                                            placeholder='Ghi chú' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='firstName'>Tên</label>
                                        <input className='form-control' id='firstName' type='text'
                                            placeholder='Ghi chú' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='classification'>Xếp loại</label>
                                        <input className='form-control' id='classification' type='text'
                                            placeholder='Xếp loại' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='kI'>K_I</label>
                                        <input className='form-control' id='kI' type='number'
                                            placeholder='K_I' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class KqDgNgoaiLeImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/kq-dg-ngoaile');
    }

    onSuccess = (response) => {
        if (response.numberOfErrors > 0 || response.warnings.length > 0) {
            this.setState({ errorLog: true, numberOfErrors: response.numberOfErrors, warnings: response.warnings });
        }
        if (response.officers.length > 0) {
            this.setState({ officer: response.officers, message: <p className='text-center' style={{ color: 'green' }}>{response.officers.length} hàng được tải lên thành công</p> });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const officer = this.state.officer, currentValue = officer[index];
        const updateValue = Object.assign({}, currentValue, {
            classification: changes.classification,
            officerId: changes.officerId,
            firstName: changes.firstName,
            lastName: changes.lastName,
            kI: changes.kI
        });
        officer.splice(index, 1, updateValue);
        this.setState({ officer });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const officer = this.state.officer;
        officer.splice(index, 1);
        this.setState({ officer });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiKqDgNgoaile(this.state.officer, () => {
            T.notify('Cập nhật thành công!', 'success');
            this.props.history.push('/user/summary/kq-dg-ngoaile');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, officer } = this.state;
        let table = null;
        if (officer && officer.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '20%' }}>Số hiệu công chức</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Họ</th>
                            <th style={{ width: '20%' }} nowrap='true'>Tên</th>
                            <th style={{ width: '20%' }} nowrap='true'>Xếp loại</th>
                            <th style={{ width: '20%' }} nowrap='true'>K_I</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {officer.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td className='text-info text-right'>{item.officerId}</td>
                                <td className='text-info text-right'>{item.lastName}</td>
                                <td className='text-info text-right'>{item.firstName}</td>
                                <td className='text-info text-right'>{item.classification}</td>
                                <td className='text-info text-right'>{item.kI}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file cập nhật</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='kq_dg_ngoaileImportFile' userData='kq_dg_ngoaileImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/SampleUploadKqDgNgoaiLe.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                    <div className='col-6 col-md-6'>
                        {errorLog ? (
                            <div className='tile'>
                                <div className='tile-body'>
                                    {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                    {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                </div>
                            </div>
                        ) : null}
                    </div>
                </div>
                <div className='row'>
                    {officer && officer.length ? (
                        <div className='tile col-12'>
                            {table}
                        </div>
                    ) : null}
                </div>
                <Link to='/user/summary/kq-dg-ngoaile' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiKqDgNgoaile };
export default connect(mapStateToProps, mapActionsToProps)(KqDgNgoaiLeImportPage);
