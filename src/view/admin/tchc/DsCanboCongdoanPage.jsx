import React from 'react';
import { connect } from 'react-redux';
import { getDsCanboCongdoanInPage, createCBCD, updateCBCD, deleteCBCD } from '../../redux/dsCanboCongdoan.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const width = (100 / 6) + '%';
const schema = {
    shcc: { type: 'text', title: 'SHCC' },
    maQL: { type: 'text', title: 'Mã quản lý' },
    chucVu: { type: 'text', title: 'Chức vụ' },
    toCDBM: { type: 'text', title: 'Tổ CĐ BM' },
    ghiChu: { type: 'text', title: 'Ghi chú' },
    nam: { type: 'number', title: 'Năm' }
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null, isUpdate: false })
            });
        })
    }

    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val(item[key] ? item[key] : null);
            });
            this.state.isUpdate = true;
        }
        this.setState({ _id: item && item._id ? item._id : null });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        if (this.state.isUpdate) {
            this.props.updateCBCD(this.state._id, changes, () => {
                T.notify('Cập nhật thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            this.props.createCBCD(changes, () => {
                T.notify('Tạo cán bộ công đoàn thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật danh sách cán bộ công đoàn</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{schema[key].title}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step}
                                        placeholder={schema[key].title} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsCanboCongdoanPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getDsCanboCongdoanInPage(1, 50, {});
        T.ready();
    }

    create = (e) => {
        this.modal.current.show();
        e.preventDefault();
    };

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        T.confirm('Cán bộ công chức', 'Bạn có chắc bạn muốn xóa cán bộ công chức này?', 'warning', true, isConfirm =>
            isConfirm && this.props.deleteCBCD(item._id));
        e.preventDefault();
    };

    search = (e) => {
        e.preventDefault();
        const condition = {};
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value }
            ]
        }
        this.props.getDsCanboCongdoanInPage(undefined, undefined, (condition), () => this.setState({ isSearching: value != '' }));
    };
    render() {
        let { pageNumber, pageSize, pageTotal, totalItem, pageCondition, list } = this.props.dsCanboCongdoan && this.props.dsCanboCongdoan.page ?
            this.props.dsCanboCongdoan.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, pageCondition: {}, list };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            {Object.keys(schema).map((key, index) => (<th key={index} style={{ width: width, whiteSpace: 'nowrap' }}>{schema[key].title}</th>))}
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                {Object.keys(schema).map((key, i) =>
                                    (<td key={i} style={{ width: width }}>{key == 'shcc' ? <a href='#' onClick={e => this.edit(e, item)}>{item[key]}</a> : item[key]}</td>)
                                )}
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> DS Cán bộ công đoàn</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>
                <div className='row tile'>{table}</div>

                <Pagination name='dsCanboCongdoanPage' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} pageCondition={pageCondition}
                    getPage={this.props.getDsCanboCongdoanInPage} />

                <Link to='/user/summary/ds-canbo-congdoan/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>

                <a href='#' className='btn btn-primary btn-circle' onClick={e => this.create(e, null)} style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-plus' />
                </a>

                <EditModal ref={this.modal} createCBCD={this.props.createCBCD} updateCBCD={this.props.updateCBCD} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsCanboCongdoan: state.dsCanboCongdoan });
const mapActionsToProps = { getDsCanboCongdoanInPage, createCBCD, updateCBCD, deleteCBCD };
export default connect(mapStateToProps, mapActionsToProps)(DsCanboCongdoanPage);
