import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { updateMultiPgsGsKhtnv } from '../../redux/dsPgsGsKhtnv.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('show.bs.modal', () => $('#shcc').focus());
            $(this.modal.current).on('hidden.bs.modal', () => this.setState({ index: -1 }));
        })
    }

    show = (index, item) => {
        const { shcc, note, year } = item ?
            item : { shcc: 0, note: 0, year: 0 };
        $('#shcc').val(shcc);
        $('#note').val(note);
        $('#year').val(year);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            shcc: $('#shcc').val(),
            note: $('#note').val(),
            year: $('#year').val()
        };

        if (changes.shcc === '') {
            T.notify('Số hiệu công chức bị trống!', 'danger');
            $('#shcc').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật PGS GS KHTNV</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='shcc'>SHCC</label>
                                        <input className='form-control' id='shcc' type='text' placeholder='SHCC' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='year'>Năm</label>
                                        <input className='form-control' id='year' type='number' placeholder='Năm' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='note'>Ghi chú</label>
                                        <input className='form-control' id='note' type='text' placeholder='Ghi chú' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DSPgsGsKhongHoanThanhNhiemVuImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/pgsgs_khtnv');
    }

    onSuccess = (response) => {
        if (response.numberOfErrors > 0 || response.warnings.length > 0) {
            this.setState({ errorLog: true, numberOfErrors: response.numberOfErrors, warnings: response.warnings });
        }
        if (response.officers.length > 0) {
            this.setState({ officer: response.officers, message: <p className='text-center' style={{ color: 'green' }}>{response.officers.length} hàng được tải lên thành công</p> });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const officer = this.state.officer, currentValue = officer[index];
        const updateValue = Object.assign({}, currentValue, {
            note: changes.note,
            shcc: changes.shcc,
            year: changes.year
        });
        officer.splice(index, 1, updateValue);
        this.setState({ officer });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const officer = this.state.officer;
        officer.splice(index, 1);
        this.setState({ officer });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiPgsGsKhtnv(this.state.officer, () => {
            T.notify('Cập nhật thành công!', 'success');
            this.props.history.push('/user/summary/pgsgs_khtnv');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, officer } = this.state;
        let table = null;
        if (officer && officer.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>SHCC</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Năm</th>
                            <th style={{ width: '100%', textAlign: 'center' }}>Ghi chú</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {officer.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td className='text-info text-right'>{item.shcc}</td>
                                <td className='text-info text-right'>{item.year}</td>
                                <td className='text-info text-right'>{item.note}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file Danh sách PGS/GS không hoàn thành nhiệm vụ</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6 offset-md-3'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='PgsGsKhtnvImportFile' userData='PgsGsKhtnvImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/05.ds_pgs_gs_khtnv.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                    <div className='col-6 col-md-6'>
                        {errorLog ? (
                            <div className='tile'>
                                <div className='tile-body'>
                                    {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                    {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                </div>
                            </div>
                        ) : null}
                    </div>
                </div>
                <div className='row'>
                    {officer && officer.length ? (
                        <div className='tile col-12'>
                            {table}
                        </div>
                    ) : null}
                </div>
                <Link to='/user/summary/pgsgs_khtnv' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiPgsGsKhtnv };
export default connect(mapStateToProps, mapActionsToProps)(DSPgsGsKhongHoanThanhNhiemVuImportPage);