import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../../common/FileBox.jsx';
import { updateMultiYearPoint } from '../../redux/point.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => {
                $('#professionalPoint').focus();
            });

            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ index: -1 })
            });
        })
    }
    show = (index, item) => {
        const { ratedRatio, professionalCoefficient, leadershipCoefficient, rewardPoint, penaltyPoint, leadershipPoint, professionalPoint, professionalPoint1, professionalPoint2, professionalPoint3 } = item ?
            item : { ratedRatio: 0, professionalCoefficient: 0, leadershipCoefficient: 0, rewardPoint: 0, penaltyPoint: 0, leadershipPoint: 0, professionalPoint: 0, professionalPoint1: 0, professionalPoint2: 0, professionalPoint3: 0 };
        $('#ratedRatio').val(ratedRatio);
        $('#professionalCoefficient').val(professionalCoefficient);
        $('#leadershipCoefficient').val(leadershipCoefficient);
        $('#rewardPoint').val(rewardPoint);
        $('#penaltyPoint').val(penaltyPoint);
        $('#leadershipPoint').val(leadershipPoint);
        $('#professionalPoint').val(professionalPoint);
        $('#professionalPoint1').val(professionalPoint1);
        $('#professionalPoint2').val(professionalPoint2);
        $('#professionalPoint3').val(professionalPoint3);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            ratedRatio: parseFloat($('#ratedRatio').val()),
            professionalCoefficient: parseFloat($('#professionalCoefficient').val()),
            leadershipCoefficient: parseFloat($('#leadershipCoefficient').val()),
            rewardPoint: parseFloat($('#rewardPoint').val()),
            penaltyPoint: parseFloat($('#penaltyPoint').val()),
            leadershipPoint: parseFloat($('#leadershipPoint').val()),
            professionalPoint: parseFloat($('#professionalPoint').val()),
            professionalPoint1: parseFloat($('#professionalPoint1').val()),
            professionalPoint2: parseFloat($('#professionalPoint2').val()),
            professionalPoint3: parseFloat($('#professionalPoint3').val()),
        };

        if (isNaN(changes.ratedRatio)) {
            T.notify('Tỷ lệ định mức không chính xác!', 'danger');
            $('#ratedRatio').focus();
        } else if (isNaN(changes.professionalCoefficient)) {
            T.notify('Hệ số chuyên môn không chính xác!', 'danger');
            $('#professionalCoefficient').focus();
        } else if (isNaN(changes.leadershipCoefficient)) {
            T.notify('Hệ số lãnh đạo không chính xác!', 'danger');
            $('#leadershipCoefficient').focus();
        } else if (isNaN(changes.rewardPoint)) {
            T.notify('Điểm thưởng không chính xác!', 'danger');
            $('#rewardPoint').focus();
        } else if (isNaN(changes.penaltyPoint)) {
            T.notify('Điểm phạt không chính xác!', 'danger');
            $('#penaltyPoint').focus();
        } else if (isNaN(changes.leadershipPoint)) {
            T.notify('Điểm lãnh đạo không chính xác!', 'danger');
            $('#leadershipPoint').focus();
        } else if (isNaN(changes.professionalPoint)) {
            T.notify('Điểm chuyên môn không chính xác!', 'danger');
            $('#professionalPoint2').focus();
        } else if (isNaN(changes.professionalPoint1)) {
            T.notify('Điểm nhiệm vụ 1 không chính xác!', 'danger');
            $('#professionalPoint2').focus();
        } else if (isNaN(changes.professionalPoint2)) {
            T.notify('Điểm nhiệm vụ 2 không chính xác!', 'danger');
            $('#professionalPoint2').focus();
        } else if (isNaN(changes.professionalPoint3)) {
            T.notify('Điểm nhiệm vụ 3 không chính xác!', 'danger');
            $('#professionalPoint3').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật tất cả điểm</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='row'>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='ratedRatio'>Tỷ số định mức</label>
                                        <input className='form-control' id='ratedRatio' type='number'
                                            placeholder='Tỷ số định mức' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='leadershipPoint'>Điểm lãnh đạo</label>
                                        <input className='form-control' id='leadershipPoint' type='number'
                                            placeholder='Điểm lãnh đạo' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalCoefficient'>Hệ số chuyên môn</label>
                                        <input className='form-control' id='professionalCoefficient' type='number'
                                            placeholder='Hệ số chuyện môn' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='leadershipCoefficient'>Hệ số lãnh đạo</label>
                                        <input className='form-control' id='leadershipCoefficient' type='number'
                                            placeholder='Hệ số lãnh đạo' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint'>Điểm chuyên môn</label>
                                        <input className='form-control' id='professionalPoint' type='number'
                                            placeholder='Điểm chuyên môn' step='0.01' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint1'>Điểm nhiệm vụ 1</label>
                                        <input className='form-control' id='professionalPoint1' type='number'
                                            placeholder='Điểm nhiệm vụ 1' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint2'>Điểm nhiệm vụ 2</label>
                                        <input className='form-control' id='professionalPoint2' type='number'
                                            placeholder='hùng' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='professionalPoint3'>Điểm nhiệm vụ 3</label>
                                        <input className='form-control' id='professionalPoint3' type='number'
                                            placeholder='Điểm nhiệm vụ 3' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='rewardPoint'>Điểm thưởng</label>
                                        <input className='form-control' id='rewardPoint' type='number'
                                            placeholder='Điểm thưởng' />
                                    </div>
                                </div>
                                <div className='col-12 col-md-6'>
                                    <div className='form-group'>
                                        <label htmlFor='penaltyPoint'>Điểm phạt</label>
                                        <input className='form-control' id='penaltyPoint' type='number'
                                            placeholder='Điểm phạt' />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class YearPointImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };
        this.modal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/year');
    }

    onSuccess = (response) => {
        if (response.numberOfErrors > 0 || response.warnings.length > 0) {
            this.setState({ errorLog: true, numberOfErrors: response.numberOfErrors, warnings: response.warnings });
        }
        if (response.points.length > 0) {
            this.setState({ points: response.points, message: <p className='text-center' style={{ color: 'green' }}>{response.points.length} hàng được tải lên thành công</p> });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.modal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const points = this.state.points, currentValue = points[index];
        const updateValue = Object.assign({}, currentValue, {
            ratedRatio: changes.ratedRatio,
            professionalCoefficient: changes.professionalCoefficient,
            leadershipCoefficient: changes.leadershipCoefficient,
            rewardPoint: changes.rewardPoint,
            penaltyPoint: changes.penaltyPoint,
            leadershipPoint: changes.leadershipPoint,
            professionalPoint: changes.professionalPoint,
            professionalPoint1: changes.professionalPoint1,
            professionalPoint2: changes.professionalPoint2,
            professionalPoint3: changes.professionalPoint3,
        });
        points.splice(index, 1, updateValue);
        this.setState({ points });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const points = this.state.points;
        points.splice(index, 1);
        this.setState({ points });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiYearPoint(this.state.points, () => {
            T.notify('Cập nhật điểm thành công!', 'success');
            this.props.history.push('/user/summary/year');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, points } = this.state;
        let table = null;
        if (points && points.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>MSCB</th>
                            <th style={{ width: '100%' }}>Họ tên</th>
                            <th style={{ width: 'auto' }}>Email</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Tỷ số ĐM</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Hệ số CM</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Hệ số LĐ</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm lãnh đạo</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm CM</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm CM1</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm CM2</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm CM3</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm thưởng</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm phạt</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {points.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td style={{ textAlign: 'right' }}>{item.userInfo.organizationId}</td>
                                <td>{item.userInfo.lastname + ' ' + item.userInfo.firstname}</td>
                                <td>{item.userInfo.email}</td>
                                <td className='text-info text-right'>{item.ratedRatio}</td>
                                <td className='text-info text-right'>{item.professionalCoefficient}</td>
                                <td className='text-info text-right'>{item.leadershipCoefficient}</td>
                                <td className='text-info text-right'>{item.leadershipPoint}</td>
                                <td className='text-info text-right'>{item.professionalPoint}</td>
                                <td className='text-info text-right'>{item.professionalPoint1}</td>
                                <td className='text-info text-right'>{item.professionalPoint2}</td>
                                <td className='text-info text-right'>{item.professionalPoint3}</td>
                                <td className='text-info text-right'>{item.rewardPoint}</td>
                                <td className='text-info text-right'>{item.penaltyPoint}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file cập nhật điểm</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='YearPointFile' userData='yearPointImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/SampleUploadYearPoint.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                    <div className='col-6 col-md-6'>
                        {errorLog ? (
                            <div className='tile'>
                                <div className='tile-body'>
                                    {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                    {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                </div>
                            </div>
                        ) : null}
                    </div>
                </div>
                <div className='row'>
                    {points && points.length ? (
                        <div className='tile col-12'>
                            {table}
                        </div>
                    ) : null}
                </div>
                <Link to='/user/summary/year' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <EditModal ref={this.modal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiYearPoint };
export default connect(mapStateToProps, mapActionsToProps)(YearPointImportPage);
