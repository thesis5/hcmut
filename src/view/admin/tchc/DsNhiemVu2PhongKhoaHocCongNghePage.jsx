import React from 'react';
import { connect } from 'react-redux';
import { getDs_nv2_pkhcnInPage, deleteDs_nv2_pkhcn, createDs_nv2_pkhcn, updateDs_nv2_pkhcn } from '../../redux/dsNhiemVu2PhongKhoaHocCongNghe.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    shcc: { type: 'text', title: 'SHCC' },
    fullName: { type: 'text', title: 'Họ tên đầy đủ' },
    shtt: { type: 'text', title: 'SHTT' },
    cgcn: { type: 'text', title: 'CNCN' },
    ldsx: { type: 'text', title: 'LDSX' },
    articleOfUniversity: { type: 'text', title: 'Đề tài thuộc trường + VNU' },
    articleOutUniversity: { type: 'text', title: 'Đề tài bên ngoài' },
    reward: { type: 'text', title: 'Giải thưởng' },
    elseAction: { type: 'text', title: 'Hoạt động khoa học công nghệ khác' },
    article: { type: 'text', title: 'Bài báo' },
    point: { type: 'number', title: 'Điểm' },
    totalHours: { type: 'number', title: 'Tổng giờ NCKH' },
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: null, isUpdate: true };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null, isUpdate: true })
            });
        })
    }
    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val(item[key] ? item[key] : null);
            });
            this.setState({
                _id: item && item._id ? item._id : null,
                isUpdate: true
            });
        }
        else {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val('');
            });
            this.setState({
                _id: null,
                isUpdate: false
            });
        }

        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        if (this.state.isUpdate) {
            this.props.update(this.state._id, changes, () => {
                T.notify('Cập nhật thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            this.props.create(changes, () => {
                T.notify('Tạo mới thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
    };
    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{this.state.isUpdate ? 'Cập nhật' : 'Tạo mới'}</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{schema[key].title}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step}
                                        placeholder={schema[key].title} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsNhiemVu2PhongKhoaHocCongNghePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { condition: { isStaff: true }, userId: {}, };
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getDs_nv2_pkhcnInPage();
        T.ready();
    }

    create = (e) => {
        this.modal.current.show();
        e.preventDefault();
    }

    edit = (e, item) => {
        e.preventDefault();
        this.modal.current.show(item);
    };

    delete = (e, item) => {
        T.confirm('Thông tin', 'Bạn có chắc bạn muốn xóa mẫu thông tin này?', 'warning', true, isConfirm =>
            isConfirm && this.props.deleteDs_nv2_pkhcn(item._id));
        e.preventDefault();
    }
    search = (e) => {
        e.preventDefault();
        const condition = {};
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value },
                { fullName: value },
            ];
        } else {
            delete condition['$or'];
        }
        this.props.getDs_nv2_pkhcnInPage(undefined, undefined, condition, () => {
            this.setState({ condition });
        });
    };
    render() {

        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.ds_nv2_pkhcn && this.props.ds_nv2_pkhcn && this.props.ds_nv2_pkhcn.page ?
            this.props.ds_nv2_pkhcn.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.ds_nv2_pkhcn && this.props.ds_nv2_pkhcn.page && this.props.ds_nv2_pkhcn.page.list && this.props.ds_nv2_pkhcn.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>SHCC</th>
                            <th style={{ width: '100%' }}>Họ và tên</th>
                            <th style={{ width: 'auto' }}>SHTT</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>CGCN</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Bài báo</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Điểm</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.ds_nv2_pkhcn.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>{item.shcc}</td>
                                <td><a href='#' onClick={e => this.edit(e, item)}> {item.fullName}</a></td>
                                <td>{item.shtt}</td>
                                <td>{item.cgcn}</td>
                                <td>{item.article}</td>
                                <td>{item.point ? item.point.toFixed(2) : 0}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={(e) => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-star' /> DS Nhiệm vụ 2 - Phòng Khoa học công nghệ</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }}
                            onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search'
                                placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }}
                                onClick={e => this.search(e)}><i className='fa fa-search' /></a>
                        </form>
                        {this.state.condition && this.state.condition['$or'] ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)}
                                style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pageDs_nv2_pkhcn' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getDs_nv2_pkhcnInPage} />

                <Link to='/user/summary/nv2-pkhcn/upload-point' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>
                <EditModal ref={this.modal} create={this.props.createDs_nv2_pkhcn} update={this.props.updateDs_nv2_pkhcn} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, ds_nv2_pkhcn: state.ds_nv2_pkhcn });
const mapActionsToProps = { getDs_nv2_pkhcnInPage, deleteDs_nv2_pkhcn, createDs_nv2_pkhcn, updateDs_nv2_pkhcn };
export default connect(mapStateToProps, mapActionsToProps)(DsNhiemVu2PhongKhoaHocCongNghePage);