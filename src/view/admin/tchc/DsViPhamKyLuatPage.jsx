import React from 'react';
import { connect } from 'react-redux';
import { getViPhamKyLuatInPage, createViPhamKyLuat, updateViPhamKyLuat, deleteViPhamKyLuat } from '../../redux/dsViPhamKyLuat.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#dsvpklShcc').focus());
            $(this.modal.current).on('hidden.bs.modal', () => this.setState({ _id: null }));
        })
    }

    show = (item) => {
        const { shcc, nam, ghiChu } = item ? item : { shcc: 0, nam: 2019, ghiChu: '' };
        $('#dsvpklShcc').val(shcc);
        $('#dsvpklNam').val(nam);
        $('#dsvpklGhiChu').val(ghiChu);

        this.setState({ _id: item && item._id ? item._id : null });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            shcc: $('#dsvpklShcc').val(),
            nam: parseInt($('#dsvpklNam').val()),
            ghiChu: $('#dsvpklGhiChu').val(),
        };

        if (changes.shcc == '') {
            T.notify('Số hiệu công chức trống!', 'danger');
            $('#dsvpklShcc').focus();
        } else if (changes.dsvpklNam == '') {
            T.notify('Điểm phạt không chính xác!', 'danger');
            $('#dsvpklNam').focus();
        } else {
            if (this.state._id) {
                this.props.update(this.state._id, changes, () => {
                    T.notify('Cập nhật vi phạm kỷ luật thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            } else {
                this.props.create(changes, () => {
                    T.notify('Tạo vi phạm kỷ luật thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật điểm thưởng</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='dsvpklShcc'>SHCC</label>
                                <input className='form-control is-valid' id='dsvpklShcc' type='text' placeholder='SHCC' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dsvpklNam'>Năm</label>
                                <input className='form-control' id='dsvpklNam' type='number' placeholder='Năm' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dsvpklGhiChu'>Ghi chú</label>
                                <textarea className='form-control' id='dsvpklGhiChu' placeholder='Ghi chú' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsViPhamKyLuatPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { condition: { year: new Date().getFullYear() } };
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getViPhamKyLuatInPage(1, 50, { nam: this.state.year });
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        e.preventDefault();
        T.confirm('Xóa phần tử', 'Bạn có chắc bạn muốn xóa phần tử này?', true, isConfirm =>
            isConfirm && this.props.deleteViPhamKyLuat(item._id));
    };

    search = (e) => {
        e.preventDefault();
        const condition = this.state.condition;
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value },
            ];
        } else {
            delete condition['$or'];
        }
        this.props.getViPhamKyLuatInPage(undefined, undefined, condition, () => {
            this.setState({ condition });
        });
    };

    render() {
        let { pageNumber, pageSize, pageTotal, pageCondition, totalItem } = this.props.dsViPhamKyLuat && this.props.dsViPhamKyLuat.page ?
            this.props.dsViPhamKyLuat.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, pageCondition: {}, totalItem: 0 };
        let table = null;
        if (this.props.dsViPhamKyLuat && this.props.dsViPhamKyLuat.page && this.props.dsViPhamKyLuat.page.list && this.props.dsViPhamKyLuat.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: 'auto' }}>Năm</th>
                            <th style={{ width: '100%' }}>Ghi chú</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.dsViPhamKyLuat.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td style={{ textAlign: 'center' }}>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a>
                                </td>
                                <td style={{ textAlign: 'right' }}>{item.nam}</td>
                                <td style={{ textAlign: 'right' }}>{item.ghiChu}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Danh sách trống!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-user' /> Danh sách vi phạm kỷ luật</h1>
                        <p />
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.condition && this.state.condition['$or'] ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='adminViPhamKyLuat' pageCondition={pageCondition} pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getViPhamKyLuatInPage} />

                <Link to='/user/summary/ds_vi_pham_ky_luat/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <a href='#' className='btn btn-primary btn-circle' onClick={e => this.edit(e, null)} style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-plus' />
                </a>
                <EditModal ref={this.modal} create={this.props.createViPhamKyLuat} update={this.props.updateViPhamKyLuat} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsViPhamKyLuat: state.dsViPhamKyLuat });
const mapActionsToProps = { getViPhamKyLuatInPage, createViPhamKyLuat, updateViPhamKyLuat, deleteViPhamKyLuat };
export default connect(mapStateToProps, mapActionsToProps)(DsViPhamKyLuatPage);