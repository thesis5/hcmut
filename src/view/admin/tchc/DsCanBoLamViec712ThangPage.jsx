import React from 'react';
import { connect } from 'react-redux';
import { getCanBoLamViec712ThangInPage, createCanBoLamViec712Thang, updateCanBoLamViec712Thang, deleteCanBoLamViec712Thang } from '../../redux/dsCanBoLamViec712Thang.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null, isUpdate: true };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => $('#dscblv712tShcc').focus());
            $(this.modal.current).on('hidden.bs.modal', () => this.setState({ _id: null, isUpdate: true }));
        })
    }

    show = (item) => {
        const { shcc, nam, soThangLamViec, lyDoKhongTinh } = item ? item : { shcc: '', nam: 2019, soThangLamViec: 0, lyDoKhongTinh: '' };
        $('#dscblv712tShcc').val(shcc);
        $('#dscblv712tNam').val(nam);
        $('#dscblv712tSoThangLamViec').val(soThangLamViec);
        $('#dscblv712tLyDoKhongTinh').val(lyDoKhongTinh);

        if (shcc === '') {
            this.setState({ shcc, isUpdate: false });
        } else {
            this.setState({ shcc, isUpdate: true });
        }
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            shcc: $('#dscblv712tShcc').val(),
            nam: parseInt($('#dscblv712tNam').val()),
            soThangLamViec: parseInt($('#dscblv712tSoThangLamViec').val()),
            lyDoKhongTinh: $('#dscblv712tLyDoKhongTinh').val(),
        };

        if (changes.shcc == '') {
            T.notify('Số hiệu công chức trống!', 'danger');
            $('#dscblv712tShcc').focus();
        } else if (changes.nam == '') {
            T.notify('Năm không chính xác!', 'danger');
            $('#dscblv712tNam').focus();
        } else if (changes.soThangLamViec == '') {
            T.notify('Số tháng làm việc không chính xác!', 'danger');
            $('#dscblv712tSoThangLamViec').focus();
        } else {
            if (this.state._id) {
                this.props.update(this.state._id, changes, () => {
                    T.notify('Cập nhật thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            } else {
                this.props.create(changes, () => {
                    T.notify('Tạo thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{this.state.isUpdate ? 'Cập nhật' : 'Tạo mới'}</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='dscblv712tShcc'>SHCC</label>
                                <input className='form-control is-valid' id='dscblv712tShcc' type='text' placeholder='SHCC' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dscblv712tNam'>Năm</label>
                                <input className='form-control' id='dscblv712tNam' type='number' placeholder='Năm' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dscblv712tSoThangLamViec'>Số tháng làm việc</label>
                                <input className='form-control' id='dscblv712tSoThangLamViec' type='number' placeholder='Số tháng làm việc' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='dscblv712tLyDoKhongTinh'>Lý do không tính</label>
                                <textarea className='form-control' id='dscblv712tLyDoKhongTinh' placeholder='Lý do không tính' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class DsCanBoLamViec712ThangPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isSearching: false };
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getCanBoLamViec712ThangInPage(1, 50, { nam: this.state.year });
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    delete = (e, item) => {
        e.preventDefault();
        T.confirm('Xóa phần tử', 'Bạn có chắc bạn muốn xóa phần tử này?', true, isConfirm =>
            isConfirm && this.props.deleteCanBoLamViec712Thang(item._id));
    };

    search = (e) => {
        e.preventDefault();
        const condition = { '$and': [{ nam: new Date().getFullYear() }] };
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$and'].push({ shcc: value });
        }
        this.props.getCanBoLamViec712ThangInPage(undefined, undefined, condition, () => this.setState({ isSearching: value != '' }));
    };

    render() {
        let { pageNumber, pageSize, pageTotal, pageCondition, totalItem } = this.props.dsCanBoLamViec712Thang && this.props.dsCanBoLamViec712Thang.page ?
            this.props.dsCanBoLamViec712Thang.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, pageCondition: {}, totalItem: 0 };
        let table = null;
        if (this.props.dsCanBoLamViec712Thang && this.props.dsCanBoLamViec712Thang.page && this.props.dsCanBoLamViec712Thang.page.list && this.props.dsCanBoLamViec712Thang.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>SHCC</th>
                            <th style={{ width: 'auto' }}>Năm</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Số tháng làm việc</th>
                            <th style={{ width: '100%' }}>Lý do không tính</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.dsCanBoLamViec712Thang.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td style={{ textAlign: 'center' }}>
                                    <a href='#' onClick={e => this.edit(e, item)}>{item.shcc}</a>
                                </td>
                                <td style={{ textAlign: 'right' }}>{item.nam}</td>
                                <td style={{ textAlign: 'right' }}>{item.soThangLamViec}</td>
                                <td>{item.lyDoKhongTinh}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Danh sách trống!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Danh sách viên chức làm việc từ 6 đến dưới 12 tháng</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='adminCanBoLamViec712Thang' pageCondition={pageCondition} pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getCanBoLamViec712ThangInPage} />

                <Link to='/user/summary/ds_can_bo_lam_viec_712_thang/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <a href='#' className='btn btn-primary btn-circle' onClick={e => this.edit(e, null)} style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-plus' />
                </a>
                <EditModal ref={this.modal} create={this.props.createCanBoLamViec712Thang} update={this.props.updateCanBoLamViec712Thang} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ dsCanBoLamViec712Thang: state.dsCanBoLamViec712Thang });
const mapActionsToProps = { getCanBoLamViec712ThangInPage, createCanBoLamViec712Thang, updateCanBoLamViec712Thang, deleteCanBoLamViec712Thang };
export default connect(mapStateToProps, mapActionsToProps)(DsCanBoLamViec712ThangPage);