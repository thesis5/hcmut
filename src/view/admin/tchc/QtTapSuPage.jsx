import React from 'react';
import { connect } from 'react-redux';
import { getQtTapSuInPage, updatePoint } from '../../redux/qtTapSu.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    SHCC: { type: 'text' },
    STT: { type: 'number' },
    SO_KYHIEU_QD_TAP_SU: { type: 'text' },
    NGAY_QD_TAP_SU: { type: 'text', isDate: true },
    TU_NGAY: { type: 'text', isDate: true },
    DEN_NGAY: { type: 'text', isDate: true },
    TYLE_TAP_SU: { type: 'number' },
    NGAY_NHAP_HS: { type: 'text', isDate: true }
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null })
            });
            Object.keys(schema).forEach(key => {
                if (schema[key].isDate) {
                    $(this[key].current).datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                }
            });
        })
    }

    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                if (schema[key].isDate) {
                    $(this[key].current).datepicker('update', item[key] ? T.dateToText(item[key], 'dd/mm/yyyy') : '');
                } else {
                    $(this[key].current).val(item[key] ? item[key] : null);
                }
            });
        }
        this.setState({ _id: item && item._id ? item._id : null });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        this.props.updatePoint(this.state._id, changes, () => {
            T.notify('Cập nhật dữ liệu thành công!', 'success');
            $(this.modal.current).modal('hide');
        });
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật dữ liệu</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{key}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step}
                                        placeholder={key} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class QtTapSuPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.getQtTapSuInPage();
        T.ready();
    }

    edit = (e, item) => {
        this.modal.current.show(item);
        e.preventDefault();
    };

    render() {
        let { pageNumber, pageSize, pageTotal, totalItem, list } = this.props.qtTapSu && this.props.qtTapSu.page ?
            this.props.qtTapSu.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, list };
        let table = null;
        if (list && list.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            {Object.keys(schema).map((key, index) => (<th key={index} style={{ width: 'auto' }}>{key}</th>))}
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                {Object.keys(schema).map((key, index) => {
                                    if (schema[key].isDate) {
                                        return <td key={index} style={{ width: 'auto' }}>{T.dateToText(item[key], 'dd/mm/yyyy')}</td>;
                                    } else {
                                        return <td key={index} style={{ width: 'auto' }}>{item[key]}</td>;
                                    }
                                })}
                                <td>
                                    <div className=''>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có dữ liệu!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Danh sách Tập sự</h1>
                </div>
                <div className='row tile'>{table}</div>
                <Pagination name='qtTapSuPage' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getQtTapSuInPage} />
                <Link to='/user/summary/qt-tap-su/upload' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <EditModal ref={this.modal} updatePoint={this.props.updatePoint} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ qtTapSu: state.qtTapSu });
const mapActionsToProps = { getQtTapSuInPage, updatePoint };
export default connect(mapStateToProps, mapActionsToProps)(QtTapSuPage);
