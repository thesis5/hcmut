import React from 'react';
import { connect } from 'react-redux';
import { getDsNhiemVu1CoiThiInPage, deleteDsNhiemVu1CoiThi, createDsNhiemVu1CoiThi, updateDsNhiemVu1CoiThi } from '../../redux/dsNhiemVu1CoiThi.jsx';
import { Link } from 'react-router-dom';
import Pagination from '../../common/Pagination.jsx';

const schema = {
    shcc: { type: 'text', title: 'SHCC' },
    fullname: { type: 'text', title: 'Tên đầy đủ' },
    ngay: { type: 'date', title: 'Ngày' },
    maMonHoc: { type: 'text', title: 'Mã môn học' },
    monHoc: { type: 'text', title: 'Môn học' },
    thoiLuong: { type: 'text', title: 'Thời lượng' },
    kyThi: { type: 'text', title: 'Kỳ thi' },
    donViCongTac: { type: 'text', title: 'Môn học' },
    vn1: { type: 'number', title: 'VN1' },
    ghiChu: { type: 'text', title: 'Ghi chú' },
};

class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { _id: null, isUpdate: true };
        this.modal = React.createRef();
        Object.keys(schema).forEach(key => this[key] = React.createRef());
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ _id: null, isUpdate: true })
            });
        })
    }
    show = (item) => {
        if (item) {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val(item[key] ? item[key] : null);
            });
            this.setState({
                _id: item && item._id ? item._id : null,
                isUpdate: true
            });
        }
        else {
            Object.keys(schema).forEach(key => {
                $(this[key].current).val('');
            });
            this.setState({
                _id: null,
                isUpdate: false
            });
        }

        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {};
        Object.keys(schema).forEach(key => changes[key] = $(this[key].current).val());
        if (this.state.isUpdate) {
            this.props.update(this.state._id, changes, () => {
                T.notify('Cập nhật thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            this.props.create(changes, () => {
                T.notify('Tạo mới thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{this.state.isUpdate ? 'Cập nhật' : 'Tạo mới'}</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            {Object.keys(schema).map((key, index) => (
                                <div key={index} className='form-group col-12 col-md-6'>
                                    <label>{schema[key].title}</label>
                                    <input ref={this[key]} className='form-control' type={schema[key].type} step={schema[key].step} placeholder={schema[key].title} />
                                </div>
                            ))}
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
class DsNhiemVu1CoiThiPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isSearching: false };
        this.modal = React.createRef();

    }
    componentDidMount() {
        this.props.getDsNhiemVu1CoiThiInPage(1, 50, {});
        T.ready();
    }
    create = (e) => {
        this.modal.current.show();
    }

    edit = (item) => {
        this.modal.current.show(item);
    };

    delete = (item) => {
        T.confirm('Thông báo!', 'Bạn có chắc bạn muốn xóa mẫu thông tin này?', 'warning', true, isConfirm =>
            isConfirm && this.props.deleteDsNhiemVu1CoiThi(item._id));
    }

    search = (e) => {
        e.preventDefault();
        const condition = {};
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { shcc: value }
            ]
        }
        this.props.getDsNhiemVu1CoiThiInPage(undefined, undefined, (condition), () => this.setState({ isSearching: value != '' }));
    }

    render() {
        const { pageNumber, pageSize, pageTotal, totalItem, pageCondition } = this.props.dsCoithiNv1 && this.props.dsCoithiNv1 && this.props.dsCoithiNv1.page ?
            this.props.dsCoithiNv1.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0, pageCondition: {} };
        let table = null;
        if (this.props.dsCoithiNv1 && this.props.dsCoithiNv1.page && this.props.dsCoithiNv1.page.list && this.props.dsCoithiNv1.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Số hiệu CC</th>
                            <th style={{ width: '100%' }}>Họ và tên</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }} >Môn học</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap', textAlign: 'center' }}>Mã môn học</th>
                            <th style={{ width: 'auto', textAlign: 'center' }}>Ngày</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thời lượng</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap', textAlign: 'center' }}>Kỳ thi</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap', textAlign: 'center' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.dsCoithiNv1.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td> {item.shcc}</td>
                                <td><a href='#' onClick={e => this.edit(item)}> {item.fullname}</a></td>
                                <td style={{ width: 'auto', whiteSpace: 'nowrap', textAlign: 'center' }}> {item.monHoc}</td>
                                <td> {item.maMonHoc}</td>
                                <td> {T.dateToText(item.ngay, 'dd/mm/yyyy')}</td>
                                <td> {item.thoiLuong}</td>
                                <td style={{ width: 'auto', whiteSpace: 'nowrap', textAlign: 'center' }}> {item.kyThi}</td>
                                <td className='btn-group'>
                                    <div className=''>
                                        <a className='btn btn-primary' href='#' onClick={e => this.edit(item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                    </div>
                                    <a className='btn btn-danger' href='#' onClick={e => this.delete(item)}>
                                        <i className='fa fa-lg fa-trash' />
                                    </a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có bản ghi nào!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-star' /> Danh sách coi thi nhiệm vụ 1: Danh sách</h1>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }} onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search' placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }} onClick={e => this.search(e)}>
                                <i className='fa fa-search' />
                            </a>
                        </form>
                        {this.state.isSearching ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)} style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pageEvent'
                    pageCondition={pageCondition} pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getEventInPage} />
                <Link to='/user/summary/ds-coithi-nv1/upload-point' className='btn btn-success btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px' }}>
                    <i className='fa fa-lg fa-cloud-upload' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.create}>
                    <i className='fa fa-lg fa-plus' />
                </button>
                <EditModal ref={this.modal} create={this.props.createDsNhiemVu1CoiThi} update={this.props.updateDsNhiemVu1CoiThi} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, dsCoithiNv1: state.dsCoithiNv1 });
const mapActionsToProps = { getDsNhiemVu1CoiThiInPage, deleteDsNhiemVu1CoiThi, createDsNhiemVu1CoiThi, updateDsNhiemVu1CoiThi };
export default connect(mapStateToProps, mapActionsToProps)(DsNhiemVu1CoiThiPage);
