import React from 'react';
import { connect } from 'react-redux';
import { getAllDivisions } from '../redux/division.jsx';
import { getStaffInPage, createUser, updateUser, deleteUser } from '../redux/user.jsx';
import { Link } from 'react-router-dom';
import Editor from '../common/CkEditor4.jsx';
import Pagination from '../common/Pagination.jsx';
import { UserModal } from './UserPage.jsx';

class MoreInfoModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.modal = React.createRef();
        this.editorViBiography = React.createRef();
        this.editorEnBiography = React.createRef();
        this.editorViResearchFields = React.createRef();
        this.editorEnResearchFields = React.createRef();
        this.editorViPostgraduateCourses = React.createRef();
        this.editorEnPostgraduateCourses = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            setTimeout(() => $(this.modal.current).on('shown.bs.modal', () => {
                $(this.modal.current).find('li:first-child a').tab('show');
                $('#staffMasterThesis').focus();
            }), 250);
        });
    }

    show = (item) => {
        let { _id, masterThesis, goingDoctorThesis, finishedDoctorThesis, trainingExpertise, currentExpertise, biography, researchFields, postgraduateCourses } = item ? item : {};
        masterThesis = masterThesis ? masterThesis : 0;
        goingDoctorThesis = goingDoctorThesis ? goingDoctorThesis : 0;
        finishedDoctorThesis = finishedDoctorThesis ? finishedDoctorThesis : 0;

        trainingExpertise = T.language.parse(trainingExpertise, true);
        currentExpertise = T.language.parse(currentExpertise, true);
        biography = T.language.parse(biography, true);
        researchFields = T.language.parse(researchFields, true);
        postgraduateCourses = T.language.parse(postgraduateCourses, true);

        $('#staffMasterThesis').val(masterThesis);
        $('#staffGoingDoctorThesis').val(goingDoctorThesis);
        $('#staffFinishedDoctorThesis').val(finishedDoctorThesis);

        $('#staffViTrainingExpertise').val(trainingExpertise.vi);
        $('#staffEnTrainingExpertise').val(trainingExpertise.en);
        $('#staffViCurrentExpertise').val(currentExpertise.vi);
        $('#staffEnCurrentExpertise').val(currentExpertise.en);

        this.editorViBiography.current.html(biography.vi);
        this.editorEnBiography.current.html(biography.en);
        this.editorViResearchFields.current.html(researchFields.vi);
        this.editorEnResearchFields.current.html(researchFields.en);
        this.editorViPostgraduateCourses.current.html(postgraduateCourses.vi);
        this.editorEnPostgraduateCourses.current.html(postgraduateCourses.en);

        $(this.modal.current).modal('show');
        this.setState({ _id });
    }

    save = (e) => {
        e.preventDefault();
        const _id = this.state._id,
            trainingExpertise = {
                vi: $('#staffViTrainingExpertise').val(),
                en: $('#staffEnTrainingExpertise').val(),
            },
            currentExpertise = {
                vi: $('#staffViCurrentExpertise').val(),
                en: $('#staffEnCurrentExpertise').val(),
            },
            biography = {
                vi: this.editorViBiography.current.html(),
                en: this.editorEnBiography.current.html(),
            },
            researchFields = {
                vi: this.editorViResearchFields.current.html(),
                en: this.editorEnResearchFields.current.html(),
            },
            postgraduateCourses = {
                vi: this.editorViPostgraduateCourses.current.html(),
                en: this.editorEnPostgraduateCourses.current.html(),
            },
            changes = {
                masterThesis: $('#staffMasterThesis').val(),
                goingDoctorThesis: $('#staffGoingDoctorThesis').val(),
                finishedDoctorThesis: $('#staffFinishedDoctorThesis').val(),
                trainingExpertise: JSON.stringify(trainingExpertise),
                currentExpertise: JSON.stringify(currentExpertise),
                biography: JSON.stringify(biography),
                researchFields: JSON.stringify(researchFields),
                postgraduateCourses: JSON.stringify(postgraduateCourses),
            };

        this.props.updateUser(_id, changes);
        $(this.modal.current).modal('hide');
    }

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin chi tiết nhân viên</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <ul className='nav nav-tabs'>
                                <li className='nav-item'>
                                    <a className='nav-link active show' data-toggle='tab' href='#staffViTab'>Việt Nam</a>
                                </li>
                                <li className='nav-item'>
                                    <a className='nav-link' data-toggle='tab' href='#staffEnTab'>English</a>
                                </li>
                            </ul>

                            <div className='row mt-3'>
                                <div className='col-md-4 form-group'>
                                    <label htmlFor='staffMasterThesis'>LVThS đã hướng dẫn thành công</label>
                                    <input className='form-control' id='staffMasterThesis' type='number' placeholder='LVThS đã hướng dẫn thành công' />
                                </div>
                                <div className='col-md-4 form-group'>
                                    <label htmlFor='staffFinishedDoctorThesis'>LVTS đã hướng dẫn thành công</label>
                                    <input className='form-control' id='staffFinishedDoctorThesis' type='number' placeholder='LVTS đã hướng dẫn thành công' />
                                </div>
                                <div className='col-md-4 form-group'>
                                    <label htmlFor='staffGoingDoctorThesis'>LVTS đang hướng dẫn</label>
                                    <input className='form-control' id='staffGoingDoctorThesis' type='number' placeholder='LVTS đang hướng dẫn' />
                                </div>
                            </div>

                            <div className='tab-content'>
                                <div id='staffViTab' className='tab-pane fade show active'>
                                    <div className='row'>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffViTrainingExpertise'>Chuyên ngành (Tiếng Việt)</label>
                                            <input className='form-control' id='staffViTrainingExpertise' type='text' placeholder='Chuyên ngành' />
                                        </div>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffViCurrentExpertise'>Lĩnh vực chuyên môn hiện tại (Tiếng Việt)</label>
                                            <input className='form-control' id='staffViCurrentExpertise' type='text' placeholder='Lĩnh vực chuyên môn hiện tại' />
                                        </div>
                                    </div>

                                    <ul className='nav nav-tabs'>
                                        <li className='nav-item'>
                                            <a className='nav-link active show' data-toggle='tab' href='#staffBiographyViTab'>Tiểu sử</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab' href='#staffResearchFieldsViTab'>Các hướng nghiên cứu chính</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab' href='#staffPostgraduateCoursesViTab'>Giảng dạy các môn học Sau đại học</a>
                                        </li>
                                    </ul>
                                    <div className='tab-content'>
                                        <div id='staffBiographyViTab' className='tab-pane fade show active'>
                                            <Editor ref={this.editorViBiography} placeholder='Tiểu sử' height={400} />
                                        </div>
                                        <div id='staffResearchFieldsViTab' className='tab-pane fade'>
                                            <Editor ref={this.editorViResearchFields} placeholder='Các hướng nghiên cứu chính' height={400} />
                                        </div>
                                        <div id='staffPostgraduateCoursesViTab' className='tab-pane fade'>
                                            <Editor ref={this.editorViPostgraduateCourses} placeholder='Giảng dạy các môn học Sau đại học' height={400} />
                                        </div>
                                    </div>
                                </div>

                                <div id='staffEnTab' className='tab-pane fade'>
                                    <div className='row'>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffEnTrainingExpertise'>Chuyên ngành (English)</label>
                                            <input className='form-control' id='staffEnTrainingExpertise' type='text' placeholder='Chuyên ngành' />
                                        </div>
                                        <div className='col-md-6 form-group'>
                                            <label htmlFor='staffEnCurrentExpertise'>Lĩnh vực chuyên môn hiện tại (English)</label>
                                            <input className='form-control' id='staffEnCurrentExpertise' type='text' placeholder='Lĩnh vực chuyên môn hiện tại' />
                                        </div>
                                    </div>

                                    <ul className='nav nav-tabs'>
                                        <li className='nav-item'>
                                            <a className='nav-link active show' data-toggle='tab' href='#staffBiographyEnTab'>Tiểu sử</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab' href='#staffResearchFieldsEnTab'>Các hướng nghiên cứu chính</a>
                                        </li>
                                        <li className='nav-item'>
                                            <a className='nav-link' data-toggle='tab' href='#staffPostgraduateCoursesEnTab'>Giảng dạy các môn học Sau đại học</a>
                                        </li>
                                    </ul>
                                    <div className='tab-content'>
                                        <div id='staffBiographyEnTab' className='tab-pane fade show active'>
                                            <Editor ref={this.editorEnBiography} placeholder='Tiểu sử' height={400} />
                                        </div>
                                        <div id='staffResearchFieldsEnTab' className='tab-pane fade'>
                                            <Editor ref={this.editorEnResearchFields} placeholder='Các hướng nghiên cứu chính' height={400} />
                                        </div>
                                        <div id='staffPostgraduateCoursesEnTab' className='tab-pane fade'>
                                            <Editor ref={this.editorEnPostgraduateCourses} placeholder='Giảng dạy các môn học Sau đại học' height={400} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' ref={this.btnSave}>Lưu</button> </div>
                    </div>
                </form>
            </div>
        );
    }
}

class StaffPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { condition: {} };
        this.moreInfoModal = React.createRef();
        this.userModal = React.createRef();
    }

    componentDidMount() {
        T.ready();
        this.props.getAllDivisions();
        this.props.getStaffInPage();
    }

    moreInfo = (e, item) => {
        this.moreInfoModal.current.show(item);
        e.preventDefault();
    }

    createUser = (e) => {
        this.userModal.current.show(null);
        e.preventDefault();
    }

    editUser = (e, item) => {
        this.userModal.current.show(item);
        e.preventDefault();
    }

    changeUserActive = (item) => {
        this.props.updateUser(item._id, { active: !item.active }, () => this.props.getStaffInPage());
    }

    delete = (e, item) => {
        T.confirm('Xóa nhân viên', 'Bạn có chắc bạn muốn xóa nhân viên này?', true, isConfirm =>
            isConfirm && this.props.deleteUser(item._id, () => this.props.getStaffInPage()));
        e.preventDefault();
    }

    search = (e) => {
        e.preventDefault();
        const condition = this.state.condition;
        let value = $('#searchTextBox').val();
        if (value) {
            value = { $regex: `.*${value}.*`, $options: 'i' };
            condition['$or'] = [
                { email: value },
                { firstname: value },
                { lastname: value },
            ];
        } else {
            delete condition['$or'];
        }
        this.props.getStaffInPage(undefined, undefined, condition, () => {
            this.setState({ condition });
        });
    }

    render() {
        const { pageNumber, pageSize, pageTotal, pageCondition, totalItem } = this.props.user && this.props.user.staffPage ?
            this.props.user.staffPage : { pageNumber: 1, pageSize: 50, pageTotal: 1, pageCondition: {}, totalItem: 0 };
        let table = null;
        if (this.props.user && this.props.user.staffPage && this.props.user.staffPage.list && this.props.user.staffPage.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '40%' }}>Tên</th>
                            <th style={{ width: '60%' }}>Email</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Hình ảnh</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Đề tài</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Xuất bản</th>
                            <th style={{ width: 'auto' }} nowrap='true'>Kích hoạt</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.user.staffPage.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>
                                    <a href='#' onClick={e => this.editUser(e, item)}>
                                        {item.firstname + ' ' + item.lastname}
                                    </a>
                                </td>
                                <td>{item.email}</td>
                                <td style={{ textAlign: 'center' }}>
                                    <img src={item.image ? item.image : '/img/avatar.png'} alt='avatar' style={{ height: '32px' }} />
                                </td>
                                <td style={{ textAlign: 'center' }}>
                                    <Link to={'/user/project/' + item._id} className='text-danger'>
                                        <i className='fa fa-lg fa-cog' />
                                    </Link>
                                </td>
                                <td style={{ textAlign: 'center' }}>
                                    <Link to={'/user/publication/' + item._id} className='text-info'>
                                        <i className='fa fa-lg fa-newspaper-o' />
                                    </Link>
                                </td>
                                <td className='toggle' style={{ textAlign: 'center' }}>
                                    <label>
                                        <input type='checkbox' checked={item.active} onChange={() => this.changeUserActive(item)} />
                                        <span className='button-indecator' />
                                    </label>
                                </td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-success' href='#' onClick={e => this.moreInfo(e, item)}>
                                            <i className='fa fa-lg fa-graduation-cap' />
                                        </a>
                                        <a className='btn btn-primary' href='#' onClick={e => this.editUser(e, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có nhân viên!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <div>
                        <h1><i className='fa fa-user' /> Nhân viên</h1>
                    </div>
                    <ul className='app-breadcrumb breadcrumb'>
                        <form style={{ position: 'relative', border: '1px solid #ddd', marginRight: 6 }}
                            onSubmit={e => this.search(e)}>
                            <input className='app-search__input' id='searchTextBox' type='search'
                                placeholder='Search' />
                            <a href='#' style={{ position: 'absolute', top: 6, right: 9 }}
                                onClick={e => this.search(e)}><i className='fa fa-search' /></a>
                        </form>
                        {this.state.condition && this.state.condition['$or'] ?
                            <a href='#' onClick={e => $('#searchTextBox').val('') && this.search(e)}
                                style={{ color: 'red', marginRight: 12, marginTop: 6 }}>
                                <i className='fa fa-trash' />
                            </a> : null}
                    </ul>
                </div>

                <div className='row tile'>{table}</div>
                <Pagination name='adminStaff' pageCondition={pageCondition}
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem} getPage={this.props.getStaffInPage} />

                <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.createUser}>
                    <i className='fa fa-lg fa-plus' />
                </button>

                <MoreInfoModal ref={this.moreInfoModal} updateUser={this.props.updateUser} />

                <UserModal ref={this.userModal} allDivisions={this.props.division ? this.props.division : []}
                    updateUser={this.props.updateUser} createUser={this.props.createUser} getPage={this.props.getStaffInPage} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ division: state.division, user: state.user });
const mapActionsToProps = { getStaffInPage, createUser, updateUser, deleteUser, getAllDivisions };
export default connect(mapStateToProps, mapActionsToProps)(StaffPage);