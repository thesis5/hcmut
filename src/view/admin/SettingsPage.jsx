import React from 'react';
import { connect } from 'react-redux';
import { saveSystemState } from '../redux/system.jsx';
import { Link } from 'react-router-dom';
import ImageBox from '../common/ImageBox.jsx';

class SettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.enAddress = React.createRef();
        this.viAddress = React.createRef();
        this.email = React.createRef();
        this.emailPassword1 = React.createRef();
        this.emailPassword2 = React.createRef();
        this.mobile = React.createRef();
        this.fax = React.createRef();
        this.facebook = React.createRef();
        this.youtube = React.createRef();
        this.twitter = React.createRef();
        this.instagram = React.createRef();
        this.logoImage = React.createRef();
        this.footerImage = React.createRef();
        this.logoUploadBox = React.createRef();
        this.footerUploadBox = React.createRef();
        this.latitude = React.createRef();
        this.longitude = React.createRef();
    }

    componentDidMount() {
        T.ready();
    }

    saveCommonInfo = () => {
        this.props.saveSystemState({
            address: JSON.stringify({ en: $(this.enAddress.current).val().trim(), vi: $(this.viAddress.current).val().trim() }),
            email: $(this.email.current).val().trim(),
            mobile: $(this.mobile.current).val().trim(),
            fax: $(this.fax.current).val().trim(),
            facebook: $(this.facebook.current).val().trim(),
            youtube: $(this.youtube.current).val().trim(),
            twitter: $(this.twitter.current).val().trim(),
            instagram: $(this.instagram.current).val().trim(),
        });
    }

    saveMapInfo = () => {
        this.props.saveSystemState({
            latitude: $(this.latitude.current).val().trim(),
            longitude: $(this.longitude.current).val().trim(),
        });
    }

    changePassword = () => {
        const emailPassword1 = $(this.emailPassword1.current).val(),
            emailPassword2 = $(this.emailPassword2.current).val();
        if (emailPassword1 == '') {
            T.notify('Mật khẩu mới của email hiện tại bị trống!', 'danger');
            $(this.emailPassword1.current).focus();
        } else if (emailPassword2 == '') {
            T.notify('Vui lòng nhập lại mật khẩu mới của email!', 'danger');
            $(this.emailPassword2.current)
        } else if (emailPassword1 != emailPassword2) {
            T.notify('Mật khẩu mới của email không trùng nhau!', 'danger');
            $(this.emailPassword1.current)
        } else {
            this.props.saveSystemState({ password: emailPassword1 });
            $(this.emailPassword1.current).val('');
            $(this.emailPassword2.current).val('');
        }
    }

    render() {
        let { address, email, mobile, fax, facebook, youtube, twitter, instagram, logo, footer, latitude, longitude, map } = this.props.system ?
            this.props.system : { address: '', email: '', mobile: '', fax: '', facebook: '', youtube: '', twitter: '', instagram: '', logo: '', footer: '' };
        address = T.language.parse(address, true);

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-cog' /> Cấu hình</h1>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='tile'>
                            <h3 className='tile-title'>Thông tin HCMUT</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Địa chỉ</label>
                                    <input className='form-control' type='text' placeholder='Địa chỉ' ref={this.viAddress} defaultValue={address.vi} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Address</label>
                                    <input className='form-control' type='text' placeholder='Address' ref={this.enAddress} defaultValue={address.en} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Email</label>
                                    <input className='form-control' type='email' placeholder='Email' ref={this.email} defaultValue={email} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Số điện thoại</label>
                                    <input className='form-control' type='text' placeholder='Số điện thoại' ref={this.mobile} defaultValue={mobile} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Fax</label>
                                    <input className='form-control' type='text' placeholder='Fax' ref={this.fax} defaultValue={fax} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Facebook</label>
                                    <input className='form-control' type='text' placeholder='Facebook' ref={this.facebook} defaultValue={facebook} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Youtube</label>
                                    <input className='form-control' type='text' placeholder='Youtube' ref={this.youtube} defaultValue={youtube} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Twitter</label>
                                    <input className='form-control' type='text' placeholder='Twitter' ref={this.twitter} defaultValue={twitter} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Instagram</label>
                                    <input className='form-control' type='text' placeholder='Instagram' ref={this.instagram} defaultValue={instagram} />
                                </div>
                            </div>
                            <div className='tile-footer' style={{ textAlign: 'right' }}>
                                <button className='btn btn-primary' type='button' onClick={this.saveCommonInfo}>
                                    <i className='fa fa-fw fa-lg fa-check-circle'></i>Lưu
                                </button>
                            </div>
                        </div>


                    </div>

                    <div className='col-md-6'>
                        <div className='tile'>
                            <h3 className='tile-title'>Đổi mật khẩu Email khoa</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Mật khẩu mới</label>
                                    <input className='form-control' type='password' placeholder='Mật khẩu mới' ref={this.emailPassword1} defaultValue='' autoComplete='new-password' />
                                    <input className='form-control' type='password' placeholder='Nhập lại mật khẩu' ref={this.emailPassword2} defaultValue='' autoComplete='new-password' />
                                </div>
                            </div>
                            <div className='tile-footer'>
                                <div className='row'>
                                    <div className='col-md-12' style={{ textAlign: 'right' }}>
                                        <button className='btn btn-primary' type='button' onClick={this.changePassword}>
                                            <i className='fa fa-fw fa-lg fa-check-circle'></i>Đổi mật khẩu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='tile'>
                            <h3 className='tile-title'>Hình ảnh</h3>
                            <div className='tile-body'>
                                <div className='tile-body'>
                                    <div className='form-group'>
                                        <label className='control-label'>Logo</label>
                                        <ImageBox postUrl='/user/upload' uploadType='SettingImage' userData='logo' image={logo} />
                                    </div>
                                    <div className='form-group'>
                                        <label className='control-label'>Hình ảnh Footer</label>
                                        <ImageBox postUrl='/user/upload' uploadType='SettingImage' userData='footer' image={footer} />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='tile'>
                            <h3 className='tile-title'>Bản đồ</h3>
                            <div className='tile-body'>
                                <div className='form-group'>
                                    <label className='control-label'>Latitude</label>
                                    <input className='form-control' type='number' placeholder='Latitude' ref={this.latitude} defaultValue={latitude} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Longitude</label>
                                    <input className='form-control' type='number' placeholder='Longitude' ref={this.longitude} defaultValue={longitude} />
                                </div>
                                <div className='form-group'>
                                    <label className='control-label'>Bản đồ</label>
                                    <ImageBox postUrl='/user/upload' uploadType='SettingImage' userData='map' image={map} />
                                </div>
                            </div>
                            <div className='tile-footer' style={{ textAlign: 'right' }}>
                                <button className='btn btn-primary' type='button' onClick={this.saveMapInfo}>
                                    <i className='fa fa-fw fa-lg fa-check-circle'></i>Lưu
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system });
const mapActionsToProps = { saveSystemState };
export default connect(mapStateToProps, mapActionsToProps)(SettingsPage);