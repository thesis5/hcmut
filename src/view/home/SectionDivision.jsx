import React from 'react';
import { connect } from 'react-redux';
import { getAllDivisionsByUser } from '../redux/division.jsx';
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        division: 'Các bộ phận của khoa',
        department: 'Bộ môn',
        lab: 'Phòng thí nghiệm',
    },
    en: {
        division: 'Division',
        department: 'Department',
        lab: 'Lab',
    }
}, styleString = '1px solid #b3b3b3', borderStyle = {
    borderTop: styleString,
    borderRight: styleString,
    borderBottom: styleString
}, imageStyle = (image) => {
    return {
        backgroundImage: `url('${image}')`,
        minHeight: '300px',
        cursor: 'pointer',
        borderTop: styleString,
        borderLeft: styleString,
        borderBottom: styleString
    }
};

class SectionDivision extends React.Component {
    componentDidMount() {
        this.props.getAllDivisionsByUser();
    }

    componentDidUpdate() {
        setTimeout(T.ftcoAnimate, 250);
    }

    goContent = (_id) => {
        window.location.href = $(`#link${_id}`).attr('href');
    }

    render() {
        const language = T.language(texts);
        const elements = [];
        (this.props.division && this.props.division.items ? this.props.division.items : []).forEach((item, index) => {
            if (item.active) {
                elements.push(
                    <div key={index} className='col-12 col-md-6 d-flex' style={{ paddingTop: '20px' }}>
                        <Link to={'/division/item/' + item._id} id={'link' + item._id} style={{ display: 'none' }} />
                        <div className='col-md-6 ftco-animate img ' onClick={() => this.goContent(item._id)}
                            style={imageStyle(item.image)}>
                        </div>
                        <div className='col-md-6 ftco-animate' style={borderStyle}>
                            <div className='row'>
                                <div className='col-md-12 d-flex align-items-center'>
                                    <div>
                                        <h2 className='mb-1'>{T.language.parse(item.name)}</h2>
                                        <p>{language[item.type]}</p>
                                        <p>{T.language.parse(item.abstract)}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            };
        });

        return (
            <section className='ftco-section'>
                <div className='col-12 heading-section ftco-animate text-center'>
                    <h2 className='mb-4'>{language.division}</h2>
                </div>
                <div className='row'>
                    {elements}
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ division: state.division });
const mapActionsToProps = { getAllDivisionsByUser };
export default connect(mapStateToProps, mapActionsToProps)(SectionDivision);