import React from 'react';
import { connect } from 'react-redux';
import { getNewsInPageByUser } from '../redux/news.jsx'
import { Link } from 'react-router-dom';
import inView from 'in-view';

const linkFormat = '/tintuc/',
    idFormat = '/news/item/';

const texts = {
    vi: {
        noNewsTitle: 'Không có tin tức',
        newsTitle: 'Tin tức mới',
        view: 'Lượt xem',
    },
    en: {
        noNewsTitle: 'No latest news!',
        newsTitle: 'Latest news',
        view: 'View'
    }
};

class NewsListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.loading = false;
    }

    componentDidMount() {
        this.props.getNewsInPageByUser(1, T.defaultUserPageSize, () => this.loading = false);
    }

    componentDidUpdate() {
        setTimeout(T.ftcoAnimate, 250);
    }

    ready = () => {
        inView('.listViewLoading').on('enter', () => {
            let userPage = this.props.news.userPage;
            if (!this.loading && this.props.getNewsInPageByUser && userPage && userPage.pageNumber < userPage.pageTotal) {
                this.loading = true;
                this.props.getNewsInPageByUser(userPage.pageNumber + 1, T.defaultUserPageSize, () => this.loading = false);
            }
        });
    }

    render() {
        const language = T.language(texts);
        let userPage = this.props.news ? this.props.news.userPage : null,
            elements = [];
        if (userPage) {
            elements = userPage.list.map((item, index) => {
                const link = item.link ? linkFormat + item.link : idFormat + item._id;
                return (
                    <div key={index} className='col-12 col-sm-6 col-md-4 col-lg-3 d-sm-flex ftco-animate'>
                        <div className='blog-entry align-self-stretch'>
                            <Link to={link} className='block-20'
                                style={{ backgroundImage: `url('${item.image}')` }}>
                            </Link>
                            <div className='text p-4 d-block'>
                                <h3 className='heading'><a href='#'>{T.language.parse(item.title)}</a></h3>
                                <span className='tag'>{T.language.parse(item.abstract)}</span>
                                <div className='meta mb-3'>
                                    <div><a href='#'>{new Date(item.createdDate).getText()}</a></div>
                                    <div>
                                        {item.view} <i className='fa fa-eye' />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        }

        if (userPage && userPage.pageNumber < userPage.pageTotal) {
            elements.push(
                <div key={elements.length} style={{ width: '100%', textAlign: 'center' }}>
                    <img className='listViewLoading' src='/img/loading.gif' style={{ width: '48px', height: 'auto' }} onLoad={this.ready} />
                </div>
            );
        }

        return <section className='row d-flex justify-content-center'>{elements}</section>;
    }
}

const mapStateToProps = state => ({ news: state.news });
const mapActionsToProps = { getNewsInPageByUser };
export default connect(mapStateToProps, mapActionsToProps)(NewsListView);