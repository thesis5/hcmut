import React from 'react';
import { connect } from 'react-redux';
import { getNewsByUser } from '../redux/news.jsx';
import SectionSideBar from './SectionSideBar.jsx';

class NewsDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = { language: '' };
    }

    componentDidMount() {
        let url = window.location.pathname,
            params = T.routeMatcher(url.startsWith('/tintuc/') ? '/tintuc/:link' : '/news/item/:id').parse(url);
        this.setState({ _id: params.id, link: params.link });
    }

    componentDidUpdate() {
        if (this.state.language != T.language()) {
            this.props.getNewsByUser(this.state._id, this.state.link);
            this.setState({ language: T.language() });
        }

        setTimeout(() => {
            T.ftcoAnimate();
            $('html, body').stop().animate({ scrollTop: 0 }, 500, 'swing');
        }, 250);
    }

    render() {
        const item = this.props.news && this.props.news.userNews ? this.props.news.userNews : null;
        if (item == null) {
            return <p>...</p>;
        } else {
            let categories = !item.categories ? [] : item.categories.map((item, index) => {
                return (<a key={index} href='#' className='tag-cloud-link'>{T.language.parse(item.title)}</a>);
            });

            return [
                <section className='ftco-section ftco-degree-bg'>
                    <div className='container-fluid'>
                        <div className='row'>
                            <div className='col-md-8 ftco-animate'>
                                <h2 className='mb-3'>{T.language.parse(item.title)}</h2>
                                <p>
                                    <img src={item.image} alt={item.title} className='img-fluid' />
                                </p>
                                <p dangerouslySetInnerHTML={{ __html: T.language.parse(item.content) }} />
                                <div className='tag-widget post-tag-container mb-5 mt-5'>
                                    <div className='tagcloud'>
                                        {categories}
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-4 sidebar ftco-animate'>
                                <SectionSideBar />
                            </div>

                        </div>
                    </div>
                </section>
            ];
        }
    }
}

const mapStateToProps = state => ({ news: state.news });
const mapActionsToProps = { getNewsByUser };
export default connect(mapStateToProps, mapActionsToProps)(NewsDetail);