import React from 'react';
import SectionCarousel from './SectionCarousel.jsx';
import SectionHot from './SectionHot.jsx';
import SectionSlogan from './SectionSlogan.jsx';
import SectionVideo from './SectionVideo.jsx';
import SectionStatistic from './SectionStatistic.jsx';
import SectionLogo from './SectionLogo.jsx';
import SectionTestimony from './SectionTestimony.jsx';
import SectionStaffGroup from './SectionStaffGroup.jsx';
import SectionDivision from './SectionDivision.jsx';
import SectionStaff from './SectionStaff.jsx';
import SectionNews from './SectionNews.jsx';
import SectionEvent from './SectionEvent.jsx';
import SectionJob from './SectionJob.jsx';
import NewsListView from './NewsListView.jsx';
import EventListView from './EventListView.jsx';
import JobListView from './JobListView.jsx';

import subscriber from '../../module/subscriber/index.jsx';
import photoBooth from '../../module/photoBooth/index.jsx';
import contact from '../../module/contact/index.jsx';

export default class MenuPage extends React.Component {
    state = { component: null };

    componentDidMount() {
        T.get('/api/menu', res => {
            if (res.error) {
                this.props.history.push('/');
            } else {
                this.setState({ component: res });
            }
        });
    }

    renderComponents(index, ins, outs, isFirst) {
        if (index < ins.length) {
            let item = ins[index],
                itemView = null,
                itemStyle = {};
            if (item.style) {
                let [key, value] = item.style.split(':');
                key = key.trim();
                value = value.trim();
                itemStyle[key] = value;
            }
            if (item.viewType == 'carousel') {
                itemView = <SectionCarousel viewId={item.viewId} />;
            } else if (item.viewType == 'hot') {
                itemView = <SectionHot />;
            } else if (item.viewType == 'slogan') {
                itemView = <SectionSlogan sloganId={item.viewId} />;
            } else if (item.viewType == 'video') {
                itemView = <SectionVideo videoId={item.viewId} />;
            } else if (item.viewType == 'statistic') {
                itemView = <SectionStatistic statisticId={item.viewId} />;
            } else if (item.viewType == 'logo') {
                itemView = <SectionLogo logoId={item.viewId} />;
            } else if (item.viewType == 'staff group') {
                itemView = <SectionStaffGroup staffGroupId={item.viewId} />;
            } else if (item.viewType == 'testimony') {
                itemView = <SectionTestimony testimonyId={item.viewId} />;
            } else if (item.viewType == 'all divisions') {
                itemView = <SectionDivision />;
            } else if (item.viewType == 'all staffs') {
                itemView = <SectionStaff />;
            } else if (item.viewType == 'last news') {
                itemView = <SectionNews />;
            } else if (item.viewType == 'last events') {
                itemView = <SectionEvent />;
            } else if (item.viewType == 'last jobs') {
                itemView = <SectionJob />;
            } else if (item.viewType == 'subscribe') {
                itemView = <subscriber.Section />;
            } else if (item.viewType == 'photoBooth') {
                itemView = <photoBooth.Section photoBoothId={item.viewId} />;
            } else if (item.viewType == 'contact') {
                itemView = <contact.Section />;
            } else if (item.viewType == 'content' && item.view) {
                itemView = <div dangerouslySetInnerHTML={{ __html: T.language.parse(item.view.content) }} />;
            } else if (item.viewType == 'all news') {
                itemView = <NewsListView />;
            } else if (item.viewType == 'all events') {
                itemView = <EventListView />;
            } else if (item.viewType == 'all jobs') {
                itemView = <JobListView />;
            }

            let childComponents = [];
            if (item.components) {
                this.renderComponents(0, item.components, childComponents);
            }

            outs.push(
                <div key={index} className={item.className + (isFirst ? ' first-component' : '')} style={itemStyle}>
                    {itemView}
                    {childComponents}
                </div>
            );
            outs.push(this.renderComponents(index + 1, ins, outs));
        }
    }

    render() {
        let components = [];
        if (this.state.component) {
            this.renderComponents(0, [this.state.component], components, true);
        }
        return components;
    }
}