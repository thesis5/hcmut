import React from 'react';
import { connect } from 'react-redux';
import { getEventFeed } from '../redux/event.jsx'
import { Link } from 'react-router-dom';


const texts = {
    vi: {
        noEventsTitle: 'Không có sự kiện mới',
        eventsTitle: 'Sự kiện mới',
        register: 'Đăng ký',
        view: 'Lượt xem'
    },
    en: {
        noEventsTitle: 'No latest events',
        eventsTitle: 'Latest events',
        register: 'Register',
        view: 'View'
    }
};

class SectionEvent extends React.Component {
    componentDidMount() {
        this.props.getEventFeed();
    }

    componentDidUpdate() {
        $('.event-slider').owlCarousel({
            autoplay: true,
            loop: true,
            items: 1,
            margin: 30,
            stagePadding: 0,
            nav: true,
            dots: true,
            navText: [`<span class='ion-ios-arrow-back'>`, `<span class='ion-ios-arrow-forward'>`],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });
        setTimeout(T.ftcoAnimate, 250);
    }

    render() {
        const language = T.language(texts), userId = this.props.system.user ? this.props.system.user._id : null;
        let title = language.noEventsTitle,
            events = null;
        if (this.props.event && this.props.event.newsFeed) {
            if (this.props.event.newsFeed.length > 0) title = language.eventsTitle;

            events = this.props.event.newsFeed.map((item, index) => {
                const link = item.link ? '/sukien/' + item.link : '/event/item/' + item._id;
                const registerLink = item.link ? '/sukien/dangky/' + item.link : '/event/registration/item/' + item._id;
                return (
                    <div key={index} className='destination' style={{ border: '1px solid #e6e6e6' }}>
                        <Link to={link} className='img img-2 d-flex justify-content-center align-items-center' style={{ backgroundImage: `url('${item.image}')` }} />
                        <div className='text p-3'>
                            <div className='d-flex'>
                                <div className='one'>
                                    <h3><Link to={link}>{T.language.parse(item.title)}</Link></h3>
                                    {userId?'Ngày CTXH: '+item.socialWorkDay:null}
                                    {userId? <p>Điểm rèn luyện: {item.trainingPoint}</p>:null}

                                    {/* <p className='rate'>
                                        <i className='icon-star' />
                                        <i className='icon-star' />
                                        <i className='icon-star' />
                                        <i className='icon-star' />
                                        <i className='icon-star' />
                                    </p> */}
                                </div>
                                <div className='two'>
                                    <span className='price per-price'>{item.view}<br /><small>{language.view}</small></span>
                                </div>
                            </div>
                            <p style={{ paddingBottom: '80px' }}>{T.language.parse(item.abstract)}</p>
                        </div>

                        <p className='bottom-area d-flex p-3'>
                            <span><i className='icon-map-o' />&nbsp; {item.location}</span>
                            <span className='ml-auto'>
                                <Link to={registerLink} style={{ whiteSpace: 'nowrap' }}>{language.register}</Link>
                            </span>
                        </p>
                    </div>
                );
            });
        }

        return (
            <section>
                <div className='container ftco-animate'>
                    <div className='row justify-content-start mt-5 mb-3'>
                        <div className='col-md-12 heading-section'>
                            <h2 className='mb-4 text-center'><strong>{title}</strong></h2>
                        </div>
                    </div>
                </div>
                <div className='event-slider owl-carousel ftco-animate'>
                    {events}
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ event: state.event,system:state.system });
const mapActionsToProps = { getEventFeed };
export default connect(mapStateToProps, mapActionsToProps)(SectionEvent);