import React from 'react';
import { connect } from 'react-redux';
import { getEventFeed } from '../redux/event.jsx';
import { getNewsFeed } from '../redux/news.jsx';
const texts = {
    vi: {
        homeTitle: 'Trang chủ',
        eventTitle: 'Sự kiện',
        register: 'Đăng ký',
        category: 'Danh mục',
        recentEvent: 'Sự kiện mới',
        recentNews: 'Tin tức mới',
        locationANumber: 'Địa điểm và số lượng đăng kí',
        location: 'Địa điểm',
        number: 'Số lượng đăng kí',
        registerTime: 'Thời gian đăng kí',
        eventTime: 'Thời gian bắt đầu',
        updateLater: 'Cập nhật sau',
        unlimited: 'Không giới hạn',
        forever: 'Không kết thúc',
    },
    en: {
        homeTitle: 'Home',
        eventTitle: 'Event',
        register: 'Register',
        category: 'Category',
        recentEvent: 'Recent Events',
        recentNews: 'Recent News',
        locationANumber: 'Location and number of registers',
        location: 'Location',
        number: 'Number of registers',
        registerTime: 'Register time',
        eventTime: 'Event time',
        updateLater: 'Update later',
        unlimited: 'Unlimited',
        forever: 'Forever',
    }
};

class SectionSideBar extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.getEventFeed();
        this.props.getNewsFeed();
    }

    render() {
        const language = T.language(texts);
        const recentEvents = (this.props.event && this.props.event.newsFeed ? this.props.event.newsFeed : []).map((item, index) => {
            const link = item.link ? '/sukien/' + item.link : '/event/item/' + item._id;
            return (
                <div key={index} className='block-21 mb-4 d-flex'>
                    <a href={link} className='blog-img mr-4' style={{ backgroundImage: `url('${item.image}')` }} />
                    <div className='text'>
                        <h3 className='heading'><a href={link}>{T.language.parse(item.title)}</a></h3>
                        <div className='meta'>
                            <div><a href='#'>
                                <span className='icon-calendar' />&nbsp; {new Date(item.createdDate).getText()}
                            </a></div>
                        </div>
                    </div>
                </div>
            )
        });
        const recentNews = (this.props.news && this.props.news.newsFeed ? this.props.news.newsFeed : []).map((item, index) => {
            const link = item.link ? '/tintuc/' + item.link : '/news/item/' + item._id;
            return (
                <div key={index} className='block-21 mb-4 d-flex'>
                    <a href={link} className='blog-img mr-4' style={{ backgroundImage: `url('${item.image}')` }} />
                    <div className='text'>
                        <h3 className='heading'><a href={link}>{T.language.parse(item.title)}</a></h3>
                        <div className='meta'>
                            <div><a href='#'>
                                <span className='icon-calendar' />&nbsp; {new Date(item.createdDate).getText()}
                            </a></div>
                        </div>
                    </div>
                </div>
            )
        });
        return [
            <div key='event' className='sidebar-box ftco-animate'>
                <h3>{language.recentEvent}</h3>
                {recentEvents}
            </div>,
            <div key='news' className='sidebar-box ftco-animate'>
                <h3>{language.recentNews}</h3>
                {recentNews}
            </div>
        ];
    }
}

const mapStateToProps = state => ({ event: state.event, news: state.news });
const mapActionsToProps = { getEventFeed, getNewsFeed };
export default connect(mapStateToProps, mapActionsToProps)(SectionSideBar);