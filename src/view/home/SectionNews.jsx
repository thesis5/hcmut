import React from 'react';
import { connect } from 'react-redux';
import { getNewsFeed } from '../redux/news.jsx'
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        noNewsTitle: 'Không có tin tức',
        newsTitle: 'Tin tức mới',
        view: 'Lượt xem',
    },
    en: {
        noNewsTitle: 'No latest news!',
        newsTitle: 'Latest news',
        view: 'View'
    }
};

class SectionNews extends React.Component {
    componentDidMount() {
        this.props.getNewsFeed();
    }

    componentDidUpdate() {
        setTimeout(T.ftcoAnimate, 250);
    }

    render() {
        const language = T.language(texts);
        let title = language.noNewsTitle,
            news = null;
        if (this.props.news && this.props.news.newsFeed) {
            if (this.props.news.newsFeed.length > 0) title = language.newsTitle;
            news = this.props.news.newsFeed.map((item, index) => {
                const link = item.link ? '/tintuc/' + item.link : '/news/item/' + item._id;
                return (
                    <div key={index} className='col-12 col-sm-6 col-md-4 col-lg-3 d-sm-flex ftco-animate'>
                        <div className='blog-entry align-self-stretch'>
                            <Link to={link} className='block-20' style={{ backgroundImage: `url('${item.image}')` }} />
                            <div className='text p-4 d-block w-100'>
                                <h3 className='heading'><a href='#'>{T.language.parse(item.title)}</a></h3>
                                <span className='tag'>{T.language.parse(item.abstract)}</span>
                                <div className='meta mb-3'>
                                    <div><a href='#'>{new Date(item.createdDate).getText()}</a></div>
                                    <div><a href='#'>{language.view} {item.view}</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        }

        return (
            <section className='row bg-light'>
                <div className='col-md-12 justify-content-start mt-5 mb-3'>
                    <div className='heading-section ftco-animate'>
                        <h2 className='text-center'><strong>{title}</strong></h2>
                    </div>
                </div>
                <div className='container-fluid'>
                    <div className='row d-flex'>
                        {news}
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = state => ({ news: state.news });
const mapActionsToProps = { getNewsFeed };
export default connect(mapStateToProps, mapActionsToProps)(SectionNews);