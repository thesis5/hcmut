import React from 'react';
import { connect } from 'react-redux';
import { getJobInPageByUser } from '../redux/job.jsx'
import { Link } from 'react-router-dom';
import inView from 'in-view';

const linkFormat = '/vieclam/', idFormat = '/job/item/';
const texts = {
    vi: {
        register: 'Đăng ký',
    }, en: {
        register: 'Register'
    }
};

class JobListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.loading = false;
    }

    componentDidMount() {
        this.props.getJobInPageByUser(1, T.defaultUserPageSize, () => this.loading = false);
        setTimeout(T.ftcoAnimate, 250);
    }

    ready = () => {
        inView('.listViewLoading').on('enter', () => {
            let userPage = this.props.job.userPage;
            if (!this.loading && this.props.getJobInPageByUser && userPage && userPage.pageNumber < userPage.pageTotal) {
                this.loading = true;
                this.props.getJobInPageByUser(userPage.pageNumber + 1, T.defaultUserPageSize, () => this.loading = false);
            }
        });
    }

    render() {
        const language = T.language(texts);
        let userPage = this.props.job ? this.props.job.userPage : null,
            elements = [];
        if (userPage) {
            elements = userPage.list.map((item, index) => {
                const link = item.link ? linkFormat + item.link : idFormat + item._id;
                const registerLink = item.link ? '/vieclam/dangky/' + item.link : '/job/registration/item/' + item._id;
                if (index % 4 < 2)
                    return (
                        <div key={index} className='col-12 col-md-6 ftco-animate fadeInUp ftco-animated'>
                            <div className='blog-entry align-self-stretch d-flex'>
                                <Link to={link} className='block-20 order-md-last' style={{ backgroundImage: 'url(' + item.image + ')' }} />
                                <div className='text p-4 d-block'>
                                    <div className='meta mb-3'>
                                        <div>{new Date(item.startEvent).getText()}</div>
                                        {/*<div><a href='#' className='meta-chat'><span className='icon-chat' /> {item.numOfRegisterUsers}</a>*/}
                                    </div>
                                </div>
                                <Link to={link}><h3 className='heading mt-3'>{T.language.parse(item.title)}</h3></Link>
                                <p>{T.language.parse(item.abstract)}</p>
                                <Link to={registerLink} style={{ whiteSpace: 'nowrap' }}>{language.register}</Link>
                            </div>
                        </div>
                    );
                else
                    return (
                        <div key={index} className='col-12 col-md-6 ftco-animate fadeInUp ftco-animated'>
                            <div className='blog-entry align-self-stretch d-flex'>
                                <Link to={link} className='block-20' style={{ backgroundImage: 'url(' + item.image + ')' }}></Link>
                                <div className='text p-4 d-block'>
                                    <div className='meta mb-3'>
                                        <div>{new Date(item.startEvent).getText()}</div>
                                        <div><a href='#' className='meta-chat'><span className='icon-chat' /> {item.numOfRegisterUsers}</a>
                                        </div>
                                    </div>
                                    <Link to={link}><h3 className='heading mt-3'>{T.language.parse(item.title)}</h3></Link>
                                    <p>{T.language.parse(item.abstract)}</p>
                                </div>
                            </div>
                        </div>
                    );
            });
        }

        if (userPage && userPage.pageNumber < userPage.pageTotal) {
            elements.push(
                <div key={elements.length} style={{ width: '100%', textAlign: 'center' }}>
                    <img className='listViewLoading' src='/img/loading.gif' style={{ width: '48px', height: 'auto' }} onLoad={this.ready} />
                </div>
            );
        }

        return <section className='row d-flex'>{elements}</section>;
    }
}

const mapStateToProps = state => ({ job: state.job });
const mapActionsToProps = { getJobInPageByUser };
export default connect(mapStateToProps, mapActionsToProps)(JobListView);