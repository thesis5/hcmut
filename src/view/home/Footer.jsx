import React from 'react';
import { connect } from 'react-redux';

const texts = {
    vi: {
        facultyName: 'Trường đại học Bách Khoa',
        contactUs: 'Thông tin liên hệ:',
        views: 'Lượt truy cập:',
        socialNetworks: 'Kết nối với chúng tôi:',
        allViews: 'Tổng truy cập: ',
        todayViews: 'Hôm nay: ',
        copyright: 'Copyright &copy;' + new Date().getFullYear() + '. Bản quyền thuộc về Trường đại học Bách Khoa.',
    },
    en: {
        facultyName: 'Department of Civil Engineering',
        contactUs: 'Contact us:',
        views: 'Views:',
        socialNetworks: 'Let us be social:',
        allViews: 'All views: ',
        todayViews: 'Today views: ',
        copyright: 'Copyright &copy;' + new Date().getFullYear() + '. Department of Civil Engineering. All rights reserved.',
    }
};

class Footer extends React.Component {
    render() {
        const language = T.language(texts);
        let { footer, facebook, youtube, twitter, instagram, todayViews, allViews, mobile, address, email } =
            this.props.system ? this.props.system : { logo: '', todayViews: 0, allViews: 0 };
        address = T.language.parse(address);
        facebook = facebook ? <li><a href={facebook} target='_blank'><span className='icon-facebook' /></a></li> : '';
        youtube = youtube ? <li><a href={youtube} target='_blank'><span className='icon-youtube' /></a></li> : '';
        twitter = twitter ? <li><a href={twitter} target='_blank'><span className='icon-twitter' /></a></li> : '';
        instagram = instagram ?
            <li><a href={instagram} target='_blank'><span className='icon-instagram' /></a></li> : '';
        if (email) email = <a href={'mailto:' + email}>{email}</a>

        return (
            <footer className='ftco-footer ftco-bg-dark ftco-section img footer' style={{ paddingBottom: '10px', paddingTop: '30px'}}>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12 col-md-4'>
                            <div className='ftco-footer-widget mb-4'>
                                <h2 className='ftco-heading-2' style={{ marginBottom: '0.5rem' }}>{language.views}</h2>
                                <ul className='list-unstyled' >
                                    <li>{language.todayViews + todayViews}</li>
                                    <li>{language.allViews + allViews}</li>
                                </ul>
                            </div>
                            <div className='ftco-footer-widget mb-4'>
                                <h2 className='ftco-heading-2' style={{ marginBottom: '0.5rem' }}>{language.socialNetworks}</h2>
                                <ul className='ftco-footer-social list-unstyled float-lft'>
                                    {facebook} {youtube} {twitter} {instagram}
                                </ul>
                            </div>
                        </div>
                        <div className='col-12 col-md-8'>
                            <div className='ftco-footer-widget mb-4'>
                                <h2 className='ftco-heading-2' style={{ marginBottom: '0.5rem' }}>{language.facultyName}</h2>
                                <div className='block-23 mb-3'>
                                    <ul>
                                        <li>
                                            <span className='icon icon-map-marker' />
                                            <span className='text'>{address}</span>
                                        </li>
                                        <li>
                                            <span className='icon icon-phone' />
                                            <span className='text'>{mobile}</span>
                                        </li>
                                        <li>
                                            <span className='icon icon-envelope' />
                                            <span className='text'>{email}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 text-center'>
                            <p dangerouslySetInnerHTML={{ __html: language.copyright }}></p>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

const mapStateToProps = state => ({ system: state.system });
const mapActionsToProps = {};
export default connect(mapStateToProps, mapActionsToProps)(Footer);
