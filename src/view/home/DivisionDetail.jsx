import React from 'react';
import { connect } from 'react-redux';
import { getDivisionByUser, getAllDivisionsByUser } from '../redux/division.jsx';
import { getNewsInPageByUser } from '../redux/news.jsx';
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        homeTitle: 'Trang chủ',
        divisionTitle: 'Bộ phận',
        divisionSidebar: 'Bộ phận của khoa',
        recentNews: 'Tin tức gần đây',
        staffListTitle: 'Danh sách nhân viên',
        faculty: 'Khoa',
        department: 'Bộ môn',
        lab: 'Phòng thí nghiệm'
    },
    en: {
        homeTitle: 'Home',
        divisionTitle: 'Division',
        divisionSidebar: 'Other Divisions',
        recentNews: 'Recent News',
        staffListTitle: 'Staffs list',
        faculty: 'Faculty',
        department: 'Department',
        lab: 'Lab'
    }
};

class DivisionDetail extends React.Component {
    componentDidMount() {
        let url = window.location.pathname,
            params = T.routeMatcher('/division/item/:divisionId').parse(url);
        this.props.getDivisionByUser(params.divisionId, () => this.props.getAllDivisionsByUser(() => this.props.getNewsInPageByUser(1, T.defaultUserSidebarSize)));
        setTimeout(() => {
            T.ftcoAnimate();
            $('html, body').stop().animate({ scrollTop: 0 }, 500, 'swing');
        }, 250);
    }

    componentDidUpdate() {
        $(document).ready(() => {
            setTimeout(() => {
                $('.js-fullheight').css('height', $(window).height());
                $(window).resize(function () {
                    $('.js-fullheight').css('height', $(window).height());
                });
                T.ftcoAnimate();
            }, 250);
        })

    }

    changeDivision = (e, _id) => {
        this.props.getDivisionByUser(_id, () => {
            this.props.history.push(`/division/item/${_id}`);
            $('html, body').stop().animate({ scrollTop: 0 }, 500, 'swing');
        });
        e.preventDefault();
    }

    render() {
        const language = T.language(texts);
        const item = this.props.division && this.props.division.userDivision ? this.props.division.userDivision : null;
        let staffs = item && item.staffs ? item.staffs.map((staff, index) => (
            <li key={index}>
                <Link to={'/personnel/' + staff.email.substring(0, staff.email.indexOf('@'))}>{staff.lastname} {staff.firstname}</Link>
            </li>
        )) : null;
        let divisionSidebar = (this.props.division && this.props.division.items ? this.props.division.items : []).map((division, index) => (
            <li key={index}>
                <a href='#' onClick={e => this.changeDivision(e, division._id)}>{T.language.parse(division.name)}</a>
            </li>));
        let newsSidebar = (this.props.news && this.props.news.userPage && this.props.news.userPage.list ? this.props.news.userPage.list : []).map((news, index) => (
            <div key={index} className='block-21 mb-4 d-flex'>
                <Link to={'/news/item/' + news._id} className='blog-img mr-4' style={{ backgroundImage: 'url(' + news.image + ')' }} />
                <div className='text'>
                    <h3 className='heading'><Link to={'/news/item/' + news._id}>{T.language.parse(news.title)}</Link></h3>
                    <div className='meta'>
                        <div><a href='#'><span className='icon-calendar' />{new Date(news.createdDate).getText()}</a></div>
                        <div><a href='#'><span className='icon-eye' />{news.view}</a></div>
                    </div>
                </div>
            </div>));

        if (item == null) {
            return <p>...</p>;
        } else {
            return [
                <div key={0} className='hero-wrap js-fullheight' style={{ backgroundImage: 'url(' + item.image + ')', backgroundAttachment: 'fixed' }}>
                    <div className='overlay' />
                    <div className='container'>
                        <div
                            className='row no-gutters slider-text js-fullheight align-items-center justify-content-center'
                            data-scrollax-parent='true'>
                            <div className='col-md-9 ftco-animate text-center' style={{ padding: '15px', background: 'rgba(0,0,0,0.5)' }}
                                data-scrollax=" properties: { translateY: '70%' }">
                                <p className='breadcrumbs'
                                    data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                                    <span>{language[item.type]}</span>
                                </p>
                                <h1 className="mb-3 bread"
                                    data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">{T.language.parse(item.name)}</h1>
                            </div>
                        </div>
                    </div>
                </div>,
                <section key={1} className='ftco-section ftco-degree-bg'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-md-8 ftco-animate'>
                                <h2 className='mb-3'>{T.language.parse(item.name)}</h2>
                                <p><img src={item.image} alt='Division image' className='img-fluid' /></p>
                                <p dangerouslySetInnerHTML={{ __html: T.language.parse(item.content) }} />

                                <h3>{language.staffListTitle}</h3>
                                <ul>{staffs}</ul>
                            </div>

                            <div className='col-md-4 sidebar ftco-animate'>
                                <div className='sidebar-box ftco-animate'>
                                    <div className='categories'>
                                        <h3>{language.divisionSidebar}</h3>
                                        {divisionSidebar}
                                    </div>
                                </div>

                                <div className='sidebar-box ftco-animate'>
                                    <h3>{language.recentNews}</h3>
                                    {newsSidebar}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            ];
        }
    }
}

const mapStateToProps = state => ({ division: state.division, news: state.news });
const mapActionsToProps = { getDivisionByUser, getAllDivisionsByUser, getNewsInPageByUser };
export default connect(mapStateToProps, mapActionsToProps)(DivisionDetail);