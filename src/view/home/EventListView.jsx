import React from 'react';
import { connect } from 'react-redux';
import { getEventInPageByUser } from '../redux/event.jsx'
import { Link } from 'react-router-dom';
import inView from 'in-view';

const linkFormat = '/sukien/',
    idFormat = '/event/item/';

const texts = {
    vi: {
        register: 'Đăng ký',
        view: 'Lượt xem'
    },
    en: {
        register: 'Register',
        view: 'View'
    }
};

class EventListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.loading = false;
    }

    componentDidMount() {
        this.props.getEventInPageByUser(1, T.defaultUserPageSize, () => this.loading = false);
        setTimeout(T.ftcoAnimate, 250);
    }

    ready = () => {
        inView('.listViewLoading').on('enter', () => {
            let userPage = this.props.event.userPage;
            if (!this.loading && this.props.getEventInPageByUser && userPage && userPage.pageNumber < userPage.pageTotal) {
                this.loading = true;
                this.props.getEventInPageByUser(userPage.pageNumber + 1, T.defaultUserPageSize, () => this.loading = false);
            }
        });
    }

    render() {
        const language = T.language(texts);
        let userPage = this.props.event ? this.props.event.userPage : null,
            elements = [];
        if (userPage) {
            elements = userPage.list.map((item, index) => {
                const link = item.link ? linkFormat + item.link : idFormat + item._id;
                const registerLink = item.link ? '/sukien/dangky/' + item.link : '/event/registration/item/' + item._id;
                return (
                    <div key={index} className='d-flex col-12 col-sm-6 col-md-4 col-lg-3 col-xs-3'>
                        <div className='destination' style={{ border: '1px solid #e6e6e6' }}>
                            <Link to={link} className='img img-2 d-flex justify-content-center align-items-center' style={{ backgroundImage: `url('${item.image}')` }} />
                            <div className='text p-3'>
                                <div className='d-flex'>
                                    <div className='one'>
                                        <h3><Link to={link}>{T.language.parse(item.title)}</Link></h3>
                                        {/* <p className='rate'>
                                            <i className='icon-star' />
                                            <i className='icon-star' />
                                            <i className='icon-star' />
                                            <i className='icon-star' />
                                            <i className='icon-star-o' />
                                            <span>8 Rating</span>
                                        </p> */}
                                    </div>
                                    <div className='two'>
                                        <span className='price per-price'>
                                            {item.view} <i className='fa fa-eye' />
                                        </span>
                                    </div>
                                </div>
                                <p style={{ paddingBottom: '80px' }}>{T.language.parse(item.abstract)}</p>
                            </div>

                            <p className='bottom-area d-flex p-3'>
                                <span><i className='icon-map-o' />&nbsp; {item.location}</span>
                                <span className='ml-auto'>
                                    <Link to={registerLink} style={{ whiteSpace: 'nowrap' }}>{language.register}</Link>
                                </span>
                            </p>
                        </div>
                    </div>
                );
            });
        }

        if (userPage && userPage.pageNumber < userPage.pageTotal) {
            elements.push(
                <div key={elements.length} style={{ width: '100%', textAlign: 'center' }}>
                    <img className='listViewLoading' src='/img/loading.gif' style={{ width: '48px', height: 'auto' }} onLoad={this.ready} />
                </div>
            );
        }

        return <section className='row d-flex'>{elements}</section>;
        // return <section className='row d-flex justify-content-center'>{elements}</section>;
    }
}

const mapStateToProps = state => ({ event: state.event });
const mapActionsToProps = { getEventInPageByUser };
export default connect(mapStateToProps, mapActionsToProps)(EventListView);