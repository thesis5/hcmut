import React from 'react';
import { connect } from 'react-redux';
import { getEventByUser } from '../redux/event.jsx';
import SectionSideBar from './SectionSideBar.jsx';
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        eventTime: 'Thời gian diễn ra: ',
        registrationTime: 'Thời gian đăng ký: ',
        location: 'Địa điểm: ',
        unlimited: 'Không giới hạn',
        register: 'Đăng ký'
    }, en: {
        eventTime: 'Event time: ',
        registrationTime: 'Registration time: ',
        location: 'Location: ',
        unlimited: 'unlimited',
        register: 'Register'
    }
};

class EventDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = { language: '' };
    }

    componentDidMount() {
        let url = window.location.pathname,
            isLink = url.startsWith('/sukien/'),
            params = T.routeMatcher(isLink ? '/sukien/:link' : '/event/item/:id').parse(url);
        this.setState({ _id: params.id, link: params.link });
    }

    componentDidUpdate() {
        if (this.state.language != T.language()) {
            this.props.getEventByUser(this.state._id, this.state.link);
            this.setState({ language: T.language() });
        }

        setTimeout(() => {
            T.ftcoAnimate();
            $('html, body').stop().animate({ scrollTop: 0 }, 500, 'swing');
        }, 250);
    }

    render() {
        const language = T.language(texts);
        const item = this.props.event && this.props.event.userEvent ? this.props.event.userEvent : null;
        if (item == null) {
            return <p>...</p>;
        } else {
            let categories = !item.categories ? [] : item.categories.map((item, index) => {
                return (<a key={index} href='#' className='tag-cloud-link'>{T.language.parse(item.title)}</a>);
            });

            return (
                <section className='ftco-section ftco-degree-bg'>
                    <div className='container-fluid'>
                        <div className='row'>
                            <div className='col-md-8 ftco-animate'>
                                <h2 className='mb-3'>{T.language.parse(item.title)}</h2>
                                <p>{item.abstract? item.abstract.getText():''}</p>
                                <p className='text-center'>
                                    <img src={item.image} alt={item.title} className='img-fluid' />
                                </p>
                                <p dangerouslySetInnerHTML={{ __html: T.language.parse(item.content) }}/>
                                <div className='tag-widget post-tag-container mb-5 mt-5'>
                                    <div className='tagcloud'>
                                        {categories}
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col-12 col-md'>
                                        <p>{language.eventTime}{T.dateToText(item.startEvent)} - {item.stopEvent ? T.dateToText(item.stopEvent) : language.unlimited}</p>
                                        <p>{language.registrationTime}{T.dateToText(item.startRegister)} - {item.stopRegister ? T.dateToText(item.stopRegister) : language.unlimited}</p>
                                        {item.location? <p>{language.location}{item.location.getText()}</p>:null}
                                    </div>
                                    <div className='col-12 col-md-auto'>
                                        <Link to={'/event/registration/item/' + item._id} className='btn py-3 px-4 btn-primary'>{language.register}</Link>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-4 sidebar ftco-animate'>
                                <SectionSideBar />
                            </div>

                        </div>
                    </div>
                </section>
            );
        }
    }
}

const mapStateToProps = state => ({ event: state.event });
const mapActionsToProps = { getEventByUser };
export default connect(mapStateToProps, mapActionsToProps)(EventDetail);