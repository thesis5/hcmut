import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../redux/system.jsx';
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        // loginButton: 'Đăng nhập | Đăng ký',
        loginButton: 'Đăng nhập',
        logoutButton: 'Đăng xuất',
    },
    en: {
        // loginButton: 'Sign in | Sign up',
        loginButton: 'Sign in',
        logoutButton: 'Sign out',
    }
};

class HomeMenu extends React.Component {
    constructor(props) {
        super(props);
        this.nav = React.createRef();
    }

    componentDidMount() {
        const ready = () => {
            if ($.fn.classyNav && this.nav.current) {
                $(this.nav.current).classyNav();
                $('.clever-main-menu').sticky({ topSpacing: 0 });
            } else {
                setTimeout(ready, 100);
            }
        };
        $(document).ready(ready);
    }

    onMenuClick = () => {
        $('#ftco-nav').removeClass('show');
    }

    logout = (e) => {
        e.preventDefault();
        if (this.props.system && this.props.system.user) {
            this.props.logout();
        } else {
            window.location = '/auth/cas';
        }
    }

    render() {
        const language = T.language(texts);
        let menus = [];
        if (this.props.system && this.props.system.menus) {
            menus = this.props.system.menus.map((item, index) => {
                let link = item.link ? item.link.toLowerCase().trim() : '/',
                    isExternalLink = link.startsWith('http://') || link.startsWith('https://');
                link = item.link ? item.link : '#';
                const title = T.language.parse(item.title);

                return (item.submenus && item.submenus.length > 0) ? (
                    <li key={index} className='nav-item has-down'>
                        {isExternalLink ? <a href={link} className='nav-link' htmlFor={`link${index}MenuCheck`}>{title}</a> :
                            (item.link ? <Link to={link} className='nav-link' htmlFor={`link${index}MenuCheck`} onClick={this.onMenuClick}>{title}</Link> :
                                <a href='#' className='nav-link' htmlFor={`link${index}MenuCheck`} onClick={e => e.preventDefault()}>{title}</a>)
                        }
                        <label className='nav-link' htmlFor={`link${index}MenuCheck`}>{title}<img src='/img/angle-double-down.svg' style={{ height: '22px', width: '22px' }} alt='angle-double-down' /></label>
                        <input id={`link${index}MenuCheck`} type='checkbox' style={{ visibility: 'hidden', position: 'absolute' }} />
                        <ul className='menu-dropdown'>{
                            item.submenus.map((subMenu, subIndex) => {
                                const link = subMenu.link ? subMenu.link.trim() : '/';
                                if (subMenu.title == '-') {
                                    return <li key={subIndex}>---</li>;
                                } else {
                                    return isExternalLink ?
                                        <li key={subIndex}><a href={link}>{T.language.parse(subMenu.title)}</a></li> :
                                        <li key={subIndex}><Link to={link} onClick={this.onMenuClick}>{T.language.parse(subMenu.title)}</Link></li>;
                                }
                            })}
                        </ul>
                    </li>
                ) :
                    <li key={index} className='nav-item'>
                        {isExternalLink ? <a href={link} target='_blank' className='nav-link'>{title}</a> :
                            <Link to={link} className='nav-link' onClick={this.onMenuClick}>{title}</Link>}
                    </li>;
            });
        }

        const favicon = this.props.system ? this.props.system.logo : null;
        return (
            <nav className='navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light' id='ftco-navbar' ref={this.nav}>
                <div className='container-fluid'>
                    <Link className='navbar-brand' to='/'>
                        {favicon ? <img src={favicon} style={{ height: '48px', width: 'auto' }} /> : ''} HCMUT
                    </Link>
                    <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#ftco-nav'
                        aria-controls='ftco-nav' aria-expanded='false' aria-label='Toggle navigation'>
                        <span className='oi oi-menu' /> Menu
                    </button>

                    <div className='collapse navbar-collapse' id='ftco-nav' style={{ marginRight: 20 }}>
                        <ul className='navbar-nav ml-auto'>
                            {menus}
                            {this.props.system && this.props.system.user ?
                                <li className='nav-item'>
                                    <a href='/user' className='nav-link' >User</a>
                                </li> : ''}
                            <li className='nav-item cta'>
                                {/* <a href='#' onClick={this.props.showLoginModal} className='nav-link'> */}
                                <a href='#' className='nav-link' onClick={this.logout}>
                                    <span>{this.props.system && this.props.system.user ? language.logoutButton : language.loginButton}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = state => ({ system: state.system });
const mapActionsToProps = { logout };
export default connect(mapStateToProps, mapActionsToProps)(HomeMenu);