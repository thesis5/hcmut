import React from 'react';
import { connect } from 'react-redux';
import { getAllDivisionsByUser } from '../redux/division.jsx';
import { getNewsInPageByUser } from '../redux/news.jsx';
import { userGetAllStaffs } from '../redux/user.jsx';
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        divisionSidebar: 'Bộ phận của khoa',
        recentNews: 'Tin tức gần đây',
        staffListTitle: 'Danh sách nhân viên',
    },
    en: {
        divisionSidebar: 'Other Divisions',
        recentNews: 'Recent News',
        staffListTitle: 'Staff list',
    }
};

class SectionStaff extends React.Component {
    componentDidMount() {
        this.props.getNewsInPageByUser(1, T.defaultUserSidebarSize);
        this.props.getAllDivisionsByUser();
        this.props.userGetAllStaffs();
        setTimeout(T.ftcoAnimate, 250);
    }

    render() {
        const language = T.language(texts);
        let divisions = this.props.division && this.props.division.items ? this.props.division.items : [],
            divisionMap = {};
        divisions.map(division => divisionMap[division._id] = Object.assign({}, division));
        (this.props.user && this.props.user.staffs ? this.props.user.staffs : []).map(staff => {
            staff.divisions.forEach(divisionId => {
                if (divisionMap[divisionId]) {
                    const division = divisionMap[divisionId];
                    if (division.staffs == null) division.staffs = [];
                    division.staffs.push(
                        <li key={division.staffs.length}>
                            <Link to={'/personnel/' + staff.email.substring(0, staff.email.indexOf('@'))}>{staff.lastname} {staff.firstname}</Link>
                        </li>
                    );
                }
            });
        });

        let divisionStaffs = [],
            divisionSidebar = divisions.map((division, index) => {
                divisionStaffs.push(
                    <Link key={index * 2} to={'/division/item/' + division._id}>
                        <h4 className='mb-3'>{T.language.parse(division.name)}</h4>
                    </Link>
                );
                divisionStaffs.push(
                    <ul key={index * 2 + 1}>
                        {divisionMap[division._id].staffs}
                    </ul>
                );
                return (
                    <li key={index}>
                        <Link to={'/division/item/' + division._id}>{T.language.parse(division.name)}</Link>
                    </li>
                );
            });
        let newsSidebar = (this.props.news && this.props.news.userPage && this.props.news.userPage.list ? this.props.news.userPage.list : []).map((news, index) => (
            <div key={index} className='block-21 mb-4 d-flex'>
                <a className='blog-img mr-4' style={{ backgroundImage: 'url(' + news.image + ')' }}></a>
                <div className='text'>
                    <h3 className='heading'><Link to={'/news/item/' + news._id}>{T.language.parse(news.title)}</Link></h3>
                    <div className='meta'>
                        <div><a href='#'><span className='icon-calendar'></span>{new Date(news.createdDate).getText()}</a></div>
                        <div><a href='#'><span className='icon-eye'></span>{news.view}</a></div>
                    </div>
                </div>
            </div>));

        return (
            <section className='ftco-section ftco-degree-bg'>
                <div className='row'>
                    <div className='col-md-8 ftco-animate'>
                        <h2 className='mb-3'>{language.staffListTitle}</h2>
                        {divisionStaffs}
                    </div>

                    <div className='col-md-4 sidebar ftco-animate'>
                        <div className='sidebar-box ftco-animate'>
                            <div className='categories'>
                                <h3>{language.divisionSidebar}</h3>
                                {divisionSidebar}
                            </div>
                        </div>

                        <div className='sidebar-box ftco-animate'>
                            <h3>{language.recentNews}</h3>
                            {newsSidebar}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ division: state.division, news: state.news, user: state.user });
const mapActionsToProps = { getAllDivisionsByUser, getNewsInPageByUser, userGetAllStaffs };
export default connect(mapStateToProps, mapActionsToProps)(SectionStaff);