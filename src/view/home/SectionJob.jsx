import React from 'react';
import { connect } from 'react-redux';
import { getJobFeed } from '../redux/job.jsx'
import { Link } from 'react-router-dom';

const texts = {
    vi: {
        noJobsTitle: 'Không có công việc mới',
        jobsTitle: 'Công việc mới',
        register: 'Đăng ký'
    },
    en: {
        noJobsTitle: 'No latest jobs!',
        jobsTitle: 'Latest jobs',
        register: 'Register'
    }
};

class SectionJob extends React.Component {
    componentDidMount() {
        this.props.getJobFeed();
        setTimeout(T.ftcoAnimate, 250);
    }

    render() {

        const language = T.language(texts);
        let title = language.noNewsTitle,
            jobs = null;
        if (this.props.job && this.props.job.newsFeed) {
            if (this.props.job.newsFeed.length > 0) title = language.jobsTitle;

            jobs = this.props.job.newsFeed.map((item, index) => {
                const link = item.link ? '/vieclam/' + item.link : '/job/item/' + item._id;
                const registerLink = item.link ? '/vieclam/dangky/' + item.link : '/job/registration/item/' + item._id;
                if (index % 4 < 2)
                    return (
                        <div key={index} className='col-12 col-md-6 ftco-animate fadeInUp ftco-animated'>
                            <div className='blog-entry align-self-stretch d-flex'>
                                <Link to={link} className='block-20 order-md-last'
                                    style={{ backgroundImage: 'url(' + item.image + ')' }}>
                                </Link>
                                <div className='text p-4 d-block'>
                                    <div className='meta mb-3'>
                                        <div>{new Date(item.startEvent).getText()}</div>
                                        <div><a href='#' className='meta-chat'><span className='icon-chat' /> {item.numOfRegisterUsers}</a>
                                        </div>
                                    </div>
                                    <Link to={link}><h3 className='heading mt-3'>{T.language.parse(item.title)}</h3></Link>
                                    <p>{T.language.parse(item.abstract)}</p>
                                    <Link to={registerLink}>{language.register}</Link>
                                </div>
                            </div>
                        </div>
                    );
                else
                    return (
                        <div key={index} className='col-12 col-md-6 ftco-animate fadeInUp ftco-animated'>
                            <div className='blog-entry align-self-stretch d-flex'>
                                <a href='#' className='block-20'
                                    style={{ backgroundImage: 'url(' + item.image + ')' }}>
                                </a>
                                <div className='text p-4 d-block'>
                                    <div className='meta mb-3'>
                                        <div>{new Date(item.startEvent).getText()}</div>
                                        <div><a href='#' className='meta-chat'><span className='icon-chat' /> {item.numOfRegisterUsers}</a>
                                        </div>
                                    </div>
                                    <Link to={link}><h3 className='heading mt-3'>{T.language.parse(item.title)}</h3></Link>
                                    <p>{T.language.parse(item.abstract)}</p>
                                    <Link to={registerLink}>{language.register}</Link>
                                </div>
                            </div>
                        </div>
                    );
            });
        }

        return (
            <section className='row ftco-section'>
                <div className='justify-content-start mb-3'>
                    <div className='col-md-12 heading-section ftco-animate'>
                        <h2 className='text-center'><strong>{title}</strong></h2>
                    </div>
                </div>
                <div className='d-flex'>
                    {jobs}
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ job: state.job });
const mapActionsToProps = { getJobFeed };
export default connect(mapStateToProps, mapActionsToProps)(SectionJob);