import React from 'react';
import { connect } from 'react-redux';
import { updateSystemState } from '../redux/system.jsx';

class LanguageSwitch extends React.Component {
    render() {
        return <img src={`/img/flag/${T.language.next()}.png`} style={{ position: 'fixed', top: '6px', right: '6px', width: '32px', zIndex: 1000, cursor: 'pointer' }}
            onClick={() => this.props.updateSystemState(T.language.switch())} />;
    }
}

const mapStateToProps = state => ({ system: state.system });
const mapActionsToProps = { updateSystemState };
export default connect(mapStateToProps, mapActionsToProps)(LanguageSwitch);