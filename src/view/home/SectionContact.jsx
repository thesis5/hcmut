import React from 'react';
import { connect } from 'react-redux';
import { createContact } from '../redux/contact.jsx';

const texts = {
    vi: {
        contactHeader: 'Thông tin liên hệ:',
        phone: 'Số điện thoại',
        address: 'Địa chỉ',
        yourName: 'Tên của bạn',
        yourEmail: 'Email của bạn',
        subject: 'Tiêu đề thư',
        message: 'Nội dung',
        sendMessage: 'Gửi',
        alertEmptyName: 'Tên của bạn đang trống!',
        alertEmptyEmail: 'Email của bạn đang trống!',
        alertInvalidEmail: 'Email không hợp lệ!',
        alertEmptySubject: 'Chủ đề thông điệp đang trống!',
        alertEmptyMessage: 'Nội dung thông điệp đang trống!',
    },
    en: {
        contactHeader: 'Contact Information:',
        phone: 'Phone',
        address: 'Address',
        yourName: 'Your name',
        yourEmail: 'Your email',
        subject: 'Subject',
        sendMessage: 'Send message',
        alertEmptyName: 'Your name is empty!',
        alertEmptyEmail: 'Your email is empty!',
        alertInvalidEmail: 'Invalid email!',
        alertEmptySubject: 'Your subject is empty!',
        alertEmptyMessage: 'Your message is empty!',
    }
};

class SectionContact extends React.Component {
    constructor(props) {
        super(props);
        this.name = React.createRef();
        this.email = React.createRef();
        this.subject = React.createRef();
        this.message = React.createRef();
    }

    sendMessage = (e) => {
        e.preventDefault();
        const language = T.language(texts);
        if (this.name.current.value == '') {
            T.notify(language.alertEmptyName, 'danger');
            (this.name.current).focus();
        } else if (this.email.current.value == '') {
            T.notify(language.alertEmptyEmail, 'danger');
            (this.email.current).focus();
        } else if (!T.validateEmail(this.email.current.value)) {
            T.notify(language.alertInvalidEmail, 'danger');
            (this.email.current).focus();
        } else if (this.subject.current.value == '') {
            T.notify(language.alertEmptySubject, 'danger');
            (this.subject.current).focus();
        } else if (this.message.current.value == '') {
            T.notify(language.alertEmptyMessage, 'danger');
            (this.message.current).focus();
        } else {
            this.props.createContact({
                name: this.name.current.value,
                email: this.email.current.value,
                subject: this.subject.current.value,
                message: this.message.current.value
            }, () => {
                this.name.current.value = this.email.current.value = this.subject.current.value = this.message.current.value = '';
                T.notify('Your message has been sent!', 'success', true, 3000);
            });
        }
    }

    render() {
        const language = T.language(texts);
        let { address, mobile, email, map, latitude, longitude } = this.props.system ? this.props.system : { map: '', latitude: 0, longitude: 0 };
        address = T.language.parse(address);

        return (
            <section className='contact-section ftco-degree-bg' style={{ padding: '5em 0' }}>
                <div className='container'>
                    <div className='row d-flex mt5 mb-3 contact-info'>
                        <div className='col-md-12 mb-4'>
                            <h2 className='h4'>{language.contactHeader}</h2>
                        </div>
                        <div className='w-100' />
                        <div className='col-md-6'>
                            <p><span>{language.address}:</span> <a href={'https://www.google.com/maps/@' + latitude + ',' + longitude + ',16z'} target='_blank'>
                                {address}
                            </a></p>
                        </div>
                        <div className='col-md-3'>
                            <p><span>{language.phone}:</span> <a href={'tel:' + mobile}>{mobile}</a></p>
                        </div>
                        <div className='col-md-3'>
                            <p><span>Email:</span> <a href={'mailto:' + email}>{email}</a></p>
                        </div>
                    </div>
                    <div className='row block-9'>
                        <div className='col-md-6 pr-md-5'>
                            <form onSubmit={this.sendMessage}>
                                <div className='form-group'>
                                    <input type='text' className='form-control' ref={this.name} placeholder={language.yourName} />
                                </div>
                                <div className='form-group'>
                                    <input type='email' className='form-control' ref={this.email} placeholder={language.yourEmail} />
                                </div>
                                <div className='form-group'>
                                    <input type='text' className='form-control' ref={this.subject} placeholder={language.subject} />
                                </div>
                                <div className='form-group'>
                                    <textarea name='message' className='form-control' ref={this.message} cols='30' rows='7' placeholder={language.message} />
                                </div>
                                <div className='form-group'>
                                    <button onClick={this.sendMessage} className='btn btn-primary py-3 px-5 '>{language.sendMessage}</button>
                                </div>
                            </form>
                        </div>
                        <div className='col-md-6'>
                            <a href={'https://www.google.com/maps/@' + latitude + ',' + longitude + ',15z'} target='_blank'>
                                <div style={{ height: '100%', background: 'url(' + map + ') no-repeat center center' }} />
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, contact: state.contact });
const mapActionsToProps = { createContact };
export default connect(mapStateToProps, mapActionsToProps)(SectionContact);