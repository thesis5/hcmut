import T from '../common/js/common';
import React from 'react';
import { connect } from 'react-redux';
import { getEventWithQuestionByUser, getEventFeed } from '../redux/event.jsx';
import { addAnswerByUser } from '../redux/answer.jsx';
import { Link } from 'react-router-dom';
import Select from 'react-select';

class RegisterElement extends React.Component {
    constructor(props) {
        super(props);
        this.state = { selectedValue: null };
        this.value = React.createRef();
    }

    componentDidMount() {

    }

    onSelectType = (selectedItem) => {
        this.setState({ selectedValue: selectedItem.value });
    }

    onSiteChanged = (e) => {
        this.setState({ selectedValue: e.target.value });
    }

    getValue = () => {
        let value;
        if (this.props.element && this.props.element.active == false) value = this.props.element.defaultAnswer;
        else if (this.props.element && this.props.element.typeName == 'choice') {
            value = this.state.selectedValue ? this.state.selectedValue : this.props.element.defaultAnswer;
        }
        else value = $(this.value.current).val();
        return value;
    }

    setFocus = (e) => {
        $(this.value.current).focus();
        e.preventDefault();
    }

    render() {
        const item = this.props.element ? this.props.element : {
            content: '',
            active: false,
            typeName: '',
            typeValue: []
        };
        if (item.active) {
            if (item.typeName == 'choice') {
                let types = item.typeValue.map((value, index) => ({ value: value, label: value }));
                let returnElement = [];
                returnElement.push(
                    <label key='label'><p dangerouslySetInnerHTML={{ __html: item.content }} /></label>
                );
                types.map((type, index) => {
                    returnElement.push(
                        <div key={index} className='icheck-material-blue'>
                            <input
                                className='hidden'
                                type='radio'
                                name={type.label}
                                id={'radio' + index.toString()}
                                value={type.value}
                                checked={this.state.selectedValue === type.value}
                                onChange={this.onSiteChanged} />
                            <label className='entry' htmlFor={'radio' + index.toString()}><div className='circle'></div><div className='entry-label'>{type.label}</div></label>
                        </div>
                    )
                })
                return returnElement;
            } else {
                return (
                    <div className='form-group'>
                        <label htmlFor={(this.props.index).toString()}><p dangerouslySetInnerHTML={{ __html: item.content }} /></label>
                        <input type='text' className='form-control' id={(this.props.index).toString()}
                            aria-describedby='help'
                            ref={this.value} />
                    </div>
                );
            }
        } else return null;
    }
}

const texts = {
    vi: {
        homeTitle: 'Trang chủ',
        eventTitle: 'Sự kiện',
        register: 'Đăng ký sự kiện',
        recentEvent: 'Sự kiện mới',
        notDated: 'Chưa đến ngày mở đăng ký',
        overDated: 'Đã quá hạn đăng ký tham gia',
        enough: 'Đã đủ số lượng đăng ký tham gia',
        noForm: 'Không có form đăng ký từ sự kiện này',
        registAction: 'Đăng kí',
    },
    en: {
        homeTitle: 'Home',
        eventTitle: 'Event',
        register: 'Register event',
        recentEvent: 'Recent Events',
        notDated: 'Not yet to open the registration',
        overDated: 'Registration deadline is exceeded',
        enough: 'Enough number of participants',
        noForm: 'No registration form from this event',
        registAction: 'Regist',
    }
};

class EventRegister extends React.Component {
    constructor(props) {
        super(props);
        this.state = { language: '', itemId: null };

        this.form = React.createRef();
        this.btn = React.createRef();
        this.valueList = [];
        for (let i = 0; i < 50; i++) {
            this.valueList[i] = React.createRef();
        }
    }

    componentDidMount() {
        let url = window.location.pathname,
            isLink = url.startsWith('/sukien/'),
            params = T.routeMatcher(isLink ? '/sukien/dangky/:link' : '/event/register/item/:id').parse(url);
        this.setState({ _id: params.id, link: params.link });
        this.props.getEventFeed();
    }

    componentDidUpdate() {
        if (this.state.language != T.language()) {
            this.props.getEventWithQuestionByUser(this.state._id);
            this.setState({ language: T.language() });
        }

        setTimeout(T.ftcoAnimate, 250);
    }

    createForm = () => {
        const language = T.language(texts);
        if (this.props.event && this.props.event.userEvent) {
            const userEvent = this.props.event.userEvent;
            const questionList = userEvent.questions ? userEvent.questions : [];
            const { startRegister, stopRegister, maxRegisterUsers, numOfRegisterUsers } = userEvent ? userEvent : { startRegister: null, stopRegister: null, maxRegisterUsers: -1, numOfRegisterUsers: 0 };
            const currentTime = new Date();
            if (startRegister && currentTime < new Date(startRegister)) {
                return <p>{language.notDated}</p>;
            }
            if (stopRegister && currentTime > new Date(stopRegister)) {
                return <p>{language.overDated}</p>;
            }
            if (maxRegisterUsers >= 0 && numOfRegisterUsers >= maxRegisterUsers) {
                return <p>{language.enough}</p>;
            }
            if (!questionList || questionList.length == 0) {
                return <p>{language.noForm}</p>;
            }
            const elementList = questionList.map((question) => ({
                defaultAnswer: question.defaultAnswer,
                content: question.content,
                active: question.active,
                typeName: question.typeName,
                typeValue: question.typeValue
            }));

            let form = [];
            for (let i = 0; i < elementList.length; i++) {
                form.push(<RegisterElement key={i} element={elementList[i]} index={i} ref={this.valueList[i]} />);
            }
            form.push(<button key='button' className='btn btn-primary' ref={this.btn} onClick={this.submit}>{language.registAction}</button>);
            return form;
        } else return <p>{language.noForm}</p>;

    }

    submit = (e) => {
        const questionList = this.props.event.userEvent.questions;
        const postId = this.props.event.userEvent._id;
        const userId = this.props.system.user ? this.props.system.user._id : null;
        if (userId == null) {
            T.notify('Bạn chưa đăng nhập!', 'danger');
            e.preventDefault();
            return;
        }
        let record = [];
        let i = 0;
        for (i; i < questionList.length; i++) {
            const value = this.valueList[i].current.getValue();
            if (!value || value == '' || value == '') {
                T.notify('Xin vui lòng nhập đầy đủ thông tin', 'danger');
                this.valueList[i].current.setFocus(e);
                break;
            } else {
                record.push({
                    questionId: questionList[i]._id,
                    answer: value,
                });
            }
        }
        if (i == questionList.length) {
            const newData = {
                user: userId,
                postId,
                record,
            };

            this.props.addAnswerByUser(newData);
        }
        e.preventDefault();
    }

    render() {
        const language = T.language(texts);
        const item = this.props.event && this.props.event.userEvent ? this.props.event.userEvent : null;
        const evensFeed = this.props.event && this.props.event.newsFeed ? this.props.event.newsFeed : [];
        let recentEvents = evensFeed.map((item, index) => {
            return (
                <div key={index} className='block-21 mb-4 d-flex'>
                    <Link to={'/event/item/' + item._id} className='blog-img mr-4' style={{
                        backgroundImage: 'url(' + item.image + ')'
                    }} />
                    <div className='text'>
                        <h3 className='heading'><Link to={'/event/item/' + item._id}>{T.language.parse(item.title)}</Link></h3>
                        <div className='meta'>
                            <div><a href='#'><span className='icon-calendar' />{new Date(item.createdDate).getText()}</a>
                            </div>
                            <div><a href='#'><span className='icon-chat' />{item.view}</a></div>
                        </div>
                    </div>
                </div>)
        });
        if (item == null) {
            return <p>...</p>;
        } else {
            return (
                <div>
                    <div className='hero-wrap'
                        style={{
                            backgroundImage: 'url(' + item.image + ')',
                            backgroundAttachment: 'fixed',
                        }}>
                        <div className='overlay' />
                        <div className='container'>
                            <div className='row no-gutters slider-text align-items-center justify-content-center'
                                data-scrollax-parent='true'>
                                <div className='col-md-8 ftco-animate text-center'>
                                    <p className='breadcrumbs'><span className='mr-2'><Link to='/'>{language.homeTitle}</Link></span> <span className='mr-2'><a href='#'>{language.eventTitle}</a></span></p>
                                    <h1 className='mb-3 bread'>{T.language.parse(item.title)}</h1>
                                    <p>{language.register}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section className='ftco-section ftco-degree-bg'>
                        <div className='container'>
                            <div className='row'>
                                <div className='col-12 col-md-8 ftco-animate'>
                                    <h2 className='mb-3 text-center'>{T.language.parse(item.title)}</h2>
                                    <h2 className='text-center' style={{ fontWeight: '600' }}>{language.register}</h2>
                                    <div className='row'>
                                        <div className='col-12'>
                                            {this.createForm()}
                                        </div>
                                    </div>
                                </div>

                                <div className='col-12 col-md-4 sidebar ftco-animate'>
                                    <div className='sidebar-box ftco-animate'>
                                        <h3>{language.recentEvent}</h3>
                                        {recentEvents}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            );
        }
    }
}

const mapStateToProps = state => ({ event: state.event, answer: state.answer, system: state.system });
const mapActionsToProps = { getEventWithQuestionByUser, getEventFeed, addAnswerByUser };
export default connect(mapStateToProps, mapActionsToProps)(EventRegister);