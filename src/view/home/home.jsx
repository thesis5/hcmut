import './scss/style.scss';
import './home.scss';
import T from '../common/js/common';
import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { connect, Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import system, { getSystemState, register, login, forgotPassword, logout } from '../redux/system.jsx';
import menu from '../redux/menu.jsx';
import user from '../redux/user.jsx';
import carousel from '../redux/carousel.jsx';
import content from '../redux/content.jsx';
import category from '../redux/category.jsx';
import division from '../redux/division.jsx';
import slogan from '../redux/slogan.jsx';
import statistic from '../redux/statistic.jsx';
import logo from '../redux/logo.jsx';
import testimony from '../redux/testimony.jsx';
import staffGroup from '../redux/staffGroup.jsx';
import video from '../redux/video.jsx';
import news from '../redux/news.jsx';
import event from '../redux/event.jsx';
import job from '../redux/job.jsx';
import answer from '../redux/answer.jsx';

import Loadable from 'react-loadable';
import Loading from '../common/Loading.jsx';
import Loader from './Loader.jsx';
import HomeMenu from './HomeMenu.jsx';
import Footer from './Footer.jsx';
import LoginModal from './LoginModal.jsx';
import LanguageSwitch from './LanguageSwitch.jsx';

// Load modules -------------------------------------------------------------------------------------------------------------------------------------
import contact from '../../module/contact/index.jsx';
import subscriber from '../../module/subscriber/index.jsx';
import photoBooth from '../../module/photoBooth/index.jsx';
const modules = [contact, subscriber, photoBooth];

// Initialize Redux ---------------------------------------------------------------------------------------------------------------------------------
const reducers = {
    system, menu, user,
    carousel, content, division, slogan, statistic, testimony, staffGroup, video, answer,
    category, news, event, job, logo
};
modules.forEach(item => {
    Object.keys(item.redux).forEach(key => reducers[key] = item.redux[key])
});

const store = createStore(combineReducers(reducers), {}, composeWithDevTools(applyMiddleware(thunk)));
store.dispatch(getSystemState());
window.T = T;

// Router -------------------------------------------------------------------------------------------------------------------------------------------
class Router extends React.Component {
    constructor(props) {
        super(props);
        this.loader = React.createRef();
        this.loginModal = React.createRef();

        this.routeData = {
            '/registered': Loadable({ loading: Loading, loader: () => import('../common/MessagePage.jsx') }),
            '/active-user/:userId': Loadable({ loading: Loading, loader: () => import('../common/MessagePage.jsx') }),
            '/forgot-password/:userId/:userToken': Loadable({ loading: Loading, loader: () => import('./ForgotPasswordPage.jsx') }),
            '/news/item/:newsId': Loadable({ loading: Loading, loader: () => import('./NewsDetail.jsx') }),
            '/tintuc/:link': Loadable({ loading: Loading, loader: () => import('./NewsDetail.jsx') }),
            '/event/item/:newsId': Loadable({ loading: Loading, loader: () => import('./EventDetail.jsx') }),
            '/sukien/:link': Loadable({ loading: Loading, loader: () => import('./EventDetail.jsx') }),
            '/event/registration/item/:newsId': Loadable({ loading: Loading, loader: () => import('./EventRegistration.jsx') }),
            '/sukien/dangky/:link': Loadable({ loading: Loading, loader: () => import('./EventRegistration.jsx') }),
            '/job/item/:newsId': Loadable({ loading: Loading, loader: () => import('./JobDetail.jsx') }),
            '/vieclam/:link': Loadable({ loading: Loading, loader: () => import('./JobDetail.jsx') }),
            '/job/registration/item/:jobId': Loadable({ loading: Loading, loader: () => import('./JobRegistration.jsx') }),
            '/vieclam/dangky/:link': Loadable({ loading: Loading, loader: () => import('./JobRegistration.jsx') }),
            '/division/item/:divisionId': Loadable({ loading: Loading, loader: () => import('./DivisionDetail.jsx') }),
            '/personnel/:firstPartOfEmail': Loadable({ loading: Loading, loader: () => import('./StaffDetail.jsx') }),
        };
    }

    componentDidMount() {
        const done = () => $(this.loader.current).length > 0 ? this.loader.current.isShown() && this.loader.current.hide() : setTimeout(done, 200);
        $(document).ready(done);
    }

    showLoginModal = (e) => {
        e.preventDefault();
        if (this.props.system && this.props.system.user) {
            this.props.logout();
        } else {
            this.loginModal.current.showLogin();
        }
    }

    render() {
        const routes = Object.assign({}, this.routeData),
            solveMenus = menus => {
                for (let i = 0; i < menus.length; i++) {
                    const item = menus[i],
                        link = item.link ? item.link.toLowerCase() : '/';
                    if (!link.startsWith('http://') && !link.startsWith('https://') && routes[link] == undefined) {
                        routes[link] = Loadable({ loading: Loading, loader: () => import('./MenuPage.jsx') });
                    }
                    item.submenus && solveMenus(item.submenus);
                }
            };

        if (this.props.system && this.props.system.menus) {
            solveMenus(this.props.system.menus);
        }
        let routePathes = Object.keys(routes).sort().reverse();

        for (let i = 0, pathname = window.location.pathname; i <= routePathes.length; i++) {
            if (pathname.length > 1 && pathname.endsWith('/')) pathname = pathname.substring(0, pathname.length - 1);

            if (T.routeMatcher(routePathes[i]).parse(pathname)) {
                return [
                    <HomeMenu key={0} showLoginModal={this.showLoginModal} />,
                    <Switch key={1}>{routePathes.map((path, key) => <Route key={key} path={path} component={routes[path]} />)}</Switch>,
                    <Footer key={2} />,
                    <LanguageSwitch key={3} />,
                    <LoginModal key={4} ref={this.loginModal} register={this.props.register} login={this.props.login} forgotPassword={this.props.forgotPassword} pushHistory={url => this.props.history.push(url)} />,
                    <Loader key={5} ref={this.loader} />,
                ];
            } else if (i == routePathes.length) {
                return (
                        <main className='app'>
                            <Route component={Loadable({ loading: Loading, loader: () => import('../common/MessagePage.jsx') })} />
                        </main >
                );
            }
        }
    }
}

// Main DOM render -----------------------------------------------------------------------------------------------------
class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Router system={this.props.system} register={this.props.register} login={this.props.login} forgotPassword={this.props.forgotPassword} logout={this.props.logout} />
            </BrowserRouter >
        )
    }
}

const Main = connect(state => ({ system: state.system }), { register, login, forgotPassword, logout })(App);
ReactDOM.render(<Provider store={store}><Main /></Provider>, document.getElementById('app'));
