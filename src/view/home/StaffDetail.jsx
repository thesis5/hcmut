import React from 'react';
import { connect } from 'react-redux';
import { userGetStaff, userGetAllProjectLevels, userGetAllPublicationTypes } from '../redux/user.jsx';
import { Link } from 'react-router-dom';
import { getName } from 'country-list';

const texts = {
    vi: {
        tabInformation: 'Thông tin',
        tabBiography: 'Tiểu sử',
        tabScienceProjects: 'Đề tài khoa học',
        tabPublications: 'Bài báo tạp chí/hội nghị khoa học đã xuất bản',
        birthday: 'Ngày tháng năm sinh',
        sex: 'Giới tính',
        male: 'Nam',
        female: 'Nữ',
        department: 'Bộ môn',
        phone: 'Điện thoại liên hệ',
        doctor: 'Tiến sĩ',
        master: 'Thạc sĩ',
        professor: 'Giáo sư',
        'associate professor': 'Phó giáo sư',
        graduatedCountry: 'Quốc gia tốt nghiệp',
        trainingExpertise: 'Chuyên ngành',
        currentExpertise: 'Lĩnh vực chuyên môn hiện tại',
        researchFields: 'Các hướng nghiên cứu chính',
        masterThesis: 'Số luận án thạc sĩ đã hướng dẫn thành công',
        finishedDoctorThesis: 'Số luận án tiến sĩ đã hướng dẫn thành công',
        goingDoctorThesis: 'Số luận án tiến sĩ đang hướng dẫn',
        postgraduateCourses: 'Giảng dạy các môn học Sau đại học',

        role: 'Vai trò',
        projectLevel: 'Cấp quản lý',
        finish: 'Nghiệm thu',
        manager: 'Chủ nhiệm',
        member: 'Thành viên',

        year: 'Năm',
        title: 'Tiêu đề',
        type: 'Loại',
    },
    en: {
        tabInformation: 'Information',
        tabBiography: 'Biography',
        tabScienceProjects: 'Science projects',
        tabPublications: 'Publications',
        birthday: 'Birthday',
        sex: 'Gender',
        male: 'Male',
        female: 'Female',
        department: 'Department',
        phone: 'Phone',
        doctor: 'Doctor',
        master: 'Master',
        professor: 'Professor',
        'associate professor': 'Associate Professor',
        graduatedCountry: 'graduated country',
        trainingExpertise: 'Training expertise',
        currentExpertise: 'Current expertise',
        researchFields: 'Research fields',
        masterThesis: 'The number of master thesis has successfully guided',
        finishedDoctorThesis: 'The number of doctoral thesis has successfully guided',
        goingDoctorThesis: 'The number of doctoral thesis is guiding',
        postgraduateCourses: 'Postgraduate courses',

        role: 'Role',
        projectLevel: 'Type',
        finish: 'Finish',
        manager: 'Manager',
        member: 'Member',

        year: 'Year',
        title: 'Title',
        type: 'Type',
    }
};

class StaffDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        let url = window.location.pathname,
            params = T.routeMatcher('/personnel/:firstPartOfEmail').parse(url);
        this.props.userGetStaff(params.firstPartOfEmail, staff => this.setState({ staff }));
        this.props.userGetAllProjectLevels(levels => {
            const projectLevels = {};
            (levels ? levels : []).forEach(item => projectLevels[item._id] = item.level);
            this.setState({ projectLevels });
        });
        this.props.userGetAllPublicationTypes(types => {
            const publicationTypes = {};
            (types ? types : []).forEach(item => publicationTypes[item._id] = item.type);
            this.setState({ publicationTypes });
        });

        setTimeout(T.ftcoAnimate, 250);
    }

    render() {
        const language = T.language(texts);
        const projectLevels = this.state.projectLevels ? this.state.projectLevels : {};
        const publicationTypes = this.state.publicationTypes ? this.state.publicationTypes : {};
        let staff = this.state.staff ? this.state.staff : { email: '@', divisions: [], projects: [], publications: [] },
            firstPartOfEmail = staff.email.substring(0, staff.email.indexOf('@')),
            departmentText = '';
        staff.divisions.forEach(item => departmentText += ', ' + T.language.parse(item.name));
        if (departmentText.length > 0) departmentText = departmentText.substring(2);

        const titles = [];
        if (T.language.current() == 'en') {
            if (staff.academicTitle || staff.academicDistinction) titles.push(<p key={0}>Title: <span>{language[staff.academicTitle]} {language[staff.academicDistinction]}</span></p>);
        } else {
            if (staff.academicDistinction) titles.push(<p key={0}>Học vị: <span>{language[staff.academicDistinction]}</span></p>);
            if (staff.academicTitle) titles.push(<p key={1}>Học hàm: <span>{language[staff.academicTitle]}</span></p>);
            if (staff.academicTitleYear) titles.push(<p key={2}>Năm phong học hàm: <span>{staff.academicTitleYear}</span></p>);
        }

        return (
            <section className='container ftco-section ftco-degree-bg' style={{ padding: '6px' }}>
                <div className='row ftco-animate mb-3'>
                    <div className='col-md-4'>
                        <div style={{ paddingTop: '100%', backgroundImage: `url(${staff.image})`, borderRadius: '50%', border: '5px solid #167ce9', backgroundSize: 'cover' }} />
                    </div>
                    <div className='col-md-8'>
                        <h2>{staff.firstname} {staff.lastname}</h2>
                        {titles}
                        {staff.phoneNumber ? <p>{language.phone}: <span>{staff.phoneNumber}</span></p> : ''}
                        {staff.email ? <p>Email: <a href={'mailto:' + staff.email}>{staff.email}</a></p> : ''}
                        Website: {staff.website ? <a href={staff.website} target='_blank'>{staff.website}</a> :
                            <Link to={'/personnel/' + firstPartOfEmail}>{T.rootUrl}/personnel/{firstPartOfEmail}</Link>}
                        {staff.facebook ? <p>Facebook: <a href={staff.facebook} target='_blank'>{staff.facebook}</a></p> : ''}
                    </div>
                </div>
                <div className='ftco-animate'>
                    <ul className='nav nav-tabs'>
                        <li className='nav-item'>
                            <a className='nav-link active show' data-toggle='tab' href='#staffDetailInformation'>{language.tabInformation}</a>
                        </li>
                        <li className='nav-item'>
                            <a className='nav-link' data-toggle='tab' href='#staffDetailBiography'>{language.tabBiography}</a>
                        </li>
                        <li className='nav-item'>
                            <a className='nav-link' data-toggle='tab' href='#staffDetailScienceProjects'>{language.tabScienceProjects}</a>
                        </li>
                        <li className='nav-item'>
                            <a className='nav-link' data-toggle='tab' href='#staffDetailPublications'>{language.tabPublications}</a>
                        </li>
                    </ul>

                    <div className='tab-content' style={{ marginTop: '12px' }}>
                        <div id='staffDetailInformation' className='tab-pane fade show active'>
                            {staff.birthday ? <p>{language.birthday}: <span>{staff.birthday ? new Date(staff.birthday).getDateText() : ''}</span></p> : ''}
                            {staff.sex ? <p>{language.sex}: <span>{language[staff.sex]}</span></p> : ''}
                            <p>{language.department}: <span>{departmentText}</span></p>
                            {staff.graduatedCountry ? <p>{language.graduatedCountry}: <span>{getName(staff.graduatedCountry)}</span></p> : ''}

                            {staff.masterThesis ? <p>{language.masterThesis}: <span>{staff.masterThesis}</span></p> : ''}
                            {staff.finishedDoctorThesis ? <p>{language.finishedDoctorThesis}: <span>{staff.finishedDoctorThesis}</span></p> : ''}
                            {staff.goingDoctorThesis ? <p>{language.goingDoctorThesis}: <span>{staff.goingDoctorThesis}</span></p> : ''}

                            {staff.trainingExpertise ? <p>{language.trainingExpertise}: <span>{T.language.parse(staff.trainingExpertise)}</span></p> : ''}
                            {staff.currentExpertise ? <p>{language.currentExpertise}: <span>{T.language.parse(staff.currentExpertise)}</span></p> : ''}

                            {staff.researchFields ? [<p key={0}>{language.researchFields}:</p>, <div key={1} style={{ paddingLeft: '32px' }} dangerouslySetInnerHTML={{ __html: T.language.parse(staff.researchFields) }} />] : ''}
                            {staff.postgraduateCourses ? [<p key={0}>{language.postgraduateCourses}:</p>, <div key={1} style={{ paddingLeft: '32px' }} dangerouslySetInnerHTML={{ __html: T.language.parse(staff.postgraduateCourses) }} />] : ''}
                        </div>

                        <div id='staffDetailBiography' className='tab-pane fade' dangerouslySetInnerHTML={{ __html: T.language.parse(staff.biography) }} />

                        <div id='staffDetailScienceProjects' className='tab-pane fade'>
                            <table className='table table-hover table-bordered' style={{ display: staff.projects.length > 0 ? 'block' : 'none' }}>
                                <thead>
                                    <tr>
                                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                                        <th style={{ width: '100%' }}>{language.title}</th>
                                        <th style={{ width: 'auto', textAlign: 'center' }}>{language.year}</th>
                                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>{language.role}</th>
                                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>{language.projectLevel}</th>
                                        <th style={{ width: 'auto' }} nowrap='true'>{language.finish}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {staff.projects.map((item, index) => (
                                        <tr key={index}>
                                            <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                            <td>{item.title}</td>
                                            <td style={{ whiteSpace: 'nowrap' }}>{item.startYear + ' - ' + item.endYear}</td>
                                            <td style={{ whiteSpace: 'nowrap' }}>{item.isLeader ? language.manager : language.member}</td>
                                            <td style={{ whiteSpace: 'nowrap' }}>{item.level && projectLevels[item.level] ? T.language.parse(projectLevels[item.level]) : ''}</td>
                                            <td className='toggle' style={{ textAlign: 'center' }}>{item.finished ? 'X' : ''}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <div id='staffDetailPublications' className='tab-pane fade'>
                            <table className='table table-hover table-bordered' style={{ display: staff.publications.length > 0 ? 'block' : 'none' }}>
                                <thead>
                                    <tr>
                                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                                        <th style={{ width: 'auto' }}>{language.year}</th>
                                        <th style={{ width: '100%' }}>{language.title}</th>
                                        <th style={{ width: 'auto', textAlign: 'center' }}>{language.type}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {staff.publications.map((item, index) => (
                                        <tr key={index}>
                                            <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                            <td style={{ textAlign: 'right' }}>{item.year}</td>
                                            <td>{item.title}</td>
                                            <td style={{ whiteSpace: 'nowrap' }}>{item.type && publicationTypes[item.type] ? T.language.parse(publicationTypes[item.type]) : ''}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ user: state.user });
const mapActionsToProps = { userGetStaff, userGetAllProjectLevels, userGetAllPublicationTypes };
export default connect(mapStateToProps, mapActionsToProps)(StaffDetail);