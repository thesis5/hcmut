import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_PAGE = 'user: getdsSauDaiHocTrongNuocInPage';

export default function dsSauDaiHocTrongNuocReducer(state = null, data) {
    switch (data.type) {
        case GET_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsSauDaiHocTrongNuocPage', true);
export function getdsSauDaiHocTrongNuocInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsSauDaiHocTrongNuocPage', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds-sdh-trong-nuoc/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, {condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updateDsSdhTrongNuoc(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-sdh-trong-nuoc';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getdsSauDaiHocTrongNuocInPage());
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'));
    }
}

export function createDsSdhTrongNuoc(data, done) {
    return dispatch => {
        const url = '/api/ds-sdh-trong-nuoc';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getdsSauDaiHocTrongNuocInPage());
            }
        }, () => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-sdh-trong-nuoc/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function deleteDsSdhTrongNuoc(_id) {
    return dispatch => {
        const url = '/api/ds-sdh-trong-nuoc';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa cán bộ công đoàn bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Cán bộ công đoàn được xóa thành công!', 'error', false, 800);
                dispatch(getdsSauDaiHocTrongNuocInPage());
            }
        }, error => T.notify('Xóa cán bộ công đoàn bị lỗi!', 'danger'));
    }
}