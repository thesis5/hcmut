import T from '../common/js/common';
T.initCookiePage('pageDsNhiemVu1CoiThi');

// Reducer -------------------------------------------------------------------------------------------------------------
const GET_DS_COITHI_NV1_IN_PAGE = 'dsCoithiNv1:getDsNhiemVu1CoiThiInPage';
const GET_DS_COITHI_NV1 = 'dsCoithiNv1:GetDsNhiemVu1CoiThi';
const GET_DS_COITHI_NV1_IN_PAGE_BY_USER = 'dsCoithiNv1:GetDsNhiemVu1CoiThiInPageByUser';
const GET_DS_COITHI_NV1_BY_USER = 'dsCoithiNv1:GetDsNhiemVu1CoiThiByUser';
const GET_DS_COITHI_NV1_NEWS_FEED = 'dsCoithiNv1:GetDsNhiemVu1CoiThiNewsFeed';

export default function dsCoithiNv1Reducer(state = null, data) {
    switch (data.type) {
        case GET_DS_COITHI_NV1_IN_PAGE:
            return Object.assign({}, state, { page: data.page });
        case GET_DS_COITHI_NV1:
            return Object.assign({}, state, { dsCoithiNv1: data.item });

        case GET_DS_COITHI_NV1_IN_PAGE_BY_USER:
            if (state == null || state.userCondition != data.condition) {
                return Object.assign({}, state, { userCondition: data.condition, userPage: data.page });
            } else {
                const userPage = Object.assign({}, data.page);
                userPage.list = state.userPage && state.userPage.list ? state.userPage.list.slice() : [];
                let _ids = userPage.list.map(item => item._id);
                if (data.page && data.page.list && data.page.list.length > 0) {
                    data.page.list.forEach(item => {
                        if (_ids.indexOf(item._id) == -1) {
                            _ids.push(item._id);
                            userPage.list.push(item);
                        }
                    });
                }
                return Object.assign({}, state, { userPage });
            }

        case GET_DS_COITHI_NV1_BY_USER:
            return Object.assign({}, state, { userDsNhiemVu1CoiThi: data.item });

        case GET_DS_COITHI_NV1_NEWS_FEED:
            return Object.assign({}, state, { newsFeed: data.list });

        default:
            return state;
    }
}

// Actions (admin) ----------------------------------------------------------------------------------------------------
export function getDsNhiemVu1CoiThiInPage(pageNumber, pageSize, pageCondition, done) {
    // const page = T.updatePage('pageDsNhiemVu1CoiThi', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = '/api/ds-coithi-nv1/page/' + page.pageNumber + '/' + page.pageSize;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy danh sách phòng bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '.', data.error);
    //         } else {
    //             if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
    //             dispatch({ type: GET_DS_COITHI_NV1_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy danh sách phòng bị lỗi!', 'danger'));
    // };
    const page = T.updatePage('pageDsNhiemVu1CoiThi', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds-coithi-nv1/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_DS_COITHI_NV1_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách phòng bị lỗi!', 'danger'));
    }
}

export function createDsNhiemVu1CoiThi(points, done) {
    return dispatch => {
        const url = '/api/ds-coithi-nv1';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getDsNhiemVu1CoiThiInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function updateDsNhiemVu1CoiThi(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-coithi-nv1/update';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin thành công!', 'info');
                dispatch(getDsNhiemVu1CoiThiInPage());
                done && done();
            }
        }, error => T.notify('Cập nhật thông tin bị lỗi!', 'danger'));
    }
}

export function swapDsNhiemVu1CoiThi(_id, isMoveUp) {
    return dispatch => {
        const url = '/api/ds-coithi-nv1/swap/';
        T.put(url, { _id, isMoveUp }, data => {
            if (data.error) {
                T.notify('Thay đổi thứ tự phòng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                T.notify('Thay đổi thứ tự phòng thành công!', 'info');
                dispatch(getDsNhiemVu1CoiThiInPage());
            }
        }, error => T.notify('Thay đổi thứ tự phòng bị lỗi!', 'danger'));
    }
}

export function deleteDsNhiemVu1CoiThi(_id) {
    return dispatch => {
        const url = '/api/ds-coithi-nv1/delete';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa thông tin bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Thay đổi được xóa thành công!', 'error', false, 800);
                dispatch(getDsNhiemVu1CoiThiInPage());
            }
        }, error => T.notify('Xóa thông tin bị lỗi!', 'danger'));
    }
}

export function getDsNhiemVu1CoiThi(_id, done) {
    return dispatch => {
        const url = '/api/ds-coithi-nv1/item/' + _id;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data);
                dispatch({ type: GET_DS_COITHI_NV1, item: data.item, categories: data.categories });
            }
        }, error => done({ error }));
    }
}

export function getDsNhiemVu1CoiThiWithQuestion(_id, done) {
    return dispatch => {
        const url = '/api/ds-coithi-nv1/item-question/' + _id;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_COITHI_NV1, item: data.item, categories: data.categories });
                if (done) done(data);
            }
        }, error => done({ error }));
    }
}

// Actions (editor) ---------------------------------------------------------------------------------------------------
export function getDsNhiemVu1CoiThiInPageByEditor(pageNumber, pageSize, done) {
    const page = T.updatePage('pageDsNhiemVu1CoiThi', pageNumber, pageSize);
    return dispatch => {
        const url = '/editor/dsCoithiNv1/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_DS_COITHI_NV1_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách phòng bị lỗi!', 'danger'));
    };
}



export function updateMultiDsNhiemVu1CoiThi(points, done) {
    return dispatch => {
        const url = '/api/user/ds-coithi-nv1-multi';
        T.put(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu tổng hợp bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                dispatch(getDsNhiemVu1CoiThiInPage());
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu tổng hợp bị lỗi!', 'danger'))
    }
}
// Actions (user) -----------------------------------------------------------------------------------------------------
const texts = {
    vi: {
        getDsNhiemVu1CoiThisInPageByUserError: 'Lấy danh sách công việc bị lỗi!',
        getDsNhiemVu1CoiThisByUserError: 'Lấy công việc bị lỗi!',
        getDsNhiemVu1CoiThisFeedError: 'Lấy dsCoithiNv1 feed bị lỗi!',
    },
    en: {
        getDsNhiemVu1CoiThisInPageByUserError: 'Errors when get dsCoithiNv1s list!',
        getDsNhiemVu1CoiThisByUserError: 'Errors when get one dsCoithiNv1!',
        getDsNhiemVu1CoiThisFeedError: 'Errors when get dsCoithiNv1s feed!',
    }
};

const language = T.language(texts);

export function getDsNhiemVu1CoiThiInPageByUser(pageNumber, pageSize) {
    return dispatch => {
        const url = '/dsCoithiNv1/page/' + pageNumber + '/' + pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getDsNhiemVu1CoiThisInPageByUserError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_COITHI_NV1_IN_PAGE_BY_USER, page: data.page });
            }
        }, error => T.notify(language.getDsNhiemVu1CoiThisInPageByUserError, 'danger'));
    }
}

export function getDsNhiemVu1CoiThiByUser(dsCoithiNv1Id, dsCoithiNv1Link, done) {
    return dispatch => {
        const url = dsCoithiNv1Id ? '/ds-coithi-nv1/item/id/' + dsCoithiNv1Id : '/dsCoithiNv1/item/link/' + dsCoithiNv1Link;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getDsNhiemVu1CoiThisByUserError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_COITHI_NV1_BY_USER, item: data.item });
                done && done(data);
            }
        }, error => T.notify(language.getDsNhiemVu1CoiThisByUserError, 'danger'));
    }
}

export function getDsNhiemVu1CoiThiFeed() {
    return dispatch => {
        const url = '/ds-coithi-nv1/page/1/' + T.newsFeedPageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getDsNhiemVu1CoiThisFeedError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_COITHI_NV1_NEWS_FEED, list: data.page.list });
            }
        }, error => T.notify(language.getDsNhiemVu1CoiThisFeedError, 'danger'));
    }
}

export function checkLink(_id, link) {
    return dispatch => {
        const url = '/ds-coithi-nv1/item/check-link';
        T.put(url, { _id, link }, data => {
            if (data.error) {
                T.notify('Link không hợp lệ!', 'danger');
                console.error('PUT: ' + url + '.', error);
            } else {
                T.notify('Link hợp lệ!', 'success');
            }
        }, error => T.notify('Kiểm tra Link bị lỗi!', 'danger'));
    }
}
