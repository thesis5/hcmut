import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const DS_TAP_SU_IN_PAGE = 'DSTS:getDsTapSuInPage';

export default function dsTapSuReducer(state = null, data) {
    switch (data.type) {
        case DS_TAP_SU_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsTapSuPage', true);
export function getDsTapSuInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsTapSuPage', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = `/api/ds-tap-su/page/${page.pageNumber}/${page.pageSize}`;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy dữ liệu bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '. ' + data.error);
    //         } else {
    //             if (done) done(data.page);
    //             dispatch({ type: DS_TAP_SU_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    // }
    return dispatch => {
        const url = `/api/ds-tap-su/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: DS_TAP_SU_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updateDsTapSu(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-tap-su';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDsTapSuInPage());
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-tap-su/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function createDsTapSu(points, done) {
    return dispatch => {
        const url = '/api/ds-tap-su';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getDsTapSuInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteDsTapSu(_id, done) {
    return dispatch => {
        const url = '/api/ds-tap-su';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getDsTapSuInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}