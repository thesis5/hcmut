import T from '../common/js/common';

T.initCookiePage('pageEventRegister');

// Reducer -------------------------------------------------------------------------------------------------------------
const GET_ANSWER_LIST = 'answer:getAnswersByQuestions';
const GET_POST_ANSWER_LIST = 'answer:getAnswersByPost';
const ADD_ANSWER_BY_ADMIN = 'answer:addAnswerByAdmin';
const GET_ANSWERS_IN_PAGE = 'answer:getAnswersInPage';

export default function answerReducer(state = null, data) {
    switch (data.type) {
        case GET_ANSWER_LIST:
            return Object.assign({}, state, { userAnswers: data.items });
        
        case GET_POST_ANSWER_LIST:
            return Object.assign({}, state, { answers: data.page });
        
        case GET_ANSWERS_IN_PAGE:
            const source2 = {};
            source2[data.field] = data[data.field];
            return Object.assign({}, state, source2);
        
        case ADD_ANSWER_BY_ADMIN:
            let answers = state.answers ? state.answers : [];
            if (data.answer) answers.push(data.answer);
            return Object.assign({}, state, { answers: answers });
        default:
            return state;
    }
}

T.initCookiePage('pageEventRegistration');

export function adminGetEventRegistrationInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pageEventRegistration', pageNumber, pageSize);
    return (dispatch, getState) => {
        const state = getState(),
            eventId = state && state.event && state.event.event && state.event.event._id ? state.event.event._id : null;
        const url = `/api/answer-page/${eventId}/event:read/questions/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách câu trả lời bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                const dispatcher = {};
                dispatcher.type = GET_ANSWERS_IN_PAGE;
                dispatcher.questions = data.page;
                dispatcher.field = 'questions';
                dispatch(dispatcher);
                done && done(data.page);
            }
        }, () => T.notify('Lấy danh sách câu trả lời bị lỗi!', 'danger'));
    }
}

T.initCookiePage('pageJobRegistration');

export function adminGetJobRegistrationInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pageJobRegistration', pageNumber, pageSize);
    return (dispatch, getState) => {
        const state = getState(),
            jobId = state && state.job && state.job.job && state.job.job._id ? state.job.job._id : null;
        const url = `/api/answer-page/${jobId}/job:read/questions/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách câu trả lời bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                const dispatcher = {};
                dispatcher.type = GET_ANSWERS_IN_PAGE;
                dispatcher.questions = data.page;
                dispatcher.field = 'questions';
                dispatch(dispatcher);
                done && done(data.page);
            }
        }, () => T.notify('Lấy danh sách câu trả lời bị lỗi!', 'danger'));
    }
}

export function getAnswer(_id, permission, done) {
    return dispatch => {
        const url = `/api/answer/item/${permission}/${_id}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy câu trả lời bị lỗi', 'danger');
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Lấy câu trả lời bị lỗi', 'danger'));
    }
}

export function searchUserFromSystem(email, done) {
    return dispatch => {
        const url = `/api/user-search/${email}`;
        T.get(url, data => {
            done && done(data);
        }, () => done && done({error: true}));
    }
}

export function addAnswer(newData, getPageType, permission, done) {
    return dispatch => {
        const url = '/api/answer';
        T.post(url, { newData, permission }, data => {
            if (data.error) {
                T.notify('Thêm câu trả lời bị lỗi!', 'danger');
                console.error('POST: ' + url + '.', data.error);
            } else if (data.exist) {
                T.notify('Người dùng đã có trong danh sách đăng ký!', 'warning');
            } else {
                switch (getPageType) {
                    case 'eventQuestions':
                        dispatch(adminGetEventRegistrationInPage());
                        break;
        
                    case 'jobQuestions':
                        dispatch(adminGetJobRegistrationInPage());
                        break;
        
                    default:
                        break;
                }
                done && done(data.item);
            }
        }, error => T.notify('Thêm câu trả lời bị lỗi!', 'danger'));
    }
}

export function updateAnswer(_id, changes, getPageType, permission, done) {
    return dispatch => {
        const url = '/api/answer';
        T.put(url, { _id, changes, permission }, data => {
            if (data.error) {
                T.notify('Cập nhật câu trả lời bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                switch (getPageType) {
                    case 'eventQuestions':
                        dispatch(adminGetEventRegistrationInPage());
                        break;
                    
                    case 'jobQuestions':
                        dispatch(adminGetJobRegistrationInPage());
                        break;
                    
                    default:
                        break;
                }
                done && done(data);
            }
        }, () => T.notify('Cập nhật câu trả lời bị lỗi!', 'danger'));
    }
}

export function deleteAnswer(_id, getPageType, permission, done) {
    return dispatch => {
        const url = '/api/answer';
        T.delete(url, { _id, permission }, data => {
            if (data.error) {
                T.notify('Xoá câu trả lời bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Xoá câu trả lời thành công!', 'success', false, 1000);
                switch (getPageType) {
                    case 'eventQuestions':
                        dispatch(adminGetEventRegistrationInPage());
                        break;
                    
                    case 'jobQuestions':
                        dispatch(adminGetJobRegistrationInPage());
                        break;
                    
                    default:
                        break;
                }
                done && done(data);
            }
        }, error => T.alert('Xoá câu trả lời bị lỗi!', 'error'));
    }
}

export function deleteManyAnswerByAdmin(postId, done) {
    const url = '/api/answer/post/' + postId;
    T.delete(url, data => {
        if (data.error) {
            T.notify('Xoá câu trả lời bị lỗi!', 'danger');
            console.error('DELETE: ' + url + '.', data.error);
        } else {
            if (done) done(data);
            T.notify('Xoá câu trả lời thành công!', 'success');
        }
    }, error => T.notify('Xoá câu trả lời bị lỗi!', 'danger'));
}

export function importRegisters(postId, field, questions, getPageType, permission, done) {
    return dispatch => {
        const url = '/api/answer/import';
        T.post(url, { postId, field, questions, permission }, data => {
            if (data.error || !data.success) {
                T.notify(data.error, 'error');
            } else {
                T.notify('Tải lên thành công!','success');
                switch (getPageType) {
                    case 'eventQuestions':
                        dispatch(adminGetEventRegistrationInPage());
                        break;
        
                    case 'jobQuestions':
                        dispatch(adminGetJobRegistrationInPage());
                        break;
        
                    default:
                        break;
                }
                done && done(data);
            }
        }, error => console.error(error));
    }
}

export function exportRegisters( postType, postId, field, permission, eventName) {
    return dispatch => {
        T.download(T.url(`/api/answer/export/${postType}/${postId}/${field}/${permission}`), eventName + '.xlsx');
    }
}
// Actions (user) -----------------------------------------------------------------------------------------------------
export function addAnswerByUser(newData, postType) {
    return dispatch => {
        const url = '/answer';
        T.post(url, { newData, postType }, data => {
            if (data.error) {
                T.notify(data.error, 'danger');
                console.error('POST: ' + url + '.', data.error);
            } else if (data.exist) {
                T.notify('Bạn đã đăng ký tham gia!', 'warning');
            } else {
                T.notify('Đăng ký tham gia thành công!', 'info');
            }
        }, error => T.notify('Đăng ký bị lỗi!', 'danger'));
    }
}

export function countAnswer(postId, field, done) {
    return dispatch => {
        const url = `/answer/count/${postId}/${field}`;
        T.get(url, data => {
            if (data.error) {
                console.error('GET: ' + url + ' has error!');
            } else {
                done && done(data.total);
            }
        }, () => console.error('GET: ' + url + ' has error!'));
    }
}