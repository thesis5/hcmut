import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_USER_POINT_IN_PAGE = 'user:getUserPointInPage';
const UPDATE_POINT = 'user:UpdatePoint';

export default function pointReducer(state = null, data) {
    switch (data.type) {
        case GET_USER_POINT_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        case UPDATE_POINT:
            if (state) {
                let updatedPage = Object.assign({}, state.page),
                    updatedItem = data.item;

                if (updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { page: updatedPage });
            } else {
                return null;
            }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('adminUserPoint', true);
export function getUserPointInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('adminUserPoint', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/year-point/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách điểm người dùng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_USER_POINT_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách điểm người dùng bị lỗi!', 'danger'));
    }
}
export function updateYearPoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/user/summary/year';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật điểm tổng hợp bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getUserPointInPage());
            }
        }, () => T.notify('Cập nhật điểm tổng hợp bị lỗi!', 'danger'));
    }
}
export function updateMultiYearPoint(points, done) {
    return dispatch => {
        const url = '/api/user/summary/year-multi';
        T.put(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật điểm tổng hợp bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật điểm tổng hợp bị lỗi!', 'danger'))
    }
}

export function professional2GetPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('adminUserPoint', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/permission2/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách điểm người dùng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_USER_POINT_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách điểm người dùng bị lỗi!', 'danger'));
    }
}

export function updateBonusPoint(userId, changes, done) {
    return dispatch => {
        const url = '/api/user/summary/bonus';
        T.put(url, { userId, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật điểm thưởng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                done && done(data.item);
                dispatch(getUserPointInPage());
            }
        }, () => T.notify('Cập nhật điểm thưởng bị lỗi!', 'danger'));
    }
}

export function updateProfessionalPoint(userId, changes, done) {
    return dispatch => {
        const url = '/api/user/summary/professional';
        T.put(url, { userId, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật điểm chuyên môn bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                done && done(data.item);
                dispatch(getUserPointInPage());
            }
        }, () => T.notify('Cập nhật điểm chuyên môn bị lỗi!', 'danger'));
    }
}

export function updateMultiBonusPoint(points, done) {
    return dispatch => {
        const url = '/api/user/summary/bonus-multi';
        T.put(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật điểm thưởng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật điểm thưởng bị lỗi!', 'danger'))
    }
}

export function updateMultiProfessionalPoint(points, done) {
    return dispatch => {
        const url = '/api/user/summary/professional-multi';
        T.put(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật điểm chuyên môn bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật điểm chuyên môn bị lỗi!', 'danger'))
    }
}

export function calculateYearPoint(done) {
    return dispatch => {
        const url = '/api/user/summary/calculate';
        T.put(url, {}, data => {
            if (data.error) {
                T.notify('Tính điểm tổng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                done && done();
                dispatch(getUserPointInPage());
            }
        }, () => T.notify('Tính điểm tổng bị lỗi!', 'danger'))
    }
}

export function changePoint(point) {
    return { type: UPDATE_POINT, item: point };
}
