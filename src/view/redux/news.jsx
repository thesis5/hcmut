import T from '../common/js/common';
T.initCookiePage('pageNews');
T.initCookiePage('pageDraftNews');

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_NEWS_IN_PAGE = 'news:getNewsInPage';
const GET_DRAFT_NEWS_IN_PAGE = 'news:getDraftNewsInPage';
const GET_NEWS = 'news:GetNews';
const GET_DRAFT_NEWS = 'news:GetDraftNews';

const GET_NEWS_IN_PAGE_BY_USER = 'news:GetNewsInPageByUser';
const GET_NEWS_BY_USER = 'news:GetNewsByUser';
const GET_NEWS_FEED = 'news:GetNewsFeed';

export default function newsReducer(state = null, data) {
    switch (data.type) {
        case GET_NEWS_IN_PAGE:
            return Object.assign({}, state, { page: data.page });
        case GET_DRAFT_NEWS_IN_PAGE:
            return Object.assign({}, state, { draft: data.page });
        case GET_NEWS:
            return Object.assign({}, state, { news: data.item, categories: data.categories, docDraftUser: data.docDraftUser });
        case GET_DRAFT_NEWS:
            return Object.assign({}, state, { draftNews: data.item, categories: data.categories });

        case GET_NEWS_IN_PAGE_BY_USER:
            if (state == null || state.userCondition != data.condition) {
                return Object.assign({}, state, { userCondition: data.condition, userPage: data.page });
            } else {
                const userPage = Object.assign({}, data.page);
                userPage.list = state.userPage && state.userPage.list ? state.userPage.list.slice() : [];
                let _ids = userPage.list.map(item => item._id);
                if (data.page && data.page.list && data.page.list.length > 0) {
                    data.page.list.forEach(item => {
                        if (_ids.indexOf(item._id) == -1) {
                            _ids.push(item._id);
                            userPage.list.push(item);
                        }
                    });
                }
                return Object.assign({}, state, { userPage });
            }

        case GET_NEWS_BY_USER:
            return Object.assign({}, state, { userNews: data.item });

        case GET_NEWS_FEED:
            return Object.assign({}, state, { newsFeed: data.list });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
export function getNewsInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pageNews', pageNumber, pageSize);
    return (dispatch) => {
        const url = '/api/news/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách tin tức bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_NEWS_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách tin tức bị lỗi!', 'danger'));
    }
}

export function getDraftNewsInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pageDraftNews', pageNumber, pageSize);
    return (dispatch) => {
        const url = '/api/draft-news/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách bản nháp tin tức bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_DRAFT_NEWS_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách tin tức bị lỗi!', 'danger'));
    }
}
export function draftToNews(draftNewsId, done) {
    return dispatch => {
        const url = '/api/draft-news/toNews/' + draftNewsId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Thao tác bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                T.notify('Bản nháp đã được duyệt thành công!', 'info');
                dispatch(getDraftNewsInPage());
                dispatch(getNewsInPage());
            }
        }, error => T.notify('Thao tác bị lỗi bị lỗi!', 'danger'));
    }
}

export function createNews(done) {
    return dispatch => {
        const url = '/api/news/default';
        T.post(url, data => {
            if (data.error) {
                T.notify('Tạo tin tức bị lỗi!', 'danger');
                console.error('POST: ' + url + '.', data.error);
            } else {
                dispatch(getNewsInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo tin tức bị lỗi!', 'danger'));
    }
}
export function createDraftNewsDefault(done) {
    return (dispatch, getState) => {
        const state = getState();
        const docData = {
            categories: [],
            link: '',
            active: false,
            abstract: JSON.stringify({ vi: '', en: '' }),
            content: JSON.stringify({ vi: '', en: '' }),
        }, passValue = {
            title: '{\"vi\":\"Bản nháp\",\"en\":\"Draft\"}',
            editorId: state.system.user._id,
            documentType: 'news',
            documentJson: JSON.stringify(docData),
            editorName: state.system.user.firstname,
        }
        const url = '/api/news/draft';
        T.post(url, passValue, data => {
            if (data.error) {
                T.notify('Tạo bản nháp tin tức bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Bản nháp tin tức đã tạo thành công!', 'info');
                dispatch(getDraftNewsInPage());
                done && done(data);
            }
        })
    }
}
export function createDraftNews(result, done) {
    return dispatch => {
        const url = '/api/news/draft';
        T.post(url, result, data => {
            if (data.error) {
                T.notify('Tạo bản nháp tin tức bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Bản nháp tin tức đã tạo thành công!', 'info');
                dispatch(getDraftNewsInPage());
                done && done();
            }
            if (done) done(data);
        })
    }
}

export function updateNews(_id, changes, done) {
    return dispatch => {
        const url = '/api/news';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin tin tức bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin tin tức thành công!', 'info');
                dispatch(getNewsInPage());
                done && done();
            }
        }, error => T.notify('Cập nhật thông tin tin tức bị lỗi!', 'danger'));
    }
}
export function updateDraftNews(_id, changes, done) {
    return dispatch => {
        const url = '/api/draft-news';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin bản nháp tin tức bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin bản nháp tin tức thành công!', 'info');
                dispatch(getDraftNewsInPage());
                done && done();
            }
        }, () => T.notify('Cập nhật thông tin bản nháp tin tức bị lỗi!', 'danger'));
    }
}

export function swapNews(_id, isMoveUp) {
    return dispatch => {
        const url = '/api/news/swap/';
        T.put(url, { _id, isMoveUp }, data => {
            if (data.error) {
                T.notify('Thay đổi thứ tự tin tức bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                T.notify('Thay đổi thứ tự tin tức thành công!', 'info');
                dispatch(getNewsInPage());
            }
        }, error => T.notify('Thay đổi thứ tự tin tức bị lỗi!', 'danger'));
    }
}

export function deleteNews(_id) {
    return dispatch => {
        const url = '/api/news';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa tin tức bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Người dùng được xóa thành công!', 'error', false, 800);
                dispatch(getNewsInPage());
            }
        }, error => T.notify('Xóa tin tức bị lỗi!', 'danger'));
    }
}
export function deleteDraftNews(_id) {
    return dispatch => {
        const url = '/api/draft-news';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa mẫu tin tức bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Người dùng được xóa thành công!', 'error', false, 800);
                dispatch(getDraftNewsInPage());
            }
        }, error => T.notify('Xóa bản nháp bị lỗi!', 'danger'));
    }
}

export function getNews(_id, done) {
    return (dispatch, getState) => {
        const url = '/api/news/item/' + _id;
        const state = getState();
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy tin tức bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                const url2 = '/api/draft/news/' + state.system.user._id;
                T.get(url2, draft => {
                    if (done) done(data);
                    dispatch({ type: GET_NEWS, item: data.item, categories: data.categories, docDraftUser: draft });
                }, error => T.notify('Lấy danh sách tin tức bị lỗi!', 'danger'))
                if (done) done(data);
            }
        }, error => done({ error }));
    }
}
export function getDraftNews(_id, done) {
    return dispatch => {
        const url = '/api/draft-news/item/' + _id;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy tin tức bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data);
                dispatch({ type: GET_DRAFT_NEWS, item: data.item, categories: data.categories });
            }
        }, error => done({ error }));
    }
}

// Actions (user) -----------------------------------------------------------------------------------------------------
const texts = {
    vi: {
        getNewsInPageByUserError: 'Lấy danh sách tin tức bị lỗi!',
        getNewsByUserError: 'Lấy tin tức bị lỗi!',
        getNewsFeedError: 'Lấy new feed bị lỗi!',
    },
    en: {
        getNewsInPageByUserError: 'Errors when get news list!',
        getNewsByUserError: 'Errors when get one news!',
        getNewsFeedError: 'Errors when get news feed!',
    }
};
const language = T.language(texts);

export function getNewsInPageByUser(pageNumber, pageSize) {
    return dispatch => {
        const url = '/news/page/' + pageNumber + '/' + pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getNewsInPageByUserError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_NEWS_IN_PAGE_BY_USER, page: data.page });
            }
        }, error => T.notify(language.getNewsInPageByUserError, 'danger'));
    }
}

export function getNewsByUser(newsId, newsLink, done) {
    return dispatch => {
        const url = newsId ? '/news/item/id/' + newsId : '/news/item/link/' + newsLink;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getNewsByUserError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_NEWS_BY_USER, item: data.item });
                done && done(data);
            }
        }, error => T.notify(language.getNewsByUserError, 'danger'));
    }
}

export function getNewsFeed() {
    return dispatch => {
        const url = '/news/page/1/' + T.newsFeedPageSize
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getNewsFeedError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_NEWS_FEED, list: data.page.list });
            }
        }, error => T.notify(language.getNewsFeedError, 'danger'));
    }
}

export function checkLink(_id, link) {
    return dispatch => {
        const url = '/news/item/check-link';
        T.put(url, { _id, link }, data => {
            if (data.error) {
                T.notify('Link không hợp lệ!', 'danger');
                console.error('PUT: ' + url + '.', error);
            } else {
                T.notify('Link hợp lệ!', 'success');
            }
        }, error => T.notify('Kiểm tra Link bị lỗi!', 'danger'));
    }
}