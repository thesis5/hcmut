import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_PAGE = 'user: getDmChucVuInPage';

export default function dmChucVuInPageReducer(state = null, data) {
    switch (data.type) {
        case GET_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('DmChucVuPage', true);
export function getDmChucVuInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('DmChucVuPage', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/dm-chucvu/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, {condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updateDmChucVu(_id, changes, done) {
    return dispatch => {
        const url = '/api/dm-chucvu';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDmChucVuInPage());
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'));
    }
}

export function createDmChucVu(data, done) {
    return dispatch => {
        const url = '/api/dm-chucvu';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDmChucVuInPage());
            }
        }, () => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function createMultipleDmChucVu(points, done) {
    return dispatch => {
        const url = '/api/dm-chucvu/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function deleteDmChucVu(_id) {
    return dispatch => {
        const url = '/api/dm-chucvu';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa danh mục chức vụ bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Danh mục chức vụ được xóa thành công!', 'error', false, 800);
                dispatch(getDmChucVuInPage());
            }
        }, error => T.notify('Xóa danh mục chức vụ bị lỗi!', 'danger'));
    }
}
