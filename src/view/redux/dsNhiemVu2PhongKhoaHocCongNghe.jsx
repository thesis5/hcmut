import T from '../common/js/common';
T.initCookiePage('pageDs_nv2_pkhcn');

// Reducer -------------------------------------------------------------------------------------------------------------
const GET_DS_NV2_PKHCN_IN_PAGE = 'ds_nv2_pkhcn:getDs_nv2_pkhcnInPage';
const GET_DS_NV2_PKHCN = 'ds_nv2_pkhcn:GetDs_nv2_pkhcn';
const GET_DS_NV2_PKHCN_IN_PAGE_BY_USER = 'ds_nv2_pkhcn:GetDs_nv2_pkhcnInPageByUser';
const GET_DS_NV2_PKHCN_BY_USER = 'ds_nv2_pkhcn:GetDs_nv2_pkhcnByUser';
const GET_DS_NV2_PKHCN_NEWS_FEED = 'ds_nv2_pkhcn:GetDs_nv2_pkhcnNewsFeed';

export default function ds_nv2_pkhcnReducer(state = null, data) {
    switch (data.type) {
        case GET_DS_NV2_PKHCN_IN_PAGE:
            return Object.assign({}, state, { page: data.page });
        case GET_DS_NV2_PKHCN:
            return Object.assign({}, state, { ds_nv2_pkhcn: data.item });

        case GET_DS_NV2_PKHCN_IN_PAGE_BY_USER:
            if (state == null || state.userCondition != data.condition) {
                return Object.assign({}, state, { userCondition: data.condition, userPage: data.page });
            } else {
                const userPage = Object.assign({}, data.page);
                userPage.list = state.userPage && state.userPage.list ? state.userPage.list.slice() : [];
                let _ids = userPage.list.map(item => item._id);
                if (data.page && data.page.list && data.page.list.length > 0) {
                    data.page.list.forEach(item => {
                        if (_ids.indexOf(item._id) == -1) {
                            _ids.push(item._id);
                            userPage.list.push(item);
                        }
                    });
                }
                return Object.assign({}, state, { userPage });
            }

        case GET_DS_NV2_PKHCN_BY_USER:
            return Object.assign({}, state, { userDs_nv2_pkhcn: data.item });

        case GET_DS_NV2_PKHCN_NEWS_FEED:
            return Object.assign({}, state, { newsFeed: data.list });

        default:
            return state;
    }
}

// Actions (admin) ----------------------------------------------------------------------------------------------------
export function getDs_nv2_pkhcnInPage(pageNumber, pageSize, pageCondition, done) {    
    const page = T.updatePage('pageDs_nv2_pkhcn', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = '/api/ds_nv2_pkhcn/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} },data => {
            if (data.error) {
                T.notify('Lấy danh sách phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_DS_NV2_PKHCN_IN_PAGE, page: data.page });                
            }
        }, error => T.notify('Lấy danh sách phòng bị lỗi!', 'danger'));
    };
}

export function createDs_nv2_pkhcn(points, done) {
    return dispatch => {
        const url = '/api/ds_nv2_pkhcn';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getDs_nv2_pkhcnInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function updateDs_nv2_pkhcn(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds_nv2_pkhcn/update';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin phòng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin phòng thành công!', 'info');
                dispatch(getDs_nv2_pkhcnInPage());
                done && done();
            }
        }, error => T.notify('Cập nhật thông tin phòng bị lỗi!', 'danger'));
    }
}

export function deleteDs_nv2_pkhcn(_id) {
    return dispatch => {
        const url = '/api/ds_nv2_pkhcn/delete';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa thông tin bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Dữ liệu được xóa thành công!', 'error', false, 800);
                dispatch(getDs_nv2_pkhcnInPage());
            }
        }, error => T.notify('Xóa thông tin bị lỗi!', 'danger'));
    }
}

export function getDs_nv2_pkhcn(_id, done) {
    return dispatch => {
        const url = '/api/ds_nv2_pkhcn/item/' + _id;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data);
                dispatch({ type: GET_DS_NV2_PKHCN, item: data.item, categories: data.categories });
            }
        }, error => done({ error }));
    }
}

export function getDs_nv2_pkhcnWithQuestion(_id, done) {
    return dispatch => {
        const url = '/api/ds_nv2_pkhcn/item-question/' + _id;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_NV2_PKHCN, item: data.item, categories: data.categories });
                if (done) done(data);
            }
        }, error => done({ error }));
    }
}

// Actions (editor) ---------------------------------------------------------------------------------------------------
export function getDs_nv2_pkhcnInPageByEditor(pageNumber, pageSize, done) {
    const page = T.updatePage('pageDs_nv2_pkhcn', pageNumber, pageSize);
    return dispatch => {
        const url = '/editor/ds_nv2_pkhcn/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_DS_NV2_PKHCN_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách phòng bị lỗi!', 'danger'));
    };
}

export function getDs_nv2_pkhcnByEditor(_id, done) {
    return dispatch => {
        const url = '/editor/ds_nv2_pkhcn/item/' + _id;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy phòng bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data);
                dispatch({ type: GET_DS_NV2_PKHCN, item: data.item, categories: data.categories });
            }
        }, error => done({ error }));
    }
}

export function swapDs_nv2_pkhcnByEditor(_id, isMoveUp) {
    return dispatch => {
        const url = '/editor/ds_nv2_pkhcn/swap/';
        T.put(url, { _id, isMoveUp }, data => {
            if (data.error) {
                T.notify('Thay đổi thứ tự phòng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                T.notify('Thay đổi thứ tự phòng thành công!', 'info');
                dispatch(getDs_nv2_pkhcnInPageByEditor());
            }
        }, error => T.notify('Thay đổi thứ tự phòng bị lỗi!', 'danger'));
    }
}

export function changeDs_nv2_pkhcnActiveByEditor(_id, active) {
    return dispatch => {
        const url = '/editor/ds_nv2_pkhcn/active/';
        T.put(url, { _id, active }, data => {
            if (data.error) {
                T.notify('Thay đổi trạng thái phòng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                T.notify('Thay đổi trạng thái phòng thành công!', 'info');
                dispatch(getDs_nv2_pkhcnInPageByEditor());
            }
        }, error => T.notify('Thay đổi trạng thái phòng bị lỗi!', 'danger'));
    }
}
export function updateMultiDs_nv2_pkhcn(points, done) {
    return dispatch => {
        const url = '/api/user/ds_nv2_pkhcn-multi';
        T.put(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu tổng hợp bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                dispatch(getDs_nv2_pkhcnInPage());
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu tổng hợp bị lỗi!', 'danger'))
    }
}
// Actions (user) -----------------------------------------------------------------------------------------------------
const texts = {
    vi: {
        getDs_nv2_pkhcnsInPageByUserError: 'Lấy danh sách công việc bị lỗi!',
        getDs_nv2_pkhcnsByUserError: 'Lấy công việc bị lỗi!',
        getDs_nv2_pkhcnsFeedError: 'Lấy ds_nv2_pkhcn feed bị lỗi!',
    },
    en: {
        getDs_nv2_pkhcnsInPageByUserError: 'Errors when get ds_nv2_pkhcns list!',
        getDs_nv2_pkhcnsByUserError: 'Errors when get one ds_nv2_pkhcn!',
        getDs_nv2_pkhcnsFeedError: 'Errors when get ds_nv2_pkhcns feed!',
    }
};

const language = T.language(texts);

export function getDs_nv2_pkhcnInPageByUser(pageNumber, pageSize) {
    return dispatch => {
        const url = '/ds_nv2_pkhcn/page/' + pageNumber + '/' + pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getDs_nv2_pkhcnsInPageByUserError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_NV2_PKHCN_IN_PAGE_BY_USER, page: data.page });
            }
        }, error => T.notify(language.getDs_nv2_pkhcnsInPageByUserError, 'danger'));
    }
}

export function getDs_nv2_pkhcnByUser(ds_nv2_pkhcnId, ds_nv2_pkhcnLink, done) {
    return dispatch => {
        const url = ds_nv2_pkhcnId ? '/ds_nv2_pkhcn/item/id/' + ds_nv2_pkhcnId : '/ds_nv2_pkhcn/item/link/' + ds_nv2_pkhcnLink;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getDs_nv2_pkhcnsByUserError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_NV2_PKHCN_BY_USER, item: data.item });
                done && done(data);
            }
        }, error => T.notify(language.getDs_nv2_pkhcnsByUserError, 'danger'));
    }
}

export function getDs_nv2_pkhcnFeed() {
    return dispatch => {
        const url = '/ds_nv2_pkhcn/page/1/' + T.newsFeedPageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getDs_nv2_pkhcnsFeedError, 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                dispatch({ type: GET_DS_NV2_PKHCN_NEWS_FEED, list: data.page.list });
            }
        }, error => T.notify(language.getDs_nv2_pkhcnsFeedError, 'danger'));
    }
}

export function checkLink(_id, link) {
    return dispatch => {
        const url = '/ds_nv2_pkhcn/item/check-link';
        T.put(url, { _id, link }, data => {
            if (data.error) {
                T.notify('Link không hợp lệ!', 'danger');
                console.error('PUT: ' + url + '.', error);
            } else {
                T.notify('Link hợp lệ!', 'success');
            }
        }, error => T.notify('Kiểm tra Link bị lỗi!', 'danger'));
    }
}
