import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const QT_TAP_SU_IN_PAGE = 'user:getQtTapSuInPage';

export default function qtTapSuReducer(state = null, data) {
    switch (data.type) {
        case QT_TAP_SU_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('qtTapSuPage', true);
export function getQtTapSuInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('qtTapSuPage', pageNumber, pageSize);
    return dispatch => {
        const url = `/api/qt-tap-su/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page);
                dispatch({ type: QT_TAP_SU_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updatePoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/qt-tap-su';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getQtTapSuInPage());
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/qt-tap-su/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}
