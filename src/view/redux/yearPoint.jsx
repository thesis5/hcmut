import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_PAGE = 'yearPoint:getPage';
const UPDATE = 'yearPoint:update';

export default function pointReducer(state = null, data) {
    switch (data.type) {
        case GET_PAGE:
            return Object.assign({}, state, { page: data.page });

        case UPDATE:
            if (state) {
                let updatedPage = Object.assign({}, state.page),
                    updatedItem = data.item;

                if (updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { page: updatedPage });
            } else {
                return null;
            }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('yearPoint', true);
export function getYearPointInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('yearPoint', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/year-point/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách điểm người dùng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách điểm người dùng bị lỗi!', 'danger'));
    }
}
export function updateYearPoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/year-point';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật điểm tổng hợp bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getYearPointInPage());
            }
        }, () => T.notify('Cập nhật điểm tổng hợp bị lỗi!', 'danger'));
    }
}

export function calculateYearPoint(done) {
    return dispatch => {
        const url = '/api/year-point/calculate';
        T.put(url, {}, data => {
            if (data.error) {
                T.notify('Tính điểm tổng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
            } else {
                done && done();
                dispatch(getYearPointInPage());
            }
        }, () => T.notify('Tính điểm tổng bị lỗi!', 'danger'))
    }
}

export function changePoint(point) {
    return { type: UPDATE, item: point };
}