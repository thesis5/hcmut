import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_THUONG_IN_PAGE = 'user:getPointDiemThuongInPage';

export default function dsDiemThuongReducer(state = null, data) {
    switch (data.type) {
        case GET_THUONG_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsDiemThuongPage', true);
export function dsDiemThuongInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsDiemThuongPage', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds-diem-thuong/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách điểm bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_THUONG_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách điểm bị lỗi!', 'danger'));
    }
}

export function updateDsDiemThuong(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-diem-thuong';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật điểm bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(dsDiemThuongInPage());
            }
        }, () => T.notify('Cập nhật điểm bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-diem-thuong/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật điểm bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật điểm bị lỗi!', 'danger'))
    }
}

export function createDsDiemThuong(points, done) {
    return dispatch => {
        const url = '/api/ds-diem-thuong';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(dsDiemThuongInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteDsDiemThuong(_id, done) {
    return dispatch => {
        const url = '/api/ds-diem-thuong';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(dsDiemThuongInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}

