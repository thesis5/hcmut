import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_USERS = 'user:getUsers';
const GET_STAFFS = 'user:getStaffs';
const GET_PROJECT_LEVELS = 'user:getProjectLevels';
const GET_PUBLICATION_TYPES = 'user:getPublicationTypes';
const GET_USER_IN_PAGE = 'user:getUserInPage';
const GET_STAFF_IN_PAGE = 'user:getStaffInPage';
const GET_PROJECT_IN_PAGE = 'user:getProjectInPage';
const GET_PUBLICATION_IN_PAGE = 'user:getPublicationInPage';
// const GET_STAFF_INFO = 'user:getStaffInfo';
const UPDATE_USER = 'user:UpdateUser';

export default function userReducer(state = null, data) {
    switch (data.type) {
        case GET_USERS:
            return Object.assign({}, state, { items: data.items });
        case GET_STAFFS:
            return Object.assign({}, state, { staffs: data.items });

        case GET_PROJECT_LEVELS:
            return Object.assign({}, state, { projectLevels: data.items });
        case GET_PUBLICATION_TYPES:
            return Object.assign({}, state, { publicationTypes: data.items });

        case GET_USER_IN_PAGE:
            return Object.assign({}, state, { page: data.page });
        case GET_STAFF_IN_PAGE:
            return Object.assign({}, state, { staffPage: data.page });
        case GET_PROJECT_IN_PAGE:
            return Object.assign({}, state, { projectPage: data.page });
        case GET_PUBLICATION_IN_PAGE:
            return Object.assign({}, state, { publicationPage: data.page });

        case UPDATE_USER:
            if (state) {
                let updatedItems = Object.assign({}, state.items),
                    updatedStaffs = Object.assign({}, state.staffs),
                    updatedPage = Object.assign({}, state.page),
                    updatedStaffPage = Object.assign({}, state.staffPage),
                    updatedItem = data.item;
                if (updatedItems) {
                    for (let i = 0, n = updatedItems.length; i < n; i++) {
                        if (updatedItems[i]._id == updatedItem._id) {
                            updatedItems.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                if (updatedStaffs) {
                    for (let i = 0, n = updatedStaffs.length; i < n; i++) {
                        if (updatedStaffs[i]._id == updatedItem._id) {
                            updatedStaffs.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                if (updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                if (updatedStaffPage.list) {
                    for (let i = 0, n = updatedStaffPage.list.length; i < n; i++) {
                        if (updatedStaffPage.list[i]._id == updatedItem._id) {
                            updatedStaffPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { items: updatedItems, staffs: updatedStaffs, page: updatedPage, staffPage: updatedStaffPage });
            } else {
                return null;
            }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('adminProject');
T.initCookiePage('adminPublication');
export function getAllUsers(done) {
    return dispatch => {
        const url = '/api/user/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách người dùng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_USERS, items: data.items });
            }
        }, error => T.notify('Lấy danh sách người dùng bị lỗi!', 'danger'));
    }
}
export function getAllStaffs(done) {
    return dispatch => {
        const url = '/api/staff/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách nhân viên bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_STAFFS, items: data.items });
            }
        }, error => T.notify('Lấy danh sách người dùng bị lỗi!', 'danger'));
    }
}
export function getAllProjectLevels(done) {
    return dispatch => {
        const url = '/api/project-levels/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách cấp quản lý dự án bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_PROJECT_LEVELS, items: data.items });
            }
        }, error => T.notify('Lấy danh sách cấp quản lý dự án bị lỗi!', 'danger'));
    }
}
export function getAllPublicationTypes(done) {
    return dispatch => {
        const url = '/api/publication-types/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách loại xuất bản bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_PUBLICATION_TYPES, items: data.items });
            }
        }, error => T.notify('Lấy danh sách loại xuất bản bị lỗi!', 'danger'));
    }
}

T.initCookiePage('adminUser', true);
export function getUserInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('adminUser', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = '/api/user/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách người dùng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_USER_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách người dùng bị lỗi!', 'danger'));
    }
}

T.initCookiePage('adminStaff', true);
export function getStaffInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('adminStaff', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = '/api/staff/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách người dùng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem, data.page.pageCondition);
                dispatch({ type: GET_STAFF_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách người dùng bị lỗi!', 'danger'));
    }
}
export function getProjectInPage(pageNumber, pageSize, done) {
    const route = T.routeMatcher('/user/project/:userId'),
        params = route.parse(window.location.pathname);
    const page = T.updatePage('adminProject', pageNumber, pageSize);
    return dispatch => {
        const url = `/api/project/page/${params.userId}/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách đề tài bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_PROJECT_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách đề tài bị lỗi!', 'danger'));
    }
}
export function getPublicationInPage(pageNumber, pageSize, done) {
    const route = T.routeMatcher('/user/publication/:userId'),
        params = route.parse(window.location.pathname);
    const page = T.updatePage('adminPublication', pageNumber, pageSize);
    return dispatch => {
        const url = `/api/publication/page/${params.userId}/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách tài liệu xuất bản bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_PUBLICATION_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách tài liệu xuất bản bị lỗi!', 'danger'));
    }
}

export function createProjectLevel(projectLevel, done) {
    return dispatch => {
        const url = `/api/project-level`;
        T.post(url, { projectLevel }, data => {
            if (data.error) {
                T.notify('Tạo cấp quản lý đề tài bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getAllProjectLevels(done));
            }
        }, error => T.notify('Tạo cấp quản lý đề tài bị lỗi!', 'danger'));
    }
}
export function updateProjectLevel(_id, changes, done) {
    return dispatch => {
        const url = '/api/project-level';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật cấp quản lý đề tài bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật cấp quản lý đề tài thành công!', 'info');
                dispatch(getAllProjectLevels(done));
            }
        }, error => T.notify('Cập nhật cấp quản lý đề tài bị lỗi!', 'danger'));
    }
}
export function deleteProjectLevel(_id, done) {
    return dispatch => {
        const url = '/api/project-level';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa cấp quản lý đề tài dùng bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Cấp quản lý đề tài được xóa thành công!', 'error', false, 800);
                dispatch(getAllProjectLevels());
            }
            done && done();
        }, error => T.notify('Xóa cấp quản lý đề tài bị lỗi!', 'danger'));
    }
}

export function createProject(project, done) {
    const route = T.routeMatcher('/user/project/:userId'),
        params = route.parse(window.location.pathname);
    project.user = params.userId;
    return dispatch => {
        const url = `/api/project`;
        T.post(url, { project }, data => {
            if (data.error) {
                T.notify('Tạo thông tin đề tài bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getProjectInPage());
                if (done) done(data.item);
            }
        }, error => T.notify('Tạo thông tin đề tài bị lỗi!', 'danger'));
    }
}
export function createProjectList(list, done) {
    const route = T.routeMatcher('/user/project/:userId'),
        params = route.parse(window.location.pathname);
    return dispatch => {
        const url = `/api/project/list`;
        T.post(url, { userId: params.userId, list }, data => {
            if (data.error) {
                T.notify('Tạo danh sách đề tài bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getProjectInPage());
                if (done) done();
            }
        }, error => T.notify('Tạo danh sách đề tài bị lỗi!', 'danger'));
    }
}
export function updateProject(_id, changes, done) {
    return dispatch => {
        const url = '/api/project';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin đề tài bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin đề tài thành công!', 'info');
                dispatch(getProjectInPage());
                done && done();
            }
        }, error => T.notify('Cập nhật thông tin đề tài bị lỗi!', 'danger'));
    }
}
export function deleteProject(_id, done) {
    return dispatch => {
        const url = '/api/project';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa thông tin đề tài dùng bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Thông tin đề tài được xóa thành công!', 'error', false, 800);
                dispatch(getProjectInPage());
            }
            done && done();
        }, error => T.notify('Xóa thông tin đề tài bị lỗi!', 'danger'));
    }
}

export function createPublicationType(publicationType, done) {
    return dispatch => {
        const url = `/api/publication-type`;
        T.post(url, { publicationType }, data => {
            if (data.error) {
                T.notify('Tạo loại xuất bản bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getAllPublicationTypes(done));
            }
        }, error => T.notify('Tạo loại xuất bản bị lỗi!', 'danger'));
    }
}
export function updatePublicationType(_id, changes, done) {
    return dispatch => {
        const url = '/api/publication-type';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật loại xuất bản bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật loại xuất bản thành công!', 'info');
                dispatch(getAllPublicationTypes(done));
            }
        }, error => T.notify('Cập nhật loại xuất bản bị lỗi!', 'danger'));
    }
}
export function deletePublicationType(_id, done) {
    return dispatch => {
        const url = '/api/publication-type';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa loại xuất bản dùng bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Cấp quản lý đề tài được xóa thành công!', 'error', false, 800);
                dispatch(getAllPublicationTypes());
            }
            done && done();
        }, error => T.notify('Xóa loại xuất bản bị lỗi!', 'danger'));
    }
}

export function createPublication(publication, done) {
    const route = T.routeMatcher('/user/publication/:userId'),
        params = route.parse(window.location.pathname);
    publication.user = params.userId;
    return dispatch => {
        const url = `/api/publication`;
        T.post(url, { publication }, data => {
            if (data.error) {
                T.notify('Tạo thông tin xuất bản bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getPublicationInPage());
                if (done) done(data.item);
            }
        }, error => T.notify('Tạo thông tin xuất bản bị lỗi!', 'danger'));
    }
}
export function createPublicationList(list, done) {
    const route = T.routeMatcher('/user/publication/:userId'),
        params = route.parse(window.location.pathname);
    return dispatch => {
        const url = `/api/publication/list`;
        T.post(url, { userId: params.userId, list }, data => {
            if (data.error) {
                T.notify('Tạo danh sách xuất bản bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getPublicationInPage());
                if (done) done();
            }
        }, error => T.notify('Tạo danh sách xuất bản bị lỗi!', 'danger'));
    }
}
export function updatePublication(_id, changes, done) {
    return dispatch => {
        const url = '/api/publication';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin xuất bản bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin xuất bản thành công!', 'info');
                dispatch(getPublicationInPage());
                done && done();
            }
        }, error => T.notify('Cập nhật thông tin xuất bản bị lỗi!', 'danger'));
    }
}
export function deletePublication(_id, done) {
    return dispatch => {
        const url = '/api/publication';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa thông tin xuất bản dùng bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Thông tin xuất bản được xóa thành công!', 'error', false, 800);
                dispatch(getPublicationInPage());
            }
            done && done();
        }, error => T.notify('Xóa thông tin xuất bản bị lỗi!', 'danger'));
    }
}

export function getUser(userId, done) {
    return dispatch => {
        const url = '/api/user/' + userId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy thông tin nhân viên bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.item);
                // dispatch({ type: GET_USERS, items: data.items });
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}

export function getStaffInfo(staffId, done) {
    return dispatch => {
        const url = '/api/staff/item/' + staffId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy thông tin nhân viên bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.staff);
                // dispatch({ type: GET_STAFF_INFO, staff: data.staff });
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}

export function updateStaffInfo(staffId, changes, done) {
    return dispatch => {
        const url = '/api/staff';
        T.put(url, { staffId, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật thông tin nhân viên bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.staff);
            }
        }, error => {
            console.error('PUT: ' + url + '. ' + error);
        });
    }
}

export function createUser(user, done) {
    return dispatch => {
        const url = '/api/user';
        T.post(url, { user }, data => {
            if (data.error) {
                T.notify('Tạo người dùng bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                T.notify('Tạo người dùng thành công!', 'success');
                dispatch(getUserInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo người dùng bị lỗi!', 'danger'));
    }
}

export function createStaffsFromFile(done) {
    return dispatch => {
        const url = '/api/multi-staff';
        T.post(url, {}, data => {
            if (data.error && data.error.length) {
                T.notify(data.error.toString(), 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                T.notify('Tạo danh sách nhân viên từ file thành công!', 'success');
                dispatch(getUserInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo danh sách nhân viên từ file bị lỗi!', 'danger'));
    }
}

export function updateUser(_id, changes, done) {
    return dispatch => {
        const url = '/api/user';
        T.put(url, { _id, changes }, data => {
            if (data.error || data.user == null) {
                T.notify('Cập nhật thông tin người dùng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật thông tin người dùng thành công!', 'info');
                dispatch(changeUser(data.user));
                done && done();
            }
        }, error => T.notify('Cập nhật thông tin người dùng bị lỗi!', 'danger'));
    }
}
export function deleteUser(_id, done) {
    return dispatch => {
        const url = '/api/user';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa người dùng bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Người dùng được xóa thành công!', 'error', false, 800);
                dispatch(getUserInPage());
            }
            done && done();
        }, error => T.notify('Xóa người dùng bị lỗi!', 'danger'));
    }
}

export function changeUser(user) {
    return { type: UPDATE_USER, item: user };
}


export function userGetStaff(firstPartOfEmail, done) {
    return dispatch => {
        const url = '/api/user/personnel/item/' + firstPartOfEmail;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy thông tin nhân viên bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.item);
                // dispatch({ type: GET_USERS, items: data.items });
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}
export function userGetAllProjectLevels(done) {
    return dispatch => {
        const url = '/api/user/project-levels/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách cấp quản lý dự án bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_PROJECT_LEVELS, items: data.items });
            }
        }, error => T.notify('Lấy danh sách cấp quản lý dự án bị lỗi!', 'danger'));
    }
}
export function userGetAllPublicationTypes(done) {
    return dispatch => {
        const url = '/api/user/publication-types/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách loại xuất bản bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_PUBLICATION_TYPES, items: data.items });
            }
        }, error => T.notify('Lấy danh sách loại xuất bản bị lỗi!', 'danger'));
    }
}

export function userGetAllStaffs(done) {
    return dispatch => {
        const url = '/api/user/personnel/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách nhân viên bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_STAFFS, items: data.items });
            }
        }, error => T.notify('Lấy danh sách người dùng bị lỗi!', 'danger'));
    }
}
export function userUpdateProfile(changes, done) {
    return dispatch => {
        const url = '/api/user/profile';
        T.put(url, { changes }, data => {
            if (data.error) {
                T.notify('Error when update profile!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                T.notify('Update profile successfully!', 'info');
            }
            done && done(data);
        }, error => T.notify('Error when update profile!', 'danger'));
    }
}

export function clearParticipantsSession() {
    return dispatch => {
        const url = '/api/user/clear-participants-session';
        T.delete(url);
    }
}
