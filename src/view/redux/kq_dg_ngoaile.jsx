import T from '../common/js/common';

T.initCookiePage('pageKgDgNgoaile');

const GET_KQ_DG_NGOAILE_IN_PAGE = 'kq_dg_ngoaile: getKqDgNgoaileInPage';

export default function KqDgNgoaileReducer(state = null, data) {
    switch (data.type) {
        case GET_KQ_DG_NGOAILE_IN_PAGE:
            return Object.assign({}, state, { page: data.page });
        default:
            return state;
    }
}

export function getKqDgNgoaileInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pageKgDgNgoaile', pageNumber, pageSize);
    return dispatch => {
        const url = '/api/kq-dg-ngoaile/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_KQ_DG_NGOAILE_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    };
}

export function updateKqDgNgoaile(officerId, changes, done) {
    return dispatch => {
        const url = '/api/kq-dg-ngoaile';
        T.put(url, { officerId, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getKqDgNgoaileInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function updateMultiKqDgNgoaile(officers, done) {
    return dispatch => {
        const url = '/api/kq-dg-ngoaile-multi';
        T.put(url, { officers }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}

export function createOfficerFromFile(done) {
    return dispatch => {
        const url = '/api/kq-dg-ngoaile-multi';
        T.post(url, {}, data => {
            if (data.error && data.error.length) {
                T.notify(data.error.toString(), 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                T.notify('Tạo danh sách nhân viên từ file thành công!', 'success');
                dispatch(getKqDgNgoaileInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo danh sách nhân viên từ file bị lỗi!', 'danger'));
    }
}

export function deleteKqDgNgoaile(_id, done) {
    return dispatch => {
        const url = '/api/kq-dg-ngoaile';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getKqDgNgoaileInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}

export function createKqDgNgoaile(officer, done) {
    return dispatch => {
        const url = '/api/kq-dg-ngoaile';
        T.post(url, { officer }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getKqDgNgoaileInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}