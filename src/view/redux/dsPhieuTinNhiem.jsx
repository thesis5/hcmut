import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_PHIEU_TIN_NHIEM_IN_PAGE = 'user:getPhieuTinNhiemInPage';

export default function dsPhieuTinNhiemReducer(state = null, data) {
    switch (data.type) {
        case GET_PHIEU_TIN_NHIEM_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsPhieuTinNhiemPage', true);
export function getPointInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('dsPhieuTinNhiemPage', pageNumber, pageSize);
    return dispatch => {
        const url = `/api/ds-phieu-tin-nhiem/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page);
                dispatch({ type: GET_PHIEU_TIN_NHIEM_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    }
}

export function updatePoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-phieu-tin-nhiem';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getPointInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-phieu-tin-nhiem/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}
