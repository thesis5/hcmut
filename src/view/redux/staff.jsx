import T from '../common/js/common';
T.initCookiePage('staffProjectPage');
T.initCookiePage('staffPublicationPage');

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_PROJECT_IN_PAGE = 'project:getProjectInPage';
const GET_PROJECT_LEVELS = 'project:getProjectLevels';

const GET_PUBLICATION_IN_PAGE = 'publication:getPublicationInPage';
const GET_PUBLICATION_TYPES = 'publication:getPublicationTypes';

export default function staffReducer(state = null, data) {
     switch (data.type) {
          case GET_PROJECT_IN_PAGE:
               return Object.assign({}, state, { projectPage: data.page });
     
          case GET_PROJECT_LEVELS:
               return Object.assign({}, state, { projectLevels: data.items });
     
               
          case GET_PUBLICATION_IN_PAGE:
               return Object.assign({}, state, { publicationPage: data.page });
               
          case GET_PUBLICATION_TYPES:
               return Object.assign({}, state, { publicationTypes: data.items });
               
          default:
               return state;
     }
}

// Project ------------------------------------------------------------------------------------------------------------
export function getProjectInPage(pageNumber, pageSize, done) {
     const page = T.updatePage('staffProjectPage', pageNumber, pageSize);
     return dispatch => {
          const url = `/staff/project/page/${page.pageNumber}/${page.pageSize}`;
          T.get(url, data => {
               if (data.error) {
                    T.notify('Lấy danh sách đề tài bị lỗi!', 'danger');
                    console.error('GET: ' + url + '. ' + data.error);
               } else {
                    if (done) done(data.page);
                    dispatch({ type: GET_PROJECT_IN_PAGE, page: data.page });
               }
          }, error => T.notify('Lấy danh sách đề tài bị lỗi!', 'danger'));
     }
}

export function createProject(project, done) {
     return dispatch => {
          const url = '/staff/project';
          T.post(url, { project }, data => {
               if (data.error) {
                    T.notify('Tạo thông tin đề tài bị lỗi!', 'danger');
                    console.error('POST: ' + url + '. ' + data.error);
               } else {
                    dispatch(getProjectInPage());
                    if (done) done(data.item);
               }
          }, error => T.notify('Tạo thông tin đề tài bị lỗi!', 'danger'));
     }
}

export function createProjectList(list, done) {
     return dispatch => {
          const url = `/staff/project/list`;
          T.post(url, { list }, data => {
               if (data.error) {
                    T.notify('Tạo danh sách đề tài bị lỗi!', 'danger');
                    console.error('POST: ' + url + '. ' + data.error);
               } else {
                    dispatch(getProjectInPage());
                    if (done) done();
               }
          }, error => T.notify('Tạo danh sách đề tài bị lỗi!', 'danger'));
     }
}

export function updateProject(_id, changes, done) {
     return dispatch => {
          const url = '/staff/project';
          T.put(url, { _id, changes }, data => {
               if (data.error) {
                    T.notify('Cập nhật thông tin đề tài bị lỗi!', 'danger');
                    console.error('PUT: ' + url + '. ' + data.error);
               } else {
                    T.notify('Cập nhật thông tin đề tài thành công!', 'info');
                    dispatch(getProjectInPage());
                    done && done(data);
               }
          }, error => T.notify('Cập nhật thông tin đề tài bị lỗi!', 'danger'));
     }
}

export function deleteProject(_id, done) {
     return dispatch => {
          const url = '/staff/project';
          T.delete(url, { _id }, data => {
               if (data.error) {
                    T.notify('Xóa đề tài dùng bị lỗi!', 'danger');
                    console.error('DELETE: ' + url + '. ' + data.error);
               } else {
                    T.alert('Đề tài được xóa thành công!', 'error', false, 800);
                    dispatch(getProjectInPage());
               }
               done && done();
          }, error => T.notify('Xóa đề tài bị lỗi!', 'danger'));
     }
}

// Project Level --------------------------------------------------------------------------------------------------------
export function getAllProjectLevels(done) {
     return dispatch => {
          const url = '/staff/project-levels/all';
          T.get(url, data => {
               if (data.error) {
                    T.notify('Lấy danh sách cấp quản lý dự án bị lỗi!', 'danger');
                    console.error('GET: ' + url + '. ' + data.error);
               } else {
                    if (done) done(data.items);
                    dispatch({ type: GET_PROJECT_LEVELS, items: data.items });
               }
          }, error => T.notify('Lấy danh sách cấp quản lý dự án bị lỗi!', 'danger'));
     }
}

// Publication ----------------------------------------------------------------------------------------------------------
export function getPublicationInPage(pageNumber, pageSize, done) {
     const page = T.updatePage('staffPublicationPage', pageNumber, pageSize);
     return dispatch => {
          const url = `/staff/publication/page/${page.pageNumber}/${page.pageSize}`;
          T.get(url, data => {
               if (data.error) {
                    T.notify('Lấy danh sách tài liệu xuất bản bị lỗi!', 'danger');
                    console.error('GET: ' + url + '. ' + data.error);
               } else {
                    if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                    dispatch({ type: GET_PUBLICATION_IN_PAGE, page: data.page });
               }
          }, error => T.notify('Lấy danh sách tài liệu xuất bản bị lỗi!', 'danger'));
     }
}

//Publication Type -----------------------------------------------------------------------------------------------------
export function getAllPublicationTypes(done) {
     return dispatch => {
          const url = '/staff/publication-types/all';
          T.get(url, data => {
               if (data.error) {
                    T.notify('Lấy danh sách loại xuất bản bị lỗi!', 'danger');
                    console.error('GET: ' + url + '. ' + data.error);
               } else {
                    if (done) done(data.items);
                    dispatch({ type: GET_PUBLICATION_TYPES, items: data.items });
               }
          }, error => T.notify('Lấy danh sách loại xuất bản bị lỗi!', 'danger'));
     }
}

export function createPublication(publication, done) {
     return dispatch => {
          const url = `/staff/publication`;
          T.post(url, { publication }, data => {
               if (data.error) {
                    T.notify('Tạo thông tin xuất bản bị lỗi!', 'danger');
                    console.error('POST: ' + url + '. ' + data.error);
               } else {
                    dispatch(getPublicationInPage());
                    if (done) done(data.item);
               }
          }, error => T.notify('Tạo thông tin xuất bản bị lỗi!', 'danger'));
     }
}

export function createPublicationList(list, done) {
     return dispatch => {
          const url = `/staff/publication/list`;
          T.post(url, { list }, data => {
               if (data.error) {
                    T.notify('Tạo danh sách xuất bản bị lỗi!', 'danger');
                    console.error('POST: ' + url + '. ' + data.error);
               } else {
                    dispatch(getPublicationInPage());
                    if (done) done();
               }
          }, error => T.notify('Tạo danh sách xuất bản bị lỗi!', 'danger'));
     }
}

export function updatePublication(_id, changes, done) {
     return dispatch => {
          const url = '/staff/publication';
          T.put(url, { _id, changes }, data => {
               if (data.error) {
                    T.notify('Cập nhật thông tin xuất bản bị lỗi!', 'danger');
                    console.error('PUT: ' + url + '. ' + data.error);
               } else {
                    T.notify('Cập nhật thông tin xuất bản thành công!', 'info');
                    dispatch(getPublicationInPage());
                    done && done();
               }
          }, error => T.notify('Cập nhật thông tin xuất bản bị lỗi!', 'danger'));
     }
}

export function deletePublication(_id, done) {
     return dispatch => {
          const url = '/staff/publication';
          T.delete(url, { _id }, data => {
               if (data.error) {
                    T.notify('Xóa thông tin xuất bản dùng bị lỗi!', 'danger');
                    console.error('DELETE: ' + url + '. ' + data.error);
               } else {
                    T.alert('Thông tin xuất bản được xóa thành công!', 'error', false, 800);
                    dispatch(getPublicationInPage());
                    done && done();
               }
          }, error => T.notify('Xóa thông tin xuất bản bị lỗi!', 'danger'));
     }
}
