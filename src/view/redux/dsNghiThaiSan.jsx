import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_NGHI_THAI_SAN_IN_PAGE = 'user:getDsNghiThaiSanInPage';

export default function dsNghiThaiSanReducer(state = null, data) {
    switch (data.type) {
        case GET_NGHI_THAI_SAN_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsNghiThaiSanPage', true);
export function getDsNghiThaiSanInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsNghiThaiSanPage', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds-nghi-khai-san/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, {condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_NGHI_THAI_SAN_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    }
}

export function updateDsNghiThaiSan(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-nghi-khai-san';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDsNghiThaiSanInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-nghi-khai-san/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}

export function createDsNghiThaiSan(points, done) {
    return dispatch => {
        const url = '/api/ds-nghi-khai-san';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getDsNghiThaiSanInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteDsNghiThaiSan(_id, done) {
    return dispatch => {
        const url = '/api/ds-nghi-khai-san';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getDsNghiThaiSanInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}
