import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const DS_CANBO_CONGDOAN_IN_PAGE = 'user:getDsCanboCongdoanInPage';

export default function dsCanboCongdoanReducer(state = null, data) {
    switch (data.type) {
        case DS_CANBO_CONGDOAN_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsCanboCongdoanPage', true);
export function getDsCanboCongdoanInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsCanboCongdoanPage', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = `/api/ds-canbo-congdoan/page/${page.pageNumber}/${page.pageSize}`;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy dữ liệu bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '. ' + data.error);
    //         } else {
    //             if (done) done(data.page);
    //             dispatch({ type: DS_CANBO_CONGDOAN_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    // }
    return dispatch => {
        const url = `/api/ds-canbo-congdoan/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: DS_CANBO_CONGDOAN_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updateCBCD(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-canbo-congdoan';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDsCanboCongdoanInPage());
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'));
    }
}

export function createCBCD(data, done) {
    return dispatch => {
        const url = '/api/ds-canbo-congdoan';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDsCanboCongdoanInPage());
            }
        }, () => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-canbo-congdoan/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function deleteCBCD(_id) {
    return dispatch => {
        const url = '/api/ds-canbo-congdoan';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa cán bộ công đoàn bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Cán bộ công đoàn được xóa thành công!', 'error', false, 800);
                dispatch(getDsCanboCongdoanInPage());
            }
        }, error => T.notify('Xóa cán bộ công đoàn bị lỗi!', 'danger'));
    }
}
