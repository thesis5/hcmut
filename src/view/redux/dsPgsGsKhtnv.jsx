import T from '../common/js/common';

T.initCookiePage('pagePgsGsKhtnv');

const GET_IN_PAGE = 'pgs_gs_khtnv:getInPage';

export default function PgsGsKhtnvReducer(state = null, data) {
    switch (data.type) {
        case GET_IN_PAGE:
            return Object.assign({}, state, { page: data.page });
        default:
            return state;
    }
}

export function getPgsGsKhtnvInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('pagePgsGsKhtnv', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = '/api/pgsgs-khtnv/page/' + page.pageNumber + '/' + page.pageSize;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy danh sách bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '.', data.error);
    //         } else {
    //             if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
    //             dispatch({ type: GET_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    // };
    return dispatch => {
        const url = `/api/pgsgs-khtnv/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    };
}

export function updatePgsGsKhtnv(shcc, changes, done) {
    return dispatch => {
        const url = '/api/pgsgs-khtnv';
        T.put(url, { shcc, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getPgsGsKhtnvInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function updateMultiPgsGsKhtnv(officers, done) {
    return dispatch => {
        const url = '/api/pgsgs-khtnv-multi';
        T.put(url, { officers }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}

export function createOfficerFromFile(done) {
    return dispatch => {
        const url = '/api/pgsgs-khtnv-multi';
        T.post(url, {}, data => {
            if (data.error && data.error.length) {
                T.notify(data.error.toString(), 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                T.notify('Tạo danh sách nhân viên từ file thành công!', 'success');
                dispatch(getPgsGsKhtnvInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo danh sách nhân viên từ file bị lỗi!', 'danger'));
    }
}

export function deletePgsGsKhtnv(_id, done) {
    return dispatch => {
        const url = '/api/pgsgs-khtnv';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getPgsGsKhtnvInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}

export function createPgsGsKhtnv(officer, done) {
    return dispatch => {
        const url = '/api/pgsgs-khtnv';
        T.post(url, { officer }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getPgsGsKhtnvInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}