import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_CAN_BO_LAM_VIEC_712_THANG_IN_PAGE = 'user:getCanBoLamViec712ThangInPage';
const UPDATE_CAN_BO_LAM_VIEC_712_THANG = 'user:UpdateCanBoLamViec712Thang';

export default function dsCanBoLamViec712ThangReducer(state = null, data) {
    switch (data.type) {
        case GET_CAN_BO_LAM_VIEC_712_THANG_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        case UPDATE_CAN_BO_LAM_VIEC_712_THANG:
            if (state) {
                let updatedPage = Object.assign({}, state.page),
                    updatedItem = data.item;

                if (updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { page: updatedPage });
            } else {
                return null;
            }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('adminCanBoLamViec712Thang', true);
export function getCanBoLamViec712ThangInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('adminCanBoLamViec712Thang', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds_can_bo_lam_viec_712_thang/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách viên chức làm việc từ 6 đến dưới 12 tháng bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_CAN_BO_LAM_VIEC_712_THANG_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách viên chức làm việc từ 6 đến dưới 12 tháng bị lỗi!', 'danger'));
    }
}

export function updateCanBoLamViec712Thang(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds_can_bo_lam_viec_712_thang';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật viên chức làm việc từ 6 đến dưới 12 tháng bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getCanBoLamViec712ThangInPage());
            }
        }, () => T.notify('Cập nhật viên chức làm việc từ 6 đến dưới 12 tháng bị lỗi!', 'danger'));
    }
}

export function updateMultiValues(items, done) {
    return dispatch => {
        const url = '/api/ds_can_bo_lam_viec_712_thang/multi_values';
        T.put(url, { items }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function createCanBoLamViec712Thang(data, done) {
    return dispatch => {
        const url = '/api/ds_can_bo_lam_viec_712_thang';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo dữ liệu bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getCanBoLamViec712ThangInPage());
            }
        }, () => T.notify('Tạo dữ liệu bị lỗi!', 'danger'));
    }
}

export function deleteCanBoLamViec712Thang(_id, done) {
    return dispatch => {
        const url = '/api/ds_can_bo_lam_viec_712_thang';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa dữ liệu bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getCanBoLamViec712ThangInPage());
            }
        }, () => T.notify('Xóa dữ liệu bị lỗi!', 'danger'));
    }
}

export function changeCanBoLamViec712Thang(item) {
    return { type: UPDATE_CAN_BO_LAM_VIEC_712_THANG, item };
}