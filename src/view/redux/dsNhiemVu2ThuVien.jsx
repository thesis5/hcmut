import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_NV2_IN_PAGE = 'user:getDsNv2TvInPage';

export default function dsNv2TvReducer(state = null, data) {
    switch (data.type) {
        case GET_NV2_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsNv2TvPointPage', true);
export function getDsNv2TvInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsNv2TvPointPage', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = `/api/ds-nv-2/page/${page.pageNumber}/${page.pageSize}`;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy danh sách bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '. ' + data.error);
    //         } else {
    //             if (done) done(data.page);
    //             dispatch({ type: GET_NV2_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    // }
    return dispatch => {
        const url = `/api/ds-nv-2/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_NV2_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updateDsNv2Tv(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-nv-2';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDsNv2TvInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-nv-2/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}

export function createDsNv2Tv(points, done) {
    return dispatch => {
        const url = '/api/ds-nv-2';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getDsNv2TvInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteDsNv2Tv(_id, done) {
    return dispatch => {
        const url = '/api/ds-nv-2';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getDsNv2TvInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}

