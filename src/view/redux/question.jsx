import T from '../common/js/common';

// Reducer -------------------------------------------------------------------------------------------------------------
const GET_QUESTIONS_LIST = 'question:getQuestionsList';

export default function questionReducer(state = null, data) {
     switch (data.type) {
          case GET_QUESTIONS_LIST:
               const source2 = {};
               source2[data.field] = data[data.field];
               return Object.assign({}, state, source2);
               
          default:
               return state;
     }
}

// Actions (admin) ----------------------------------------------------------------------------------------------------
export function getQuestionsList(model, field, _id, done) {
     return dispatch => {
          const url = `/api/questions/${model}/${field}/${_id}`;
          T.get(url, data => {
               if (data.error) {
                    T.notify('Lấy danh sách câu hỏi bị lỗi!', 'danger');
                    console.error('GET: ' + url + '.', data.error);
               } else {
                    const dispatcher = {};
                    dispatcher.type = GET_QUESTIONS_LIST;
                    dispatcher[field] = data.item[field];
                    dispatcher.field = field;
                    dispatch(dispatcher);
                    done && done(data.item);
               }
          }, error => {
               console.error('GET: ' + url + '.', error);
          });
     }
}

export function createQuestion(model, field, _id, permission, data, done) {
     return dispatch => {
          const url = `/api/question/${model}/${field}/${_id}`;
          T.post(url, { data, permission }, data => {
               if (data.error) {
                    T.notify('Tạo câu hỏi bị lỗi!', 'danger');
                    console.error('POST: ' + url + '.', data.error);
               } else {
                    dispatch(getQuestionsList(model, field, _id));
                    done && done(data.item);
               }
          }, error => console.error('POST: ' + url + '.', error));
     }
}

export function updateQuestion(_id, data, model, field, postId, permission, done) {
     return dispatch => {
          const url = '/api/question';
          T.put(url, { _id, data, permission }, data => {
               if (data.error) {
                    T.notify('Cập nhật câu hỏi bị lỗi!', 'danger');
                    console.error('PUT: ' + url + '.', data.error);
               } else {
                    dispatch(getQuestionsList(model, field, postId));
                    done && done();
               }
          }, error => console.error('PUT: ' + url + '.', error));
     }
}

export function swapQuestion(postId, data, model, field, permission, done) {
     return dispatch => {
          const url = `/api/question/${model}/swap`;
          T.put(url, {postId, data, permission}, data => {
               if (data.error) {
                    T.notify('Thay đổi thứ tự câu hỏi bị lỗi!', 'danger');
                    console.error('PUT: ' + url + '.', data.error);
               } else {
                    dispatch(getQuestionsList(model, field, postId));
                    done && done();
               }
          }, error => console.error('PUT: ' + url + '.', error));
     }
}

export function deleteQuestion(_id, postId, data, model, field, permission, done) {
     return dispatch => {
          const url = `/api/question/${model}/${field}`;
          T.delete(url, { data, postId, _id, permission }, data => {
               if (data.error) {
                    T.notify('Xóa câu hỏi bị lỗi!', 'danger');
                    console.error('DELETE: ' + url + '.', data.error);
               } else {
                    dispatch(getQuestionsList(model, field, postId));
                    done && done();
               }
          }, error => console.error('DELETE: ' + url + '.', error));
     }
}