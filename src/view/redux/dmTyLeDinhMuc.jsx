import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_DANH_MUC_TY_LE_DINH_MUC_IN_PAGE = 'user:danhMucTyLeDinhMucInPage';

export default function dmTyLeDinhMucReducer(state = null, data) {
    switch (data.type) {
        case GET_DANH_MUC_TY_LE_DINH_MUC_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('danhMucTyLeDinhMucPage', true);
export function danhMucTyLeDinhMucInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('danhMucTyLeDinhMucPage', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/danh-muc-ty-le-dinh-muc/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_DANH_MUC_TY_LE_DINH_MUC_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    }
}

export function updateDanhMucTyLeDinhMuc(_id, changes, done) {
    return dispatch => {
        const url = '/api/danh-muc-ty-le-dinh-muc';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(danhMucTyLeDinhMucInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function createMultiDanhMucTyLeDinhMuc(points, done) {
    return dispatch => {
        const url = '/api/danh-muc-ty-le-dinh-muc/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}

export function createDanhMucTyLeDinhMuc(points, done) {
    return dispatch => {
        const url = '/api/danh-muc-ty-le-dinh-muc';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(danhMucTyLeDinhMucInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteDanhMucTyLeDinhMuc(_id, done) {
    return dispatch => {
        const url = '/api/danh-muc-ty-le-dinh-muc';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(danhMucTyLeDinhMucInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}

