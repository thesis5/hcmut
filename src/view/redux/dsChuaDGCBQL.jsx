import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_CHUA_IN_PAGE = 'user:getPointChuaDGCBQLInPage';

export default function dsChuaDGCBQLReducer(state = null, data) {
    switch (data.type) {
        case GET_CHUA_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsChuaDGCBQLPointPage', true);
export function getPointChuaDGCBQLInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsChuaDGCBQLPointPage', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = `/api/ds-chua-dg-cbql/page/${page.pageNumber}/${page.pageSize}`;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy danh sách đánh giá bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '. ' + data.error);
    //         } else {
    //             if (done) done(data.page);
    //             dispatch({ type: GET_CHUA_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy danh sách đánh giá bị lỗi!', 'danger'));
    // }
    return dispatch => {
        const url = `/api/ds-chua-dg-cbql/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_CHUA_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updatePoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-chua-dg-cbql';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật nội dung bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getPointChuaDGCBQLInPage());
            }
        }, () => T.notify('Cập nhật nội dung bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-chua-dg-cbql/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Tạo nội dung bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Tạo nội dung bị lỗi!', 'danger'))
    }
}

export function deleteChuaDGCBQL(_id) {
    return dispatch => {
        const url = '/api/ds-chua-dg-cbql';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa nội dung bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '.', data.error);
            } else {
                T.alert('Nội dung được xóa thành công!', 'error', false, 800);
                dispatch(getPointChuaDGCBQLInPage());
            }
        }, error => T.notify('Xóa nội dung bị lỗi!', 'danger'));
    }
}

export function createChuaDGCBQL(data, done) {
    return dispatch => {
        const url = '/api/ds-chua-dg-cbql';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getPointChuaDGCBQLInPage());
            }
        }, () => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}
