import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const DS_NV1_PSDH_IN_PAGE = 'user:getDsNv1PsdhInPage';

export default function dsNv1PsdhReducer(state = null, data) {
    switch (data.type) {
        case DS_NV1_PSDH_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsNv1PsdhPage', true);
export function getDsNv1PsdhInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsNv1PsdhPage', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds-nhiem-vu-1-phong-dao-tao-sdh/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, {condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: DS_NV1_PSDH_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updateDsNv1Psdh(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-nhiem-vu-1-phong-dao-tao-sdh';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getDsNv1PsdhInPage());
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-nhiem-vu-1-phong-dao-tao-sdh/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function createDsNv1Psdh(points, done) {
    return dispatch => {
        const url = '/api/ds-nhiem-vu-1-phong-dao-tao-sdh';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getDsNv1PsdhInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteDsNv1Psdh(_id, done) {
    return dispatch => {
        const url = '/api/ds-nhiem-vu-1-phong-dao-tao-sdh';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getDsNv1PsdhInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}