import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_VI_PHAM_KY_LUAT_IN_PAGE = 'user:getViPhamKyLuatInPage';
const UPDATE_VI_PHAM_KY_LUAT = 'user:UpdateViPhamKyLuat';

export default function viPhamKyLuatReducer(state = null, data) {
    switch (data.type) {
        case GET_VI_PHAM_KY_LUAT_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        case UPDATE_VI_PHAM_KY_LUAT:
            if (state) {
                let updatedPage = Object.assign({}, state.page),
                    updatedItem = data.item;

                if (updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { page: updatedPage });
            } else {
                return null;
            }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('adminViPhamKyLuat', true);
export function getViPhamKyLuatInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('adminViPhamKyLuat', pageNumber, pageSize, pageCondition);
    return dispatch => {
        const url = `/api/ds_vi_pham_ky_luat/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách vi phạm kỷ luật bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_VI_PHAM_KY_LUAT_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách vi phạm kỷ luật bị lỗi!', 'danger'));
    }
}

export function updateViPhamKyLuat(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds_vi_pham_ky_luat';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật vi phạm kỷ luật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getViPhamKyLuatInPage());
            }
        }, () => T.notify('Cập nhật vi phạm kỷ luật bị lỗi!', 'danger'));
    }
}

export function updateMultiValues(items, done) {
    return dispatch => {
        const url = '/api/ds_vi_pham_ky_luat/multi_values';
        T.put(url, { items }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật dữ liệu bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật dữ liệu bị lỗi!', 'danger'))
    }
}

export function createViPhamKyLuat(data, done) {
    return dispatch => {
        const url = '/api/ds_vi_pham_ky_luat';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo vi phạm kỷ luật bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getViPhamKyLuatInPage());
            }
        }, () => T.notify('Tạo vi phạm kỷ luật bị lỗi!', 'danger'));
    }
}

export function deleteViPhamKyLuat(_id, done) {
    return dispatch => {
        const url = '/api/ds_vi_pham_ky_luat';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa vi phạm kỷ luật bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getViPhamKyLuatInPage());
            }
        }, () => T.notify('Xóa vi phạm kỷ luật bị lỗi!', 'danger'));
    }
}

export function changeViPhamKyLuat(item) {
    return { type: UPDATE_VI_PHAM_KY_LUAT, item };
}