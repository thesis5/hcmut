import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_KQDG_NGOAI_LE_CBQL_IN_PAGE = 'user:getKQDGNgoaiLeCBQLInPage';

export default function dsKQDGNgoaiLeCBQLReducer(state = null, data) {
    switch (data.type) {
        case GET_KQDG_NGOAI_LE_CBQL_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsKQDGNgoaiLeCBQLPage', true);
export function getPointInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('dsKQDGNgoaiLeCBQLPage', pageNumber, pageSize);
    return dispatch => {
        const url = `/api/ds-kqdg-ngoai-le-cbql/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page);
                dispatch({ type: GET_KQDG_NGOAI_LE_CBQL_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    }
}

export function updatePoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-kqdg-ngoai-le-cbql';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getPointInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-kqdg-ngoai-le-cbql/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}
