import T from '../common/js/common';

const GET_PAGE = 'categoryOffice:getCategoryInPage';

export default function categoryOfficeReducer(state = [], data) {
    switch (data.type) {
        case GET_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('pageCategoryOffice');
export function getCategoryOfficeInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pageCategoryOffice', pageNumber, pageSize);
    return dispatch => {
        const url = `/api/category-office/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách danh mục chức vụ bị lỗi!', 'danger');
                console.error('GET: ' + url + '.', data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy danh sách danh mục chức vụ bị lỗi!', 'danger'));
    }
}

export function createMultipleCategories(categories, done) {
    return dispatch => {
        const url = '/api/category-office/multiple';
        T.post(url, { categories }, data => {
            if (data.error && data.error.length) {
                T.notify('Tạo danh sách danh mục bị lỗi!', 'danger');
                console.error('POST: ' + url + '.', data.error.toString());
            } else {
                dispatch(getCategoryOfficeInPage());
                if (done) done();
            }
        }, error => T.notify('Tạo danh sách danh mục bị lỗi!', 'danger'));
    }
}

export function createCategoryOffice(data, done) {
    return dispatch => {
        const url = '/api/category-office';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo danh mục bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Tạo danh mục thành công!', 'success');
                dispatch(getCategoryOfficeInPage());
                done && done();
            }
        }, error => T.notify('Tạo danh mục bị lỗi!', 'danger'));
    }
}

export function updateCategoryOffice(_id, changes, done) {
    return dispatch => {
        const url = '/api/category-office';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật danh mục bị lỗi!', 'danger');
                console.error('PUT: ' + url + '.', data.error);
                done && done(data.error);
            } else {
                T.notify('Cập nhật danh mục thành công!', 'success');
                dispatch(getCategoryOfficeInPage());
                done && done();
            }
        }, error => T.notify('Cập nhật danh mục bị lỗi!', 'danger'));
    }
}

// export function swapCategory(_id, isMoveUp, type) {
//     return dispatch => {
//         const url = '/api/category/swap/';
//         T.put(url, { _id, isMoveUp }, data => {
//             if (data.error) {
//                 T.notify('Thay đổi vị trí danh mục bị lỗi!', 'danger')
//                 console.error('PUT: ' + url + '.', data.error);
//             } else {
//                 dispatch(getAll(type));
//             }
//         }, error => T.notify('Thay đổi vị trí danh mục bị lỗi!', 'danger'));
//     }
// }
//
// export function deleteCategory(_id) {
//     return dispatch => {
//         const url = '/api/category';
//         T.delete(url, { _id }, data => {
//             if (data.error) {
//                 T.notify('Xóa danh mục bị lỗi!', 'danger');
//                 console.error('DELETE: ' + url + '.', data.error);
//             } else {
//                 T.alert('Xóa danh mục thành công!', 'error', false, 800);
//                 dispatch({ type: DELETE_ITEM, _id });
//             }
//         }, error => T.notify('Xóa danh mục bị lỗi!', 'danger'));
//     }
// }
//
// export function changeCategory(category) {
//     return { type: UPDATE_ITEM, item: category };
// }
