import T from '../common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_POINT_IN_PAGE = 'user:getPointGiamDMInPage';

export default function dsGiamDMReducer(state = null, data) {
    switch (data.type) {
        case GET_POINT_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
T.initCookiePage('dsGiamDMPage', true);
export function getPointInPage(pageNumber, pageSize, pageCondition, done) {
    const page = T.updatePage('dsGiamDMPage', pageNumber, pageSize, pageCondition);
    // return dispatch => {
    //     const url = `/api/ds-giam-dinh-muc/page/${page.pageNumber}/${page.pageSize}`;
    //     T.get(url, data => {
    //         if (data.error) {
    //             T.notify('Lấy danh sách bị lỗi!', 'danger');
    //             console.error('GET: ' + url + '. ' + data.error);
    //         } else {
    //             if (done) done(data.page);
    //             dispatch({ type: GET_POINT_IN_PAGE, page: data.page });
    //         }
    //     }, error => T.notify('Lấy danh sách bị lỗi!', 'danger'));
    // }
    return dispatch => {
        const url = `/api/ds-giam-dinh-muc/page/${page.pageNumber}/${page.pageSize}`;
        T.get(url, { condition: page.pageCondition ? JSON.parse(page.pageCondition) : {} }, data => {
            if (data.error) {
                T.notify('Lấy dữ liệu bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (page.pageCondition) data.page.pageCondition = JSON.parse(page.pageCondition);
                if (done) done(data.page);
                dispatch({ type: GET_POINT_IN_PAGE, page: data.page });
            }
        }, error => T.notify('Lấy dữ liệu bị lỗi!', 'danger'));
    }
}

export function updatePoint(_id, changes, done) {
    return dispatch => {
        const url = '/api/ds-giam-dinh-muc';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                done && done(data.item);
                dispatch(getPointInPage());
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'));
    }
}

export function createMultiPoint(points, done) {
    return dispatch => {
        const url = '/api/ds-giam-dinh-muc/multiple';
        T.post(url, { points }, data => {
            if (data.error && data.error.length) {
                T.notify('Cập nhật bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error.toString());
            } else {
                done && done(data.item);
            }
        }, () => T.notify('Cập nhật bị lỗi!', 'danger'))
    }
}

export function createGiamDM(points, done) {
    return dispatch => {
        const url = '/api/ds-giam-dinh-muc';
        T.post(url, { points }, data => {
            if (data.error) {
                T.notify('Tạo mới bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getPointInPage());
                if (done) done(data);
            }
        }, error => T.notify('Tạo mới bị lỗi!', 'danger'));
    }
}

export function deleteGiamDM(_id, done) {
    return dispatch => {
        const url = '/api/ds-giam-dinh-muc';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Xóa thành công!', 'success', false, 800);
                dispatch(getPointInPage());
            }
            done && done();
        }, error => T.notify('Xóa bị lỗi!', 'danger'));
    }
}
