import T from '../common/js/common';
const texts = {
    vi: {
        getAllSlogansError: 'Lấy danh sách slogan bị lỗi!',
        createSloganError: 'Tạo slogan bị lỗi!',
        updateSloganError: 'Cập nhật slogan bị lỗi!',
        updateSloganSuccess: 'Cập nhật slogan thành công!',
        deleteSloganError: 'Xóa slogan bị lỗi!',
        deleteSloganSuccess: 'Slogan được xóa thành công!',
        getSloganError: 'Lấy slogan bị lỗi!',
    },
    en: {
        getAllSlogansError: 'Errors when get slogan list!',
        createSloganError: 'Errors when create new slogan!',
        updateSloganError: 'Errors when update slogan!',
        updateSloganSuccess: 'Slogan updated successfully!',
        deleteSloganError: 'Errors when delete slogan!',
        deleteSloganSuccess: 'Slogan deleted successfully!',
        getSloganError: 'Errors when get slogan!',
    }
};
const language = T.language(texts);

// Reducer ------------------------------------------------------------------------------------------------------------
const GET_SLOGAN_GROUPS = 'slogan:getAllSloganGroups';
const ADD_SLOGAN_INTO_GROUP = 'slogan:addSloganIntoGroup';
const UPDATE_SLOGAN_IN_GROUP = 'slogan:updateSloganInGroup';
const REMOVE_SLOGAN_IN_GROUP = 'slogan:removeSloganInGroup';
const SWAP_SLOGAN_IN_GROUP = 'slogan:swapSloganInGroup';
const UPDATE_SLOGAN_GROUP = 'slogan:updateSloganGroup';

export default function sloganReducer(state = null, data) {
    switch (data.type) {
        case GET_SLOGAN_GROUPS:
            return Object.assign({}, state, { list: data.items });

        case ADD_SLOGAN_INTO_GROUP:
            if (state && state.item) {
                state = Object.assign({}, state);
                state.item.items.push({
                    title: data.title,
                    image: data.image,
                    content: data.content
                });
            }
            return state;

        case UPDATE_SLOGAN_IN_GROUP:
            if (state && state.item) {
                state = Object.assign({}, state);
                if (0 <= data.index && data.index < state.item.items.length) {
                    state.item.items.splice(data.index, 1, {
                        title: data.title,
                        image: data.image,
                        content: data.content
                    });
                }
            }
            return state;

        case REMOVE_SLOGAN_IN_GROUP:
            if (state && state.item) {
                state = Object.assign({}, state);
                if (0 <= data.index && data.index < state.item.items.length) {
                    state.item.items.splice(data.index, 1);
                }
            }
            return state;

        case SWAP_SLOGAN_IN_GROUP:
            if (state && state.item) {
                state = Object.assign({}, state);
                const slogan = state.item.items[data.index];
                if (data.isMoveUp && data.index > 0) {
                    state.item.items.splice(data.index, 1);
                    state.item.items.splice(data.index - 1, 0, slogan);
                } else if (!data.isMoveUp && data.index < state.item.items.length - 1) {
                    state.item.items.splice(data.index, 1);
                    state.item.items.splice(data.index + 1, 0, slogan);
                }
            }
            return state;

        case UPDATE_SLOGAN_GROUP:
            return Object.assign({}, state, { item: data.item });

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
export function getAllSlogans(done) {
    return dispatch => {
        const url = '/api/slogan/all';
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getAllSlogansError, 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_SLOGAN_GROUPS, items: data.items });
            }
        }, error => T.notify(language.getAllSlogansError, 'danger'));
    }
}

export function createSlogan(title, done) {
    return dispatch => {
        const url = '/api/slogan';
        T.post(url, { title }, data => {
            if (data.error) {
                T.notify(language.createSloganError, 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getAllSlogans());
                if (done) done(data);
            }
        }, error => T.notify(language.createSloganError, 'danger'));
    }
}

export function updateSlogan(_id, changes, done) {
    return dispatch => {
        const url = '/api/slogan';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify(language.updateSloganError, 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
                done && done(data.error);
            } else {
                T.notify(language.updateSloganSuccess, 'info');
                dispatch(getAllSlogans());
                done && done();
            }
        }, error => T.notify(language.updateSloganError, 'danger'));
    }
}

export function deleteSlogan(_id) {
    return dispatch => {
        const url = '/api/slogan';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify(language.deleteSloganError, 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert(language.deleteSloganSuccess, 'error', false, 800);
                dispatch(getAllSlogans());
            }
        }, error => T.notify(language.deleteSloganError, 'danger'));
    }
}



export function getSloganItem(sloganId, done) {
    return dispatch => {
        const url = '/api/slogan/item/' + sloganId;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getSloganError, 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done({ item: data.item });
                dispatch({ type: UPDATE_SLOGAN_GROUP, item: data.item });
            }
        }, error => T.notify(language.getSloganError, 'danger'));
    }
}

export function addSloganIntoGroup(title, image, content) {
    return { type: ADD_SLOGAN_INTO_GROUP, title, image, content };
}

export function updateSloganInGroup(index, title, image, content) {
    return { type: UPDATE_SLOGAN_IN_GROUP, index, title, image, content };
}

export function removeSloganFromGroup(index) {
    return { type: REMOVE_SLOGAN_IN_GROUP, index };
}

export function swapSloganInGroup(index, isMoveUp) {
    return { type: SWAP_SLOGAN_IN_GROUP, index, isMoveUp };
}


export function getSloganByUser(sloganId, done) {
    return dispatch => {
        const url = '/home/slogan/' + sloganId;
        T.get(url, data => {
            if (data.error) {
                T.notify(language.getSloganError, 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.item);
            }
        }, error => T.notify(language.getSloganError, 'danger'));
    }
}