import React from 'react';
import {connect} from 'react-redux';
import { getQuestionsList, createQuestion, updateQuestion, swapQuestion, deleteQuestion } from '../redux/question.jsx';
import Select from 'react-select';
import Editor from '../common/CkEditor4.jsx';

class QuestionModal extends React.Component {
    constructor(props) {
        super(props);
        let types = Object.keys(T.questionTypes).map(key => ({value: key, label: T.questionTypes[key]}));
        this.state = {
            questionTypes: types,
            itemID: null,
            value: [],
            active: false,
        };

        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this.changeActive = this.changeActive.bind(this);
        this.onSelectType = this.onSelectType.bind(this);
        this.save = this.save.bind(this);

        this.modal = React.createRef();
        this.btnSave = React.createRef();
        this.editor = React.createRef();
        this.dataType = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            setTimeout(() => $(this.modal.current).on('shown.bs.modal', () => $('#questionTitle').focus()), 250);
        });
    }


    show(item) {
        let {title, defaultAnswer, content, typeName, typeValue, active} = item ?
            item : {
                title: '', defaultAnswer: '', content: '', typeName: '', typeValue: [], active: false,
            };
        const parentType = (this.props.model && this.props.field) ? this.props.model + this.props.field.upFirstChar() : 'null';
        $(this.btnSave.current).data('isNewMember', item == null);
        $('#questionTitle' + parentType).val(title);
        $('#questionDefault' + parentType).val(defaultAnswer);
        $('#questionAnswer' + parentType).val(typeValue.join('\n'));
        this.setState({
            selectedItem: {value: typeName, label: T.questionTypes[typeName]},
            itemId: item ? item._id : null,
            active: active,
        });
        this.editor.current.html(content ? content : '');
        $(this.modal.current).modal('show');
    }

    hide() {
        $(this.modal.current).modal('hide');
    }

    changeActive(event) {
        this.setState({active: event.target.checked});
    }

    onSelectType(selectedItem) {
        this.setState({selectedItem})
    }

    save(event) {
        const itemId = this.state.itemId, btnSave = $(this.btnSave.current), isNewMember = btnSave.data('isNewMember');
        const parentType = (this.props.model && this.props.field) ? this.props.model + this.props.field.upFirstChar() : 'null';
        const answerString = $('#questionAnswer' + parentType).val();
        let ret = (answerString ? answerString : '').split('\n');
        for (let i = 0; i < ret.length; i++) {
            if (ret[i] == '') ret.splice(i, 1);
        }

        const changes = {
            title: $('#questionTitle' + parentType).val().trim(),
            defaultAnswer: $('#questionDefault' + parentType).val() ? $('#questionDefault' + parentType).val().trim() : '',
            content: this.editor.current.html(),
            active: this.state.active,
            typeName: this.state.selectedItem ? this.state.selectedItem.value : null,
            typeValue: ret,
        };

        if (changes.title == '') {
            T.notify('Tên câu hỏi bị trống', 'danger');
            $('#questionTitle' + parentType).focus();
        } else if (changes.typeName == '' || !changes.typeName) {
            T.notify('Loại câu hỏi bị trống!', 'danger');
        } else {
            if (isNewMember) {
                this.props.add(changes);
                this.hide();
            } else {
                const updateChanges = changes;
                if (updateChanges.typeValue.length == 0 || updateChanges.typeValue[0] == '') updateChanges.typeValue = 'empty';
                this.props.update(itemId, updateChanges);
                this.hide();
            }

        }
        event.preventDefault();
    }

    render() {
        const select = this.state.selectedItem;
        const parentType = (this.props.model && this.props.field) ? this.props.model + this.props.field.upFirstChar() : 'null';
        let isShow = (select && select.value && (select.value == 'choice' || select.value == 'multiChoice')) ? 'block' : 'none';
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document'
                      onSubmit={e => this.save(e, this.state.itemId, true)}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin câu hỏi</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group row'>
                                <div className='col-12'>
                                    <label htmlFor={'questionTitle' + parentType}>Tên câu hỏi</label>
                                    <input type='text' className='form-control'
                                           id={'questionTitle' + parentType}/>
                                </div>
                            </div>
                            <div className='form-group row'>
                                <div className='col-4'>
                                    <label>Kích hoạt</label>
                                    <div className='col-12 col-sm-12 toggle'>
                                        <label>
                                            <input type='checkbox' checked={this.state.active}
                                                   onChange={this.changeActive}/><span
                                            className='button-indecator'/>
                                        </label>
                                    </div>
                                </div>
                                <div className='col-8'>
                                    <label htmlFor=''>Loại câu hỏi</label>
                                    <Select options={this.state.questionTypes} onChange={this.onSelectType}
                                            value={this.state.selectedItem}/>
                                </div>
                            </div>
                            <div className='form-group row'>
                                <div className='col-12'>
                                    <label htmlFor=''>Nội dung câu hỏi</label>
                                    <Editor ref={this.editor} placeholder='Nội dung sự kiện'/>
                                </div>
                            </div>

                            <div className='form-group row' style={{display: isShow}}>
                                <div key={0} className='col-12'>
                                    <label>Danh sách câu trả lời</label>
                                    <textarea defaultValue='' className='form-control'
                                              id={'questionAnswer' + parentType}
                                              style={{width: '100%', minHeight: '100px', padding: '0 3px'}}/>
                                </div>
                            </div>
                            <div className='form-group row'>
                                <div className='col-12'>
                                    <label htmlFor={'questionDefault' + parentType}>Câu trả lời mặc định</label>
                                    <input type='text' className='form-control'
                                           id={'questionDefault' + parentType}/>
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng
                            </button>
                            <button type='button' className='btn btn-primary' ref={this.btnSave}
                                    onClick={e => this.save(e, this.state.itemId, true)}>Lưu
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class QuestionFormComponent extends React.Component {
    constructor(props) {
        super(props);

        this.showModal = this.showModal.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
        this.updateQuestion = this.updateQuestion.bind(this);
        this.changeQuestionActive = this.changeQuestionActive.bind(this);
        this.swap = this.swap.bind(this);
        this.removeQuestion = this.removeQuestion.bind(this);

        this.questionModal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            const getData = (condition) => {
                if (condition) {
                    const {model, postId, field} = this.props;
                    this.props.getQuestionsList(model, field, postId);
                } else {
                    setTimeout(() => {
                        getData(this.props.model && this.props.postId && this.props.field);
                    }, 250);
                }
            };
            getData(this.props.model && this.props.postId && this.props.field);
        });
    }

    addQuestion(data) {
        const { model, field, postId, permission } = this.props;
        this.props.createQuestion(model, field, postId, permission, data, () => {
            T.notify('Thêm câu hỏi thành công!', 'info');
        });
    }

    updateQuestion(_id, changes) {
        const { model, field, postId, permission } = this.props;
        this.props.updateQuestion(_id, changes, model, field, postId, permission, () => {
            T.notify('Cập nhật câu hỏi thành công!', 'info');
        })
    }

    changeQuestionActive(item) {
        this.updateQuestion(item._id, {active: !item.active});
    }

    swap(e, index, isMoveUp) {
        const {model, postId, field, permission} = this.props;
        const questionList = this.props.question && this.props.question[field] ? this.props.question[field] : [];
        if (questionList.length == 1) {
            T.notify('Thay đổi thứ tự câu hỏi thành công', 'info');
        } else {
            if (isMoveUp) {
                if (index == 0) {
                    T.notify('Thay đổi thứ tự câu hỏi thành công', 'info');
                } else {
                    const temp = questionList[index - 1], changes = {};

                    questionList[index - 1] = questionList[index];
                    questionList[index] = temp;

                    changes[field] = questionList;
                    this.props.swapQuestion(postId, changes, model, field, permission, () => {
                        T.notify('Thay đổi thứ tự câu hỏi thành công', 'info');
                    });
                }
            } else {
                if (index == questionList.length - 1) {
                    T.notify('Thay đổi thứ tự câu hỏi thành công', 'info');
                } else {
                    const temp = questionList[index + 1], changes = {};

                    questionList[index + 1] = questionList[index];
                    questionList[index] = temp;

                    changes[field] = questionList;
                    this.props.swapQuestion(postId, changes, model, field, permission, () => {
                        T.notify('Thay đổi thứ tự câu hỏi thành công', 'info');
                    });
                }
            }
        }
        e.preventDefault();
    }

    removeQuestion(e, id, index) {
        T.confirm('Xóa Câu hỏi', 'Bạn có chắc bạn muốn xóa câu hỏi này?', true, isConfirm => {
            if (isConfirm) {
                const {model, postId, field, permission} = this.props, changes = {};
                let questionList = this.props.question && this.props.question[field] ? this.props.question[field] : [];
                questionList.splice(index, 1);
                if (questionList.length == 0) questionList = 'empty';
                changes[field] = questionList;
                this.props.deleteQuestion(id, postId, changes, model, field, permission, () => {
                    T.alert('Xoá câu hỏi thành công!', 'success', false, 1000);
                })
            } else {
                T.alert('Cancelled!', 'error', false, 500);
            }
        });
        e.preventDefault();
    }

    showModal(e, item) {
        this.questionModal.current.show(item);
        e.preventDefault();
    }

    render() {
        const {model, postId, field, title, permission, currentPermission} = this.props ? this.props : {title: '', permission: {}};
        const questionList = this.props.question && this.props.question[field] ? this.props.question[field] : [];
        const questionTable = questionList.length ? (
            <table className='table table-hover table-bordered'>
                <thead>
                <tr>
                    <th style={{width: 'auto'}}>STT</th>
                    <th style={{width: '100%', textAlign: 'center'}}>Câu hỏi</th>
                    <th style={{width: 'auto', textAlign: 'center'}}>Loại</th>
                    <th style={{width: 'auto'}} nowrap='true'>Kích hoạt</th>
                    {currentPermission.contains(permission) ? <th style={{width: 'auto', textAlign: 'center'}}>Thao tác</th> : null }
                </tr>
                </thead>
                <tbody>
                {questionList.map((item, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>
                            {currentPermission.contains(permission) ?
                            <a href='#' onClick={e => this.showModal(e, item)}>
                                {item.title}
                            </a> : item.title }
                        </td>
                        <td nowrap='true'>
                            {T.questionTypes[item.typeName]}
                        </td>
                        <td className='toggle' style={{textAlign: 'center'}}>
                            <label>
                                <input type='checkbox' checked={item.active} disabled={!currentPermission.contains(permission)}
                                       onChange={() => this.changeQuestionActive(item)}/><span
                                className='button-indecator'/>
                            </label>
                        </td>
                        {currentPermission.contains(permission) ?
                        <td className='btn-group'>
                            <a className='btn btn-success' href='#' onClick={e => this.swap(e, index, true)}>
                                <i className='fa fa-lg fa-arrow-up'/>
                            </a>
                            <a className='btn btn-success' href='#' onClick={e => this.swap(e, index, false)}>
                                <i className='fa fa-lg fa-arrow-down'/>
                            </a>
                            <button type='button' className='btn btn-primary'
                                    onClick={e => this.showModal(e, item)}>
                                <i className='fa fa-lg fa-edit'/>
                            </button>
                            <button type='button' className='btn btn-danger'
                                    onClick={e => this.removeQuestion(e, item._id, index)}
                            >
                                <i className='fa fa-lg fa-trash'/>
                            </button>
                        </td> : null }
                    </tr>
                ))}
                </tbody>
            </table>
        ) : <p>Không có câu hỏi</p>;
        return [
            <div className='tile' key={0}>
                <h3 className='tile-title'>{title}</h3>
                <div className='tile-body'>
                    {questionTable}
                </div>
                {currentPermission.contains(permission) ?
                <button type='button' className='btn btn-primary' style={{position: 'absolute', right: '30px', top: '10px'}}
                        onClick={e => this.showModal(e, null)}>
                    Thêm câu hỏi
                </button> : null}
            </div>,
            <QuestionModal key={1} _id={postId} model={model} field={field} add={this.addQuestion}
                           update={this.updateQuestion} getSchema={this.props.getSchema} ref={this.questionModal}/>
        ];
    }
}

const mapStateToProps = state => ({system: state.system, question: state.question});
const mapActionsToProps = { getQuestionsList, createQuestion, updateQuestion, swapQuestion, deleteQuestion };
export default connect(mapStateToProps, mapActionsToProps, null, {forwardRef: true})(QuestionFormComponent);