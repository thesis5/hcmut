import './material-button/icheck-material-custom.min.css';
import './material-button/icheck-material.min.css';
import React from 'react';
import { connect } from 'react-redux';
import { countAnswer, addAnswerByUser } from '../redux/answer.jsx';

class RegisterElement extends React.Component {
    constructor(props) {
        super(props);
        this.getValue = this.getValue.bind(this);
        this.setFocus = this.setFocus.bind(this);
        this.onSelectType = this.onSelectType.bind(this);
        this.onSiteChanged = this.onSiteChanged.bind(this);

        this.value = React.createRef();
        this.dateInput = React.createRef();
        this.state = { selectedValue: null };
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.dateInput.current).datepicker(T.birthdayFormat);
        });
    }

    onSelectType(selectedItem) {
        this.setState({ selectedValue: selectedItem.value });
    }

    onSiteChanged(e) {
        this.setState({ selectedValue: e.target.value });
    }

    getValue() {
        const element = this.props.element ? this.props.element : null;
        let defaultChoiceValue;
        if (element.typeName === 'choice' || element.typeName === 'multiChoice') {
            let index = parseInt(element.defaultAnswer), valueLength = element.typeValue.length;
            if (!index || 1 > index || index > valueLength) {
                defaultChoiceValue = element.typeValue[0];
            } else defaultChoiceValue = element.typeValue[index - 1];
        }
        if (element && element.active === false) {
            if (element.typeName === 'choice' || element.typeName === 'multiChoice') {
                return defaultChoiceValue;
            } else if (element.typeName === 'date') {
                return T.dateToText(new Date());
            } else {
                return this.props.element.defaultAnswer;
            }
        } else if (element && element.typeName === 'choice') {
            return this.state.selectedValue ? this.state.selectedValue : defaultChoiceValue;
        } else if (element && element.typeName === 'multiChoice') {
            let value = '';
            $('input[id*=checkbox' + element._id + ']:checked').each((index, val) => value += ';' + val.value);
            return value ? value.substring(1) : '';
        } else if (element && element.typeName === 'date') {
            return $(this.dateInput.current).val();
        } else {
            return $(this.value.current).val();
        }
    }

    setFocus(e) {
        $(this.value.current).focus();
        e.preventDefault();
    }

    render() {
        const item = this.props.element ? this.props.element : {
            _id: '',
            content: '',
            active: false,
            typeName: '',
            typeValue: []
        },
            questionContent = <div className='row'>
                <div className='col-auto'>
                    <p><strong>{this.props.index != null ? (this.props.index + 1) + '. ' : ' '}</strong></p>
                </div>
                <div className='col' style={{ marginLeft: '-15px' }}>
                    <p className='content-style' dangerouslySetInnerHTML={{ __html: item.content }} />
                </div>
            </div>;
        if (item.active) {
            if (item.typeName == 'choice') {
                return <div className='form-group'>
                    {questionContent}
                    {item.typeValue.map((value, index) => (
                        <div key={index} className='icheck-material-blue'>
                            <input
                                className='hidden' type='radio' id={'radio' + item._id + index.toString()}
                                value={value} checked={this.state.selectedValue === value} onChange={this.onSiteChanged} />
                            <label className='entry' htmlFor={'radio' + item._id + index.toString()}>
                                <div className='circle' />
                                <div className='entry-label'>{value}</div>
                            </label>
                        </div>
                    ))}
                </div>;
            } else if (item.typeName === 'multiChoice') {
                return (
                    <div className='form-group'>
                        {questionContent}
                        {item.typeValue.map((value, index) => (
                            <div key={index} className='icheck-material-blue'>
                                <input type='checkbox' id={'checkbox' + item._id + index} value={value} />
                                <label htmlFor={'checkbox' + item._id + index}>{value}</label>
                            </div>
                        ))}
                    </div>
                )
            } else if (item.typeName === 'date') {
                return (
                    <div className='form-group'>
                        {questionContent}
                        <input type='text' className='form-control' id={(this.props.index).toString()} ref={this.dateInput} />
                    </div>
                );
            } else if (item.typeName === 'textArea') {
                return (
                    <div className='form-group'>
                        {questionContent}
                        <textarea rows={4} className='form-control' id={(this.props.index).toString()} aria-describedby='help'
                            ref={this.value} />
                    </div>
                );
            } else {
                return (
                    <div className='form-group'>
                        {questionContent}
                        <input type='text' className='form-control' id={(this.props.index).toString()} aria-describedby='help'
                            ref={this.value} />
                    </div>
                );
            }
        } else return null;
    }
}

const texts = {
    vi: {
        register: 'Đăng ký sự kiện',
        notDated: 'Chưa đến ngày mở đăng ký',
        overDated: 'Đã quá hạn đăng ký tham gia',
        enough: 'Đã đủ số lượng đăng ký tham gia',
        noForm: 'Không có form đăng ký từ sự kiện này',
        registerAction: 'Đăng kí',
        notLoggedIn: 'Bạn chưa đăng nhập!'
    },
    en: {
        register: 'Register event',
        notDated: 'Not yet to open the registration',
        overDated: 'Registration deadline is exceeded',
        enough: 'Enough number of participants',
        noForm: 'No registration form from this event',
        registerAction: 'Register',
        notLoggedIn: `You haven't logged in yet!`
    }
};

class HomeRegistrationForm extends React.Component {
    constructor(props) {
        super(props);

        this.submit = this.submit.bind(this);
        this.valueList = [];
        for (let i = 0; i < 50; i++) {
            this.valueList[i] = React.createRef();
        }

        this.state = { total: 0 };
    }

    componentDidMount() {
        $(document).ready(() => {
            const { postId, field } = this.props;
            if (postId && field) {
                this.props.countAnswer(postId, field, (total) => {
                    this.setState({ total });
                });
            } else {
                setTimeout(() => {
                    this.props.countAnswer(postId, field, (total) => {
                        this.setState({ total });
                    });
                }, 250);
            }
        });
    }

    componentDidUpdate() {
        setTimeout(T.ftcoAnimate, 250);
    }

    submit(e) {
        const { postId, field, postType, questions, system } = this.props;
        const language = T.language(texts);
        const userId = system.user ? system.user._id : null;
        if (userId == null) {
            T.notify(language.notLoggedIn, 'danger');
            e.preventDefault();
            return;
        }
        let record = [];
        let i = 0;
        for (i; i < questions.length; i++) {
            const value = this.valueList[i].current.getValue();
            if (!value || value == '') {
                T.notify('Xin vui lòng nhập đầy đủ thông tin', 'danger');
                this.valueList[i].current.setFocus(e);
                break;
            } else {
                record.push({
                    questionId: questions[i]._id,
                    answer: value,
                });
            }
        }
        if (i == questions.length) {
            const newData = {
                field,
                user: userId,
                postId,
                record,
            };
            this.props.addAnswerByUser(newData, postType);
        }
        e.preventDefault();
    }

    render() {
        const { className, postId, formInfo, questions, system } = this.props;
        const language = T.language(texts);
        const createForm = () => {
            if (system && system.user && system.user._id && postId) {
                const { startRegister, stopRegister, maxRegisterUsers } = formInfo ? formInfo : {
                    startRegister: null,
                    stopRegister: null,
                    maxRegisterUsers: -1
                };
                const numOfRegisterUsers = this.state.total;
                const currentTime = new Date();
                if (startRegister && currentTime < new Date(startRegister)) {
                    return <p>{language.notDated}</p>;
                }
                if (stopRegister && currentTime > new Date(stopRegister)) {
                    return <p>{language.overDated}</p>;
                }
                if (maxRegisterUsers >= 0 && numOfRegisterUsers >= maxRegisterUsers) {
                    return <p>{language.enough}</p>;
                }
                if (!questions || questions.length == 0) {
                    return <p>{language.noForm}</p>;
                }

                let form = [];
                for (let i = 0; i < questions.length; i++) {
                    form.push(<RegisterElement key={i} element={questions[i]} index={i} ref={this.valueList[i]} />);
                }
                form.push(<button key='button' className='btn btn-primary'
                    onClick={this.submit}>{language.registerAction}</button>);
                return form;
            } else {
                return <p>{language.notLoggedIn}</p>;
            }
        };

        return (
            <div className={className}>
                <div className='col-12 col-md-8  ftco-animate'>
                    <div className='row'>
                        <div className='col-12'>
                            {createForm()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({ answer: state.answer, system: state.system });
const mapActionsToProps = { countAnswer, addAnswerByUser };
export default connect(mapStateToProps, mapActionsToProps)(HomeRegistrationForm);