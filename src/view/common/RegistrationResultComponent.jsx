import React from 'react';
import { connect } from 'react-redux';
import { adminGetEventRegistrationInPage, getAnswer, updateAnswer, deleteAnswer, exportRegisters } from '../redux/answer.jsx';
import Pagination from '../common/Pagination.jsx';
import EditAnswerModal from './RegistrationModals/EditAnswerModal.jsx';
import AddAnswerModal from './RegistrationModals/AddAnswerModal.jsx';
import ImportStudentModal from './RegistrationModals/ImportStudentModal.jsx';

class RegistrationResultComponent extends React.Component {
    constructor(props) {
        super(props);

        this.showEdit = this.showEdit.bind(this);
        this.addNew = this.addNew.bind(this);
        this.changeAttendance = this.changeAttendance.bind(this);
        this.export = this.export.bind(this);
        this.remove = this.remove.bind(this);

        this.modal = React.createRef();
        this.addModal = React.createRef();
        this.importModal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            const postId = this.props.postId;
            if (postId) {
                this.props.getPage();
            } else {
                setTimeout(this.props.getPage, 250);
            }
        });
    }

    componentDidUpdate() {
        T.tooltip();
    }
    
    addNew(e) {
        this.addModal.current.show();
        e.preventDefault();
    }

    showEdit(e, _id) {
        const { permission } = this.props ? this.props : {};
        this.props.getAnswer(_id, permission.manager, item => {
            this.modal.current.show(item);
        });
        e.preventDefault();
    }

    changeAttendance(event, item) {
        const { permission, getPageType } = this.props ? this.props : { permission: {}, getPageType: '' };
        this.props.updateAnswer(item._id, { attendance: !item.attendance }, getPageType, permission.roller,  () => {
            T.notify('Thay đổi điểm danh thành công!', 'success');
        });
        event.preventDefault();
    }
    
    export(e, fileName) {
        const { postId, field, permission, getPageType } = this.props ? this.props : { postId: null, field: '', permission: {}, getPageType: '' };
        const postType = getPageType.includes('event') ? 'event' : 'job';
        this.props.exportRegisters(postType, postId, field, permission.export, fileName);
        e.preventDefault();
    }
    
    remove(e, _id) {
        const { permission, getPageType } = this.props ? this.props : { permission: {}, getPageType: '' };
        T.confirm('Xoá câu trả lời', 'Bạn có chắc muốn xóa câu trả lời này?', 'info', isConfirm => {
            isConfirm && this.props.deleteAnswer(_id, getPageType, permission.manager);
        });
    }

    render() {
        const createRow = (list, pageNumber, pageSize) => {
            return list.map((item, index) => {
                return (
                    <tr key={index}>
                        <td style={{ whiteSpace: 'nowrap' }}>{(pageNumber - 1)*pageSize + index + 1}</td>
                        <td style={{ whileSpace: 'nowrap' }}>
                            {currentPermission.contains(permission.manager) ? (
                                <a href='#' onClick={e => this.showEdit(e, item._id)} >{item.user.lastname + ' ' + item.user.firstname}</a>
                            ) : item.user.lastname + ' ' + item.user.firstname}
                        </td>
                        <td className='toggle' style={{ textAlign: 'center' }}>
                            <label>
                                <input type='checkbox' disabled={!currentPermission.contains(permission.roller)}
                                       checked={item.attendance} onChange={e => this.changeAttendance(e, item)} /><span className='button-indecator' />
                            </label>
                        </td>
                        { currentPermission.contains(permission.manager) ? (
                            <td key='action' className='btn-group' >
                                <button type='button' className='btn btn-primary' data-toggle='tooltip' data-placement='top' title='Chỉnh sửa câu trả lời'
                                        onClick={e => this.showEdit(e, item._id)}
                                >
                                    <i className='fa fa-lg fa-edit' />
                                </button>
                                <button type='button' className='btn btn-danger' onClick={e => this.remove(e, item._id)}>
                                    <i className='fa fa-lg fa-trash' />
                                </button>
                            </td>
                            ) : null }
                    </tr>
                );
            });
        };
        const currentPermission = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        const { questions, postId, field, paginationName, permission, getPageType, maxRegisterUsers } = this.props ? this.props : { questions: [], postId: null, field: '', permission: {}, getPageType: '', maxRegisterUsers };
        const { totalItem, pageSize, pageTotal, pageNumber, list } = this.props.answer && this.props.answer[field] ?
            this.props.answer[field] : { totalItem: 0, pageSize: 50, pageTotal: 0, pageNumber: 1, list: [] };
        const table = list && list.length > 0 ? (
            <table className='table table-hover table-bordered'>
                <thead>
                <tr>
                    <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>STT</th>
                    <th style={{ width: '100%', whileSpace: 'nowrap' }}>Họ và tên</th>
                    <th style={{ width: 'auto' }} nowrap='true'>Điểm danh</th>
                    { currentPermission.contains(permission.manager) ? (
                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                    ) : null }
                </tr>
                </thead>
                <tbody>
                {createRow(list, pageNumber, pageSize)}
                </tbody>
            </table>
        ) : <p>Không có đăng ký nào</p>;

        return (
            <div>
                <div className='row tile'>
                    {table}
                </div>
                <Pagination name={paginationName} style={{marginLeft: '75px'}}
                    pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getPage} />
                { currentPermission.contains(permission.import) ? (
                    <button type='button' className='btn btn-info btn-circle'
                            onClick={() => this.importModal.current.show()}
                            style={{ position: 'fixed', right: '130px', bottom: '10px' }}
                            data-toggle='tooltip' data-placement='top' title='Import danh sách tham gia'>
                        <i className='fa fa-lg fa-cloud-upload'/>
                    </button>
                ): null}
                { currentPermission.contains(permission.export) ? (
                    <button type='button' className='btn btn-success btn-circle'
                        onClick={(e) => this.export(e, 'Danh sách tham dự')} style={{ position: 'fixed', right: '70px', bottom: '10px' }}
                        data-toggle='tooltip' data-placement='top' title='Xuất danh sách tham gia'
                    >
                        <i className='fa fa-lg fa-file-excel-o'/>
                    </button>
                ) : null}
                { currentPermission.contains(permission.manager) ? (
                    <button type='button' data-toggle='tooltip' data-placement='top' title='Thêm người tham gia'
                            className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.addNew}>
                        <i className='fa fa-lg fa-plus' />
                    </button>
                ) : null }
                <EditAnswerModal questions={questions} permission={permission} getPageType={getPageType} ref={this.modal} />
                <AddAnswerModal questions={questions} permission={permission} postId={postId} field={field} getPageType={getPageType} ref={this.addModal} />
                <ImportStudentModal questions={questions} permission={permission} postId={postId} field={field} getPageType={getPageType} maxRegisterUsers={maxRegisterUsers} ref={this.importModal}/>
            </div>
        );
    }

}

const mapStateToProps = state => ({ system: state.system, answer: state.answer });
const mapActionsToProps = { adminGetEventRegistrationInPage, updateAnswer, deleteAnswer, getAnswer, exportRegisters };
export default connect(mapStateToProps, mapActionsToProps)(RegistrationResultComponent);