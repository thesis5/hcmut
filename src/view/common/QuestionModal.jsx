import React from 'react';
import Select from 'react-select';
import Editor from '../common/CkEditor4.jsx';

export default class QuestionModal extends React.Component {
    constructor(props) {
        super(props);
        let types = Object.keys(T.questionTypes).map(key => ({ value: key, label: T.questionTypes[key] }));
        this.state = { questionTypes: types, itemID: null, value: [], active: false };

        this.modal = React.createRef();
        this.btnSave = React.createRef();
        this.editor = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            setTimeout(() => $(this.modal.current).on('shown.bs.modal', () => $('#questionTitle').focus()), 250);
        });
    }

    show = (item, index) => {
        const { title, defaultAnswer, content, typeName, typeValue, active } = item ?
            item : { title: '', defaultAnswer: '', content: '', typeName: '', typeValue: [], active: false };
        const parentType = this.props.type ? this.props.type : 'null';
        $(this.btnSave.current).data('isNewMember', item == null).data('index', index);
        $('#questionTitle' + parentType).val(title);
        $('#questionDefault' + parentType).val(defaultAnswer);
        $('#questionAnswer' + parentType).val(typeValue.join('\n'));
        this.setState({ selectedItem: { value: typeName, label: T.questionTypes[typeName] } });
        this.setState({ itemId: item ? item._id : null, active: active });
        this.editor.current.html(content ? content : '');
        $(this.modal.current).modal('show');
    }

    hide = () => {
        $(this.modal.current).modal('hide');
    }

    changeActive = (event) => {
        this.setState({ active: event.target.checked });
    }

    onSelectType = (selectedItem) => {
        this.setState({ selectedItem })
    }

    save = (event, itemId, isClose) => {
        const btnSave = $(this.btnSave.current),
            isNewMember = btnSave.data('isNewMember');
        const parentType = this.props.type ? this.props.type : 'null';
        const answerString = $('#questionAnswer' + parentType).val();
        let ret = answerString.split('\n');
        for (let i = 0; i < ret.length; i++) {
            if (ret[i] == '') ret.splice(i, 1);
        }

        const changes = {
            title: $('#questionTitle' + parentType).val().trim(),
            defaultAnswer: $('#questionDefault' + parentType).val().trim(),
            content: this.editor.current.html(),
            active: this.state.active,
            typeName: this.state.selectedItem ? this.state.selectedItem.value : null,
            typeValue: ret,
        };

        if (changes.title == '') {
            T.notify('Tên câu hỏi bị trống', 'danger');
            $('#questionTitle' + parentType).focus();
        } else if (changes.defaultAnswer == '') {
            T.notify('Câu trả lời mặc định bị trống', 'danger');
            $('#questionDefault' + parentType).focus();
        } else if (changes.typeName == '' || !changes.typeName) {
            T.notify('Loại câu hỏi bị trống!', 'danger');
        } else {
            if (isNewMember) { //Add
                this.props.add(changes);
                this.hide();
            }
            else {//Update
                let updateChanges = changes;
                if (updateChanges.typeValue.length == 0 || updateChanges.typeValue[0] == '') updateChanges.typeValue = 'empty';
                this.props.update(itemId, updateChanges);
                if (isClose) this.hide();
            }

        }
        event.preventDefault();
    }

    render() {
        const select = this.state.selectedItem;
        const parentType = this.props.type ? this.props.type : 'null';
        let isShow = (select && select.value && select.value == 'choice') ? 'block' : 'none';
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={e => this.save(e, this.state.itemId, true)}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Thông tin câu hỏi</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group row'>
                                <div className='col-12'>
                                    <label htmlFor={'questionTitle' + parentType}>Tên câu hỏi</label>
                                    <input type='text' className='form-control' id={'questionTitle' + parentType} />
                                </div>
                            </div>
                            <div className='form-group row'>
                                <div className='col-4'>
                                    <label>Kích hoạt</label>
                                    <div className='col-12 col-sm-12 toggle'>
                                        <label>
                                            <input type='checkbox' checked={this.state.active} onChange={this.changeActive} /><span className='button-indecator' />
                                        </label>
                                    </div>
                                </div>
                                <div className='col-8'>
                                    <label htmlFor=''>Loại câu hỏi</label>
                                    <Select options={this.state.questionTypes} onChange={this.onSelectType} value={this.state.selectedItem} />
                                </div>
                            </div>
                            <div className='form-group row'>
                                <div className='col-12'>
                                    <label htmlFor=''>Nội dung câu hỏi</label>
                                    <Editor ref={this.editor} placeholder='Nội dung sự kiện' />
                                </div>
                            </div>
                            <div className='form-group row'>
                                <div className='col-12'>
                                    <label htmlFor={'questionDefault' + parentType}>Câu trả lời mặc định</label>
                                    <input type='text' className='form-control' id={'questionDefault' + parentType} />
                                </div>
                            </div>

                            <div className='form-group row' style={{ display: isShow }}>
                                <div className='col-12'>
                                    <label>Danh sách câu trả lời</label>
                                    <textarea defaultValue='' id={'questionAnswer' + parentType} style={{ border: 'solid 1px #eee', width: '100%', minHeight: '100px', padding: '0 3px' }} />
                                </div>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='button' className='btn btn-primary' ref={this.btnSave} onClick={e => this.save(e, this.state.itemId, true)}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}