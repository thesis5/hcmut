import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FileBox from '../common/FileBox.jsx';
import { updateMultiBonusPoint } from '../redux/point.jsx';

class BonusPointModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = { index: -1 };
        this.modal = React.createRef();
    }

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => {
                $('#rewardPoint').focus();
            });

            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ index: -1 })
            });
        })
    }


    show = (index, item) => {
        const { rewardPoint, penaltyPoint } = item ? item : { rewardPoint: 0, penaltyPoint: 0 };
        $('#rewardPoint').val(rewardPoint);
        $('#penaltyPoint').val(penaltyPoint);
        this.setState({ index });
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            rewardPoint: parseInt($('#rewardPoint').val()),
            penaltyPoint: parseInt($('#penaltyPoint').val())
        };

        if (isNaN(changes.rewardPoint)) {
            T.notify('Điểm thưởng không chính xác!', 'danger');
            $('#rewardPoint').focus();
        } else if (isNaN(changes.penaltyPoint)) {
            T.notify('Điểm phạt không chính xác!', 'danger');
            $('#penaltyPoint').focus();
        } else {
            this.props.update(this.state.index, changes, () => {
                $(this.modal.current).modal('hide');
            });
        }
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Cập nhật điểm thưởng</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='rewardPoint'>Điểm thưởng</label>
                                <input className='form-control is-valid' id='rewardPoint' type='number'
                                    placeholder='Điểm thưởng' />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='penaltyPoint'>Điểm phạt</label>
                                <input className='form-control is-invalid' id='penaltyPoint' type='number'
                                    placeholder='Điểm phạt' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

class YearPointBonusImportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errorLog: false, numberOfErrors: 0, warnings: [] };

        this.editModal = React.createRef();
    }

    componentDidMount() {
        T.ready('/user/summary/bonus');
    }

    onSuccess = (response) => {
        if (response.numberOfErrors > 0 || response.warnings.length > 0) {
            this.setState({ errorLog: true, numberOfErrors: response.numberOfErrors, warnings: response.warnings });
        }
        if (response.points.length > 0) {
            this.setState({ points: response.points, message: <p className='text-center' style={{ color: 'green' }}>{response.points.length} hàng được tải lên thành công</p> });
        }
    };

    showEdit = (e, index, item) => {
        e.preventDefault();
        this.editModal.current.show(index, item);
    };

    update = (index, changes, done) => {
        const points = this.state.points, currentValue = points[index];
        const updateValue = Object.assign({}, currentValue, { rewardPoint: changes.rewardPoint, penaltyPoint: changes.penaltyPoint });
        points.splice(index, 1, updateValue);
        this.setState({ points });
        done && done();
    };

    delete = (e, index) => {
        e.preventDefault();
        const points = this.state.points;
        points.splice(index, 1);
        this.setState({ points });
    };

    save = (e) => {
        e.preventDefault();
        this.props.updateMultiBonusPoint(this.state.points, () => {
            T.notify('Cập nhật điểm thành công!', 'success');
            this.props.history.push('/user/summary/bonus');
        })
    };

    render() {
        const { errorLog, numberOfErrors, warnings, points } = this.state;
        let table = null;
        if (points && points.length > 0) {
            table = (
                <table className='table table-hover table-bordered table-responsive'>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: 'auto' }}>MSCB</th>
                            <th style={{ width: '100%' }}>Họ tên</th>
                            <th style={{ width: 'auto' }}>Email</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm thưởng</th>
                            <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Điểm phạt</th>
                            <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {points.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                                <td style={{ textAlign: 'right' }}>{item.userInfo.organizationId}</td>
                                <td>{item.userInfo.lastname + ' ' + item.userInfo.firstname}</td>
                                <td>{item.userInfo.email}</td>
                                <td className='text-info text-right'>{item.rewardPoint}</td>
                                <td className='text-danger text-right'>{item.penaltyPoint}</td>
                                <td>
                                    <div className='btn-group'>
                                        <a className='btn btn-primary' href='#' onClick={e => this.showEdit(e, index, item)}>
                                            <i className='fa fa-lg fa-edit' />
                                        </a>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có người dùng!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-user' /> Tải lên file cập nhật điểm</h1>
                </div>
                <div className='row'>
                    <div className='col-12 col-md-6 offset-md-3'>
                        <div className='tile'>
                            <div className='tile-body'>
                                <FileBox ref={this.fileBox} postUrl='/user/upload' uploadType='BonusPointFile' userData='bonusPointImportData' style={{ width: '100%', backgroundColor: '#fdfdfd' }} success={this.onSuccess} />
                                {this.state.message}
                                {errorLog ? (
                                    <div>
                                        {numberOfErrors > 0 ? <p className='text-danger'>Số hàng xảy ra lỗi: {numberOfErrors}</p> : null}
                                        {warnings.length > 0 ? <p className='text-warning' dangerouslySetInnerHTML={{ __html: warnings.toString().replaceAll(',', '') }} /> : null}
                                    </div>
                                ) : null}
                            </div>
                            <div className='tile-footer text-right'>
                                <a href='/download/SampleUploadUpdatePoint.xlsx' className='btn btn-info'>Tải xuống file mẫu</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    {points && points.length ? (
                        <div className='tile col-12'>
                            {table}
                        </div>
                    ) : null}
                </div>
                <Link to='/user/summary/bonus' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
                <button type='button' className='btn btn-primary btn-circle'
                    style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={this.save}>
                    <i className='fa fa-lg fa-save' />
                </button>
                <BonusPointModal ref={this.editModal} update={this.update} />
            </main>
        );
    }
}

const mapStateToProps = state => ({});
const mapActionsToProps = { updateMultiBonusPoint };
export default connect(mapStateToProps, mapActionsToProps)(YearPointBonusImportPage);
