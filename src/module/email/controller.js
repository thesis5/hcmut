module.exports = app => {
    app.permission.add(
        {
            name: 'system:email',
            menu: {
                parentMenu: { index: 2000, title: 'Cấu hình', icon: 'fa-cog' },
                menus: { 2020: { title: 'Email', link: '/user/email' } }
            },
        }
    );
    app.get('/user/email', app.permission.check('system:email'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------

    const EmailParams = [
        'emailRegisterMemberTitle', 'emailRegisterMemberText', 'emailRegisterMemberHtml',
        'emailCreateMemberByAdminTitle', 'emailCreateMemberByAdminText', 'emailCreateMemberByAdminHtml',
        'emailNewPasswordTitle', 'emailNewPasswordText', 'emailNewPasswordHtml',
        'emailForgotPasswordTitle', 'emailForgotPasswordText', 'emailForgotPasswordHtml',
        'emailContactTitle', 'emailContactText', 'emailContactHtml',
    ];

    app.get('/api/email/all', app.permission.check('system:email'), (req, res) => app.model.setting.get(EmailParams, result => res.send(result)));

    app.put('/api/email', app.permission.check('system:email'), (req, res) => {
        const title = req.body.type + 'Title',
            text = req.body.type + 'Text',
            html = req.body.type + 'Html',
            changes = {};

        if (EmailParams.indexOf(title) != -1) changes[title] = req.body.email.title;
        if (EmailParams.indexOf(text) != -1) changes[text] = req.body.email.text;
        if (EmailParams.indexOf(html) != -1) changes[html] = req.body.email.html;

        app.model.setting.set(changes, error => res.send({ error }));
    });
};