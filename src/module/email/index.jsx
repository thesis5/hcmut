import Loadable from 'react-loadable';
import Loading from '../../view/common/Loading.jsx';

export default {
    redux: {
    },
    routes: [
        {
            path: '/user/email',
            component: Loadable({ loading: Loading, loader: () => import('./admin.jsx') })
        }
    ],
};