import React from 'react';
import Loadable from 'react-loadable';
import Loading from '../../view/common/Loading.jsx';
import homeSection from './home.jsx';
import contact from './redux.jsx';

let showModal = null;
let PageTag = Loadable({ loading: Loading, loader: () => import('./admin.jsx') });
export function initContact(showContactModal) {
    // PageTag = <PageTag showContactModal={showContactModal} />;
    showModal = showContactModal;
}

export default {
    redux: {
        contact,
    },
    routes: [
        {
            path: '/user/contact',
            component: <PageTag showContactModal={contact => console.log(contact)} />
        }
    ],
    Section: homeSection
};