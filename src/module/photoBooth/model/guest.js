module.exports = app => {
    const schema = app.db.Schema({
        numberId: String,
        photoBooth: { type: app.db.Schema.Types.ObjectId, ref: 'PhotoBooth' },
        priority: Number,
        fullname: String,
        description: String,
        company: String,
        image: String,
        active: { type: Boolean, default: false },
    });
    const model = app.db.model('Guest', schema);

    app.model.guest = {
        create: (data, done) => model.create(data, done),

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ numberId: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, list) => {
                    result.list = list;
                    done(error, result);
                });
            }
        }),

        get: (condition, done) => typeof condition == 'object' ? model.findOne(condition, done) : model.findById(condition, done),

        getAll: (condition, selector, done) => done ? model.find(condition, selector).sort({ priority: -1 }).exec(done) : model.find(condition).sort({ priority: -1 }).exec(selector),

        update: (_id, $set, $unset, done) => done ?
            model.findOneAndUpdate({ _id }, { $set, $unset }, { new: true }, done) :
            model.findOneAndUpdate({ _id }, { $set }, { new: true }, $unset),

        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done && done(error);
            } else if (item == null) {
                done && done('Id không hợp lệ!');
            } else {
                app.deleteImage(item.image, () => item.remove(done));
            }
        }),

        deleteMany: (condition, done) => model.find(condition).exec((error, items) => {
            if (error) {
                done && done(error);
            } else {
                (items.length ? items : []).forEach(item => app.model.guest.delete(item._id));
                done && done();
            }
        }),

        deleteImage: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                app.deleteImage(item.image, () => {
                    model.findOneAndUpdate({ _id: item._id }, { $unset: { image: null } }, { new: true }, done)
                });
            }
        })
    };
};
