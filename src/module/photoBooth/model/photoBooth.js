module.exports = app => {
    const schema = app.db.Schema({
        title: String,
        photographer: { type: app.db.Schema.Types.ObjectId, ref: 'User' },
        active: { type: Boolean, default: false },
        password: String,
        image: String,
    });

    schema.methods.equalPassword = function (password) {
        return app.crypt.compareSync(password, this.password);
    };

    const model = app.db.model('PhotoBooth', schema);
    app.model.photoBooth = {
        hashPassword: password => app.crypt.hashSync(password, app.crypt.genSaltSync(8), null),

        auth: (email, password, done) => app.model.user.get({ email }, (error, user) => {
            if (error || user == null) {
                done(null);
            } else {
                model.findOne({ photographer: user._id }, (error, photoBooth) =>
                    done(error == null && photoBooth != null && photoBooth.equalPassword(password) ? photoBooth : null));
            }
        }),

        create: (data, done) => {
            data = app.clone(data, { password: data.password ? app.model.guest.hashPassword(data.password) : null });
            model.create(data, done);
        },

        getAll: (condition, done) => done ? model.find(condition, '-password').sort({ title: 1 }).exec(done) : model.find({}).sort({ title: 1 }).exec(condition),

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);

                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition, '-password').sort({ title: 1 }).skip(skipNumber).limit(result.pageSize).populate('photographer', '-password -token -tokenDate').exec((error, list) => {
                    result.list = list;
                    done(error, result);
                });
            }
        }),

        get: (condition, done) => typeof condition == 'object' ?
            model.findOne(condition, '-password').populate('photographer', '-password -token -tokenDate').exec(done) :
            model.findById(condition, '-password').populate('photographer', '-password -token -tokenDate').exec(done),

        update: (_id, changes, done) => {
            if (changes.password) changes.password = app.model.photoBooth.hashPassword(changes.password);
            model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
        },

        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                app.model.guest.deleteMany({ photoBooth: item._id }, (error) => {
                    if (error) {
                        done(error);
                    } else {
                        app.deleteImage(item.image, () => {
                            item.remove(done);
                        });
                    }
                });
            }
        }),

        deleteImage: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                app.deleteImage(item.image, () => {
                    model.findOneAndUpdate({ _id: item._id }, { $unset: { image: null } }, { new: true }, done)
                });
            }
        })
    };
};
