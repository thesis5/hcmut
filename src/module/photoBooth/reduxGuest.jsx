import T from '../../view/common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET = 'guest:getItem';
const GET_ALL = 'guest:getAll';
const GET_IN_PAGE = 'guest:getInPage';
const ADD = 'guest:ADD';
const UPDATE = 'guest:update';
const DELETE = 'guest:delete';
const ADD_FILE = 'guest:addFile';
const DELETE_FILE = 'guest:deleteFile';
const EMPTY_FILE = 'guest:emptyFile';

export default function guestReducer(state = null, data) {
    switch (data.type) {
        case GET:
            return Object.assign({}, state, { current: data.item });

        case GET_ALL:
            return Object.assign({}, state, { items: data.items, files: data.files });

        case GET_IN_PAGE:
            return Object.assign({}, state, { page: data.page, pageCondition: data.pageCondition ? data.pageCondition : state.pageCondition });

        case ADD: if (state) {
            let items = [...state.items],
                page = Object.assign({}, state.page),
                item = data.item;

            if (items && items.length) {
                items.unshift(item);
            }
            if (page && page.list) {
                page.list.unshift(item);
            }
            return Object.assign({}, state, { items, page });
        } else {
            return null;
        }

        case UPDATE:
            if (state) {
                let updatedItems = [...state.items],
                    updatedPage = Object.assign({}, state.page),
                    updatedItem = data.item;
                if (updatedItems && updatedItems.length) {
                    for (let i = 0, n = updatedItems.length; i < n; i++) {
                        if (updatedItems[i]._id == updatedItem._id) {
                            updatedItems.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                if (updatedPage && updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                updatedItems.sort((a, b) => { return b.priority - a.priority });
                return Object.assign({}, state, { items: updatedItems, page: updatedPage });
            } else {
                return null;
            }

        case DELETE:
            if (state) {
                let items = [...state.items], page = Object.assign({}, state.page), _id = data._id;
                if (items && items.length) {
                    for (let i = 0, n = items.length; i < n; i++) {
                        if (items[i]._id == _id) {
                            items.splice(i, 1);
                            break;
                        }
                    }
                }
                if (page && page.list) {
                    for (let i = 0, n = page.list.length; i < n; i++) {
                        if (page.list[i]._id == _id) {
                            page.list.splice(i, 1);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { items, page });
            } else {
                return null;
            }

        case ADD_FILE: {
            let files = [...state.files];
            files.indexOf(data.file) == -1 && files.push(data.file);
            return Object.assign({}, state, { files });
        }

        case DELETE_FILE: {
            let files = [...state.files];
            if (files && files.length) {
                for (let i = 0; i < files.length; i++) {
                    if (files[i] == data.file) {
                        files.splice(i,1);
                        break;
                    }
                }
            }

            return Object.assign({}, state, { files });
        }

        case EMPTY_FILE: {
            return Object.assign({}, state, { files: [] });
        }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
export function getAllGuest(condition, done) {
    return dispatch => {
        const url = '/api/guest/all';
        T.get(url, { condition: condition ? condition : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách khách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items, data.files);
                dispatch({ type: GET_ALL, items: data.items ? data.items : [], files: data.files });
            }
        }, () => T.notify('Lấy danh sách khách bị lỗi!', 'danger'));
    }
}

T.initCookiePage('pageGuest');
export function getGuestInPage(pageNumber, pageSize, condition, done) {
    const page = T.updatePage('pageGuest', pageNumber, pageSize);
    return (dispatch, getState) => {
        const url = '/api/guest/page/' + page.pageNumber + '/' + page.pageSize, state = getState(),
            pageCondition = state && state.guest && state.guest.pageCondition ? state.guest.pageCondition : {};
        T.get(url, { condition: condition ? condition : pageCondition }, data => {
            if (data.error) {
                T.notify('Lấy danh sách khách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_IN_PAGE, page: data.page, pageCondition: condition });
            }
        }, () => T.notify('Lấy danh sách khách bị lỗi!', 'danger'));
    }
}

export function getGuest(guestId, done) {
    return dispatch => {
        const url = '/api/guest/item/' + guestId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy thông tin khách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: GET, item: data.item });
                if (done) done(data.item);
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}

export function createGuest(data, done) {
    return dispatch => {
        const url = '/api/guest';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo khách bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: ADD, item: data.item });
                done && done(data.item);
            }
        }, error => T.notify('Tạo Guest bị lỗi!', 'danger'));
    }
}

export function assigneeImage(_id, file, active, done) {
    return dispatch => {
        const url = '/api/guest/assign-image';
        T.put(url, { _id, file, active }, data => {
            if (data.error) {
                T.notify('Gán hình cho khách bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                // dispatch({ type: UPDATE, item: data.item });
                // dispatch({ type: DELETE_FILE, file });
                done && done(data.item);
            }
        }, error => T.notify('Gán hình cho khách bị lỗi!', 'danger'));
    }
}

export function updateGuest(_id, changes, done) {
    return dispatch => {
        const url = '/api/guest';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật khách bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: UPDATE, item: data.item });
                done && done(data.item);
            }
        }, error => T.notify('Cập nhật khách bị lỗi!', 'danger'));
    }
}

export function changeImageGuest(data, done) {
    return dispatch => {
        const url = '/api/guest/change-image';
        T.put(url, data, res => {
            if (res.error) {
                T.notify('Đổi người gán ảnh bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + res.error);
            } else {
                dispatch({ type: UPDATE, item: res.oldItem });
                dispatch({ type: UPDATE, item: res.newItem });
                done && done(res);
            }
        }, error => T.notify('Đổi người gán ảnh bị lỗi!', 'danger'));
    }
}

export function deleteGuest(_id) {
    return dispatch => {
        const url = '/api/guest';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa khách bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Khách đã xóa thành công!', 'error', false, 800);
                dispatch({ type: DELETE, _id });
            }
        }, () => T.notify('Xóa khách bị lỗi!', 'danger'));
    }
}

export function deleteAllGuest(photoBooth) {
    return dispatch => {
        const url = '/api/guest/all';
        T.delete(url, { photoBooth }, data => {
            if (data.error) {
                T.notify('Xóa khách bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Tất cả khách đã xóa thành công!', 'error', false, 800);
                dispatch(getAllGuest({ photoBooth }));
                dispatch(getGuestInPage(undefined, undefined, { photoBooth }));
            }
        }, () => T.notify('Xóa khách bị lỗi!', 'danger'));
    }
}

export function deleteFile(_id, file, done) {
    return dispatch => {
        const url = '/api/guest/delete-file';
        T.delete(url, { _id, file }, () => {
            T.alert('Xóa thành công', 'error', false, 900);
            dispatch({ type: DELETE_FILE, file });
            done && done();
        });
    }
}

export function deleteGuestImage(_id, done) {
    return dispatch => {
        const url = '/api/guest/image';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa ảnh nền bị lỗi', 'danger');
            } else {
                T.alert('Xóa ảnh nền thành công', 'success', false, 800);
                done && done(data.item);
            }
        }, () => T.notify('Xóa ảnh nền bị lỗi', 'danger'));
    }
}

export function addFile(file) {
    return { type: ADD_FILE, file };
}

export function removeFile(file) {
    return { type: DELETE_FILE, file };
}

export function deleteAllAssetFile(photoBooth, done) {
    return dispatch => {
        const url = '/api/guest/all-files';
        T.delete(url, { photoBooth }, () => {
            T.alert('Xóa tất cả hình ảnh thành công!', 'error', false, 900);
            dispatch({ type: EMPTY_FILE });
            done && done();
        });
    }
}

export function addGuest(item, done) {
    return (dispatch) => {
        dispatch({ type: ADD, item });
        done && done()
    };
}

export function changeGuest(item, done) {
    return (dispatch) => {
        dispatch({ type: UPDATE, item });
        done && done()
    };
}

export function homeGetAllGuest(condition, done) {
    return dispatch => {
        const url = '/home/guest/all';
        T.get(url, { condition: condition ? condition : {} }, data => {
            if (data.error) {
                T.notify('Lấy danh sách khách bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_ALL, items: data.items ? data.items : [] });
            }
        }, () => T.notify('Lấy danh sách khách bị lỗi!', 'danger'));
    }
}
