import T from '../../view/common/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const GET = 'photoBooth:getItem';
const GET_ALL = 'photoBooth:getAll';
const GET_IN_PAGE = 'photoBooth:getInPage';
const UPDATE = 'photoBooth:update';
const GET_NEW_PHOTOS = 'photoBooth:getNewPhotos';

export default function photoBoothReducer(state = null, data) {
    switch (data.type) {
        case GET:
            return Object.assign({}, state, { current: data.item });

        case GET_ALL:
            return Object.assign({}, state, { items: data.items });

        case GET_IN_PAGE:
            return Object.assign({}, state, { page: data.page });

        case GET_NEW_PHOTOS:
            return Object.assign({}, state, { newPhotos: data.newPhotos });

        case UPDATE:
            if (state) {
                let updatedItems = Object.assign({}, state.items),
                    updatedPage = Object.assign({}, state.page),
                    updatedItem = data.item;
                if (updatedItems && updatedItems.length) {
                    for (let i = 0, n = updatedItems.length; i < n; i++) {
                        if (updatedItems[i]._id == updatedItem._id) {
                            updatedItems.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                if (updatedPage && updatedPage.list) {
                    for (let i = 0, n = updatedPage.list.length; i < n; i++) {
                        if (updatedPage.list[i]._id == updatedItem._id) {
                            updatedPage.list.splice(i, 1, updatedItem);
                            break;
                        }
                    }
                }
                return Object.assign({}, state, { items: updatedItems, page: updatedPage });
            } else {
                return null;
            }

        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
export function getAllPhotoBooths(done) {
    return dispatch => {
        const url = '/api/photo-booth/all';
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách Photo Booth bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.items);
                dispatch({ type: GET_ALL, items: data.items ? data.items : [] });
            }
        }, () => T.notify('Lấy danh sách Photo Booth bị lỗi!', 'danger'));
    }
}

T.initCookiePage('pagePhotoBooth');
export function getPhotoBoothInPage(pageNumber, pageSize, done) {
    const page = T.updatePage('pagePhotoBooth', pageNumber, pageSize);
    return dispatch => {
        const url = '/api/photo-booth/page/' + page.pageNumber + '/' + page.pageSize;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách Photo Booth bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                if (done) done(data.page.pageNumber, data.page.pageSize, data.page.pageTotal, data.page.totalItem);
                dispatch({ type: GET_IN_PAGE, page: data.page });
            }
        }, () => T.notify('Lấy danh sách Photo Booth bị lỗi!', 'danger'));
    }
}

export function getPhotoBooth(photoBoothId, done) {
    return dispatch => {
        const url = '/api/photo-booth/item/' + photoBoothId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy thông tin Photo Booth bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: GET, item: data.item });
                if (done) done(data.item);
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}

export function createPhotoBooth(data, done) {
    return dispatch => {
        const url = '/api/photo-booth';
        T.post(url, { data }, data => {
            if (data.error) {
                T.notify('Tạo Photo Booth bị lỗi!', 'danger');
                console.error('POST: ' + url + '. ' + data.error);
            } else {
                dispatch(getPhotoBoothInPage());
                done && done(data.item);
            }
        }, error => T.notify('Tạo Photo Booth bị lỗi!', 'danger'));
    }
}

export function updatePhotoBooth(_id, changes, done) {
    return dispatch => {
        const url = '/api/photo-booth';
        T.put(url, { _id, changes }, data => {
            if (data.error) {
                T.notify('Cập nhật Photo Booth bị lỗi!', 'danger');
                console.error('PUT: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: UPDATE, item: data.item });
                done && done(data.item);
            }
        }, error => T.notify('Cập nhật Photo Booth bị lỗi!', 'danger'));
    }
}

export function deletePhotoBooth(_id) {
    return dispatch => {
        const url = '/api/photo-booth';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa Photo Booth bị lỗi!', 'danger');
                console.error('DELETE: ' + url + '. ' + data.error);
            } else {
                T.alert('Photo Booth đã xóa thành công!', 'error', false, 800);
                dispatch(getPhotoBoothInPage());
            }
        }, () => T.notify('Xóa Photo Booth bị lỗi!', 'danger'));
    }
}

export function deletePhotoBoothImage(_id, done) {
    return dispatch => {
        const url = '/api/photo-booth/image';
        T.delete(url, { _id }, data => {
            if (data.error) {
                T.notify('Xóa ảnh nền bị lỗi', 'danger');
            } else {
                T.alert('Xóa ảnh nền thành công', 'success', false, 800);
                done && done(data.item);
            }
        }, () => T.notify('Xóa ảnh nền bị lỗi', 'danger'));
    }
}

export function searchPhotographerByEmail(email, done) {
    return dispatch => {
        const url = '/api/user-search/' + email;
        T.get(url, data => {
            if (data.error) {
                T.notify('Tìm kiếm người chụp hình bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else if (!data.user) {
                T.notify('Không tồn tại người dùng trong hệ thống!', 'warning');
            } else {
                done && done(data.user);
            }
        }, () => T.notify('Tìm kiếm người chụp hình bị lỗi!', 'danger'));
    }
}

export function changePhotoBooth(item) {
    return { type: UPDATE, item };
}

export function downloadPhotoBooth() {
    T.download(T.url('/api/photo-booth/download'), 'photoBooth.xlsx');
}

export function sendEmailToPhotoBooth(mailSubject, mailText, mailHtml, done) {
    const url = '/api/photoBooth/send';
    T.post(url, { mailSubject, mailText, mailHtml }, data => {
        if (data.error) {
            T.notify('Gửi email đến photoBooth bị lỗi!', 'danger');
            console.error('POST: ' + url + '. ' + data.error);
        } else {
            T.alert('Gửi email đến photoBooth thành công!', 'error', false, 800);
            if (done) done();
        }
    }, error => T.notify('Gửi email đến photoBooth bị lỗi!', 'danger'));
}

export function homeGetPhotoBooth(photoBoothId, done) {
    return dispatch => {
        const url = '/home/photo-booth/item/' + photoBoothId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy thông tin Photo Booth bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: GET, item: data.item });
                if (done) done(data.item);
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}

export function getNewPhotos(photoBoothId) {
    return dispatch => {
        const url = '/home/photo-booth/new-photos/' + photoBoothId;
        T.get(url, data => {
            if (data.error) {
                T.notify('Lấy danh sách hình ảnh bị lỗi!', 'danger');
                console.error('GET: ' + url + '. ' + data.error);
            } else {
                dispatch({ type: GET_NEW_PHOTOS, newPhotos: data.newPhotos });
            }
        }, error => {
            console.error('GET: ' + url + '. ' + error);
        });
    }
}
