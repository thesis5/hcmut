import React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import { getAllGuest, getGuestInPage, createGuest, updateGuest, assigneeImage, changeImageGuest, deleteFile, deleteAllAssetFile, addFile, changeGuest, removeFile, deleteGuestImage } from './reduxGuest.jsx';
import { GuestModal } from './adminEditPhotoBoothGuest.jsx';
import AdminNewPhotosComponent from './adminNewPhotosComponent.jsx';

class AssignedModal extends React.Component {
    modal = React.createRef();
    state = { originalOption: null, selectedOption: null, file: null };

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('hidden.bs.modal', () => {
                this.setState({ originalOption: null, selectedOption: null, file: null });
            });
        });
    }

    show = (e, file, selectedOption = null) => {
        e.preventDefault();
        this.setState({ file, selectedOption, originalOption: selectedOption }, () => $(this.modal.current).modal('show'));
    };

    save = (e, active = false) => {
        e.preventDefault();
        if (this.state.selectedOption && this.state.file) {
            if (this.state.originalOption) {
                const data = {
                    oldId: this.state.originalOption.value,
                    newId: this.state.selectedOption.value,
                    image: this.state.file,
                };
                this.props.changeImageGuest(data, () => {
                    T.notify('Gán hình thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            } else {
                this.props.assigneeImage(this.state.selectedOption.value, this.state.file, active, () => {
                    T.notify('Gán hình thành công!', 'success');
                    $(this.modal.current).modal('hide');
                })
            }
        } else {
            T.notify('Gán hình gặp lỗi!', 'success');
            $(this.modal.current).modal('hide');
        }
    };

    handleChange = selectedOption => {
        this.setState({ selectedOption });
    };

    render() {
        const { file, originalOption } = this.state;
        const imageSrc = originalOption ? file : `/image/photo-booth?image=/${this.props.photoBoothId}/${this.state.file}&?t=${new Date().getTime()}`;

        return (
            <div className='modal' tabIndex='-1' ref={this.modal}>
                <div className='modal-dialog' style={{ maxWidth: '80%' }}>
                    <div className='modal-content'>
                        <div className='modal-body row'>
                            <div className='form-group col-12'>
                                <h4>Chọn khách cần gán</h4>
                                <table className='table'>
                                    <tr>
                                        <td style={{ width: '100%' }}>
                                            <Select className='basic-single' classNamePrefix='select' value={this.state.selectedOption} isClearable={true} isSearchable={true} onChange={this.handleChange} options={this.props.unassignedGuest} />
                                        </td>
                                        <td style={{ width: 'auto', display: 'flex' }}>
                                            <button type='submit' className='btn btn-success' onClick={(e) => this.save(e, true)}>Lưu & hiển thị</button>&nbsp;
                                            <button type='submit' className='btn btn-primary' onClick={this.save}>Lưu</button>&nbsp;
                                            <button type='button' className='btn btn-secondary' data-dismiss='modal' aria-label='Close'>Đóng</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <img src={imageSrc} alt='image' style={{ width: '90%', margin: '0 5%' }} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class AvailableGuestComponent extends React.Component {
    assignedModal = React.createRef();
    guestModal = React.createRef();

    componentDidMount() {
        $(document).ready(() => {
            const handleGetPage = (condition) => {
                if (condition) {
                    this.props.getAllGuest({ photoBooth: this.props.photoBoothId });
                } else {
                    setTimeout(() => handleGetPage(this.props.photoBoothId), 250);
                }
            };
            handleGetPage(this.props.photoBoothId);

            T.socket.on('photo-booth-image-changed', (method, photoBoothId, file) => {
                console.log(`SOCKET: photo-booth-image-changed ==> method: ${method}, photoBoothId:${photoBoothId}, file: ${file}`);
                if (this.props.photoBoothId && this.props.photoBoothId == photoBoothId) {
                    switch (method) {
                        case 'add': {
                            this.props.addFile(file);
                            break;
                        }

                        case 'delete': {
                            this.props.removeFile(file);
                            break;
                        }

                        default: break;
                    }
                }
            });

            T.socket.on('photo-booth-assigned', (item, file) => {
                if (item.photoBooth == this.props.photoBoothId) {
                    item && this.props.changeGuest(item);
                    file && this.props.removeFile(file);
                }
            });
        });
    }

    changeActive = (item) => this.props.updateGuest(item._id, { active: !item.active }, () => {
        T.notify((item.active ? 'Hủy kích hoạt ' : 'Kích hoạt ') + 'thành công!', 'success');
    });

    deleteFile = (e, file) => {
        e.preventDefault();
        T.confirm('Xóa hình ảnh', 'Bạn có chắc muốn xóa hình ảnh này?', 'info', isConfirm => {
            isConfirm && this.props.deleteFile(this.props.photoBoothId, file);
        });
    };

    deleteAllFile = () => {
        T.confirm('Xóa tất cả ảnh', 'Bạn có chắc muốn xóa tất cả hình ảnh này?', 'info', isConfirm => {
            isConfirm && this.props.deleteAllAssetFile(this.props.photoBoothId);
        });
    };

    reloadGetAllFile = () => {
        this.props.getAllGuest({ photoBooth: this.props.photoBoothId });
    };

    deleteImage = (e, _id) => {
        e.preventDefault();
        T.confirm('Xóa ảnh?', 'Bạn có chắn muốn xóa ảnh này?', 'info', isConfirm => {
            isConfirm && this.props.deleteGuestImage(_id, (item) => {
                this.props.getAllGuest({ photoBooth: item.photoBooth });
                this.props.getGuestInPage({ photoBooth: item.photoBooth });
            });
        })
    };

    render() {
        const permissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            hasPermission = permissions.includes('photoBooth:write');
        const guestList = this.props.guest && this.props.guest.items && this.props.guest.items.length ? this.props.guest.items : [];
        const files = this.props.guest && this.props.guest.files && this.props.guest.files.length ? this.props.guest.files : [];
        let assignedGuest = [], unassignedGuest = [];
        guestList.forEach(guest => {
            if (guest.image && guest.image != '') {
                assignedGuest.push(guest);
            } else {
                unassignedGuest.push({ value: guest._id, label: guest.numberId + ': ' + guest.fullname })
            }
        });
        const assignedImage = assignedGuest.map((guest, index) => (
            <tr key={index}>
                <td style={{ textAlign: 'right' }}>{index + 1}</td>
                <td>
                    <a href='#' onClick={(e) => this.assignedModal.current.show(e, guest.image, { value: guest._id, label: guest.fullname })}>
                        {guest.fullname}
                    </a>
                </td>
                <td style={{ width: 'auto', textAlign: 'center' }}>
                    <img src={guest.image} alt='image' style={{ height: '120px' }} />
                </td>
                <td className='toggle' style={{ textAlign: 'center' }} >
                    <label>
                        <input type='checkbox' checked={guest.active} onChange={() => this.changeActive(guest)} disabled={!hasPermission} />
                        <span className='button-indecator' />
                    </label>
                </td>
                <td>
                    <div className='btn-group'>
                        <a className='btn btn-primary' href='#' onClick={(e) => this.guestModal.current.show(e, guest)}>
                            <i className='fa fa-lg fa-edit' />
                        </a>

                        <a className='btn btn-danger' href='#' onClick={(e) => this.deleteImage(e, guest._id)}>
                            <i className='fa fa-lg fa-file-image-o' />
                        </a>
                    </div>
                </td>
            </tr>
        ));
        const unassignedImage = files.map((file, index) => {
            return (
                <div key={index} className='col-12'>
                    <div style={{ marginBottom: '20px' }}>
                        <div className='thumb'>
                            <a href='#' className='text-danger' onClick={(e) => this.deleteFile(e, file)} style={{ position: 'absolute', top: '8px', right: '25px', fontSize: '20px' }}>
                                <i className='fa fa-times' />
                            </a>
                            <a href='#' onClick={(e) => this.assignedModal.current.show(e, file)}>
                                <img src={`/image/photo-booth?image=/${this.props.photoBoothId}/${file}&?t=${new Date().getTime()}`} alt='photo booth' className='img-fluid img-thumbnail w-100' />
                            </a>
                        </div>
                    </div>
                </div>
            );
        });

        return [
            <div key={0} className='row'>
                <div className='col-12 col-md-9' style={{ borderRight: '1px solid #dee2e6' }}>
                    <h3 className='tile-title'>Hình ảnh đã được gán</h3>
                    <div>
                        {assignedImage.length ? (
                            <table className='table table-hover table-bordered table-responsive'>
                                <thead>
                                    <tr>
                                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                                        <th style={{ width: '100%', whiteSpace: 'nowrap' }}>Họ và tên</th>
                                        <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Hình ảnh</th>
                                        <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Kích hoạt</th>
                                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {assignedImage}
                                </tbody>
                            </table>
                        ) : <p>Không có khách được gán ảnh!</p>}
                    </div>
                </div>
                <div className='col-12 col-md-3'>
                    <h3 className='tile-title'>Hình ảnh mới</h3>
                    {/*<AdminNewPhotosComponent photoBoothId={this.props.photoBoothId} />*/}
                    <button className='btn btn-success' style={{ position: 'absolute', top: '-5px', right: '15px' }} onClick={this.reloadGetAllFile}><i className='fa fa-refresh' /></button>
                    <div className='row'>
                        {unassignedImage}
                    </div>
                </div>
            </div>,
            <button key={1} type='button' style={{ position: 'fixed', right: '10px', bottom: '10px' }} className='btn btn-danger btn-circle mr-2' onClick={this.deleteAllFile}>
                <i className='fa fa-lg fa-picture-o' />
            </button>,
            <AssignedModal key={2} ref={this.assignedModal} assigneeImage={this.props.assigneeImage} photoBoothId={this.props.photoBoothId} unassignedGuest={unassignedGuest} changeImageGuest={this.props.changeImageGuest} />,
            <GuestModal key={3} ref={this.guestModal} photoBoothId={this.props.photoBoothId} create={this.props.createGuest} update={this.props.updateGuest} />
        ];
    }
}

const mapStateToProps = state => ({ system: state.system, guest: state.guest });
const mapActionsToProps = { getAllGuest, getGuestInPage, createGuest, updateGuest, assigneeImage, changeImageGuest, deleteFile, deleteAllAssetFile, addFile, changeGuest, removeFile, deleteGuestImage };
export default connect(mapStateToProps, mapActionsToProps)(AvailableGuestComponent);
