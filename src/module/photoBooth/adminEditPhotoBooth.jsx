import React from 'react';
import { connect } from 'react-redux';
import { getPhotoBooth, updatePhotoBooth, searchPhotographerByEmail, deletePhotoBoothImage } from './reduxPhotoBooth.jsx';
import { Link } from 'react-router-dom';
import EditPhotoBoothGuest from './adminEditPhotoBoothGuest.jsx';
import AvailableGuestComponent from './adminAvailableGuestComponent.jsx';
import ImageBox from '../../view/common/ImageBox.jsx';

class EditPhotoBoothPage extends React.Component {
    state = { _id: null, photographer: {} };
    imageBox = React.createRef();

    componentDidMount() {
        T.ready('/user/photo-booth', () => {
            const params = T.routeMatcher('/user/photo-booth/edit/:id').parse(window.location.pathname);
            this.props.getPhotoBooth(params.id, (item) => {
                $('#photoText').html(item.title);
                $('#photoBoothTitle').val(item.title);
                $('#photoBoothActive').prop('checked', item.active);
                this.imageBox.current.setData('photoBooth:' + item._id, item.image);
                const photographer = item.photographer ? item.photographer : {};
                $('#photographerEmail').val(photographer.email);
                $('#photographerEmail').on('keypress', (e) => {
                    if (e.which == 13) {
                        const email = $('#photographerEmail').val().trim();
                        if (!T.validateEmail(email)) {
                            T.notify('Email không hợp lệ', 'warning');
                        } else if (email != '') {
                            this.props.searchPhotographerByEmail($('#photographerEmail').val().trim(), (user) => {
                                $('#photographerFullname').html(user.lastname + ' ' + user.firstname);
                                this.setState({ photographer: user });
                            });
                        }
                    }
                });
                $('#photographerEmail').on('blur', () => {
                    const email = $('#photographerEmail').val().trim();
                    if (!T.validateEmail(email)) {
                        T.notify('Email không hợp lệ', 'warning');
                    } else if (email != '') {
                        this.props.searchPhotographerByEmail($('#photographerEmail').val().trim(), (user) => {
                            $('#photographerFullname').html(user.lastname + ' ' + user.firstname);
                            this.setState({ photographer: user });
                        });
                    }
                });
                $('#photographerFullname').html(photographer._id ? (photographer.lastname + ' ' + photographer.firstname) : '');
                this.setState({ _id: item._id, photographer });
            });
        });
    }

    saveCommon = (e) => {
        e.preventDefault();
        if (this.state._id) {
            const changes = {
                title: $('#photoBoothTitle').val().trim(),
                active: $('#photoBoothActive').prop('checked'),
                photographer: this.state.photographer._id
            };
            this.props.updatePhotoBooth(this.state._id, changes, (item) => {
                T.notify('Cập nhật thông tin Photo Booth thành công!', 'success');
                $('#photoText').html(item.title);
            });
        }
    };

    savePassword = (e) => {
        if (this.state._id) {
            const password = $('#photoBoothPassword').val().trim(),
                confirmPassword = $('#photoBoothConfirmPassword').val().trim();
            if (password == '' || password !== confirmPassword) {
                T.notify('Mật khẩu và xác nhận mật khẩu không khớp!', 'danger');
            } else {
                const changes = { password };
                this.props.updatePhotoBooth(this.state._id, changes, () => {
                    T.notify('Thay đổi mật khẩu cho photographer thành công!', 'success');
                    $('#photoBoothPassword').val('');
                    $('#photoBoothConfirmPassword').val('');
                });
            }
        }
        e.preventDefault();
    };

    deleteImage = (e) => {
        e.preventDefault();
        if (this.state._id) {
            T.confirm('Xóa ảnh nền', 'Bạn có chắn muốn xóa ảnh nền này?', 'info', isConfirm => {
                isConfirm && this.props.deletePhotoBoothImage(this.state._id, (item) => {
                    this.imageBox.current.setData('photoBooth:' + item._id, null);
                });
            })
        }
    };

    render() {
        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-users' /> Photo Booth: <span id='photoText' /></h1>
                </div>

                <ul className='nav nav-tabs'>
                    <li className='nav-item'><a className='nav-link' data-toggle='tab' href='#menuCommonInfo'>Thông tin chung</a></li>
                    <li className='nav-item'><a className='nav-link active show' data-toggle='tab' href='#menuAttendance'>Khách hiện diện</a></li>
                    <li className='nav-item'><a className='nav-link' data-toggle='tab' href='#menuGuest'>Thông tin khách</a></li>
                </ul>
                <div className='tab-content'>
                    <div className='tab-pane fade' id='menuCommonInfo'>
                        <div className='row'>
                            <div className='col-12'>
                                <div className='tile'>
                                    <div className='row'>
                                        <div className='col-6 form-group'>
                                            <label htmlFor='photoBoothTitle'>Tiêu đề Photo Booth</label>
                                            <input className='form-control' id='photoBoothTitle' placeholder='Tiêu đề Photo Booth' />
                                        </div>
                                        <div className='col-6 form-group'>
                                            <label className='control-label'>Kích hoạt</label>
                                            <div className='toggle'>
                                                <label>
                                                    <input type='checkbox' id='photoBoothActive' /><span className='button-indecator' />
                                                </label>
                                            </div>
                                        </div>
                                        <div className='col-6 form-group'>
                                            <label className='control-label' htmlFor='photographerEmail'>Email người chụp hình</label>
                                            <input className='form-control' id='photographerEmail' placeholder='Email người chụp hình' />
                                        </div>
                                        <div className='col-6 form-group'>
                                            <label className='control-label' htmlFor='photographerEmail'>Họ và tên người chụp hình</label>
                                            <p id='photographerFullname' />
                                        </div>
                                    </div>
                                    <div className='tile-footer text-right'>
                                        <button className='btn btn-success' onClick={this.saveCommon}><i className='fa fa-save' />Lưu</button>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-6 col-12'>
                                <div className='tile'>
                                    <h3 className='tile-title'>Mật khẩu</h3>
                                    <div className='row'>
                                        <div className='col-12 form-group'>
                                            <label htmlFor='photoBoothPassword'>Mật khẩu mới</label>
                                            <input className='form-control' type='password' id='photoBoothPassword' autoComplete='new-password' />
                                        </div>
                                        <div className='col-12 form-group'>
                                            <label htmlFor='photoBoothConfirmPassword'>Xác nhận mật khẩu</label>
                                            <input className='form-control' type='password' id='photoBoothConfirmPassword' autoComplete='new-password' />
                                        </div>
                                    </div>
                                    <div className='tile-footer text-right'>
                                        <button className='btn btn-success' onClick={this.savePassword}><i className='fa fa-save' />Lưu mật khẩu</button>
                                    </div>
                                </div>
                            </div>

                            <div className='col-md-6 col-12'>
                                <div className='tile'>
                                    <h3 className='tile-title'>Hình ảnh nền</h3>
                                    <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='PhotoBoothImage' userData='photoBooth'/>
                                    <div className='tile-footer text-right'>
                                        <button className='btn btn-danger' onClick={this.deleteImage}><i className='fa fa-trash' />Xóa ảnh nền</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='tab-pane tile fade active show' id='menuAttendance'>
                        <AvailableGuestComponent photoBoothId={this.state._id} />
                    </div>
                    <div className='tab-pane tile fade' id='menuGuest'>
                        <EditPhotoBoothGuest photoBoothId={this.state._id} />
                    </div>
                </div>
                <Link to='/user/photo-booth' className='btn btn-secondary btn-circle' style={{ position: 'fixed', bottom: '10px' }}>
                    <i className='fa fa-lg fa-reply' />
                </Link>
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, photoBooth: state.photoBooth });
const mapActionsToProps = { getPhotoBooth, updatePhotoBooth, searchPhotographerByEmail, deletePhotoBoothImage };
export default connect(mapStateToProps, mapActionsToProps)(EditPhotoBoothPage);
