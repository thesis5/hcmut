import React from 'react';
import { connect } from 'react-redux';
import { getAllGuest, getGuestInPage, createGuest, updateGuest, deleteGuest, deleteAllGuest } from './reduxGuest.jsx';
import Pagination from '../../view/common/Pagination.jsx';
import ImageBox from '../../view/common/ImageBox.jsx';
import FileBox from '../../view/common/FileBox.jsx';

export class GuestModal extends React.Component {
    modal = React.createRef();
    imageBox = React.createRef();
    guestNumberId = React.createRef();
    guestFullname = React.createRef();
    guestDescription = React.createRef();
    guestCompany = React.createRef();
    state = { _id: null };

    componentDidMount() {
        $(document).ready(() => $(this.modal.current).on('shown.bs.modal', () => {
            this.guestNumberId.current.focus();
        }));
    }

    show = (e, item) => {
        e.preventDefault();
        if (item) {
            this.guestNumberId.current.value = item.numberId;
            this.guestFullname.current.value = item.fullname;
            this.guestDescription.current.value = item.description;
            this.guestCompany.current.value = item.company;
            this.imageBox.current.setData('guest:' + item._id, item.image);
            this.setState({ _id: item._id });
        } else {
            this.guestNumberId.current.value = '';
            this.guestFullname.current.value = '';
            this.guestDescription.current.value = '';
            this.guestCompany.current.value = '';
            this.imageBox.current.setData('guest:new');
            this.setState({ _id: null });
        }
        $(this.modal.current).modal('show');
    };

    save = (e) => {
        e.preventDefault();
        const changes = {
            photoBooth: this.props.photoBoothId,
            numberId: this.guestNumberId.current.value,
            fullname: this.guestFullname.current.value,
            description: this.guestDescription.current.value,
            company: this.guestCompany.current.value,
            image: this.imageBox.current.getImage(),
        };
        if (changes.numberId == '') {
            T.notify('Mã số khách bị trống!', 'danger');
            this.guestNumberId.current.focus();
        } else if (changes.fullname == '') {
            T.notify('Họ và tên khách bị trống!', 'danger');
            this.guestFullname.current.focus();
        } else {
            if (this.state._id) {
                delete changes.image;
                this.props.update(this.state._id, changes, () => {
                    T.notify('Cập nhật khách thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            } else {
                this.props.create(changes, () => {
                    T.notify('Tạo khách thành công!', 'success');
                    $(this.modal.current).modal('hide');
                });
            }
        }
    };

    uploadSuccess = () => {
        this.props.getPage(undefined, undefined, { photoBooth: this.props.photoBoothId });
        this.props.getAll({ photoBooth: this.props.photoBoothId });

    };

    render() {
        return (
            <div className='modal' tabIndex='-1' ref={this.modal}>
                <form className='modal-dialog modal-lg' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>{!this.state._id ? 'Tạo mới ' : 'Cập nhật '}Guest</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            <div className='form-group col-12'>
                                <label>Mã số</label>
                                <input className='form-control' ref={this.guestNumberId} type='text' placeholder='Mã số' />
                            </div>
                            <div className='form-group col-12'>
                                <label>Họ và tên</label>
                                <input className='form-control' ref={this.guestFullname} type='text' placeholder='Họ và tên' />
                            </div>
                            <div className='form-group col-12'>
                                <label>Mô tả</label>
                                <textarea className='form-control' ref={this.guestDescription} placeholder='Mô tả' rows={3} />
                            </div>
                            <div className='form-group col-12'>
                                <label>Công ty</label>
                                <textarea className='form-control' ref={this.guestCompany} placeholder='Công ty' rows={3} />
                            </div>
                            <div className='form-group col-12'>
                                <label>Avatar</label>
                                <ImageBox ref={this.imageBox} postUrl='/user/upload' uploadType='GuestImage' success={this.uploadSuccess} />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary'>{this.state._id ? 'Lưu' : 'Tạo'}</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class ImportModal extends React.Component {
    modal = React.createRef();
    fileBox = React.createRef();
    state = { list: [] };

    show = (e) => {
        e.preventDefault();
        this.setState({ list: [] });
        $(this.modal.current).modal('show');
    };

    onSuccess = (res) => {
        if (res.error) {
            T.notify('Import danh sách khách bị lỗi!', 'danger');
        } else {
            T.notify(`${res.guests.length} khách được tải lên thành công!`, 'success');
            this.setState({ list: res.guests });
        }
    };

    delete = (e, index) => {
        e.preventDefault();
        let list = this.state.list;
        list.splice(index, 1);
        this.setState({ list });
    };

    save = (e) => {
        e.preventDefault();
        const dataList = this.state.list.map(item => Object.assign({}, item, { photoBooth: this.props.photoBoothId }));
        if (dataList.length) {
            this.props.create(dataList, () => {
                T.notify('Import khách thành công!', 'success');
                $(this.modal.current).modal('hide');
            });
        } else {
            $(this.modal.current).modal('hide');
        }
    };

    render() {
        const table = this.state.list && this.state.list.length ? (
            <table className='table table-hover table-bordered table-responsive'>
                <thead>
                    <tr>
                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                        <th style={{ width: 'auto', textAlign: 'center' }}>Mã số</th>
                        <th style={{ width: '20%', whiteSpace: 'nowrap' }}>Họ và tên</th>
                        <th style={{ width: '40%', whiteSpace: 'nowrap' }}>Mô tả</th>
                        <th style={{ width: '40%', whiteSpace: 'nowrap' }}>Công ty</th>
                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.list.map((item, index) => (
                        <tr key={index}>
                            <td style={{ textAlign: 'right' }}>{index + 1}</td>
                            <td>{item.numberId}</td>
                            <td>{item.fullname}</td>
                            <td>{item.description}</td>
                            <td>{item.company}</td>
                            <td>
                                <div className='btn-group'>
                                    <a className='btn btn-danger' href='#' onClick={e => this.delete(e, index)}>
                                        <i className='fa fa-lg fa-trash' />
                                    </a>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        ) : '';
        return (
            <div className='modal' tabIndex='-1' ref={this.modal}>
                <div className='modal-dialog' style={{ maxWidth: '95%' }}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Import khách</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body row'>
                            <div className='form-group col-12'>
                                <FileBox ref={this.fileBox} userData='guestImport' postUrl='/user/upload' uploadType='GuestFile' success={this.onSuccess} />
                            </div>
                            <div className='col-12'>
                                {table}
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <a href='/download/SampleUploadGuest.xlsx' className='btn btn-success'>Tải xuống file mẫu</a>
                            {this.state.list.length ? <button type='submit' className='btn btn-primary' onClick={this.save}>Import</button> : ''}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class EditPhotoBoothGuest extends React.Component {
    guestModal = React.createRef();
    importModal = React.createRef();

    componentDidMount() {
        $(document).ready(() => {
            const handleGetPage = (condition) => {
                if (condition) {
                    this.props.getGuestInPage(undefined, undefined, { photoBooth: this.props.photoBoothId });
                } else {
                    setTimeout(() => handleGetPage(this.props.photoBoothId), 250);
                }
            };
            handleGetPage(this.props.photoBoothId);
        });
    }

    delete = (e, _id) => {
        e.preventDefault();
        T.confirm('Xóa khách?', 'Bạn có chắc muốn xóa khách này?', 'info', isConfirm =>
            isConfirm && this.props.deleteGuest(_id));
    };

    deleteAll = (e) => {
        e.preventDefault();
        T.confirm('Xóa tất cả khách?', 'Bạn có chắc muốn xóa tất cả khách trong Photo Booth này?', 'info', isConfirm => {
            isConfirm && this.props.deleteAllGuest(this.props.photoBoothId);
        })
    };

    render() {
        const permissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            hasPermission = permissions.includes('photoBooth:write');
        const { pageNumber, pageSize, pageTotal, totalItem, list } = this.props.guest && this.props.guest.page ? this.props.guest.page : { pageNumber: 0, pageSize: 0, pageTotal: 0, totalItem: 0, list: [] };
        const table = list && list.length ? (
            <table className='table table-hover table-bordered table-responsive'>
                <thead>
                    <tr>
                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                        <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Mã số</th>
                        <th style={{ width: '50%', whiteSpace: 'nowrap' }}>Họ và tên</th>
                        <th style={{ width: '30%', whiteSpace: 'nowrap' }}>Mô tả</th>
                        <th style={{ width: '20%', whiteSpace: 'nowrap' }}>Hình ảnh</th>
                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    {list.map((item, index) => (
                        <tr key={index}>
                            <td style={{ textAlign: 'right' }}>{(Math.max(pageNumber, 1) - 1) * pageSize + index + 1}</td>
                            <td>{item.numberId}</td>
                            <td><a href='#' onClick={(e) => this.guestModal.current.show(e, item)}>{item.fullname}</a></td>
                            <td>{item.description}</td>
                            <td style={{ width: '20%', textAlign: 'center' }}>
                                <img src={item.image ? item.image : '/img/bk.png'} alt='avatar' style={{ height: '32px' }} />
                            </td>
                            <td>
                                <div className='btn-group'>
                                    <a className='btn btn-primary' href='#' onClick={(e) => this.guestModal.current.show(e, item)}><i className='fa fa-lg fa-edit' /></a>
                                    {hasPermission ?
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item._id)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                        : ''}
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        ) : <p>Không có dữ liệu!</p>;

        return (
            <div>
                {table}
                <Pagination name='pageGuest' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getGuestInPage} style={{ marginLeft: '40px' }} />
                {hasPermission ?
                    <div style={{ position: 'fixed', right: '10px', bottom: '10px' }}>
                        <button type='button' className='btn btn-danger btn-circle mr-2' onClick={this.deleteAll}>
                            <i className='fa fa-lg fa-user-times' />
                        </button>
                        <button type='button' className='btn btn-success btn-circle mr-2' onClick={(e) => this.importModal.current.show(e)}>
                            <i className='fa fa-lg fa-cloud-upload' />
                        </button>
                        <button type='button' className='btn btn-primary btn-circle' onClick={(e) => this.guestModal.current.show(e, null)}>
                            <i className='fa fa-lg fa-plus' />
                        </button>
                    </div>
                    : ''}
                <GuestModal ref={this.guestModal} photoBoothId={this.props.photoBoothId} create={this.props.createGuest} update={this.props.updateGuest} getAll={this.props.getAllGuest} getPage={this.props.getGuestInPage} />
                <ImportModal ref={this.importModal} photoBoothId={this.props.photoBoothId} create={this.props.createGuest} />
            </div>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, guest: state.guest });
const mapActionsToProps = { getAllGuest, getGuestInPage, createGuest, updateGuest, deleteGuest, deleteAllGuest };
export default connect(mapStateToProps, mapActionsToProps)(EditPhotoBoothGuest);
