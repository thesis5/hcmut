import React from 'react';
import { connect } from 'react-redux';
import { getNewPhotos } from './reduxPhotoBooth.jsx';

class AdminNewPhotosComponent extends React.Component {
    // createModal = React.createRef();

    componentDidMount() {
        T.ready(() => this.getnewPhotos());

        T.socket.on('photo-booth-image-changed', () => this.getnewPhotos());
    }

    getnewPhotos = () => {
        const params = T.routeMatcher('/user/photo-booth/edit/:id').parse(window.location.pathname);
        this.props.getNewPhotos(params.id);
    }

    // delete = (e, _id) => {
    //     e.preventDefault();
    //     T.confirm('Xóa hình?', 'Bạn có chắc muốn xóa hình này', 'info', isConfirm =>
    //         isConfirm && this.props.deletePhotoBooth(_id));
    // };

    render() {
        const permissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            readOnly = !permissions.includes('photoBooth:write');

        return (
            <div className={this.props.className}>
                <div className='row'>
                    {(this.props.photoBooth && this.props.photoBooth.newPhotos ? this.props.photoBooth.newPhotos : []).map((photo, index) => (
                        <div key={index}>{photo}</div>
                    ))}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, photoBooth: state.photoBooth });
const mapActionsToProps = { getNewPhotos };
export default connect(mapStateToProps, mapActionsToProps)(AdminNewPhotosComponent);
