module.exports = app => {
    app.photoBoothPath = app.path.join(app.assetPath, 'photo-booth');
    app.photoBoothPublicPath = app.path.join(app.publicPath, 'photo-booth');
    app.photoBoothPublicThmubnailPath = app.path.join(app.publicPath, '/photo-booth-thumbnail');
    app.createFolder(app.photoBoothPath, app.photoBoothPublicPath);

    const menu = {
        parentMenu: { index: 2000, title: 'Cấu hình', icon: 'fa-cog' },
        menus: {
            2120: { title: 'Photo Booth', link: '/user/photo-booth' },
        },
    };
    app.permission.add(
        { name: 'photoBooth:read', menu },
        { name: 'photoBooth:write', menu },
    );
    app.get('/user/photo-booth', app.permission.check('photoBooth:read'), app.templates.admin);
    app.get('/user/photo-booth/edit/:_id', app.permission.check('photoBooth:write'), app.templates.admin);

    //Get private image -----------------------------------------------------------------------------------------------
    app.get('/image/photo-booth-thumbnail', app.permission.check('photoBooth:read'), (req, res) => {
        app.fs.readFile(app.path.join(app.assetPath, 'photo-booth-thumbnail', req.query.image), (error, data) => {
            if (error) {
                res.send({ error });
            }
            else {
                res.writeHead(200, { 'Content-Type': 'image/jpeg' });
                res.end(data);
            }
        });
    });

    app.get('/image/photo-booth', app.permission.check('photoBooth:read'), (req, res) => {
        app.fs.readFile(app.path.join(app.photoBoothPath, req.query.image), (error, data) => {
            if (error) {
                res.send({ error });
            }
            else {
                res.writeHead(200, { 'Content-Type': 'image/jpeg' });
                res.end(data);
            }
        });
    });

    app.get('/api/photo-booth/all', app.permission.check('photoBooth:read'), (req, res) => {
        let condition = req.query.condition ? req.query.condition : {};
        app.model.photoBooth.getAll(condition, (error, items) => {
            res.send({ error, items });
        });
    });

    app.get('/api/photo-booth/page/:pageNumber/:pageSize', app.permission.check('photoBooth:read'), (req, res) => {
        let pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.photoBooth.getPage(pageNumber, pageSize, condition, (error, page) => {
            res.send({ error, page });
        });
    });

    app.get('/api/photo-booth/item/:id', app.permission.check('photoBooth:read'), (req, res) => {
        app.model.photoBooth.get(req.params.id, (error, item) => {
            res.send({ error, item });
        });
    });

    app.post('/api/photo-booth', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.photoBooth.create(req.body.data, (error, item) => {
            app.createFolder(app.path.join(app.photoBoothPath, item._id.toString()));
            app.createFolder(app.path.join(app.photoBoothPublicPath, item._id.toString()));
            res.send({ error, item });
        });
    });

    app.put('/api/photo-booth', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.photoBooth.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item }));
    });

    app.delete('/api/photo-booth', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.photoBooth.delete(req.body._id, error => {
            app.deleteFolder(app.path.join(app.photoBoothPath, req.body._id));
            app.deleteFolder(app.path.join(app.photoBoothPublicPath, req.body._id));
            res.send({ error });
        });
    });

    app.delete('/api/photo-booth/image', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.photoBooth.deleteImage(req.body._id, (error, item) => {
            app.io.emit('cover-image', 'remove', item._id);
           res.send({error, item});
        });
    });

    app.get('/home/photo-booth/item/:id', (req, res) => {
        app.model.photoBooth.get(req.params.id, (error, item) => {
            res.send({ error, item });
        });
    });

    app.get('/home/photo-booth/new-photos/:id', (req, res) => {
        app.fs.readdir(app.path.join(app.photoBoothPath, req.params.id), (error, newPhotos) => {
            if (error) {
                res.send({ error: 'Không thể đọc folder hình ảnh!' });
            } else {
                res.send({ error, newPhotos });
            }
        });
    });

    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    app.createFolder(app.path.join(app.publicPath, '/img/photoBooth'), );

    const uploadPhotoBoothImage = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('photoBooth:') && files.PhotoBoothImage && files.PhotoBoothImage.length > 0) {
            console.log('Hook: uploadPhotoBoothImage => photo booth image upload');
            app.uploadComponentImage(req, 'photoBooth', app.model.photoBooth.get, fields.userData[0].substring(11), files.PhotoBoothImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadPhotoBoothImage', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadPhotoBoothImage(req, fields, files, params, done), done, 'photoBooth:write'));

};
