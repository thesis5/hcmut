module.exports = app => {
    app.get('/api/guest/all', app.permission.check('photoBooth:write'), (req, res) => {
        let condition = req.query.condition ? req.query.condition : {};
        app.model.guest.getAll(condition, (error, items) => {
            if (condition.photoBooth) {
                app.fs.readdir(app.path.join(app.photoBoothPath, condition.photoBooth), (error, files) => {
                    if (error) {
                        res.send({ error: 'Không thể đọc folder hình ảnh!' });
                    } else {
                        res.send({ error, items, files });
                    }
                });
            } else {
                res.send({ error, items, files: [] });
            }
        });
    });

    app.get('/api/guest/page/:pageNumber/:pageSize', app.permission.check('photoBooth:write'), (req, res) => {
        let pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.guest.getPage(pageNumber, pageSize, condition, (error, page) => {
            res.send({ error, page });
        });
    });

    app.get('/api/guest/item/:id', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.guest.get(req.params.id, (error, item) => res.send({ error, item }));
    });

    app.post('/api/guest', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.guest.create(req.body.data, (error, item) => res.send({ error, item }));
    });

    app.put('/api/guest', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.guest.update(req.body._id, req.body.changes, (error, item) => {
            if (item.active) app.io.emit('guest-change', { newItem: item });
            else app.io.emit('guest-change', { oldItem: item });
            app.io.emit('photo-booth-assigned', item, null);
            res.send({ error, item });
        });
    });

    app.put('/api/guest/assign-image', app.permission.check('photoBooth:write'), (req, res) => {
        const { _id, file, active } = req.body;
        app.model.guest.get(_id, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (!item) {
                res.send({ error: 'Invalid id' });
            } else {
                const srcPath = app.path.join(app.photoBoothPath, item.photoBooth.toString(), file);
                item.image = '/' + item.photoBooth.toString() + '/' + file;
                app.fs.rename(srcPath, app.path.join(app.photoBoothPublicPath, item.image), error => {
                    if (error) {
                        res.send({ error });
                    } else {
                        item.image = '/photo-booth' + item.image + '?t=' + (new Date().getTime()).toString().slice(-8);
                        item.priority = new Date().getTime();
                        item.active = active;
                        item.save(error => {
                            if (item.active) app.io.emit('guest-change', { newItem: item });
                            app.io.emit('photo-booth-assigned', item, file);
                            res.send({ error, item });
                        });
                    }
                });
            }
        })
    });

    app.put('/api/guest/change-image', app.permission.check('photoBooth:write'), (req, res) => {
        const { oldId, newId, image } = req.body;
        app.model.guest.update(oldId, {}, { image: null }, (error1, oldItem) => {
            app.model.guest.update(newId, { image }, (error2, newItem) => {
                app.io.emit('guest-change', { oldItem: oldItem, newItem: newItem.active ? newItem : null });
                res.send({ error: error1 ? error1 : error2, oldItem, newItem });
            });
        });
    });

    app.delete('/api/guest', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.guest.get(req.body._id, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (!item) {
                res.send({ error: 'Invalid Id' });
            } else {
                app.model.guest.delete(item._id, error => {
                    app.io.emit('guest-change', { oldItem: item });
                    res.send({ error });
                });
            }
        });
    });

    app.delete('/api/guest/all', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.guest.deleteMany({ photoBooth: req.body.photoBooth }, error => res.send({ error }));
    });

    app.delete('/api/guest/delete-file', app.permission.check('photoBooth:write'), (req, res) => {
        const path = app.path.join(app.photoBoothPath, req.body._id, req.body.file);
        app.deleteFile(path, () => {
            app.io.emit('photo-booth-image-changed', 'delete', req.body._id, req.body.file);
            res.send({});
        })
    });

    app.delete('/api/guest/all-files', app.permission.check('photoBooth:write'), (req, res) => {
        app.fs.readdir(app.path.join(app.photoBoothPath, req.body.photoBooth), (error, files) => {
            files.forEach(file => app.deleteFile(app.path.join(app.photoBoothPath, req.body.photoBooth, file)));
            res.send({});
        });
    });

    app.delete('/api/guest/image', app.permission.check('photoBooth:write'), (req, res) => {
        app.model.guest.deleteImage(req.body._id, (error, item) => {
            app.io.emit('guest-change', { oldItem: item });
            res.send({error, item});
        });
    });

    app.get('/home/guest/all', (req, res) => {
        let condition = req.query.condition ? req.query.condition : {};
        app.model.guest.getAll(condition, (error, items) => {
            res.send({ error, items });
        });
    });

    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    const uploadGuestImage = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('guest:') && files.GuestImage && files.GuestImage.length > 0) {
            console.log('Hook: uploadGuestImage => guest image upload');
            app.uploadGuestImage(req, app.model.guest.get, fields.userData[0].substring(6), files.GuestImage[0].path, done);
        }
    };

    app.uploadHooks.add('uploadGuestImage', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadGuestImage(req, fields, files, params, done), done, 'photoBooth:write'));

    const uploadGuestFile = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] == 'guestImport' && files.GuestFile && files.GuestFile.length > 0) {
            console.log('Hook: uploadGuestFile => guest file upload');
            app.uploadGuestFile(req, files.GuestFile[0].path, done);
        }
    };

    app.uploadHooks.add('uploadGuestFile', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadGuestFile(req, fields, files, params, done), done, 'photoBooth:write'));
};
