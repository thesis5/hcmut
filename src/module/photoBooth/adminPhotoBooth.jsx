import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getPhotoBoothInPage, createPhotoBooth, updatePhotoBooth, deletePhotoBooth } from './reduxPhotoBooth.jsx';
import Pagination from '../../view/common/Pagination.jsx';

class CreateModal extends React.Component {
    modal = React.createRef();

    componentDidMount() {
        $(document).ready(() => {
            $(this.modal.current).on('shown.bs.modal', () => {
                $('#photoBoothTitle').focus();
            });
        });
    }

    show = (e) => {
        e.preventDefault();
        $('#photoBoothTitle').val('');
        $(this.modal.current).modal('show');
    };

    create = (e) => {
        e.preventDefault();
        const data = {
            title: $('#photoBoothTitle').val().trim(),
        };
        if (data.title == '') {
            T.notify('Tiêu đề của Photo Booth bị trống!', 'danger');
            $('#photoBoothTitle').focus();
        } else {
            this.props.create(data);
        }
    };

    hide = () => {
        $(this.modal.current).modal('hide');
    };

    render() {
        return (
            <div className='modal' tabIndex='-1' ref={this.modal}>
                <form className='modal-dialog modal-lg' onSubmit={this.create}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Tạo mới Photo Booth</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='photoBoothTitle'>Tiêu đề Photo Booth</label>
                                <input className='form-control' id='photoBoothTitle' type='text' placeholder='Tiêu đề Photo Booth' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='submit' className='btn btn-primary'>Tạo</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class AdminPhotoBooth extends React.Component {
    createModal = React.createRef();

    componentDidMount() {
        T.ready(() => this.props.getPhotoBoothInPage());
    }

    create = (data) => {
        this.props.createPhotoBooth(data, photoBooth => {
            T.notify('Tạo Photo Booth mới thành công!', 'success');
            this.createModal.current.hide();
            this.props.history.push('/user/photo-booth/edit/' + photoBooth._id);
        });
    };

    changeActive = (item) => this.props.updatePhotoBooth(item._id, { active: !item.active }, () => {
        T.notify((item.active ? 'Hủy kích hoạt ' : 'Kích hoạt ') + 'thành công!', 'success');
    });

    delete = (e, _id) => {
        e.preventDefault();
        T.confirm('Xóa Photo Booth?', 'Bạn có chắc muốn xóa Photo Booth này', 'info', isConfirm =>
            isConfirm && this.props.deletePhotoBooth(_id));
    };

    render() {
        const permissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [],
            readOnly = !permissions.includes('photoBooth:write');
        const { pageNumber, pageSize, pageTotal, totalItem, list } = this.props.photoBooth && this.props.photoBooth.page ? this.props.photoBooth.page : { pageNumber: 0, pageSize: 0, pageTotal: 0, totalItem: 0, list: [] };
        const table = list && list.length ? (
            <table className='table table-hover table-bordered table-responsive'>
                <thead>
                    <tr>
                        <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                        <th style={{ width: '60%', whiteSpace: 'nowrap' }}>Tiêu đề</th>
                        <th style={{ width: '40%', whiteSpace: 'nowrap' }}>Người chụp hình</th>
                        <th style={{ width: 'auto', whiteSpace: 'nowrap' }}>Kích hoạt</th>
                        <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    {list.map((item, index) => (
                        <tr key={index}>
                            <td style={{ textAlign: 'right' }}>{(Math.max(pageNumber, 1) - 1) * pageSize + index + 1}</td>
                            <td><Link to={'/user/photo-booth/edit/' + item._id}>{item.title}</Link></td>
                            <td>{item.photographer ? <p>{(item.photographer.lastname + ' ' + item.photographer.firstname)}<br />{item.photographer.email}</p> : null}</td>
                            <td className='toggle' style={{ textAlign: 'center' }} >
                                <label>
                                    <input type='checkbox' checked={item.active} onChange={() => this.changeActive(item)} disabled={readOnly} />
                                    <span className='button-indecator' />
                                </label>
                            </td>
                            <td>
                                <div className='btn-group'>
                                    <Link className='btn btn-primary' to={'/user/photo-booth/edit/' + item._id}><i className='fa fa-lg fa-edit' /></Link>
                                    {!readOnly ?
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item._id)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                        : ''}
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        ) : <p>Không có dữ liệu!</p>;

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-users' /> Photo Booth</h1>
                </div>

                <div className='row tile'>
                    {table}
                </div>
                <Pagination name='pagePhotoBooth' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getPhotoBoothInPage} />
                {readOnly ? '' :
                    <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px' }} onClick={(e) => this.createModal.current.show(e)}>
                        <i className='fa fa-lg fa-plus' />
                    </button>}

                <CreateModal ref={this.createModal} create={this.create} />
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, photoBooth: state.photoBooth });
const mapActionsToProps = { getPhotoBoothInPage, createPhotoBooth, updatePhotoBooth, deletePhotoBooth };
export default connect(mapStateToProps, mapActionsToProps)(AdminPhotoBooth);
