import React from 'react';
import { connect } from 'react-redux';
import { homeGetPhotoBooth } from './reduxPhotoBooth.jsx';
import { homeGetAllGuest } from './reduxGuest.jsx';
import './style.scss';

class SectionPhotoBooth extends React.Component {
    state = { item: null, files: [], guestList: [] };

    componentDidMount() {
        $(document).ready(() => {
            if (this.props.photoBoothId) {
                $('footer').fadeOut();
                this.props.homeGetPhotoBooth(this.props.photoBoothId, (item) => {
                    item && this.props.homeGetAllGuest({ photoBooth: item._id, active: true, image: { $nin: [null, ''] } }, (guestList) => {
                        this.setState({ item, guestList: guestList.filter(guest => guest.image && guest.image != '') }, () => {
                            $('.hero-wrap').css('height', $(window).height());
                        });
                    });
                });
            }

            T.socket.on('guest-change', data => {
                let guestList = this.state.guestList;
                if (data.oldItem && this.props.photoBoothId == data.oldItem.photoBooth) {
                    for (let i = 0; i < guestList.length; i++) {
                        if (data.oldItem._id == guestList[i]._id) {
                            guestList.splice(i, 1);
                            break;
                        }
                    }
                }
                if (data.newItem && this.props.photoBoothId == data.newItem.photoBooth) {
                    guestList.push(data.newItem);
                }
                this.setState({ guestList }, () => {
                    $('.js-fullheight').css('height', $(window).height());
                    $('.hero-wrap').css('height', $(window).height());
                });
            });

            T.socket.on('cover-image', (action, photoBoothId, image) => {
                if (photoBoothId == this.props.photoBoothId) {
                    if (action == 'set') {
                        this.setState({ item: Object.assign({}, this.state.item, { image })});
                    } else if (action == 'remove') {
                        this.setState({ item: Object.assign({}, this.state.item, { image: null })});
                    }
                }
            });

            $('#requestFullscreen').click(function () {
                $('#fullscreen').fullscreen();
                $('.hero-wrap').css('height', $(window).height());
                return false;
            });
            $('#fullscreen .exitFullscreen').click(function () {
                $.fullscreen.exit();
                $('.hero-wrap').css('height', $(window).height());
                return false;
            });
            $(document).bind('fscreenchange', function (e, state, elem) {
                if ($.fullscreen.isFullScreen()) {
                    $('#requestFullscreen').hide();
                    $('#fullscreen .exitFullscreen').show().addClass('exit-fullscreen');
                } else {
                    $('#requestFullscreen').show();
                    $('#fullscreen .exitFullscreen').hide().removeClass('exit-fullscreen');
                }
            });

            setTimeout(() => {
                $('.js-fullheight').css('height', $(window).height());
                $('.hero-wrap').css('height', $(window).height());
                $(window).resize(function () {
                    $('.js-fullheight').css('height', $(window).height());
                    $('.hero-wrap').css('height', $(window).height());
                });

                const firstElement = $('#carousel-division').find('.item').first();
                let currentElement = $('#carousel-division').find('.item').first();
                let nextElement = currentElement.next();
                // this.interval = setInterval(() => {
                //     if (nextElement.length == 0) nextElement = firstElement;
                //     $(currentElement).fadeOut(2000);
                //     $(nextElement).fadeIn(2000);
                //     currentElement = nextElement;
                //     nextElement = nextElement.next();
                // }, 7000);
                const handle = () => {
                    if (nextElement.length == 0) nextElement = firstElement;
                    $(currentElement).fadeOut(2000);
                    $(nextElement).fadeIn(2000);
                    currentElement = nextElement;
                    nextElement = nextElement.next();
                    setTimeout(handle, 7000);
                };
                handle();
            }, 250);
        });
    }

    // componentWillUnmount() {
    //     clearInterval(this.interval);
    // }

    render() {
        const { guestList } = this.state;
        const UploadBoxStyle = {
            // backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            width: '100%',
            // height: '100%',
            textAlign: 'center'
        };
        const carousel = guestList.map((guest, index) => (
            <div key={index} className='item' style={{ display: index == 0 ? 'block' : 'none', position: 'absolute', top: '0', left: '0', width: '100%', height: '100%' }}>
                <div className='hero-wrap' style={Object.assign({}, UploadBoxStyle, { backgroundImage: `url('${guest.image}')` })}>
                    <div className='overlay' />
                    <div className='d-flex justify-content-end'>
                        <div className='row no-gutters slider-text js-fullheight align-items-end' data-scrollax-parent='true'>
                            <div className='col-md-12 text-center p-3' style={{ background: 'rgba(20,136,219,0.7)', paddingTop: '30px', width: '600px' }}>
                                <h1 className='mb-3 bread stroke-black' style={{ color: '#fff', fontSize: '2.1rem', textStroke: '2px solid white' }}>{guest.fullname}</h1>
                                <p className='mb-3 bread description stroke-black' style={{ color: '#fff', fontSize: '1.6rem', textStroke: '2px solid white' }}>
                                    {guest.description}
                                </p>
                                <p className='mb-3 bread description stroke-black' style={{ color: '#fff', fontSize: '1.6rem', textStroke: '2px solid white' }}>
                                    {guest.company}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ));
        return [
            <a key={0} href='#' id='requestFullscreen' style={{ position: 'absolute', top: '65px', right: '20px', fontSize: '24px', zIndex: '9999' }}><i className='fa fa-arrows-alt' /></a>,
            <section key={1} id='fullscreen' style={{ position: 'relative' }}>
                <div className='cover-image'>
                    <img src={this.state.item ? this.state.item.image: ''} alt='cover'/>
                </div>
                <a href='#' className='exitFullscreen' style={{ position: 'absolute', top: '20px', right: '20px', fontSize: '24px', zIndex: '9999', display: 'none' }}><i className='fa fa-arrows-alt' /></a>
                <div id='carousel-division'>
                    {carousel}
                </div>
            </section>
        ];
    }
}

const mapStateToProps = state => ({ photoBooth: state.photoBooth, guest: state.guest });
const mapActionsToProps = { homeGetPhotoBooth, homeGetAllGuest };
export default connect(mapStateToProps, mapActionsToProps)(SectionPhotoBooth);
