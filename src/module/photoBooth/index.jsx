import Loadable from 'react-loadable';
import Loading from '../../view/common/Loading.jsx';
import homeSection from './home.jsx';
import photoBooth from './reduxPhotoBooth.jsx';
import guest from './reduxGuest.jsx';

export default {
    redux: {
        photoBooth,
        guest,
    },
    routes: [
        {
            path: '/user/photo-booth/edit/:_id',
            component: Loadable({ loading: Loading, loader: () => import('./adminEditPhotoBooth.jsx') })
        },
        {
            path: '/user/photo-booth',
            component: Loadable({ loading: Loading, loader: () => import('./adminPhotoBooth.jsx') })
        },
    ],
    Section: homeSection
};
