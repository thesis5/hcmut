import React from 'react';
import { connect } from 'react-redux';
import { createSubscriber } from './redux.jsx';

const texts = {
    vi: {
        successSubscribe: 'Email của bạn đã được đăng ký thành công!',
        invalidEmail: 'Email của bạn không hợp lệ!',
        header: 'Đăng ký nhận thông tin từ chúng tôi',
        subTitle: 'Đăng ký bản tin để nhận tin tức, cập nhật mới nhất và hoạt động đặc biệt trong hộp thư đến của bạn.',
        emailPlaceholder: 'Email của bạn',
        subscribe: 'Đăng ký',
    },
    en: {
        successSubscribe: 'Your email has been added successfully!',
        invalidEmail: 'You email is invalid!',
        header: 'Subscribe to our Newsletter',
        subTitle: 'Join our subscribers list to get the latest news, updates and special events delivered directly in your inbox.',
        emailPlaceholder: 'Enter your email',
        subscribe: 'Subscribe',
    }
};

class SectionSubscribe extends React.Component {
    componentDidMount() {
        $(document).ready(() => {
            setTimeout(T.ftcoAnimate, 250);
        })
    }

    createSubscriber = (e) => {
        const language = T.language(texts);
        const subscriberEmail = $('#subscriberEmail');
        if (T.validateEmail(subscriberEmail.val())) {
            this.props.createSubscriber(subscriberEmail.val(), data => {
                if (data.error) {
                    T.alert(data.error, 'error', true, 3000);
                    console.error(data);
                } else {
                    T.alert(language.successSubscribe, 'success', true, 3000);
                    subscriberEmail.val('');
                }
            });
        } else {
            T.alert(language.invalidEmail, 'error', false, 1200);
        }
        e.preventDefault();
    }

    render() {
        const language = T.language(texts);
        return (
            <section className=' w-100 bg-light' style={{ padding: '5em 0' }}>
                <div className='parallax-img d-flex align-items-center'>
                    <div className='container' >
                        <div className='row d-flex justify-content-center'>
                            <div className='col-md-8 text-center  ftco-animate'>
                                <h2 style={{ color: 'black' }}>{language.header}</h2>
                                <p style={{ color: '#999' }}>{language.subTitle}</p>
                                <div className='row d-flex justify-content-center mt-5'>
                                    <form action='#' onSubmit={this.createSubscriber} className='subscribe-form col-md-8'>
                                        <div className='form-group d-flex'>
                                            <input type='text' id='subscriberEmail' className='form-control subscribe' placeholder={language.emailPlaceholder} />
                                            <input type='submit' value={language.subscribe} className='submit px-3' />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({ subscriber: state.subscriber });
const mapActionsToProps = { createSubscriber };
export default connect(mapStateToProps, mapActionsToProps)(SectionSubscribe);