import React from 'react';
import { connect } from 'react-redux';
import { getSubscriberInPage, deleteSubscriber, downloadSubscriber, sendEmailToSubscriber } from './redux.jsx'
import Pagination from '../../view/common/Pagination.jsx';
import Editor from '../../view/common/CkEditor4.jsx';

class SendEmailModal extends React.Component {
    constructor(props) {
        super(props);
        this.modal = React.createRef();
        this.editor = React.createRef();
    }

    show = () => {
        $('#semSubject').val('').focus();
        this.editor.current.html('');
        $(this.modal.current).modal('show');
    }
    hide = () => {
        $(this.modal.current).modal('hide');
    }

    sendEmail = (event) => {
        let mailSubject = $('#semSubject').val(),
            mailText = this.editor.current.text(),
            mailHtml = this.editor.current.html();
        sendEmailToSubscriber(mailSubject, mailText, mailHtml, () => this.hide());
        event.preventDefault();
    }

    render() {
        return (
            <div className='modal' tabIndex='-1' role='dialog' ref={this.modal}>
                <form className='modal-dialog modal-lg' role='document' onSubmit={this.save}>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title'>Gửi email đến subscriber</h5>
                            <button type='button' className='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div className='modal-body'>
                            <div className='form-group'>
                                <label htmlFor='semSubject'>Tựa đề</label><br />
                                <input className='form-control' id='semSubject' type='text' placeholder='Tựa đề' />
                            </div>
                            <div className='form-group'>
                                <label >Nội dung</label>
                                <Editor ref={this.editor} placeholder='Nội dung' />
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type='button' className='btn btn-secondary' data-dismiss='modal'>Đóng</button>
                            <button type='button' className='btn btn-primary' onClick={this.sendEmail}>Gửi</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class SubscriberPage extends React.Component {
    constructor(props) {
        super(props);
        this.sendEmailModal = React.createRef();
    }

    componentDidMount() {
        this.props.getSubscriberInPage();
        T.ready();
    }

    delete = (e, item) => {
        T.confirm('Xóa Subscriber', 'Bạn có chắc bạn muốn xóa subscriber này?', true, isConfirm => isConfirm && this.props.deleteSubscriber(item._id));
        e.preventDefault();
    }

    sendEmail = () => {
        this.sendEmailModal.current.show();
    }

    render() {
        const currentPermissions = this.props.system && this.props.system.user && this.props.system.user.permissions ? this.props.system.user.permissions : [];
        const { pageNumber, pageSize, pageTotal, totalItem } = this.props.subscriber && this.props.subscriber.page ?
            this.props.subscriber.page : { pageNumber: 1, pageSize: 50, pageTotal: 1, totalItem: 0 };
        let table = null;
        if (this.props.subscriber && this.props.subscriber.page && this.props.subscriber.page.list && this.props.subscriber.page.list.length > 0) {
            table = (
                <table className='table table-hover table-bordered' ref={this.table}>
                    <thead>
                        <tr>
                            <th style={{ width: 'auto', textAlign: 'center' }}>#</th>
                            <th style={{ width: '60%' }}>Email</th>
                            <th style={{ width: '40%' }}>Ngày</th>
                            {currentPermissions.includes('subscriber:write') ?
                                <th style={{ width: 'auto', textAlign: 'center', whiteSpace: 'nowrap' }}>Thao tác</th> : null}
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.subscriber.page.list.map((item, index) => (
                            <tr key={index}>
                                <td style={{ textAlign: 'right' }}>{(pageNumber - 1) * pageSize + index + 1}</td>
                                <td>{item.email}</td>
                                <td>{new Date(item.createdDate).getText()}</td>
                                {currentPermissions.includes('subscriber:write') ?
                                    <td style={{ textAlign: 'center' }}>
                                        <a className='btn btn-danger' href='#' onClick={e => this.delete(e, item)}>
                                            <i className='fa fa-lg fa-trash' />
                                        </a>
                                    </td> : null}
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else {
            table = <p>Không có subscriber!</p>;
        }

        return (
            <main className='app-content'>
                <div className='app-title'>
                    <h1><i className='fa fa-users' /> Subscriber</h1>
                </div>

                <div className='row tile'>{table}</div>
                <Pagination name='pageSubscriber' pageNumber={pageNumber} pageSize={pageSize} pageTotal={pageTotal} totalItem={totalItem}
                    getPage={this.props.getSubscriberInPage} />
                <SendEmailModal ref={this.sendEmailModal} />

                {currentPermissions.includes('subscriber:download') ?
                    <button type='button' className='btn btn-warning btn-circle' style={{ position: 'fixed', right: '10px', bottom: '10px', padding: 0 }}
                        onClick={downloadSubscriber}>
                        <i className='fa fa-lg fa-file-excel-o' style={{ margin: '0 auto' }} />
                    </button> : null}

                {currentPermissions.includes('subscriber:send') ?
                    <button type='button' className='btn btn-primary btn-circle' style={{ position: 'fixed', right: '70px', bottom: '10px', padding: 0 }}
                        onClick={this.sendEmail}>
                        <i className='fa fa-lg fa-send' style={{ margin: '0 auto' }} />
                    </button> : null}
            </main>
        );
    }
}

const mapStateToProps = state => ({ system: state.system, subscriber: state.subscriber });
const mapActionsToProps = { getSubscriberInPage, deleteSubscriber };
export default connect(mapStateToProps, mapActionsToProps)(SubscriberPage);