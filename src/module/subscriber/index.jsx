import Loadable from 'react-loadable';
import Loading from '../../view/common/Loading.jsx';
import homeSection from './home.jsx';
import subscriber from './redux.jsx';

export default {
    redux: {
        subscriber,
    },
    routes: [
        {
            path: '/user/subscriber',
            component: Loadable({ loading: Loading, loader: () => import('./admin.jsx') })
        }
    ],
    Section: homeSection
};