module.exports = app => {
    const menu = {
        parentMenu: { index: 2000, title: 'Cấu hình', icon: 'fa-cog' },
        menus: {
            2040: { title: 'Subscriber', link: '/user/subscriber' },
        },
    };
    app.permission.add(
        { name: 'subscriber:read', menu },
        // { name: 'subscriber:write', menu },
        { name: 'subscriber:write', menu },
        { name: 'subscriber:send', menu },
        { name: 'subscriber:download', menu },
    );
    app.get('/user/subscriber', app.permission.check('subscriber:read'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/subscriber/page/:pageNumber/:pageSize', app.permission.check('subscriber:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.subscriber.getPage(pageNumber, pageSize, {}, (error, page) => res.send({ error, page }));
    });

    app.get('/api/subscriber/all', app.permission.check('subscriber:read'), (req, res) => app.model.subscriber.getAll((error, items) => res.send({ error, items })));

    app.get('/api/subscriber/item/:_id', app.permission.check('subscriber:read'), (req, res) =>
        app.model.subscriber.get(req.params._id, (error, item) => res.send({ error, item })));

    app.post('/api/subscriber/send', app.permission.check('subscriber:send'), (req, res) => app.model.subscriber.getAll((error, items) => {
        if (error || items == null) {
            res.send({ error: 'Lỗi khi gửi email đến subscriber!' });
        } else {
            let emails = items.map(item => item.email);
            app.email.sendEmail(app.data.email, app.data.emailPassword, emails, [], req.body.mailSubject, req.body.mailText, req.body.mailHtml, null,
                () => res.send({ error: null }),
                error => res.send({ error }));
        }
    }));

    app.get('/api/subscriber/download', app.permission.check('subscriber:download'), (req, res) => app.model.subscriber.getAll((error, items) => {
        if (error) {
            res.send({ error });
        } else {
            const workbook = app.excel.create(),
                worksheet = workbook.addWorksheet('Subscriber'),
                cells = [{ cell: 'A1', value: '#', bold: true, border: '1234' }, { cell: 'B1', value: 'Email', bold: true, border: '1234' }];
            items.forEach((element, index) => {
                cells.push({ cell: 'A' + (index + 2), border: '1234', number: index + 1 });
                cells.push({ cell: 'B' + (index + 2), border: '1234', value: element.email });
            });

            worksheet.columns = [
                { header: '#', key: 'number', width: 5 },
                { header: 'Email', key: 'email', width: 32 }
            ];
            app.excel.write(worksheet, cells);
            app.excel.attachment(workbook, res, 'Subscriber.xlsx');
        }
    }));

    app.delete('/api/subscriber', app.permission.check('subscriber:write'), (req, res) =>
        app.model.subscriber.delete(req.body._id, error => res.send({ error })));


    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    app.post('/api/subscriber', (req, res) =>
        app.model.subscriber.create({ email: req.body.email }, (error, item) => res.send({ error, item })));
};