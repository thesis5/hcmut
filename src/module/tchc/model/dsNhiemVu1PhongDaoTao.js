module.exports = app => {
    const schema = app.db.Schema({
        shcc: String,
        ma_bm: String,
        ma_kh: String,
        cq182: Number,
        cq183: Number,
        cq191: Number,
        gvcn182: Number,
        gvcn191: Number,
        dttx182: Number,
        dttx183: Number,
        dttx191: Number,
        qtcn182: Number,
        qtcn183: Number,
        qtcn191: Number,
        qt182: Number,
        qt183: Number,
        qt191: Number,
        tonggiochuan: Number,
        nam: { type: Number, default: 2019 },
    });
    const model = app.db.model('DsNhiemVu1PhongDaoTao', schema);

    app.model.dsNhiemVu1PhongDaoTao = {
        create: (data, done) => {
            model.findOne({ shcc: data.shcc }, (error, item) => {
                if (error) {
                    done && done(error);
                } else if (!item) {
                    model.create(data, done);
                } else {
                    model.findOneAndUpdate({ _id: item._id }, { $set: data }, { new: true }, done);
                }
            });
        },

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, page) => {
            if (error || page == null) {
                done('System has errors!');
            } else {
                let result = { page, pageSize, pageTotal: Math.ceil(page / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ shcc: 1 }).skip(skipNumber).limit(result.pageSize).populate('user', 'firstname lastname email').exec((error, items) => {
                    result.list = items;
                    done(error, result);
                });
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).exec(done) :
            model.find({}).exec(condition),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        update: (_id, changes, done) => {
            if (changes.shcc) {
                model.findOne({ shcc: changes.shcc }, (error, item) => {
                    if (error) {
                        done && done(error);
                    } else if (!item || item._id == _id) {
                        model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
                    } else {
                        done && done('shcc is duplicated');
                    }
                });
            } else {
                model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
            }
        },

        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                item.remove(done);
            }
        }),

        deleteMany: (condition, done) => model.deleteMany(condition, done),
    };
};
