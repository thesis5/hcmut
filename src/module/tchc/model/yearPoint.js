module.exports = app => {
    const schema = app.db.Schema({
        year: { type: Number, default: 2019 },
        shcc: String,
        fullname: String,
        email: String,
        level: String,                                          // Chức danh - Trình độ
        user: { type: app.db.Schema.Types.ObjectId, ref: 'User' }, // Tham chiếu đến cán bộ trong bản User (isStaff = true)
        ratedRatio: { type: Number, default: 0 },               // Tỷ lệ định mức
        ratedRatio1: { type: Number, default: 0 },              // Tỷ lệ định mức 1
        ratedRatio2: { type: Number, default: 0 },              // Tỷ lệ định mức 2
        ratedRatio3: { type: Number, default: 0 },              // Tỷ lệ định mức 3

        professionalCoefficient: { type: Number, default: 1 },  // Kcm: hệ số chuyên môn
        leadershipCoefficient: { type: Number, default: 0 },    // Klđ: hệ số lãnh đạo
        additionalIncomeCoefficient: { type: Number, default: 0 }, // Ki: hệ số thu nhập tăng thêm

        rewardPoint: Number,                                    // Điểm thưởng
        penaltyPoint: Number,                                   // Điểm trừ (số dương)
        leadershipPoint: { type: Number, default: 0 },          // Điểm lãnh đạo
        leadershipCodes: [String],                              // Mã lãnh đạo
        currentLeadershipCode: String,                          // Mã lãnh đạo cao nhất
        currentLeadershipName: String,                          // Mã lãnh đạo cao nhất
        leadershipCode: String,                                 // Mã lãnh đạo được tính
        leadershipName: String,                                 // Tên lãnh đạo được tính

        professionalPoint: { type: Number, default: 0 },        // Điểm chuyên môn
        professionalPoint1: { type: Number, default: 0 },       // Điểm nhiệm vụ 1
        professionalPoint2: { type: Number, default: 0 },       // Điểm nhiệm vụ 2
        professionalPoint3: { type: Number, default: 0 },       // Điểm nhiệm vụ 3
        quotaName: String,                                      // Tên định mức
        quotaPoint1: { type: Number, default: 0 },              // Định mức nhiệm vụ 1
        quotaPoint2: { type: Number, default: 0 },              // Định mức nhiệm vụ 2
        quotaPoint3: { type: Number, default: 0 },              // Định mức nhiệm vụ 3

        round1Point: { type: Number, default: 0 },              // Điểm đánh giá cán bộ lần 1
        round2Point: { type: Number, default: 0 },              // Điểm đánh giá cán bộ lần 2

        transformationType: { type: String, default: '' },      // Có chuyển điểm NV1, NV2
        point: { type: Number, default: 0 },                    // Điểm đánh giá năm
        resultBySystem: String,
        result: String,
        discipline: String,                                     // Kỷ luật

        active: { type: Boolean, default: true },
        isException: { type: Boolean, default: false },         // Là trường hợp ngoại lệ
        note: String,
        note3: String,

        facultyCode: String,                                    // Mã khoa
        facultyName: String,                                    // Tên khoa
        departmentCode: String,                                 // Mã bộ môn
        departmentName: String,                                 // Tên bộ môn
    });
    const model = app.db.model('YearPoint', schema);

    app.model.yearPoint = {
        create: (data, done) => model.create(data, done),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, page) => {
            if (error || page == null) {
                done('System has errors!');
            } else {
                let result = { page, pageSize, pageTotal: Math.ceil(page / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ shcc: 1 }).skip(skipNumber).limit(result.pageSize).populate('user', 'firstname lastname email').exec((error, items) => {
                    result.list = items;
                    done(error, result);
                });
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).sort({ shcc: 1 }).exec(done) :
            model.find({}).sort({ shcc: 1 }).exec(condition),

        update: (condition, changes, done) => typeof condition == 'string' ?
            model.findIdAndUpdate(condition, { $set: changes }, { new: true }, done) :
            model.findOneAndUpdate(condition, { $set: changes }, { new: true }, done),

        set: (user, year, data, done) => model.findOne({ user, year }, (error, point) => {
            if (error) {
                done && done(error);
            } else if (point) {
                delete data.user;
                delete data.year;
                model.findOneAndUpdate({ _id: point._id }, { $set: data }, { new: true }, done);
            } else {
                data.user = user;
                data.year = year;
                model.create(data, done);
            }
        }),

        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Id không hợp lệ!');
            } else {
                item.remove(done);
            }
        }),
    };

    // TODO: delete
    const unactiveList = [
        '001090', '001018', '002063', '002538', '0.2454', '002791', '002792', '001129', '003014', '000654', '000961', '003620', '003649', '003695', '000674', '000794', '001087', '002045', '002454',
        '001562', '002268', '002688', '003257', '003393', '003517', '003627', '003786', '008400',
        '003849', '003441', '003933', '001834', '003941',
    ];
    model.find({}, (error, items) => {
        if (items) {
            items.forEach(item => {
                item.active = !unactiveList.includes(item.shcc);
                item.save();
            })
        }
    });
};
