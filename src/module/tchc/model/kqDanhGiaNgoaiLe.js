module.exports = app => {
    const schema = app.db.Schema({
        shcc: { type: String },
        lastName: { type: String, default: '' },
        firstName: { type: String, default: '' },
        classification: { type: String, default: '' },
        kI: { type: Number, default: 0 }
    });
    const model = app.db.model('KqDanhGiaNgoaiLe', schema);

    app.model.kqDanhGiaNgoaiLe = {
        create: (data, done) => model.create(data, done),

        get: (condition, done) => typeof condition == 'string' ?
            model.findById(condition, done) : model.findOne(condition, done),

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);

                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ _id: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    result.list = error ? [] : items;
                    done(error, result);
                });
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).sort({ _id: -1 }).exec(done) :
            model.find({}).sort({ _id: -1 }).exec(condition),

        update: (condition, changes, done) => {
            model.findOne({ shcc: condition.shcc }, (error, officer) => {
                if (error) {
                    done && done(error);
                } else if (!officer) {
                    changes.shcc = condition.shcc;
                    model.create(changes, done);
                } else {
                    delete changes.shcc;
                    model.findOneAndUpdate({ _id: officer._id }, { $set: changes }, { new: true }, done);
                }
            })
        },

        delete: (_id, done) => model.findById(_id, (error, item) => {
            console.log('id', _id, item);
            if (error) {
                done(error);
            } else if (item == null) {
                done('Id không hợp lệ!');
            } else {
                item.remove(error => {
                    if (error) {
                        done(error);
                    } else {
                        app.model.component.clearViewId(_id, done);
                    }
                });
            }
        }),
    };
};
