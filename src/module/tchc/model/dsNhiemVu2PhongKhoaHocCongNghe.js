module.exports = app => {
    const schema = app.db.Schema({
        shcc: String,
        fullName: String,
        shtt: String,
        cgcn: String,
        ldsx: String,
        articleOfUniversity: String,
        articleOutUniversity: String,
        reward: String,
        elseAction: String,
        article: String,
        point: { type: Number, default: 0 },
        totalHours: { type: Number, default: 0 },
        nam: { type: Number, default: 2019 },
    });
    const model = app.db.model('DsNhiemVu2PhongKhoaHocCongNghe', schema);

    app.model.dsNhiemVu2PhongKhoaHocCongNghe = {
        create: (data, done) => {
            model.findOne({ shcc: data.shcc }, (error, item) => {
                if (error) {
                    done && done(error);
                } else if (!item) {
                    model.create(data, done);
                } else {
                    model.findOneAndUpdate({ _id: item._id }, { $set: data }, { new: true }, done);
                }
            });
        },

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ shcc: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    done(error, app.clone(result, { list: items }));
                });
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).sort({ shcc: 1 }).exec(done) :
            model.find({}).sort({ shcc: 1 }).exec(condition),

        update: (_id, changes, done) => {
            if (changes.shcc) {
                model.findOne({ shcc: changes.shcc }, (error, item) => {
                    if (error) {
                        done && done(error);
                    } else if (!item || item._id == _id) {
                        model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
                    } else {
                        done && done('shcc is duplicated');
                    }
                });
            } else {
                model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
            }
        },
        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Id không hợp lệ!');
            } else {
                item.remove(done);
            }
        }),
    };
};
