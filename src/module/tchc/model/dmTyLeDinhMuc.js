module.exports = app => {
    const schema = app.db.Schema({
        chucDanh: String,
        soGioLamViec: { type: Number, default: 0 },
        soGioGiangDay: { type: Number, default: 0 },
        nghienCuuKhoaHoc: { type: Number, default: 0 },
        nhiemVuKhac: { type: String, default: '' },
        nam: { type: Number, default: 2019 },
        kI: Number,
        ghiChu: String,
        tongTgLamViec: { type: Number, default: 0 }
    });
    const model = app.db.model('DanhMucTyLeDinhMuc', schema);

    app.model.dmTyLeDinhMuc = {
        create: (data, done) => {
            model.findOne({ chucDanh: data.chucDanh }, (error, item) => {
                if (error) {
                    done && done(error);
                } else if (!item) {
                    model.create(data, done);
                } else {
                    model.findOneAndUpdate({ _id: item._id }, { $set: data }, { new: true }, done);
                }
            });
        },

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ _id: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    done(error, app.clone(result, { list: items }));
                });
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).exec(done) :
            model.find({}).exec(condition),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        update: (_id, changes, done) => {
            if (changes.chucDanh) {
                model.findOne({ shcc: changes.chucDanh }, (error, item) => {
                    if (error) {
                        done && done(error);
                    } else if (!item || item._id == _id) {
                        model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
                    } else {
                        done && done('chức danh bị lặp');
                    }
                });
            } else {
                model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
            }
        },

        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                item.remove(done);
            }
        }),

        deleteMany: (condition, done) => model.deleteMany(condition, done),
    };
};
