// Danh mục Chức vụ
module.exports = app => {
    const schema = app.db.Schema({
        year: Number,
        maQL: String,                   //Unique
        tenChucVu: String,
        heSoPCQL: Number,
        phanNgachCBQL: String,
        maNhomQuanLy: String,
        maNhomQuanLyKhongGD: String,
        tyLeDinhMuc: Number,
        kCM: Number,
        kLD: Number,
        kI: Number,
        cap: Number,
        chucVuDoanThe: Number,
        note: String,
    });
    const model = app.db.model('categoryOffice', schema);

    app.model.categoryOffice = {
        create: (data, done) => {
            model.findOne({ maQL: data.maQL }, (error, category) => {
                if (error) {
                    done && done(error);
                } else if (!category) {
                    model.create(data, done);
                } else {
                    model.findOneAndUpdate({ _id: category._id }, { $set: data }, { new: true }, done);
                }
            });
        },

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ maQL: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    done(error, app.clone(result, { list: items }));
                });
            }
        }),

        get: (condition, done) => typeof condition == 'string' ? // condition is _id
            model.findById(condition, done) : model.findOne(condition, done),

        getAll: (condition, params, done) => {
            if (!done || typeof params === 'function') {
                model.find(condition).populate('user', '-password').exec(params);
            } else {
                model.find(condition, params).populate('user', '-password').exec(done);
            }
        },

        update: (_id, changes, done) => {
            if (changes.maQL) {
                model.findOne({ maQL: changes.maQL }, (error, item) => {
                    if (error) {
                        done && done(error);
                    } else if (!item || item._id == _id) {
                        model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
                    } else {
                        done && done('MaQL is duplicated');
                    }
                });
            } else {
                model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
            }
        },

        delete: (condition, done) => typeof condition == 'string' ? // condition is _id
            model.findById(condition, (error, item) => {
                if (error) {
                    done(error);
                } else if (item == null) {
                    done('Invalid Id!');
                } else {
                    item.remove(done);
                }
            }) : model.deleteMany(condition, (error) => done(error)),
    };
};
