module.exports = app => {
    const schema = app.db.Schema({
        shcc: String,
        fullname: String,
        ngay: Date,
        maMonHoc: String,
        monHoc: String,
        thoiLuong: String,
        kyThi: String,
        donViCongTac: String,
        vn1: { type: Number }, //Quy đổi giờ chuẩn nhiệm vụ 1
        ghiChu: String,
        nam: { type: Number, default: 2019 },
    });
    const model = app.db.model('DsNhiemVu1CoiThi', schema);

    app.model.dsNhiemVu1CoiThi = {
        create: (data, done) => {
            model.findOne({ shcc: data.shcc }, (error, item) => {
                if (error) {
                    done && done(error);
                } else if (!item) {
                    model.create(data, done);
                } else {
                    done && done(error, item);
                }
            });
        },

        getAll: (condition, done) => done ?
            model.find(condition).exec(done) :
            model.find({}).exec(condition),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        update: (_id, changes, done) => {
            if (changes.shcc) {
                model.findOne({ shcc: changes.shcc }, (error, item) => {
                    if (error) {
                        done && done(error);
                    } else if (!item || item._id == _id) {
                        model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
                    } else {
                        done && done('shcc is duplicated');
                    }
                });
            } else {
                model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
            }
        },
        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                item.remove(done);
            }
        }),
        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, page) => {
            if (error || page == null) {
                done('System has errors!');
            } else {
                let result = { page, pageSize, pageTotal: Math.ceil(page / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ shcc: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    result.list = items;
                    done(error, result);
                });
            }
        }),

        deleteMany: (condition, done) => model.deleteMany(condition, done),
    };
};