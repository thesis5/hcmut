module.exports = app => {
    const schema = app.db.Schema({
        shcc: String,
        ghiChu: String,
        nam: { type: Number, default: 2019 },
    });
    const model = app.db.model('DsViPhamKyLuat', schema);

    app.model.dsViPhamKyLuat = {
        create: (data, done) => model.create(data, done),

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, page) => {
            if (error || page == null) {
                done('System has errors!');
            } else {
                let result = { page, pageSize, pageTotal: Math.ceil(page / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ shcc: -1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    result.list = items;
                    done(error, result);
                });
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).exec(done) :
            model.find({}).exec(condition),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        update: (_id, $set, $unset, done) => done ?
            model.findOneAndUpdate({ _id }, { $set, $unset }, { new: true }, done) :
            model.findOneAndUpdate({ _id }, { $set }, { new: true }, $unset),

        delete: (_id, done) => model.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                item.remove(done);
            }
        }),

        deleteMany: (condition, done) => model.deleteMany(condition, done),
    };
};