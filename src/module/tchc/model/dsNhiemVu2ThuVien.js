module.exports = app => {
    const schema = app.db.Schema({
        shcc: String,
        authorName: String,                //Họ và tên tác giả
        acceptedHour: Number,              //Số giờ được tính
        curriculum: String,                //Giáo trình
        content: String,
        page: String,
        factor: String,                    //Hệ số
        firstFactor: String,               //Nhân hệ số lần đầu
        currentExchange: String,           //Tính quy đổi lần này
        note: String,                      //Ghi chú
        nam: { type: Number, default: 2019 },
    });
    const model = app.db.model('DsNhiemVu2ThuVien', schema);

    app.model.dsNhiemVu2ThuVien = {
        create: (data, done) => {
            model.findOne({ shcc: data.shcc }, (error, item) => {
                if (error) {
                    done && done(error);
                } else if (!item) {
                    model.create(data, done);
                } else {
                    model.findOneAndUpdate({ _id: item._id }, { $set: data }, { new: true }, done)
                }
            });
        },

        getPage: (pageNumber, pageSize, condition, done) => model.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                model.find(condition).sort({ shcc: 1 }).skip(skipNumber).limit(result.pageSize).exec((error, items) => {
                    done(error, app.clone(result, { list: items }));
                });
            }
        }),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        getAll: (condition, params, done) => {
            if (!done || typeof params === 'function') {
                model.find(condition).exec(params);
            } else {
                model.find(condition, params).exec(done);
            }
        },

        update: (_id, changes, done) => {
            if (changes.shcc) {
                model.findOne({ shcc: changes.shcc }, (error, item) => {
                    if (error) {
                        done && done(error);
                    } else if (!item || item._id == _id) {
                        model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
                    } else {
                        done && done('shcc is duplicated');
                    }
                });
            } else {
                model.findOneAndUpdate({ _id }, { $set: changes }, { new: true }, done)
            }
        },

        delete: (condition, done) => typeof condition == 'string' ? // condition is _id
            model.findById(condition, (error, item) => {
                if (error) {
                    done(error);
                } else if (item == null) {
                    done('Invalid Id!');
                } else {
                    item.remove(done);
                }
            }) : model.deleteMany(condition, (error) => done(error)),
    }
};
