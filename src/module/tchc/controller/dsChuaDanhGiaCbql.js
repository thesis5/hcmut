module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8109: { title: 'DS Chưa đánh giá cán bộ quản lý', link: '/user/summary/ds-chua-dg-cbql' } },
    };
    app.permission.add(
        { name: 'dsChuaDanhGiaCbql:read', menu },
        { name: 'dsChuaDanhGiaCbql:write', menu },
    );

    app.get('/user/summary/ds-chua-dg-cbql/upload', app.permission.check('dsChuaDanhGiaCbql:read'), app.templates.admin);
    app.get('/user/summary/ds-chua-dg-cbql', app.permission.check('dsChuaDanhGiaCbql:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-chua-dg-cbql/page/:pageNumber/:pageSize', app.permission.check('dsChuaDanhGiaCbql:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsChuaDanhGiaCbql.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds-chua-dg-cbql/multiple', app.permission.check('dsChuaDanhGiaCbql:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsChuaDanhGiaCbql.create(points[i], error =>
                    error && errorList.push(error));
            }
        }
    });

    app.put('/api/ds-chua-dg-cbql', app.permission.check('dsChuaDanhGiaCbql:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsChuaDanhGiaCbql.update(_id, changes, (error, item) => res.send({ error, item }));
    });

    app.delete('/api/ds-chua-dg-cbql', app.permission.check('dsChuaDanhGiaCbql:write'), (req, res) => app.model.dsChuaDanhGiaCbql.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-chua-dg-cbql', app.permission.check('dsChuaDanhGiaCbql:write'), (req, res) =>
        app.model.dsChuaDanhGiaCbql.create(req.body.data, (error, item) => res.send({ error, item })));

    // Hook upload dsChuaDGCBQLPointImportData ------------------------------------------------------------------------------------------------------
    const dsChuaDGCBQLPointImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsChuaDGCBQLPointImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsChuaDGCBQLPointImportData');
            app.uploadDsChuaDGCBQLPointFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsChuaDGCBQLPointImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsChuaDGCBQLPointImportData(req, fields, files, params, done), done, 'dsChuaDanhGiaCbql:write'));
}
