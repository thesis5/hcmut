module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8112: { title: 'DS Giảm định mức', link: '/user/summary/ds-giam-dinh-muc' } },
    };
    app.permission.add(
        { name: 'dsGiamDinhMuc:read', menu },
        { name: 'dsGiamDinhMuc:write', menu },
    );

    app.get('/user/summary/ds-giam-dinh-muc/upload', app.permission.check('dsGiamDinhMuc:read'), app.templates.admin);
    app.get('/user/summary/ds-giam-dinh-muc', app.permission.check('dsGiamDinhMuc:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-giam-dinh-muc/page/:pageNumber/:pageSize', app.permission.check('dsGiamDinhMuc:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsGiamDinhMuc.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-giam-dinh-muc', app.permission.check('dsGiamDinhMuc:write'), (req, res) => app.model.dsGiamDinhMuc.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-giam-dinh-muc', app.permission.check('dsGiamDinhMuc:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsGiamDinhMuc.get({ _id: body._id }, (error, item) => {
           if (error) {
               res.send({error});
           } else if (item) {
               res.send({ exist: true });
           } else {
               app.model.dsGiamDinhMuc.create(body, (error, item) => res.send({ error, item }));
           }
        });
    });

    app.post('/api/ds-giam-dinh-muc/multiple', app.permission.check('dsGiamDinhMuc:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dsGiamDinhMuc.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-giam-dinh-muc', app.permission.check('dsGiamDinhMuc:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsGiamDinhMuc.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload dsGiamDinhMucImportData ----------------------------------------------------------------------------------------------------------
    const dsGiamDinhMucImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsGiamDinhMucImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsGiamDinhMucImportData');
            app.uploadDsGiamDMPointFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsGiamDinhMucImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsGiamDinhMucImportData(req, fields, files, params, done), done, 'dsGiamDinhMuc:write'));
}
