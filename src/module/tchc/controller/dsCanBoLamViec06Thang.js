module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8113: { title: 'DS viên chức làm việc từ 0 đến dưới 6 tháng', link: '/user/summary/ds_can_bo_lam_viec_06_thang' } },
    };
    app.permission.add(
        { name: 'dsCanBoLamViec06Thang:read', menu },
        { name: 'dsCanBoLamViec06Thang:write', menu },
    );

    app.get('/user/summary/ds_can_bo_lam_viec_06_thang', app.permission.check('dsCanBoLamViec06Thang:read'), app.templates.admin);
    app.get('/user/summary/ds_can_bo_lam_viec_06_thang/upload', app.permission.check('dsCanBoLamViec06Thang:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds_can_bo_lam_viec_06_thang/page/:pageNumber/:pageSize', app.permission.check('dsCanBoLamViec06Thang:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsCanBoLamViec06Thang.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds_can_bo_lam_viec_06_thang', app.permission.check('dsCanBoLamViec06Thang:write'), (req, res) =>
        app.model.dsCanBoLamViec06Thang.create(req.body.data, (error, item) => res.send({ error, item })));

    app.put('/api/ds_can_bo_lam_viec_06_thang', app.permission.check('dsCanBoLamViec06Thang:write'), (req, res) =>
        app.model.dsCanBoLamViec06Thang.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.put('/api/ds_can_bo_lam_viec_06_thang/multi_values', app.permission.check('dsCanBoLamViec06Thang:write'), (req, res) => {
        const items = req.body.items, errorList = [];
        for (let i = 0; i <= items.length; i++) {
            if (i == items.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsCanBoLamViec06Thang.create(items[i], error => error && errorList.push(error));
            }
        }
    });

    app.delete('/api/ds_can_bo_lam_viec_06_thang', app.permission.check('dsCanBoLamViec06Thang:write'), (req, res) =>
        app.model.dsCanBoLamViec06Thang.delete(req.body._id, error => res.send({ error })));


    // Hook upload dsCanBoLamViec06Thang -------------------------------------------------------------------------------------------------------------------
    const uploadDsCanBoLamViec06Thang = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] == 'DsCanBoLamViec06ThangImportData' && files.DsCanBoLamViec06ThangFile && files.DsCanBoLamViec06ThangFile.length > 0) {
            console.log('Hook: DsCanBoLamViec06ThangImportData => DsCanBoLamViec06ThangFile upload');

            app.excel.readFile(files.DsCanBoLamViec06ThangFile[0].path, workbook => {
                app.deleteFile(files.DsCanBoLamViec06ThangFile[0].path);
                if (workbook) {
                    const worksheet = workbook.getWorksheet(1),
                        items = [],
                        totalRow = worksheet.lastRow.number;
                    let numberOfErrors = 0,
                        warnings = [];
                    const handleUpload = (index = 2) => {
                        const value = worksheet.getRow(index).values;
                        console.log('OK', value)

                        if (value.length == 0 || index == totalRow + 1) {
                            done({ items, warnings, numberOfErrors });
                        } else {
                            const nam = value[1], shcc = value[2], soThangLamViec = value[3], lyDoKhongTinh = value[4];
                            console.log({ nam, shcc, soThangLamViec, lyDoKhongTinh });
                            app.model.staff.getByShcc(shcc, (error, staff) => {
                                if (error) {
                                    numberOfErrors++;
                                    handleUpload(index + 1);
                                } else if (!staff) {
                                    warnings.push(`shcc: ${shcc} không tồn tại!</br>`);
                                    handleUpload(index + 1);
                                } else {
                                    items.push({ nam, shcc, soThangLamViec, lyDoKhongTinh });
                                    handleUpload(index + 1);
                                }
                            });
                        }
                    };
                    handleUpload();
                } else {
                    done({ error: 'Error' });
                }
            });
        }
    };
    app.uploadHooks.add('uploadDsCanBoLamViec06Thang', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadDsCanBoLamViec06Thang(req, fields, files, params, done), done, 'dsCanBoLamViec06Thang:write'));
};
