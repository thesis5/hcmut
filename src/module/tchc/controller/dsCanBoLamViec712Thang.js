module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8114: { title: 'DS viên chức làm việc từ 6 đến dưới 12 tháng', link: '/user/summary/ds_can_bo_lam_viec_712_thang' } },
    };
    app.permission.add(
        { name: 'dsCanBoLamViec712Thang:read', menu },
        { name: 'dsCanBoLamViec712Thang:write', menu },
    );

    app.get('/user/summary/ds_can_bo_lam_viec_712_thang', app.permission.check('dsCanBoLamViec712Thang:read'), app.templates.admin);
    app.get('/user/summary/ds_can_bo_lam_viec_712_thang/upload', app.permission.check('dsCanBoLamViec712Thang:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds_can_bo_lam_viec_712_thang/page/:pageNumber/:pageSize', app.permission.check('dsCanBoLamViec712Thang:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsCanBoLamViec712Thang.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds_can_bo_lam_viec_712_thang', app.permission.check('dsCanBoLamViec712Thang:write'), (req, res) =>
        app.model.dsCanBoLamViec712Thang.create(req.body.data, (error, item) => res.send({ error, item })));

    app.put('/api/ds_can_bo_lam_viec_712_thang', app.permission.check('dsCanBoLamViec712Thang:write'), (req, res) =>
        app.model.dsCanBoLamViec712Thang.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.put('/api/ds_can_bo_lam_viec_712_thang/multi_values', app.permission.check('dsCanBoLamViec712Thang:write'), (req, res) => {
        const items = req.body.items, errorList = [];
        for (let i = 0; i <= items.length; i++) {
            if (i == items.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsCanBoLamViec712Thang.create(items[i], error => error && errorList.push(error));
            }
        }
    });

    app.delete('/api/ds_can_bo_lam_viec_712_thang', app.permission.check('dsCanBoLamViec712Thang:write'), (req, res) =>
        app.model.dsCanBoLamViec712Thang.delete(req.body._id, error => res.send({ error })));


    // Hook upload dsCanBoLamViec712Thang -------------------------------------------------------------------------------------------------------------------
    const uploadDsCanBoLamViec712Thang = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] == 'DsCanBoLamViec712ThangImportData' && files.DsCanBoLamViec712ThangFile && files.DsCanBoLamViec712ThangFile.length > 0) {
            console.log('Hook: DsCanBoLamViec712ThangImportData => DsCanBoLamViec712ThangFile upload');

            app.excel.readFile(files.DsCanBoLamViec712ThangFile[0].path, workbook => {
                app.deleteFile(files.DsCanBoLamViec712ThangFile[0].path);
                if (workbook) {
                    const worksheet = workbook.getWorksheet(1),
                        items = [],
                        totalRow = worksheet.lastRow.number;
                    let numberOfErrors = 0,
                        warnings = [];
                    const handleUpload = (index = 2) => {
                        const value = worksheet.getRow(index).values;
                        if (value.length == 0 || index == totalRow + 1) {
                            done({ items, warnings, numberOfErrors });
                        } else {
                            const nam = value[1], shcc = value[2], soThangLamViec = value[3], lyDoKhongTinh = value[4];
                            app.model.staff.getByShcc(shcc, (error, staff) => {
                                if (error) {
                                    numberOfErrors++;
                                    handleUpload(index + 1);
                                } else if (!staff) {
                                    warnings.push(`shcc: ${shcc} không tồn tại!</br>`);
                                    handleUpload(index + 1);
                                } else {
                                    items.push({ nam, shcc, soThangLamViec, lyDoKhongTinh });
                                    handleUpload(index + 1);
                                }
                            });
                        }
                    };
                    handleUpload();
                } else {
                    done({ error: 'Error' });
                }
            });
        }
    };
    app.uploadHooks.add('uploadDsCanBoLamViec712Thang', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadDsCanBoLamViec712Thang(req, fields, files, params, done), done, 'dsCanBoLamViec712Thang:write'));
};
