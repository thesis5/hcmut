module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8104: { title: 'DS Nhiệm vụ 2 - phòng Khoa học công nghệ', link: '/user/summary/nv2-pkhcn' } },
    };
    app.permission.add(
        { name: 'dsNhiemVu2PhongKhoaHocCongNghe:read', menu },
        { name: 'dsNhiemVu2PhongKhoaHocCongNghe:write', menu },
    );

    app.get('/user/summary/nv2-pkhcn', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:read'), app.templates.admin);
    app.get('/user/summary/nv2-pkhcn/upload-point', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds_nv2_pkhcn/page/:pageNumber/:pageSize', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsNhiemVu2PhongKhoaHocCongNghe.getPage(pageNumber, pageSize, condition, (error, page) => {
            page.list = page.list.map(item => app.clone(item, { message: '' }));
            res.send({ error, page });
        });
    });

    app.post('/api/ds_nv2_pkhcn', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsNhiemVu2PhongKhoaHocCongNghe.get({ _id: body._id }, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (item) {
                res.send({ exist: true });
            } else {
                app.model.dsNhiemVu2PhongKhoaHocCongNghe.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.get('/api/nv2-pkhcn/all', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:read'), (req, res) => app.model.dsNhiemVu2PhongKhoaHocCongNghe.getAll((error, items) => res.send({ error, items })));

    app.get('/api/nv2-pkhcn/unread', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:read'), (req, res) => app.model.dsNhiemVu2PhongKhoaHocCongNghe.getUnread((error, items) => res.send({ error, items })));

    app.get('/api/nv2-pkhcn/item/:_id', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:write'), (req, res) => app.model.dsNhiemVu2PhongKhoaHocCongNghe.read(req.params._id, (error, item) => {
        if (item) app.io.emit('contact-changed', item);
        res.send({ error, item });
    }));
    app.put('/api/ds_nv2_pkhcn/update', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:write'), (req, res) =>
        app.model.dsNhiemVu2PhongKhoaHocCongNghe.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.put('/api/user/ds_nv2_pkhcn-multi', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsNhiemVu2PhongKhoaHocCongNghe.create(points[i], error => error && errorList.push(error));
            }
        }
    });

    app.delete('/api/ds_nv2_pkhcn/delete', app.permission.check('dsNhiemVu2PhongKhoaHocCongNghe:write'), (req, res) => app.model.dsNhiemVu2PhongKhoaHocCongNghe.delete(req.body._id, error => res.send({ error })));


    // Hook upload ds_nv2_pkhcnImportData -----------------------------------------------------------------------------------------------------------
    const ds_nv2_pkhcnImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'ds_nv2_pkhcnImportData' && files.Ds_nv2_pkhcnFile && files.Ds_nv2_pkhcnFile.length > 0) {
            console.log('Hook: ds_nv2_pkhcnImportData');
            app.uploadDs_nv2_pkhcnFile(req, files.Ds_nv2_pkhcnFile[0].path, done);
        }
    };

    app.uploadHooks.add('ds_nv2_pkhcnImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => ds_nv2_pkhcnImportData(req, fields, files, params, done), done, 'dsNhiemVu2PhongKhoaHocCongNghe:write'));
};
