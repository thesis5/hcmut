module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8111: { title: 'DS Tập sự', link: '/user/summary/ds-tap-su' } },
    };
    app.permission.add(
        { name: 'dsTapSu:read', menu },
        { name: 'dsTapSu:write', menu },
    );

    app.get('/user/summary/ds-tap-su', app.permission.check('dsTapSu:read'), app.templates.admin);
    app.get('/user/summary/ds-tap-su/upload', app.permission.check('dsTapSu:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-tap-su/page/:pageNumber/:pageSize', app.permission.check('dsTapSu:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsTapSu.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-tap-su', app.permission.check('dsTapSu:write'), (req, res) => app.model.dsTapSu.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-tap-su', app.permission.check('dsTapSu:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsTapSu.get({ _id: body._id }, (error, item) => {
           if (error) {
               res.send({error});
           } else if (item) {
               res.send({ exist: true });
           } else {
               app.model.dsTapSu.create(body, (error, item) => res.send({ error, item }));
           }
        });
    });

    app.post('/api/ds-tap-su/multiple', app.permission.check('dsTapSu:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dsTapSu.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-tap-su', app.permission.check('dsTapSu:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsTapSu.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload dsTapSuImportData ----------------------------------------------------------------------------------------------------------------
    const dsTapSuImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsTapSuImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsTapSuImportData');
            app.uploadDsTapSuFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsTapSuImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsTapSuImportData(req, fields, files, params, done), done, 'dsTapSu:write'));

};
