module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8102: { title: 'DS Nhiệm vụ 1 - coi thi', link: '/user/summary/ds-coithi-nv1' } },
    };
    app.permission.add(
        { name: 'dsNhiemVu1CoiThi:read', menu },
        { name: 'dsNhiemVu1CoiThi:write', menu },
    );
    app.get('/user/summary/ds-coithi-nv1', app.permission.check('dsNhiemVu1CoiThi:read'), app.templates.admin);
    app.get('/user/summary/ds-coithi-nv1/upload-point', app.permission.check('dsNhiemVu1CoiThi:write'), app.templates.admin);


    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-coithi-nv1/page/:pageNumber/:pageSize', app.permission.check('dsNhiemVu1CoiThi:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsNhiemVu1CoiThi.getPage(pageNumber, pageSize, condition, (error, page) => {
            page.list = page.list.map(item => app.clone(item, { message: '' }));
            res.send({ error, page });
        });
    });

    app.post('/api/ds-coithi-nv1', app.permission.check('dsNhiemVu1CoiThi:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsNhiemVu1CoiThi.get({ _id: body._id }, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (item) {
                res.send({ exist: true });
            } else {
                app.model.dsNhiemVu1CoiThi.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.get('/api/ds-coithi-nv1/all', app.permission.check('dsNhiemVu1CoiThi:read'), (req, res) => app.model.dsNhiemVu1CoiThi.getAll((error, items) => res.send({ error, items })));

    app.get('/api/ds-coithi-nv1/unread', app.permission.check('dsNhiemVu1CoiThi:read'), (req, res) => app.model.dsNhiemVu1CoiThi.getUnread((error, items) => res.send({ error, items })));

    app.get('/api/ds-coithi-nv1/item/:_id', app.permission.check('dsNhiemVu1CoiThi:read'), (req, res) => app.model.dsNhiemVu1CoiThi.read(req.params._id, (error, item) => {
        if (item) app.io.emit('contact-changed', item);
        res.send({ error, item });
    }));
    app.post('/api/user/ds-coithi-nv1-multi', app.permission.check('dsNhiemVu1CoiThi:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsNhiemVu1CoiThi.create(points[i], error => error && errorList.push(error));
            }
        }
    });
    app.put('/api/ds-coithi-nv1/update', app.permission.check('dsNhiemVu1CoiThi:write'), (req, res) =>
        app.model.dsNhiemVu1CoiThi.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.put('/api/user/ds-coithi-nv1-multi', app.permission.check('dsNhiemVu1CoiThi:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsNhiemVu1CoiThi.create(points[i], error => error && errorList.push(error));
            }
        }
    });

    app.delete('/api/ds-coithi-nv1/delete', app.permission.check('dsNhiemVu1CoiThi:write'), (req, res) => app.model.dsNhiemVu1CoiThi.delete(req.body._id, error => res.send({ error })));

    const dsNhiemVu1CoiThiFile = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsNhiemVu1CoiThiFile' && files.DsNhiemVu1CoiThiFile && files.DsNhiemVu1CoiThiFile.length > 0) {
            console.log('Hook: dsNhiemVu1CoiThiFile');
            app.uploadDsNhiemVu1CoiThiFile(req, files.DsNhiemVu1CoiThiFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsNhiemVu1CoiThiFile', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsNhiemVu1CoiThiFile(req, fields, files, params, done), done, 'dsNhiemVu1CoiThi:write'));
};