module.exports = app => { //TODO: remove the comments belows
    // const menu = {
    //     parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
    //     menus: {
    //         8119: { title: 'KQ phiếu tín nhiệm', link: '/user/summary/ds-phieu-tin-nhiem' },
    //     },
    // };

    // app.permission.add(
    //     { name: 'kqPhieuTinNhiem:read', menu },
    //     { name: 'kqPhieuTinNhiem:write', menu }
    // );

    // app.get('/user/summary/ds-phieu-tin-nhiem', app.permission.check('kqPhieuTinNhiem:read'), app.templates.admin);
    // app.get('/user/summary/ds-phieu-tin-nhiem/upload', app.permission.check('kqPhieuTinNhiem:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-phieu-tin-nhiem/page/:pageNumber/:pageSize', app.permission.check('kqPhieuTinNhiem:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.kqPhieuTinNhiem.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds-phieu-tin-nhiem/multiple', app.permission.check('kqPhieuTinNhiem:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.kqPhieuTinNhiem.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-phieu-tin-nhiem', app.permission.check('kqPhieuTinNhiem:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.kqPhieuTinNhiem.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload dsPhieuTinNhiemImportData --------------------------------------------------------------------------------------------------------
    const dsPhieuTinNhiemImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsPhieuTinNhiemImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsPhieuTinNhiemImportData');
            app.uploadDsPhieuTinNhiemFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsPhieuTinNhiemImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsPhieuTinNhiemImportData(req, fields, files, params, done), done, 'kqPhieuTinNhiem:write'));
}
