module.exports = app => { //TODO: remove the comments belows
    // const menu = {
    //     parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
    //     menus: {
    //         8116: { title: 'KQ Đánh giá ngoại lệ', link: '/user/summary/kq-dg-ngoaile' },
    //     },
    // };

    // app.permission.add(
    //     { name: 'kqDanhGiaNgoaiLe:read', menu },
    //     { name: 'kqDanhGiaNgoaiLe:write', menu }
    // );

    app.get('/user/summary/kq-dg-ngoaile', app.permission.check('kqDanhGiaNgoaiLe:read'), app.templates.admin);
    app.get('/user/summary/kq-dg-ngoaile/upload', app.permission.check('kqDanhGiaNgoaiLe:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/kq-dg-ngoaile/page/:pageNumber/:pageSize', app.permission.check('kqDanhGiaNgoaiLe:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.kqDanhGiaNgoaiLe.getPage(pageNumber, pageSize, {}, (error, page) => {
            const response = {};
            if (error || page == null) {
                response.error = 'Danh sách không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                response.page = app.clone(page, { list });
            }
            res.send(response);
        });
    });

    app.post('/api/kq-dg-ngoaile', app.permission.check('kqDanhGiaNgoaiLe:write'), (req, res) => {
        const body = req.body.officer;
        app.model.kqDanhGiaNgoaiLe.get({ officerId: body.officerId, lastName: body.lastName, firstName: body.firstName, classification: body.classification, kI: body.kI }, (error, officer) => {
            if (error) {
                res.send({ error });
            } else if (officer) {
                res.send({ exist: true });
            } else {
                app.model.kqDanhGiaNgoaiLe.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.put('/api/kq-dg-ngoaile', app.permission.check('kqDanhGiaNgoaiLe:write'), (req, res) => {
        const officerId = req.body.officerId, changes = req.body.changes, filterChanges = {};
        if (changes && changes.officerId) filterChanges.officerId = changes.officerId;
        if (changes && changes.firstName) filterChanges.firstName = changes.firstName;
        if (changes && changes.lastName) filterChanges.lastName = changes.lastName;
        if (changes && changes.kI) filterChanges.kI = changes.kI;
        if (changes && changes.classification) filterChanges.classification = changes.classification;
        app.model.kqDanhGiaNgoaiLe.update({ officerId }, filterChanges, (error, item) => {
            res.send({ error, item });
        })
    });

    app.put('/api/kq-dg-ngoaile-multi', app.permission.check('kqDanhGiaNgoaiLe:write'), (req, res) => {
        const officers = req.body.officers, errorList = [];
        for (let i = 0; i <= officers.length; i++) {
            if (i == officers.length) {
                res.send({ error: errorList });
            } else {
                const currentOfficer = {
                    officerId: officers[i].officerId,
                    changes: {
                        officerId: officers[i].officerId,
                        firstName: officers[i].firstName,
                        lastName: officers[i].lastName,
                        kI: officers[i].kI,
                        classification: officers[0].classification
                    }
                };
                app.model.kqDanhGiaNgoaiLe.update({ officerId: currentOfficer.officerId }, currentOfficer.changes, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.delete('/api/kq-dg-ngoaile', app.permission.check('kqDanhGiaNgoaiLe:write'), (req, res) => app.model.kqDanhGiaNgoaiLe.delete(req.body._id, error => res.send({ error })));


    // Hook upload kq_dg_ngoaileImportData -----------------------------------------------------------------------------------------------------------
    const kq_dg_ngoaileImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'kq_dg_ngoaileImportData' && files.kq_dg_ngoaileImportFile && files.kq_dg_ngoaileImportFile.length > 0) {
            console.log('Hook: kq_dg_ngoaileImportData');
            app.uploadkq_dg_ngoaileFile(req, files.kq_dg_ngoaileImportFile[0].path, done);
        }
    };

    app.uploadHooks.add('kq_dg_ngoaileImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => kq_dg_ngoaileImportData(req, fields, files, params, done), done, 'kqDanhGiaNgoaiLe:write'));
}