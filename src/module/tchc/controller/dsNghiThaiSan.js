module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8115: { title: 'DS Nghỉ thai sản', link: '/user/summary/ds-nghi-thai-san' } },
    };
    app.permission.add(
        { name: 'dsNghiThaiSan:read', menu },
        { name: 'dsNghiThaiSan:write', menu },
    );

    app.get('/user/summary/ds-nghi-thai-san/upload', app.permission.check('dsNghiThaiSan:read'), app.templates.admin);
    app.get('/user/summary/ds-nghi-thai-san', app.permission.check('dsNghiThaiSan:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-nghi-khai-san/page/:pageNumber/:pageSize', app.permission.check('dsNghiThaiSan:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsNghiThaiSan.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-nghi-khai-san', app.permission.check('dsNghiThaiSan:write'), (req, res) => app.model.dsNghiThaiSan.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-nghi-khai-san', app.permission.check('dsNghiThaiSan:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsNghiThaiSan.get({ _id: body._id }, (error, item) => {
           if (error) {
               res.send({error});
           } else if (item) {
               res.send({ exist: true });
           } else {
               app.model.dsNghiThaiSan.create(body, (error, item) => res.send({ error, item }));
           }
        });
    });

    app.post('/api/ds-nghi-khai-san/multiple', app.permission.check('dsNghiThaiSan:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsNghiThaiSan.create(points[i], error => error && errorList.push(error));
            }
        }
    });

    app.put('/api/ds-nghi-khai-san', app.permission.check('dsNghiThaiSan:write'), (req, res) => {
        app.model.dsNghiThaiSan.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item }));
    });


    // Hook upload dsNghiThaiSanImportData ----------------------------------------------------------------------------------------------------------
    const dsNghiThaiSanImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsNghiThaiSanImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsNghiThaiSanImportData');
            app.uploadDsNghiThaiSanFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsNghiThaiSanImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsNghiThaiSanImportData(req, fields, files, params, done), done, 'dsNghiThaiSan:write'));
}
