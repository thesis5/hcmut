module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8105: { title: 'DS PGS/GS không hoàn thành nhiệm vụ', link: '/user/summary/pgsgs_khtnv' } },
    };
    app.permission.add(
        { name: 'dsPgsGsKhongHoanThanhNhiemVu:read', menu },
        { name: 'dsPgsGsKhongHoanThanhNhiemVu:write', menu },
    );

    app.get('/user/summary/pgsgs_khtnv', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:read'), app.templates.admin);
    app.get('/user/summary/pgsgs_khtnv/upload', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.post('/api/pgsgs-khtnv', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:read'), (req, res) => {
        const body = req.body.officer;
        app.model.dsPgsGsKhongHoanThanhNhiemVu.get({ shcc: body.shcc, year: body.year, note: body.note }, (error, officer) => {
            if (error) {
                res.send({ error });
            } else if (officer) {
                res.send({ exist: true });
            } else {
                app.model.dsPgsGsKhongHoanThanhNhiemVu.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.get('/api/pgsgs-khtnv/page/:pageNumber/:pageSize', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:write'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsPgsGsKhongHoanThanhNhiemVu.getPage(pageNumber, pageSize, condition, (error, page) => {
            const response = {};
            if (error || page == null) {
                response.error = 'Danh sách không sẵn sàng!';
            } else {
                response.page = page;
            }
            res.send(response);
        });
    });

    app.put('/api/pgsgs-khtnv', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:write'), (req, res) => {
        const shcc = req.body.shcc, changes = req.body.changes, filterChanges = {};
        if (changes && changes.shcc) filterChanges.shcc = changes.shcc;
        if (changes && changes.year) filterChanges.year = changes.year;
        if (changes && changes.note) filterChanges.note = changes.note;
        app.model.dsPgsGsKhongHoanThanhNhiemVu.update({ shcc }, filterChanges, (error, item) => {
            res.send({ error, item });
        })
    });

    app.put('/api/pgsgs-khtnv-multi', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:write'), (req, res) => {
        const officers = req.body.officers, errorList = [];
        for (let i = 0; i <= officers.length; i++) {
            if (i == officers.length) {
                res.send({ error: errorList });
            } else {
                const currentOfficer = {
                    shcc: officers[i].shcc,
                    changes: {
                        shcc: officers[i].shcc,
                        year: officers[i].year,
                        note: officers[i].note
                    }
                };
                app.model.dsPgsGsKhongHoanThanhNhiemVu.update({ shcc: currentOfficer.shcc }, currentOfficer.changes, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.delete('/api/pgsgs-khtnv', app.permission.check('dsPgsGsKhongHoanThanhNhiemVu:write'), (req, res) => app.model.dsPgsGsKhongHoanThanhNhiemVu.delete(req.body._id, error => res.send({ error })));


    // Hook upload PgsGsKhtnvImportData -------------------------------------------------------------------------------------------------------------
    const PgsGsKhtnvImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'PgsGsKhtnvImportData' && files.PgsGsKhtnvImportFile && files.PgsGsKhtnvImportFile.length > 0) {
            console.log('Hook: PgsGsKhtnvImportData');
            app.uploadPgsGsKhtnvFile(req, files.PgsGsKhtnvImportFile[0].path, done);
        }
    };

    app.uploadHooks.add('PgsGsKhtnvImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => PgsGsKhtnvImportData(req, fields, files, params, done), done, 'dsPgsGsKhongHoanThanhNhiemVu:write'));
}