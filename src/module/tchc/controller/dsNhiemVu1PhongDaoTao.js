module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8101: { title: 'DS Nhiệm vụ 1 - phòng Đào tạo', link: '/user/summary/ds-nhiem-vu-1-phong-dao-tao' } },
    };
    app.permission.add(
        { name: 'dsNhiemVu1PhongDaoTao:read', menu },
        { name: 'dsNhiemVu1PhongDaoTao:write', menu },
    );

    app.get('/user/summary/ds-nhiem-vu-1-phong-dao-tao', app.permission.check('dsNhiemVu1PhongDaoTao:read'), app.templates.admin);
    app.get('/user/summary/ds-nhiem-vu-1-phong-dao-tao/upload', app.permission.check('dsNhiemVu1PhongDaoTao:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-nhiem-vu-1-phong-dao-tao/page/:pageNumber/:pageSize', app.permission.check('dsNhiemVu1PhongDaoTao:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsNhiemVu1PhongDaoTao.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-nhiem-vu-1-phong-dao-tao', app.permission.check('dsNhiemVu1PhongDaoTao:write'), (req, res) => app.model.dsNhiemVu1PhongDaoTao.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-nhiem-vu-1-phong-dao-tao', app.permission.check('dsNhiemVu1PhongDaoTao:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsNhiemVu1PhongDaoTao.get({ _id: body._id }, (error, item) => {
           if (error) {
               res.send({error});
           } else if (item) {
               res.send({ exist: true });
           } else {
               app.model.dsNhiemVu1PhongDaoTao.create(body, (error, item) => res.send({ error, item }));
           }
        });
    });

    app.post('/api/ds-nhiem-vu-1-phong-dao-tao/multiple', app.permission.check('dsNhiemVu1PhongDaoTao:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dsNhiemVu1PhongDaoTao.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-nhiem-vu-1-phong-dao-tao', app.permission.check('dsNhiemVu1PhongDaoTao:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsNhiemVu1PhongDaoTao.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload dsNv1PdtImportData ---------------------------------------------------------------------------------------------------------------
    const dsNv1PdtImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsNv1PdtImportData' && files.PointFile && files.PointFile.length > 0) {
            app.uploadDsNv1PdtFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsNv1PdtImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsNv1PdtImportData(req, fields, files, params, done), done, 'dsNhiemVu1PhongDaoTao:write'));
};