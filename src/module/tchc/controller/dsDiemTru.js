module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8108: { title: 'DS Điểm trừ', link: '/user/summary/ds-diem-tru' } },
    };
    app.permission.add(
        { name: 'dsDiemTru:read', menu },
        { name: 'dsDiemTru:write', menu },
    );

    app.get('/user/summary/ds-diem-tru/upload', app.permission.check('dsDiemTru:read'), app.templates.admin);
    app.get('/user/summary/ds-diem-tru', app.permission.check('dsDiemTru:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-diem-tru/page/:pageNumber/:pageSize', app.permission.check('dsDiemTru:read'), (req, res) => {
        let pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        try {
            if (typeof condition == 'string') condition = JSON.parse(condition);
            app.model.dsDiemTru.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
        } catch (error) {
            res.send({ error });
        }
    });

    app.delete('/api/ds-diem-tru', app.permission.check('dsDiemTru:write'), (req, res) => app.model.dsDiemTru.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-diem-tru', app.permission.check('dsDiemTru:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsDiemTru.get({ _id: body._id }, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (item) {
                res.send({ exist: true });
            } else {
                app.model.dsDiemTru.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.post('/api/ds-diem-tru/multiple', app.permission.check('dsDiemTru:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsDiemTru.create(points[i], error => error && errorList.push(error));
            }
        }
    });

    app.put('/api/ds-diem-tru', app.permission.check('dsDiemTru:write'), (req, res) => {
        app.model.dsDiemTru.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item }));
    });


    // Hook upload dsDiemTruImportData --------------------------------------------------------------------------------------------------------------
    const dsDiemTruImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsDiemTruImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsDiemTruImportData');
            app.uploadDsDiemTruFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsDiemTruImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsDiemTruImportData(req, fields, files, params, done), done, 'dsDiemTru:write'));
}
