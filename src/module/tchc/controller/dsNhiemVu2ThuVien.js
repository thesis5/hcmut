module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: {
            8106: { title: 'DS Nhiệm vụ 2 - Thư viện', link: '/user/summary/ds_nhiem_vu_2_thu_vien' },
        },
    };

    app.permission.add(
        { name: 'dsNhiemVu2ThuVien:read', menu },
        { name: 'dsNhiemVu2ThuVien:write', menu }
    );

    app.get('/user/summary/ds_nhiem_vu_2_thu_vien', app.permission.check('dsNhiemVu2ThuVien:read'), app.templates.admin);
    app.get('/user/summary/ds_nhiem_vu_2_thu_vien/upload', app.permission.check('dsNhiemVu2ThuVien:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-nv-2/page/:pageNumber/:pageSize', app.permission.check('dsNhiemVu2ThuVien:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsNhiemVu2ThuVien.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-nv-2', app.permission.check('dsNhiemVu2ThuVien:write'), (req, res) => app.model.dsNhiemVu2ThuVien.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-nv-2', app.permission.check('dsNhiemVu2ThuVien:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsNhiemVu2ThuVien.get({ _id: body._id }, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (item) {
                res.send({ exist: true });
            } else {
                app.model.dsNhiemVu2ThuVien.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.post('/api/ds-nv-2/multiple', app.permission.check('dsNhiemVu2ThuVien:write'), (req, res) => {
        const points = req.body.points, errorList = [],
            solve = i => {
                if (i == points.length) {
                    res.send({ error: errorList });
                } else {
                    const currentPoint = points[i];
                    app.model.dsNhiemVu2ThuVien.create(currentPoint, (error, item) => {
                        if (error) errorList.push(error);
                        solve(i + 1);
                    });
                }
            };
        solve(0);

        // for (let i = 0; i <= points.length; i++) {
        //     if (i == points.length) {
        //         res.send({ error: errorList });
        //     } else {
        //         const currentPoint = points[i];
        //         app.model.dsNhiemVu2ThuVien.create(currentPoint, (error, item) => {
        //             console.log(i, item ? 'Ok' : 'Error')
        //             if (error) errorList.push(error);
        //         });
        //     }
        // }
    });

    app.put('/api/ds-nv-2', app.permission.check('dsNhiemVu2ThuVien:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsNhiemVu2ThuVien.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload dsnv2tvPointImportData -----------------------------------------------------------------------------------------------------------
    const dsnv2tvPointImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsnv2tvPointImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsnv2tvPointImportData');
            app.uploadDsNv2TvPointFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsnv2tvPointImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsnv2tvPointImportData(req, fields, files, params, done), done, 'dsNhiemVu2ThuVien:write'));
}
