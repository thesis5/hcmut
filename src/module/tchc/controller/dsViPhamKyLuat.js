module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8110: { title: 'DS Vi phạm kỷ luật', link: '/user/summary/ds_vi_pham_ky_luat' } },
    };
    app.permission.add(
        { name: 'dsViPhamKyLuat:read', menu },
        { name: 'dsViPhamKyLuat:write', menu },
    );

    app.get('/user/summary/ds_vi_pham_ky_luat', app.permission.check('dsViPhamKyLuat:read'), app.templates.admin);
    app.get('/user/summary/ds_vi_pham_ky_luat/upload', app.permission.check('dsViPhamKyLuat:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds_vi_pham_ky_luat/page/:pageNumber/:pageSize', app.permission.check('dsViPhamKyLuat:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsViPhamKyLuat.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds_vi_pham_ky_luat', app.permission.check('dsViPhamKyLuat:write'), (req, res) =>
        app.model.dsViPhamKyLuat.create(req.body.data, (error, item) => res.send({ error, item })));

    app.put('/api/ds_vi_pham_ky_luat', app.permission.check('dsViPhamKyLuat:write'), (req, res) =>
        app.model.dsViPhamKyLuat.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.put('/api/ds_vi_pham_ky_luat/multi_values', app.permission.check('dsViPhamKyLuat:write'), (req, res) => {
        const items = req.body.items, errorList = [];
        for (let i = 0; i <= items.length; i++) {
            if (i == items.length) {
                res.send({ error: errorList });
            } else {
                app.model.dsViPhamKyLuat.create(items[i], error => error && errorList.push(error));
            }
        }
    });

    app.delete('/api/ds_vi_pham_ky_luat', app.permission.check('dsViPhamKyLuat:write'), (req, res) =>
        app.model.dsViPhamKyLuat.delete(req.body._id, error => res.send({ error })));


    // Hook upload dsViPhamsKyLuat -------------------------------------------------------------------------------------------------------------------
    const uploadDsViPhamKyLuat = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] == 'DsViPhamKyLuatImportData' && files.DsViPhamKyLuatFile && files.DsViPhamKyLuatFile.length > 0) {
            console.log('Hook: DsViPhamKyLuatImportData => DsViPhamKyLuatFile upload');

            app.excel.readFile(files.DsViPhamKyLuatFile[0].path, workbook => {
                app.deleteFile(files.DsViPhamKyLuatFile[0].path);
                if (workbook) {
                    const worksheet = workbook.getWorksheet(1),
                        items = [],
                        totalRow = worksheet.lastRow.number;
                    let numberOfErrors = 0,
                        warnings = [];
                    const handleUpload = (index = 2) => {
                        const value = worksheet.getRow(index).values;
                        if (value.length == 0 || index == totalRow + 1) {
                            done({ items, warnings, numberOfErrors });
                        } else {
                            const nam = value[1], shcc = value[2], ghiChu = value[3];
                            app.model.staff.getByShcc(shcc, (error, staff) => {
                                if (error) {
                                    numberOfErrors++;
                                    handleUpload(index + 1);
                                } else if (!staff) {
                                    warnings.push(`shcc: ${shcc} không tồn tại!</br>`);
                                    handleUpload(index + 1);
                                } else {
                                    items.push({ nam, shcc, ghiChu });
                                    handleUpload(index + 1);
                                }
                            });
                        }
                    };
                    handleUpload();
                } else {
                    done({ error: 'Error' });
                }
            });
        }
    };
    app.uploadHooks.add('uploadDsViPhamKyLuat', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadDsViPhamKyLuat(req, fields, files, params, done), done, 'dsViPhamKyLuat:write'));
};
