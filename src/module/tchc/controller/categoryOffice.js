module.exports = app => {
    app.permission.add(
            {
                name: 'system:office',
                menu: {
                    parentMenu: { index: 3000, title: 'Danh mục', icon: 'fa-list' },
                    menus: { 3130: { title: 'Chức vụ', link: '/user/category-office' } }
                },
            }
    );

    app.get('/user/category-office', app.permission.check('system:office'), app.templates.admin);
    app.get('/user/category-office/item/:id', app.permission.check('system:office'), app.templates.admin);
    app.get('/user/category-office/upload-point', app.permission.check('system:office'), app.templates.admin);

    app.get('/api/category-office/page/:pageNumber/:pageSize', app.permission.check('system:office'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber), pageSize = parseInt(req.params.pageSize);
        app.model.categoryOffice.getPage(pageNumber, pageSize, {}, (error, page) => res.send({error, page}));
    });

    app.post('/api/category-office', app.permission.check('system:office'), (req, res) => {
        app.model.categoryOffice.create(req.body.data, (error, item) => {
            res.send({error, item});
        });
    });

    app.post('/api/category-office/multiple', app.permission.check('system:office'), (req, res) => {
        const categories = req.body.categories ? req.body.categories : [], errorList = [];
        for (let i = 0; i <= categories.length; i++) {
            if (i == categories.length) {
                res.send({error: errorList});
            } else {
                app.model.categoryOffice.create(categories[i], (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/category-office', app.permission.check('system:office'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.categoryOffice.update(_id, changes, (error, item) => res.send({error, item}));
    });

    const categoryOfficeImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'categoryOfficeImportData' && files.CategoryOfficeFile && files.CategoryOfficeFile.length > 0) {
            console.log('Hook: categoryOfficeImportData');
            app.uploadCategoryOfficeFile(req, files.CategoryOfficeFile[0].path, done);
        }
    };

    app.uploadHooks.add('categoryOfficeImportData', (req, fields, files, params, done) =>
            app.permission.has(req, () => categoryOfficeImportData(req, fields, files, params, done), done, 'system:office'));
};
