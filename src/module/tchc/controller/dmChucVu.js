module.exports = app => {
    const parentMenu = { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menu = {
            parentMenu,
            menus: { 8121: { title: 'Danh mục chức vụ', link: '/user/summary/dm-chucvu' } },
        };
    app.permission.add(
        { name: 'dmChucVu:read', menu },
        { name: 'dmChucVu:write', menu },
    );

    app.get('/user/summary/dm-chucvu', app.permission.check('dmChucVu:write'), app.templates.admin);
    app.get('/user/summary/dm-chucvu/upload', app.permission.check('dmChucVu:read'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/dm-chucvu/page/:pageNumber/:pageSize', app.permission.check('dmChucVu:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dmChucVu.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/dm-chucvu', app.permission.check('dmChucVu:write'), (req, res) => app.model.dmChucVu.delete(req.body._id, error => res.send({ error })));

    app.post('/api/dm-chucvu', app.permission.check('dmChucVu:write'), (req, res) =>
        app.model.dmChucVu.create(req.body.data, (error, item) => res.send({ error, item })));

    app.post('/api/dm-chucvu/multiple', app.permission.check('dmChucVu:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dmChucVu.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/dm-chucvu', app.permission.check('dmChucVu:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dmChucVu.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });

    const dmChucVuImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dmChucVuImportData' && files.PointFile && files.PointFile.length > 0) {
            app.uploadDmChucVuFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dmChucVuImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dmChucVuImportData(req, fields, files, params, done), done, 'dmChucVu:write'));

};
