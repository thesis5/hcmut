module.exports = app => {
    const parentMenu = { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menu406 = {
            parentMenu,
            menus: { 8117: { title: 'DS Cán bộ công đoàn', link: '/user/summary/ds-canbo-congdoan' } },
        };
    app.permission.add(
        { name: 'dsCanBoCongDoan:read', menu: menu406 },
        { name: 'dsCanBoCongDoan:write', menu: menu406 },
    );

    app.get('/user/summary/ds-canbo-congdoan', app.permission.check('dsCanBoCongDoan:write'), app.templates.admin);
    app.get('/user/summary/ds-canbo-congdoan/upload', app.permission.check('dsCanBoCongDoan:read'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-canbo-congdoan/page/:pageNumber/:pageSize', app.permission.check('dsCanBoCongDoan:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsCanboCongdoan.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-canbo-congdoan', app.permission.check('dsCanBoCongDoan:write'), (req, res) => app.model.dsCanboCongdoan.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-canbo-congdoan', app.permission.check('dsCanBoCongDoan:write'), (req, res) =>
        app.model.dsCanboCongdoan.create(req.body.data, (error, item) => res.send({ error, item })));

    app.post('/api/ds-canbo-congdoan/multiple', app.permission.check('dsCanBoCongDoan:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dsCanboCongdoan.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-canbo-congdoan', app.permission.check('dsCanBoCongDoan:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsCanboCongdoan.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });

    const dsCanboCongdoanImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsCanboCongdoanImportData' && files.PointFile && files.PointFile.length > 0) {
            app.uploadDsCanboCongdoanFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsCanboCongdoanImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsCanboCongdoanImportData(req, fields, files, params, done), done, 'dsCanBoCongDoan:write'));
};