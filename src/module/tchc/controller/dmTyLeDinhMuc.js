module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8120: { title: 'Danh mục tỷ lệ định mức', link: '/user/summary/danh-muc-ty-le-dinh-muc' } },
    };
    app.permission.add(
        { name: 'dmTyLeDinhMuc:read', menu },
        { name: 'dmTyLeDinhMuc:write', menu },
    );

    app.get('/user/summary/danh-muc-ty-le-dinh-muc/upload', app.permission.check('dmTyLeDinhMuc:read'), app.templates.admin);
    app.get('/user/summary/danh-muc-ty-le-dinh-muc', app.permission.check('dmTyLeDinhMuc:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/danh-muc-ty-le-dinh-muc/page/:pageNumber/:pageSize', app.permission.check('dmTyLeDinhMuc:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dmTyLeDinhMuc.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/danh-muc-ty-le-dinh-muc', app.permission.check('dmTyLeDinhMuc:write'), (req, res) => app.model.dmTyLeDinhMuc.delete(req.body._id, error => res.send({ error })));

    app.post('/api/danh-muc-ty-le-dinh-muc', app.permission.check('dmTyLeDinhMuc:write'), (req, res) => {
        const body = req.body.points;
        app.model.dmTyLeDinhMuc.get({ _id: body._id }, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (item) {
                res.send({ exist: true });
            } else {
                app.model.dmTyLeDinhMuc.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.post('/api/danh-muc-ty-le-dinh-muc/multiple', app.permission.check('dmTyLeDinhMuc:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                app.model.dmTyLeDinhMuc.create(points[i], error => error && errorList.push(error));
            }
        }
    });

    app.put('/api/danh-muc-ty-le-dinh-muc', app.permission.check('dmTyLeDinhMuc:write'), (req, res) =>
        app.model.dmTyLeDinhMuc.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));


    // Hook upload danhMucTyLeDinhMucImportData -----------------------------------------------------------------------------------------------------------
    const danhMucTyLeDinhMucImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'danhMucTyLeDinhMucImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: danhMucTyLeDinhMucImportData');
            app.uploadDanhMucTyLeDinhMucFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('danhMucTyLeDinhMucImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => danhMucTyLeDinhMucImportData(req, fields, files, params, done), done, 'dmTyLeDinhMuc:write'));
}
