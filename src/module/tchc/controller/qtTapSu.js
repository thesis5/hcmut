module.exports = app => {
    // const menu = {
    //     parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
    //     menus: { 8537: { title: 'QT Tập sự', link: '/user/summary/qt-tap-su' } },
    // };
    // app.permission.add(
    //     { name: 'qtTapSu:read', menu },
    //     { name: 'qtTapSu:write', menu }
    // );

    // app.get('/user/summary/qt-tap-su', app.permission.check('qtTapSu:read'), app.templates.admin);
    // app.get('/user/summary/qt-tap-su/upload', app.permission.check('qtTapSu:write:'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/qt-tap-su/page/:pageNumber/:pageSize', app.permission.check('qtTapSu:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.qtTapSu.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/qt-tap-su/multiple', app.permission.check('qtTapSu:write:'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.qtTapSu.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/qt-tap-su', app.permission.check('qtTapSu:write:'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.qtTapSu.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload qtTapSuImportData ----------------------------------------------------------------------------------------------------------------
    const qtTapSuImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'qtTapSuImportData' && files.PointFile && files.PointFile.length > 0) {
            app.uploadQtTapSuFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('qtTapSuImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => qtTapSuImportData(req, fields, files, params, done), done, 'qtTapSu:write:'));

};
