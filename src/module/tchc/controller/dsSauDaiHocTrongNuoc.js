module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8116: { title: 'DS Sau đại học trong nước', link: '/user/summary/ds-sdh-trong-nuoc' } },
    };
    app.permission.add(
        { name: 'dsSdhTrongNuoc:read', menu },
        { name: 'dsSdhTrongNuoc:write', menu },
    );

    app.get('/user/summary/ds-sdh-trong-nuoc', app.permission.check('dsSdhTrongNuoc:read'), app.templates.admin);
    app.get('/user/summary/ds-sdh-trong-nuoc/upload', app.permission.check('dsSdhTrongNuoc:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------

    app.get('/api/ds-sdh-trong-nuoc/page/:pageNumber/:pageSize', app.permission.check('dsSdhTrongNuoc:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsSauDaiHocTrongNuoc.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-sdh-trong-nuoc', app.permission.check('dsSdhTrongNuoc:write'), (req, res) => app.model.dsSauDaiHocTrongNuoc.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-sdh-trong-nuoc', app.permission.check('dsSdhTrongNuoc:write'), (req, res) =>
        app.model.dsSauDaiHocTrongNuoc.create(req.body.data, (error, item) => res.send({ error, item })));

    app.post('/api/ds-sdh-trong-nuoc/multiple', app.permission.check('dsSdhTrongNuoc:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dsSauDaiHocTrongNuoc.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-sdh-trong-nuoc', app.permission.check('dsSdhTrongNuoc:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsSauDaiHocTrongNuoc.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });
    // Hook upload dsTapSuImportData ----------------------------------------------------------------------------------------------------------------
    const dsSdhTrongNuocImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsSdhTrongNuocImportData' && files.PointFile && files.PointFile.length > 0) {
            app.uploadDsSdhTrongNuocFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsSdhTrongNuocImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsSdhTrongNuocImportData(req, fields, files, params, done), done, 'dsSdhTrongNuoc:write'));
};