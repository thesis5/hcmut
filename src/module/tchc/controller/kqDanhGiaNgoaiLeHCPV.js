module.exports = app => { //TODO: remove the comments belows
    // const menu = {
    //     parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
    //     menus: {
    //         8118: { title: 'KQ Đánh giá ngoại lệ HC-PV', link: '/user/summary/ds-kqdg-ngoai-le-hcpv' },
    //     },
    // };

    // app.permission.add(
    //     { name: 'kqDanhGiaNgoaiLeHCPV:read', menu },
    //     { name: 'kqDanhGiaNgoaiLeHCPV:write', menu }
    // );

    app.get('/user/summary/ds-kqdg-ngoai-le-hcpv', app.permission.check('kqDanhGiaNgoaiLeHCPV:read'), app.templates.admin);
    app.get('/user/summary/ds-kqdg-ngoai-le-hcpv/upload', app.permission.check('kqDanhGiaNgoaiLeHCPV:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-kqdg-ngoai-le-hcpv/page/:pageNumber/:pageSize', app.permission.check('kqDanhGiaNgoaiLeHCPV:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.kqDanhGiaNgoaiLeHCPV.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds-kqdg-ngoai-le-hcpv/multiple', app.permission.check('kqDanhGiaNgoaiLeHCPV:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.kqDanhGiaNgoaiLeHCPV.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-kqdg-ngoai-le-hcpv', app.permission.check('kqDanhGiaNgoaiLeHCPV:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.kqDanhGiaNgoaiLeHCPV.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });


    // Hook upload dsKQDGNgoaiLeHCPVImportData ------------------------------------------------------------------------------------------------------
    const dsKQDGNgoaiLeHCPVImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsKQDGNgoaiLeHCPVImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsKQDGNgoaiLeHCPVImportData');
            app.uploadDsKQDGNgoaiLeHCPVFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsKQDGNgoaiLeHCPVImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsKQDGNgoaiLeHCPVImportData(req, fields, files, params, done), done, 'kqDanhGiaNgoaiLeHCPV:write'));
}