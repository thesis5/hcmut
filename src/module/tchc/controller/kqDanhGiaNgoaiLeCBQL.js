module.exports = app => { //TODO: remove the comments belows
    // const menu = {
    //     parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
    //     menus: {
    //         8117: { title: 'KQ Đánh giá ngoại lệ cán bộ quản lý', link: '/user/summary/ds-kqdg-ngoai-le-cbql' },
    //     },
    // };

    // app.permission.add(
    //     { name: 'kqDanhGiaNgoaiLeCBQL:read', menu },
    //     { name: 'kqDanhGiaNgoaiLeCBQL:write', menu }
    // );

    app.get('/user/summary/ds-kqdg-ngoai-le-cbql', app.permission.check('kqDanhGiaNgoaiLeCBQL:read'), app.templates.admin);
    app.get('/user/summary/ds-kqdg-ngoai-le-cbql/upload', app.permission.check('kqDanhGiaNgoaiLeCBQL:write'), app.templates.admin);

    // APIs ------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-kqdg-ngoai-le-cbql/page/:pageNumber/:pageSize', app.permission.check('kqDanhGiaNgoaiLeCBQL:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.kqDanhGiaNgoaiLeCBQL.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.post('/api/ds-kqdg-ngoai-le-cbql/multiple', app.permission.check('kqDanhGiaNgoaiLeCBQL:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.kqDanhGiaNgoaiLeCBQL.create(currentPoint, error => error && errorList.push(error));
            }
        }
    });

    app.put('/api/ds-kqdg-ngoai-le-cbql', app.permission.check('kqDanhGiaNgoaiLeCBQL:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.kqDanhGiaNgoaiLeCBQL.update(_id, changes, (error, item) => res.send({ error, item }));
    });


    // Hook upload dsKQDGNgoaiLeCBQLImportData ------------------------------------------------------------------------------------------------------
    const dsKQDGNgoaiLeCBQLImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsKQDGNgoaiLeCBQLImportData' && files.PointFile && files.PointFile.length > 0) {
            console.log('Hook: dsKQDGNgoaiLeCBQLImportData');
            app.uploadDsKQDGNgoaiLeCBQLFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsKQDGNgoaiLeCBQLImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsKQDGNgoaiLeCBQLImportData(req, fields, files, params, done), done, 'kqDanhGiaNgoaiLeCBQL:write'));
}
