module.exports = app => {
    const menu = {
        parentMenu: { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menus: { 8103: { title: 'DS Nhiệm vụ 1 - phòng Đào tạo SĐH', link: '/user/summary/ds-nhiem-vu-1-phong-dao-tao-sdh' } },
    };
    app.permission.add(
        { name: 'dsNhiemVu1PhongDaoTaoSDH:read', menu },
        { name: 'dsNhiemVu1PhongDaoTaoSDH:write', menu },
    );

    app.get('/user/summary/ds-nhiem-vu-1-phong-dao-tao-sdh', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:read'), app.templates.admin);
    app.get('/user/summary/ds-nhiem-vu-1-phong-dao-tao-sdh/upload', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:write'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/ds-nhiem-vu-1-phong-dao-tao-sdh/page/:pageNumber/:pageSize', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {};
        app.model.dsNhiemVu1PhongSauDaiHoc.getPage(pageNumber, pageSize, condition, (error, page) => res.send({ error, page }));
    });

    app.delete('/api/ds-nhiem-vu-1-phong-dao-tao-sdh', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:write'), (req, res) => app.model.dsNhiemVu1PhongSauDaiHoc.delete(req.body._id, error => res.send({ error })));

    app.post('/api/ds-nhiem-vu-1-phong-dao-tao-sdh', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:write'), (req, res) => {
        const body = req.body.points;
        app.model.dsNhiemVu1PhongSauDaiHoc.get({ _id: body._id }, (error, item) => {
            if (error) {
                res.send({ error });
            } else if (item) {
                res.send({ exist: true });
            } else {
                app.model.dsNhiemVu1PhongSauDaiHoc.create(body, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.post('/api/ds-nhiem-vu-1-phong-dao-tao-sdh/multiple', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:write'), (req, res) => {
        const points = req.body.points, errorList = [];
        for (let i = 0; i <= points.length; i++) {
            console.log(i);
            if (i == points.length) {
                res.send({ error: errorList });
            } else {
                const currentPoint = points[i];
                app.model.dsNhiemVu1PhongSauDaiHoc.create(currentPoint, (error, _) => {
                    if (error) errorList.push(error);
                });
            }
        }
    });

    app.put('/api/ds-nhiem-vu-1-phong-dao-tao-sdh', app.permission.check('dsNhiemVu1PhongDaoTaoSDH:write'), (req, res) => {
        const changes = req.body.changes, _id = req.body._id;
        app.model.dsNhiemVu1PhongSauDaiHoc.update(_id, changes, (error, item) => {
            res.send({ error, item });
        });
    });

    // Hook upload dsNv1PsdhImportData --------------------------------------------------------------------------------------------------------------
    const dsNv1PsdhImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'dsNv1PsdhImportData' && files.PointFile && files.PointFile.length > 0) {
            app.uploadDsNv1PsdhFile(req, files.PointFile[0].path, done);
        }
    };

    app.uploadHooks.add('dsNv1PsdhImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => dsNv1PsdhImportData(req, fields, files, params, done), done, 'dsNhiemVu1PhongDaoTaoSDH:write'));
};