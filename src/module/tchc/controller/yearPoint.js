module.exports = app => {
    const parentMenu = { index: 8000, title: 'Tổng kết', icon: 'fa-pagelines' },
        menu = {
            parentMenu,
            menus: { 8100: { title: 'Điểm tổng kết năm', link: '/user/summary/year' } },
        };
    app.permission.add(
        { name: 'point:read', menu },
        { name: 'point:write', menu },
    );
    app.get('/user/summary/year', app.permission.check('point:read'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/year-point/page/:pageNumber/:pageSize', app.permission.check('point:read'), (req, res) => {
        let pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {},
            pageCondition = {};
        try {
            if (condition.searchText) {
                const value = { $regex: `.*${condition.searchText}.*`, $options: 'i' };
                pageCondition['$or'] = [
                    { shcc: value },
                    { fullname: value },
                    { email: value },
                ];
            }
            app.model.yearPoint.getPage(pageNumber, pageSize, pageCondition, (error, page) => res.send({ error, page }));
        } catch (error) {
            res.send({ error });
        }
    });

    app.put('/api/year-point', app.permission.check('point:write'), (req, res) => {
        app.model.yearPoint.update({ _id: req.body._id }, req.body.changes, (error, item) => {
            res.send({ error, item });
        });
    });

    app.put('/api/year-point/calculate', app.permission.check('point:write'), (req, res) => app.model.user.getAll({ isStaff: true }, (error, users) => {
        if (error || users == null) {
            res.send({ error: 'Calculate has errors!' });
        } else {
            console.log('Start...');
            let dinhMuc = {}, cbPgsGs = [], sdhTrongNuoc = [],
                chucVu = {}, leadershipCodes = {},
                cb06 = {}, cb06Shcc = [],
                cb712 = {}, cb712Shcc = [],
                cbGiamDinhMuc = {}, cbGiamDinhMucShcc = [],
                cbNghiThaiSan = {}, cbNghiThaiSanShcc = [],
                cbTapSu = {}, cbTapSuShcc = [],
                dsResultBySystem = {}, dsNote = {}, dsKiFinal = {};
            const solve = (index = 0) => {
                if (index < users.length) {
                    const user = users[index];
                    if (user.organizationId) {
                        app.model.staff.get({ shcc: user.organizationId }).then(staff => {
                            if (staff) {
                                const changes = {
                                    shcc: user.organizationId,
                                    fullname: user.lastname + ' ' + user.firstname,
                                    email: user.email,
                                    facultyCode: staff.MS_KHOA,
                                    facultyName: staff.TEN_KHOA,
                                    departmentCode: staff.MS_BM,
                                    departmentName: staff.TEN_BM,
                                    level: staff.CHUCDANH_TRINHDO,
                                    isException: false,
                                    leadershipCoefficient: 0,
                                    professionalCoefficient: 1,
                                };

                                // Show current shcc
                                if (app.isDebug) {
                                    // process.stdout.clearLine();
                                    // process.stdout.cursorTo(0);
                                    // process.stdout.write(`Calculate: ${index} => ${user.organizationId}   `);
                                }

                                let nhiemVu1PhongDaoTao = null, nhiemVu2PhongKhoaHocCongNghe = null, diemThuong = null, diemTru = null;
                                app.model.dsNhiemVu1PhongDaoTao.get({ shcc: user.organizationId, nam: 2019 }).then(item => {
                                    nhiemVu1PhongDaoTao = item;
                                    return app.model.dsNhiemVu1PhongSauDaiHoc.get({ shcc: user.organizationId, nam: 2019 });
                                }).then(nhiemVu1PhongSauDaiHoc => {
                                    changes.professionalPoint1 = (nhiemVu1PhongDaoTao && nhiemVu1PhongDaoTao.tonggiochuan ? nhiemVu1PhongDaoTao.tonggiochuan : 0) + (nhiemVu1PhongSauDaiHoc && nhiemVu1PhongSauDaiHoc.TONG_ALL ? nhiemVu1PhongSauDaiHoc.TONG_ALL : 0);
                                    return app.model.dsNhiemVu2PhongKhoaHocCongNghe.get({ shcc: user.organizationId, nam: 2019 });
                                }).then(item => {
                                    nhiemVu2PhongKhoaHocCongNghe = item;
                                    return app.model.dsNhiemVu2ThuVien.get({ shcc: user.organizationId, nam: 2019 });
                                }).then(nhiemVu2ThuVien => {
                                    changes.professionalPoint2 = (nhiemVu2PhongKhoaHocCongNghe && nhiemVu2PhongKhoaHocCongNghe.totalHours ? nhiemVu2PhongKhoaHocCongNghe.totalHours : 0) + (nhiemVu2ThuVien && nhiemVu2ThuVien.acceptedHour ? nhiemVu2ThuVien.acceptedHour : 0);
                                    return app.model.dsDiemThuong.get({ shcc: user.organizationId, nam: 2019 });
                                }).then(item => {
                                    diemThuong = item;
                                    return app.model.dsDiemTru.get({ shcc: user.organizationId, nam: 2019 });
                                }).then(item => {
                                    diemTru = item;
                                    return app.model.yearPoint.get({ shcc: user.organizationId, year: 2019 });
                                }).then(yearPoint => {
                                    changes.rewardPoint = diemThuong && diemThuong.diemThuong && !isNaN(diemThuong.diemThuong) ? diemThuong.diemThuong : 0;
                                    changes.penaltyPoint = diemTru && diemTru.diemTru && !isNaN(diemTru.diemTru) ? diemTru.diemTru : 0;

                                    if (yearPoint) {
                                        let hasPoint = false;
                                        let quota = dinhMuc[staff.CHUCDANH_TRINHDO];

                                        // Tính hệ số thu nhập tăng thêm dựa trên CHUCDANH_TRINHDO
                                        changes.additionalIncomeCoefficient = quota && quota.kI ? quota.kI : 0;

                                        if (yearPoint.active == null || yearPoint.active == false) { // Nhóm không xét (active = false)
                                            cbNghiThaiSanShcc.removeByValue(user.organizationId);
                                            cb06Shcc.removeByValue(user.organizationId);
                                            cb712Shcc.removeByValue(user.organizationId);

                                            // Lãnh đạo
                                            if (leadershipCodes[user.organizationId]) {
                                                const codes = leadershipCodes[user.organizationId];
                                                changes.currentLeadershipName = (codes.map(code => {
                                                    const chucVuLanhDao = chucVu[code];
                                                    changes.additionalIncomeCoefficient = Math.max(changes.additionalIncomeCoefficient, chucVuLanhDao.kI)
                                                    return chucVuLanhDao.name + ` (${code})`;
                                                })).toString();
                                                changes.note = 'Chưa có điểm đánh giá';
                                            } else {
                                                changes.note = 'Không đánh giá';
                                            }

                                            // Giảm định mức
                                            if (cbGiamDinhMuc[user.organizationId]) {
                                                cbGiamDinhMucShcc.removeByValue(user.organizationId);
                                                const cb = cbGiamDinhMuc[user.organizationId];
                                                changes.leadershipCode = cb.leadershipCode;
                                                changes.leadershipName = cb.leadershipName;
                                                changes.leadershipCoefficient = chucVu[changes.leadershipCode].leadershipCoefficient;

                                                changes.ratedRatio1 = cb.quotaPoint1;
                                                changes.ratedRatio2 = cb.quotaPoint2;
                                                changes.ratedRatio3 = cb.quotaPoint3;
                                                changes.note = (changes.note ? changes.note : '') + `\nGiảm định mức: ${cb.quotaPoint1}, ${cb.quotaPoint2}, ${cb.quotaPoint3}`;
                                            }
                                        } else if (yearPoint.round1Point || yearPoint.round2Point) { // Nhóm 2
                                            changes.professionalPoint = ((yearPoint.round1Point ? yearPoint.round1Point : 0) + (yearPoint.round2Point ? yearPoint.round2Point : 0)) / 2;

                                            // Giảm định mức
                                            if (cbGiamDinhMuc[user.organizationId]) {
                                                cbGiamDinhMucShcc.removeByValue(user.organizationId);
                                                const cb = cbGiamDinhMuc[user.organizationId];
                                                changes.leadershipCode = cb.leadershipCode;
                                                changes.leadershipName = cb.leadershipName;
                                                if (changes.leadershipCode) changes.leadershipCoefficient = chucVu[changes.leadershipCode].leadershipCoefficient;

                                                changes.ratedRatio1 = cb.quotaPoint1;
                                                changes.ratedRatio2 = cb.quotaPoint2;
                                                changes.ratedRatio3 = cb.quotaPoint3;
                                                changes.note = (changes.note ? changes.note : '') + `\nGiảm định mức: ${cb.quotaPoint1}, ${cb.quotaPoint2}, ${cb.quotaPoint3}`;
                                            }

                                            // Nghỉ thai sản
                                            if (cbNghiThaiSan[user.organizationId]) {
                                                cbNghiThaiSanShcc.removeByValue(user.organizationId);
                                                const cb = cbNghiThaiSan[user.organizationId];
                                                if (cb.month == 6) {
                                                    hasPoint = true;
                                                    changes.point = 80;
                                                }
                                                changes.note = (changes.note ? changes.note : '') + `\nViên chức nghỉ thai sản ${12 - cb.month} tháng.`;
                                            }

                                            // Cán bộ làm 0-6 tháng
                                            if (cb06[user.organizationId]) {
                                                cb06Shcc.removeByValue(user.organizationId);
                                                const cb = cb06[user.organizationId];
                                                hasPoint = true;
                                                changes.point = 65;
                                                changes.note = (changes.note ? changes.note : '') + `\nViên chức làm việc ${cb.month} tháng. Lý do: ${cb.note}.`;
                                            }

                                            // Cán bộ làm 7-12 tháng
                                            if (cb712[user.organizationId]) {
                                                cb712Shcc.removeByValue(user.organizationId);
                                                const cb = cb712[user.organizationId];
                                                changes.note = (changes.note ? changes.note : '') + `\nViên chức làm việc ${cb.month} tháng. Lý do: ${cb.note}.`;
                                            }

                                            // Lãnh đạo
                                            if (leadershipCodes[user.organizationId]) {
                                                const codes = leadershipCodes[user.organizationId];
                                                changes.currentLeadershipName = (codes.map(code => {
                                                    const chucVuLanhDao = chucVu[code];
                                                    changes.additionalIncomeCoefficient = Math.max(changes.additionalIncomeCoefficient, chucVuLanhDao.kI)
                                                    return chucVuLanhDao.name + ` (${code})`;
                                                })).toString();
                                            }
                                        } else if (cbTapSu[user.organizationId]) { // Tập sự
                                            cbTapSuShcc.removeByValue(user.organizationId);
                                            const ts = cbTapSu[user.organizationId];
                                            changes.quotaPoint1 = ts.quotaPoint1;
                                            changes.quotaPoint2 = ts.quotaPoint2;
                                            changes.quotaPoint3 = ts.quotaPoint3;
                                            changes.note = (changes.note ? changes.note : '') + `\nTập sự: ${changes.quotaPoint1},  ${changes.quotaPoint2},  ${changes.quotaPoint3}`;

                                            // Tỷ lệ định mức
                                            changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = 100;

                                            // Sau đại học trong nước
                                            if (sdhTrongNuoc.includes(user.organizationId)) {
                                                sdhTrongNuoc.removeByValue(user.organizationId);
                                                changes.ratedRatio1 = changes.ratedRatio3 = 50;
                                                changes.ratedRatio2 = 100;
                                                changes.note += `\nSau đại học trong nước (giảm 50% cho NV1 và NV3).`;
                                            }

                                            // Cán bộ làm 0-6 tháng
                                            if (cb06[user.organizationId]) {
                                                cb06Shcc.removeByValue(user.organizationId);
                                                const cb = cb06[user.organizationId],
                                                    percentMonth = cb.month / 12 * 100; // Tính theo số tháng / 12
                                                changes.point = 65;
                                                if (percentMonth < changes.ratedRatio1)
                                                    changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = percentMonth;
                                                changes.note += `\nViên chức làm việc ${cb.month} tháng. Phần trăm định mức: ${(changes.ratedRatio1).toFixed(2)}. Lý do: ${cb.note}.`;
                                            }

                                            // Cán bộ làm 7-12 tháng
                                            if (cb712[user.organizationId]) {
                                                cb712Shcc.removeByValue(user.organizationId);
                                                const cb = cb712[user.organizationId],
                                                    percentMonth = cb.month / 12 * 100; // Tính theo số tháng / 12
                                                if (percentMonth < changes.ratedRatio1)
                                                    changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = percentMonth;
                                                changes.note += `\nViên chức làm việc ${cb.month} tháng. Phần trăm định mức: ${(changes.ratedRatio1).toFixed(2)}. Lý do: ${cb.note}.`;
                                            }

                                            if (changes.ratedRatio1 != 1) {
                                                changes.quotaPoint1 *= changes.ratedRatio1 / 100;
                                                changes.quotaPoint2 *= changes.ratedRatio2 / 100;
                                                changes.quotaPoint3 *= changes.ratedRatio3 / 100;
                                                changes.note += `\nĐịnh mức được tính: ${changes.quotaPoint1.toFixed(2)}, ${changes.quotaPoint2.toFixed(2)}, ${changes.quotaPoint3.toFixed(2)}`;
                                            }

                                            // Tính điểm cuối cùng
                                            let professionalPoint = 0, divisor = 0;
                                            if (changes.quotaPoint1) {
                                                professionalPoint += Math.max(0, Math.min(100, 100 * yearPoint.professionalPoint1 / changes.quotaPoint1));
                                                divisor++;
                                            }
                                            if (changes.quotaPoint2) {
                                                professionalPoint += Math.max(0, Math.min(100, 100 * yearPoint.professionalPoint2 / changes.quotaPoint2));
                                                divisor++;
                                            }
                                            if (changes.quotaPoint3) {
                                                professionalPoint += Math.max(0, Math.min(100, 100 * yearPoint.professionalPoint3 / changes.quotaPoint3));
                                                divisor++;
                                            }
                                            if (!isNaN(professionalPoint) && divisor) {
                                                changes.professionalPoint = professionalPoint / divisor;

                                                //Chuyển điểm NV1 và NV2
                                                let percent1 = yearPoint.professionalPoint1 / changes.quotaPoint1 * 100,
                                                    percent2 = yearPoint.professionalPoint2 / changes.quotaPoint2 * 100;
                                                changes.transformationType = '';
                                                if (divisor == 3 && percent1 >= 50 && percent2 >= 50) {
                                                    const transformedPoint = (Math.min(100, 100 * (yearPoint.professionalPoint1 * 2 + yearPoint.professionalPoint2) / (changes.quotaPoint1 * 2 + changes.quotaPoint2)) * 2 + Math.min(100, 100 * yearPoint.professionalPoint3 / changes.quotaPoint3)) / 3;
                                                    if (changes.professionalPoint < transformedPoint) {
                                                        changes.transformationType = (percent1 >= 80 && percent2 >= 80) ? 'c' : 'b';
                                                        changes.note += `\nChuyển điểm NV1 và NV2 (${changes.transformationType == 'c' ? 'NV có điểm từ 80%' : 'NV có điểm từ 50% đến dưới 80%'}): điểm từ ${changes.professionalPoint.toFixed(2)} thành ${transformedPoint.toFixed(2)}.`;
                                                        changes.professionalPoint = transformedPoint;
                                                    }
                                                }
                                            } else {
                                                console.log('Error: Tính điểm cuối cùng', user.organizationId, changes.quotaPoint1, changes.quotaPoint2, changes.quotaPoint3);
                                            }
                                        } else { // Nhóm 1
                                            if (quota) {
                                                changes.quotaName = staff.CHUCDANH_TRINHDO;
                                                changes.quotaPoint1 = quota.quotaPoint1;
                                                changes.quotaPoint2 = quota.quotaPoint2;
                                                changes.quotaPoint3 = quota.quotaPoint3;
                                                changes.note = (yearPoint.note3 ? yearPoint.note3 : '') + `\nĐịnh mức theo chức danh ${changes.quotaName}: ${changes.quotaPoint1},  ${changes.quotaPoint2},  ${changes.quotaPoint3}`;

                                                // Tỷ lệ định mức
                                                changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = 100;

                                                // Sau đại học trong nước
                                                if (sdhTrongNuoc.includes(user.organizationId)) {
                                                    sdhTrongNuoc.removeByValue(user.organizationId);
                                                    changes.ratedRatio1 = changes.ratedRatio3 = 50;
                                                    changes.ratedRatio2 = 100;
                                                    changes.note += `\nSau đại học trong nước (giảm 50% cho NV1 và NV3).`;
                                                }

                                                // Nghỉ thai sản
                                                if (cbNghiThaiSan[user.organizationId]) {
                                                    cbNghiThaiSanShcc.removeByValue(user.organizationId);
                                                    const cb = cbNghiThaiSan[user.organizationId],
                                                        percentMonth = cb.month / 12 * 100; // Tính theo số tháng / 12
                                                    if (cb.month == 6) changes.point = 80;
                                                    if (percentMonth < changes.ratedRatio1) {
                                                        changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = percentMonth;
                                                    }
                                                    changes.note += `\nViên chức nghỉ thai sản ${12 - cb.month} tháng. Phần trăm định mức: ${(percentMonth).toFixed(2)}.`;
                                                }

                                                // Cán bộ làm 0-6 tháng
                                                if (cb06[user.organizationId]) {
                                                    cb06Shcc.removeByValue(user.organizationId);
                                                    const cb = cb06[user.organizationId],
                                                        percentMonth = cb.month / 12 * 100; // Tính theo số tháng / 12
                                                    if (percentMonth < changes.ratedRatio1)
                                                        changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = percentMonth;
                                                    changes.point = 65;
                                                    changes.note += `\nViên chức làm việc ${cb.month} tháng. Phần trăm định mức: ${(percentMonth).toFixed(2)}. Lý do: ${cb.note}.`;
                                                }

                                                // Cán bộ làm 7-12 tháng
                                                if (cb712[user.organizationId]) {
                                                    cb712Shcc.removeByValue(user.organizationId);
                                                    const cb = cb712[user.organizationId],
                                                        percentMonth = cb.month / 12 * 100; // Tính theo số tháng / 12
                                                    if (percentMonth < changes.ratedRatio1)
                                                        changes.ratedRatio1 = changes.ratedRatio2 = changes.ratedRatio3 = percentMonth;
                                                    changes.note += `\nViên chức làm việc ${cb.month} tháng. Phần trăm định mức: ${(percentMonth).toFixed(2)}. Lý do: ${cb.note}.`;
                                                }

                                                // Lãnh đạo
                                                if (leadershipCodes[user.organizationId]) {
                                                    if ((yearPoint.professionalPoint3 == undefined || yearPoint.professionalPoint3 == null || yearPoint.professionalPoint3 == 0) && quota.quotaPoint3) {
                                                        yearPoint.professionalPoint3 = quota.quotaPoint3;
                                                        changes.professionalPoint3 = quota.quotaPoint3;
                                                    }

                                                    const codes = leadershipCodes[user.organizationId];
                                                    changes.currentLeadershipName = (codes.map(code => {
                                                        const chucVuLanhDao = chucVu[code];
                                                        changes.additionalIncomeCoefficient = Math.max(changes.additionalIncomeCoefficient, chucVuLanhDao.kI)
                                                        return chucVuLanhDao.name + ` (${code})`;
                                                    })).toString();
                                                    for (let i = 0; i < codes.length; i++) {
                                                        const cv = chucVu[codes[i]];
                                                        if (cv && (changes.ratedRatio == null || cv.ratedRatio < changes.ratedRatio)) {
                                                            changes.currentLeadershipCode = codes[i];
                                                            chucVu[changes.currentLeadershipCode].name;
                                                        }
                                                    }
                                                }

                                                // Tính Tỷ lệ định mức
                                                let selectedCode = null;
                                                if (changes.leadershipCode) {
                                                    selectedCode = changes.leadershipCode;
                                                } else if (changes.currentLeadershipCode) {
                                                    selectedCode = changes.currentLeadershipCode;
                                                }
                                                if (selectedCode) {
                                                    let chucVu2019 = chucVu[selectedCode];
                                                    changes.ratedRatio = chucVu2019.ratedRatio;

                                                    if (changes.ratedRatio < changes.ratedRatio1) {
                                                        changes.ratedRatio1 = changes.ratedRatio;
                                                        changes.ratedRatio2 = changes.ratedRatio;
                                                        changes.ratedRatio3 = changes.ratedRatio;
                                                    }

                                                    changes.note += `\nMã lãnh đạo (${selectedCode}). Tỷ lệ định mức=${changes.ratedRatio}, Kcm=${changes.professionalCoefficient ? changes.professionalCoefficient.toFixed(2) : 0}, Klđ=${changes.leadershipCoefficient ? changes.leadershipCoefficient.toFixed(2) : 0}.`;
                                                }

                                                // Giảm định mức
                                                if (cbGiamDinhMuc[user.organizationId]) {
                                                    cbGiamDinhMucShcc.removeByValue(user.organizationId);
                                                    const cb = cbGiamDinhMuc[user.organizationId];
                                                    if (cb.quotaPoint1 < changes.ratedRatio1) {
                                                        changes.ratedRatio1 = cb.quotaPoint1;
                                                        changes.ratedRatio2 = cb.quotaPoint2;
                                                        changes.ratedRatio3 = cb.quotaPoint3;
                                                    }

                                                    changes.leadershipCode = cb.leadershipCode;
                                                    changes.leadershipName = cb.leadershipName;
                                                    changes.leadershipCoefficient = chucVu[changes.leadershipCode].leadershipCoefficient;

                                                    changes.note += `\nGiảm định mức: ${changes.ratedRatio1}, ${changes.ratedRatio2}, ${changes.ratedRatio3}`;
                                                }

                                                // PGS/GS không hoàn thành nhiệm vụ
                                                if (cbPgsGs.includes(user.organizationId)) {
                                                    cbPgsGs.removeByValue(user.organizationId);
                                                    changes.note += `\nPGS/GS không hoàn thành nhiệm vụ.`;
                                                }

                                                // Tính định mức cuối cùng
                                                if (changes.ratedRatio1) changes.quotaPoint1 = changes.quotaPoint1 * changes.ratedRatio1 / 100;
                                                if (changes.ratedRatio2) changes.quotaPoint2 = changes.quotaPoint2 * changes.ratedRatio2 / 100;
                                                if (changes.ratedRatio3) changes.quotaPoint3 = changes.quotaPoint3 * changes.ratedRatio3 / 100;
                                                if (changes.ratedRatio1 || changes.ratedRatio2 || changes.ratedRatio3) {
                                                    changes.note += `\nĐịnh mức được tính: ${changes.quotaPoint1}, ${changes.quotaPoint2}, ${changes.quotaPoint3}`;
                                                }

                                                // Tính điểm cuối cùng
                                                let professionalPoint = 0, divisor = 0;
                                                if (changes.quotaPoint1) {
                                                    professionalPoint += Math.max(0, Math.min(100, 100 * yearPoint.professionalPoint1 / changes.quotaPoint1));
                                                    divisor++;
                                                }
                                                if (changes.quotaPoint2) {
                                                    professionalPoint += Math.max(0, Math.min(100, 100 * yearPoint.professionalPoint2 / changes.quotaPoint2));
                                                    divisor++;
                                                }
                                                if (changes.quotaPoint3) {
                                                    professionalPoint += Math.max(0, Math.min(100, 100 * yearPoint.professionalPoint3 / changes.quotaPoint3));
                                                    divisor++;
                                                }
                                                if (!isNaN(professionalPoint) && divisor) {
                                                    changes.professionalPoint = professionalPoint / divisor;

                                                    //Chuyển điểm NV1 và NV2
                                                    let percent1 = yearPoint.professionalPoint1 / changes.quotaPoint1 * 100,
                                                        percent2 = yearPoint.professionalPoint2 / changes.quotaPoint2 * 100;
                                                    changes.transformationType = '';
                                                    if (divisor == 3 && percent1 >= 50 && percent2 >= 50) {
                                                        const transformedPoint = (Math.min(100, 100 * (yearPoint.professionalPoint1 * 2 + yearPoint.professionalPoint2) / (changes.quotaPoint1 * 2 + changes.quotaPoint2)) * 2 + Math.min(100, 100 * yearPoint.professionalPoint3 / changes.quotaPoint3)) / 3;
                                                        if (changes.professionalPoint < transformedPoint) {
                                                            changes.note += `\nChuyển điểm NV1 và NV2: điểm từ ${changes.professionalPoint.toFixed(2)} thành ${transformedPoint.toFixed(2)}.`;
                                                            changes.transformationType = (percent1 >= 80 && percent2 >= 80) ? 'c' : 'b';
                                                            changes.professionalPoint = transformedPoint;
                                                        }
                                                    }
                                                } else {
                                                    console.log('Error: Tính điểm cuối cùng', user.organizationId, changes.quotaPoint1, changes.quotaPoint2, changes.quotaPoint3);
                                                }
                                            } else {
                                                console.log('Error: SHCC khong tinh duoc: ', user.organizationId)
                                            }
                                        }

                                        if (changes.rewardPoint) {
                                            const rewardPointNotes = [];
                                            if (diemThuong.boNhiemGsPgs) rewardPointNotes.push(diemThuong.boNhiemGsPgs);
                                            if (diemThuong.nhanBangTn) rewardPointNotes.push(diemThuong.nhanBangTn);
                                            if (diemThuong.khenThuong) rewardPointNotes.push(diemThuong.khenThuong);
                                            changes.note = (changes.note ? changes.note : '') + `\nĐiểm thưởng: ${changes.rewardPoint}. Lý do: ${rewardPointNotes}`;
                                        }
                                        if (changes.penaltyPoint) {
                                            changes.note = (changes.note ? changes.note : '') + `\nĐiểm trừ: ${changes.rewardPoint}. Lý do: ${diemTru.lyDo}`;
                                        }
                                        if (changes.note && changes.note.startsWith('\n')) {
                                            changes.note = changes.note.substring(1);
                                        }

                                        if (!hasPoint) {
                                            const point = (changes.professionalPoint ? changes.professionalPoint : 0) + changes.rewardPoint - changes.penaltyPoint;
                                            if (changes.point == null || changes.point < point) {
                                                changes.point = point;
                                            }
                                        }

                                        // Tính hệ số Chuyên môn, hệ sộ Lãnh đạo
                                        changes.professionalCoefficient = 1 - (changes.leadershipCoefficient ? changes.leadershipCoefficient : 0);
                                        if (changes.professionalPoint != null && yearPoint.leadershipPoint != null && changes.leadershipCoefficient) {
                                            changes.point = changes.professionalPoint * changes.professionalCoefficient + yearPoint.leadershipPoint * changes.leadershipCoefficient + changes.rewardPoint - changes.penaltyPoint;
                                        }
                                    }

                                    app.model.yearPoint.set(user._id, 2019, changes, () => solve(index + 1));
                                });
                            } else {
                                solve(index + 1);
                            }
                        });
                    } else {
                        console.log('Error 1000: user.organizationId =', user.organizationId, user.firstname, user.lastname);
                        solve(index + 1);
                    }
                } else {
                    let error = null;
                    if (cbTapSuShcc.length) {
                        error = error + 'Error: cán bộ tập sự chưa xét: ' + cbTapSuShcc;
                        console.log('Error: Tap su:', cbTapSuShcc);
                    }
                    if (cbNghiThaiSanShcc.length) {
                        error = error + 'Error: cán bộ nghỉ thai sản chưa xét: ' + cbNghiThaiSanShcc;
                        console.log('Error: Thai san:', cbNghiThaiSanShcc);
                    }
                    if (cb06Shcc.length) {
                        error = error + 'Error: cán bộ làm việc dưới 6 tháng chưa xét: ' + cb06Shcc;
                        console.log('Error: Lam viec 0-6:', cb06Shcc);
                    }
                    if (cb712Shcc.length) {
                        error = error + 'Error: cán bộ làm việc 7-12 tháng chưa xét: ' + cb712Shcc;
                        console.log('Error: Lam viec 7-12:', cb712Shcc);
                    }
                    if (cbGiamDinhMucShcc.length) {
                        error = error + 'Error: cán bộ giảm định mức chưa xét: ' + cbGiamDinhMucShcc;
                        console.log('Error: Giam dinh muc:', cbGiamDinhMucShcc);
                    }

                    //Các ngoại lệ
                    app.excel.readFile(app.path.join(app.assetPath, 'exceptions.xlsx')).then(workbook => {
                        const worksheet = workbook.getWorksheet(1);
                        if (worksheet) {
                            const solve = (index = 2) => {
                                let shcc = worksheet.getCell('A' + index).value,
                                    fullname = worksheet.getCell('B' + index).value,
                                    facultyName = worksheet.getCell('C' + index).value,
                                    departmentName = worksheet.getCell('D' + index).value,
                                    result = worksheet.getCell('E' + index).value;
                                if (shcc) {
                                    app.model.yearPoint.get({ shcc, year: 2019 }, (err, yearPoint) => {
                                        if (err) {
                                            error += 'Error: Gán ngoại lệ: ' + shcc;
                                            solve(index + 1);
                                        } else if (yearPoint) {
                                            yearPoint.isException = true;
                                            yearPoint.result = yearPoint.note = result;
                                            if (result == 'Không đánh giá (HĐLĐ lương khoán)') yearPoint.additionalIncomeCoefficient = 0;
                                            yearPoint.save(err => {
                                                if (err) {
                                                    error += 'Error: Gán ngoại lệ: ' + shcc;
                                                }
                                                solve(index + 1);
                                            });
                                        } else {
                                            app.model.yearPoint.create({ shcc, fullname, facultyName, departmentName, result, note: result, isException: true });
                                            solve(index + 1);
                                        }
                                    });
                                } else {
                                    //Danh sách Cán bộ vi phạm kỷ luật
                                    app.model.dsViPhamKyLuat.getAll({ nam: 2019 }, (err, items) => {
                                        if (err || items == null) {
                                            error += 'Error: Xử lý Danh sách Cán bộ vi phạm kỷ luật';
                                        } else {
                                            const solveDiscipline = index => {
                                                if (index < items.length) {
                                                    const item = items[index];
                                                    app.model.yearPoint.get({ shcc: item.shcc, year: 2019 }, (err, yearPoint) => {
                                                        if (err) {
                                                            error += 'Error: Không lấy được cán bộ ' + item.shcc;
                                                            solveDiscipline(index + 1);
                                                        } else {
                                                            if (yearPoint.discipline == null || yearPoint.discipline == 'Kỷ luật') yearPoint.discipline = item.ghiChu;
                                                            yearPoint.note = (yearPoint.note ? yearPoint.note : '') + '\nKỷ luật: ' + item.ghiChu;
                                                            if (yearPoint.discipline == 'Cảnh cáo') {
                                                                yearPoint.result = 'Không hoàn thành nhiệm vụ';
                                                            } else {
                                                                yearPoint.result = yearPoint.point >= 80 ? 'Hoàn thành nhiệm vụ' : 'Không hoàn thành nhiệm vụ';
                                                            }
                                                            yearPoint.save(err => {
                                                                if (err) error += 'Error solveDiscipline: Lưu cán bộ' + item.shcc;
                                                                solveDiscipline(index + 1);
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    //Ngoại lệ điểm cuối cùng
                                                    const exceptionPoints = [
                                                        { shcc: '001834', point: 95.2 },
                                                        { shcc: '002268', point: 98 },
                                                        { shcc: '002434', point: 97.5 },
                                                        { shcc: '002844', point: 96 },
                                                        { shcc: '003849', point: 98.75 },
                                                        { shcc: '008347', point: 96.5 },
                                                    ];
                                                    const solveExceptionPoint = (index = 0) => {
                                                        if (index < exceptionPoints.length) {
                                                            const item = exceptionPoints[index];
                                                            app.model.yearPoint.get({ shcc: item.shcc, year: 2019 }, (err, yearPoint) => {
                                                                yearPoint.point = item.point;
                                                                yearPoint.save(err => {
                                                                    if (err) error += 'Error solveExceptionPoint: Lưu cán bộ ' + item.shcc;
                                                                    solveExceptionPoint(index + 1);
                                                                });
                                                            });
                                                        } else {
                                                            //Duyệt qua tất cả để tính resultBySystem
                                                            app.model.yearPoint.getAll((err, yearPoints) => {
                                                                if (err) {
                                                                    error += 'Error: duyệt qua tất cả để tính resultBySystem';
                                                                } else {
                                                                    yearPoints.forEach(yearPoint => {
                                                                        yearPoint.resultBySystem = dsResultBySystem[yearPoint.shcc] ? dsResultBySystem[yearPoint.shcc] : getResult(yearPoint);
                                                                        if (dsNote[yearPoint.shcc]) yearPoint.note = dsNote[yearPoint.shcc];
                                                                        if (dsKiFinal[yearPoint.shcc]) yearPoint.additionalIncomeCoefficient = dsKiFinal[yearPoint.shcc];

                                                                        yearPoint.save();
                                                                    });
                                                                }

                                                                console.log('DONE');
                                                                res.send({ error });
                                                            });
                                                        }
                                                    };
                                                    solveExceptionPoint();
                                                }
                                            };
                                            solveDiscipline(0);
                                        }
                                    });
                                }
                            };
                            solve();
                        }
                    });
                }
            };

            app.model.dmTyLeDinhMuc.getAll({ nam: 2019 }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    dinhMuc[item.chucDanh] = {
                        quotaPoint1: item.soGioGiangDay,
                        quotaPoint2: item.nghienCuuKhoaHoc,
                        quotaPoint3: item.nhiemVuKhac,
                        kI: item.kI,
                    };
                }
                return app.model.dsTapSu.getAll({ nam: 2019 })
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    cbTapSuShcc.push(item.shcc);
                    cbTapSu[item.shcc] = {
                        quotaPoint1: item.nv1GioChuanDinhMuc,
                        quotaPoint2: item.nv2GioLamViecDinhMuc,
                        quotaPoint3: item.nv3GioLamViecDinhMuc,
                    };
                }
                return app.model.dsCanBoLamViec06Thang.getAll({ nam: 2019 });
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    cb06Shcc.push(item.shcc);
                    cb06[item.shcc] = {
                        month: item.soThangLamViec,
                        note: item.lyDoKhongTinh,
                    };
                }
                return app.model.dsCanBoLamViec712Thang.getAll({ nam: 2019 });
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    cb712Shcc.push(item.shcc);
                    cb712[item.shcc] = {
                        month: item.soThangLamViec,
                        note: item.lyDoKhongTinh,
                    };
                }
                return app.model.dsNghiThaiSan.getAll({ nam: 2019 });
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    cbNghiThaiSanShcc.push(item.shcc);
                    cbNghiThaiSan[item.shcc] = { month: 12 - item.thangKhongTinh };
                }
                return app.model.dmChucVu.getAll({ nam: 2019 });
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    chucVu[item.maQL] = {
                        name: item.tenChucVu,
                        ratedRatio: item.tyLeDinhMuc,
                        leadershipCoefficient: item.kLD,
                        professionalCoefficient: 1 - item.kLD,
                        kI: item.kI,
                    };
                }
                return app.model.dsGiamDinhMuc.getAll({ nam: 2019 });
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    cbGiamDinhMucShcc.push(item.shcc);
                    cbGiamDinhMuc[item.shcc] = {
                        leadershipCode: item.lyDoGiamDinhMuc,
                        leadershipName: item.ghiChu,
                        quotaPoint1: item.tyLeDinhMucNv1,
                        quotaPoint2: item.tyLeDinhMucNv2,
                        quotaPoint3: item.tyLeDinhMucNv3,
                    };
                }
                return app.model.dsPgsGsKhongHoanThanhNhiemVu.getAll({ nam: 2019 });
            }).then(items => {
                cbPgsGs = items.map(item => item.shcc);
                return app.model.dsSauDaiHocTrongNuoc.getAll({ nam: 2019 });
            }).then(items => {
                sdhTrongNuoc = items.map(item => item.shcc);
                return app.model.dsCanboCongdoan.getAll({ nam: 2019 });
            }).then(items => {
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    const codes = leadershipCodes[item.shcc] || [];
                    if (codes.indexOf(item.maQL) == -1) {
                        codes.push(item.maQL);
                        leadershipCodes[item.shcc] = codes;
                    }
                }
                return app.excel.readFile(app.path.join(app.assetPath, 'manager.xlsx'));
            }).then(workbook => {
                let errors = '';
                const worksheet = workbook.getWorksheet(1);
                if (worksheet) {
                    let index = 2;
                    while (true) {
                        let shcc = worksheet.getCell('A' + index).value,
                            code1 = worksheet.getCell('K' + index).value,
                            code2 = worksheet.getCell('P' + index).value,
                            code3 = worksheet.getCell('S' + index).value,
                            code4 = worksheet.getCell('X' + index).value,
                            code5 = worksheet.getCell('AA' + index).value,
                            chucDanhTrinhDo = worksheet.getCell('F' + index).value;

                        // Cập nhật Chức danh - Trình độ: GVCC => PGS, GS
                        if (chucDanhTrinhDo) {
                            app.model.staff.get({ shcc }).then(staff => {
                                if (staff && staff.CHUCDANH_TRINHDO == 'GVCC' && staff.shcc != '002130') {
                                    staff.CHUCDANH_TRINHDO = chucDanhTrinhDo;
                                    staff.save();
                                }
                            });
                        }

                        if (shcc) {
                            let codes = leadershipCodes[shcc] || [];
                            if (code1 && codes.indexOf(code1) == -1) codes.push(code1);
                            if (code2 && codes.indexOf(code2) == -1) codes.push(code2);
                            if (code3 && codes.indexOf(code3) == -1) codes.push(code3);
                            if (code4 && codes.indexOf(code4) == -1) codes.push(code4);
                            if (code5 && codes.indexOf(code5) == -1) codes.push(code5);
                            if (codes.length) leadershipCodes[shcc] = codes;

                            index += 1;
                        } else {
                            break;
                        }
                    }
                }
                errors && console.log('\nError when read file manager.xlsx:', errors);

                return app.excel.readFile(app.path.join(app.assetPath, 'printResultBySystem.xlsx'));
            }).then(workbook => {
                //Danh sách Điểm hệ thống chị Hạnh tính
                let errors = '';
                const worksheet = workbook.getWorksheet(1);
                if (worksheet) {
                    let index = 2;
                    while (true) {
                        let shcc = worksheet.getCell('A' + index).value;
                        if (shcc) {
                            dsResultBySystem[shcc] = worksheet.getCell('C' + index).value
                            index += 1;
                        } else {
                            break;
                        }
                    }
                }
                errors && console.log('\nError when read file printResultBySystem.xlsx:', errors);

                return app.excel.readFile(app.path.join(app.assetPath, 'printNote.xlsx'));
            }).then(workbook => {
                //Danh sách Ghi chú chị Hạnh tính
                let errors = '';
                const worksheet = workbook.getWorksheet(1);
                if (worksheet) {
                    let index = 2;
                    while (true) {
                        let shcc = worksheet.getCell('A' + index).value;
                        if (shcc) {
                            dsNote[shcc] = worksheet.getCell('C' + index).value
                            index += 1;
                        } else {
                            break;
                        }
                    }
                }
                errors && console.log('\nError when read file printNote.xlsx:', errors);

                return app.excel.readFile(app.path.join(app.assetPath, 'KiFinal.xlsx'));
            }).then(workbook => {
                //Danh sách Ki được tính khi còn GVCC
                let errors = '';
                const worksheet = workbook.getWorksheet(1);
                if (worksheet) {
                    let index = 2;
                    while (true) {
                        let shcc = worksheet.getCell('A' + index).value;
                        if (shcc) {
                            dsKiFinal[shcc] = worksheet.getCell('C' + index).value
                            index += 1;
                        } else {
                            break;
                        }
                    }
                }
                errors && console.log('\nError when read file KiFinal.xlsx:', errors);

                solve();
            });
        }
    }));

    const getResult = item => {
        if (item.discipline) {
            return item.result;
        } else if (item.isException) {
            return item.result;
        } else if (item.active == false) {
            return item.currentLeadershipName ? 'Chưa có điểm đánh giá' : 'Không đánh giá';
        } else if (item.point == null) {
            return '';
        } else if (95 <= item.point && (item.transformationType == null || item.transformationType == 'c')) {
            return 'Hoàn thành xuất sắc nhiệm vụ';
        } else if (80 <= item.point) {
            return item.transformationType == 'b' ? 'Hoàn thành tốt nhiệm vụ (Không bình chọn "Hoàn thành xuất sắc nhiệm vụ")' : 'Hoàn thành tốt nhiệm vụ';
        } else if (65 <= item.point) {
            return 'Hoàn thành nhiệm vụ';
        } else {
            return 'Không hoàn thành nhiệm vụ';
        }
    };

    app.get('/api/year-point/export', app.permission.check('point:read'), (req, res) => {
        app.model.yearPoint.getAll({}, (error, items) => {
            if (error) {
                res.send({ error });
            } else {
                const workbook = app.excel.create();

                let facultyCodes = [],
                    sheets = { All: { worksheet: workbook.addWorksheet('All'), cells: [], line: 2 } };
                let { cells, line } = sheets['All'];
                items.forEach(item => {
                    if (facultyCodes.indexOf(item.facultyCode) == -1) {
                        facultyCodes.push(item.facultyCode);
                    }
                });
                facultyCodes.sort().forEach(code => sheets[code] = {
                    worksheet: workbook.addWorksheet(code),
                    cells: [],
                    line: 2,
                });

                items.forEach(item => {
                    cells.push({ cell: 'A' + line, value: item.shcc });
                    cells.push({ cell: 'B' + line, value: item.fullname });
                    cells.push({ cell: 'C' + line, value: item.facultyName ? item.facultyName : '' });
                    cells.push({ cell: 'D' + line, value: item.departmentName ? item.departmentName : '' });
                    cells.push({ cell: 'E' + line, value: item.currentLeadershipName ? item.currentLeadershipName : '' });
                    cells.push({ cell: 'F' + line, value: item.currentLeadershipName ? item.currentLeadershipCode : '' });
                    cells.push({ cell: 'G' + line, value: item.additionalIncomeCoefficient ? item.additionalIncomeCoefficient : '' });
                    cells.push({ cell: 'H' + line, value: item.leadershipName ? item.leadershipName : item.currentLeadershipName ? item.currentLeadershipName : '' });
                    cells.push({ cell: 'I' + line, value: item.leadershipCode ? item.leadershipCode : (item.currentLeadershipCode ? item.currentLeadershipCode : '') });
                    cells.push({ cell: 'J' + line, value: item.ratedRatio });
                    cells.push({ cell: 'K' + line, value: item.quotaPoint1 });
                    cells.push({ cell: 'L' + line, value: item.professionalPoint1 });
                    cells.push({ cell: 'M' + line, value: item.quotaPoint2 });
                    cells.push({ cell: 'N' + line, value: item.professionalPoint2 });
                    cells.push({ cell: 'O' + line, value: item.quotaPoint3 });
                    cells.push({ cell: 'P' + line, value: item.professionalPoint3 });
                    cells.push({ cell: 'Q' + line, value: item.professionalPoint ? item.professionalPoint.toFixed(2) : 0 });
                    cells.push({ cell: 'R' + line, value: item.professionalCoefficient ? item.professionalCoefficient.toFixed(2) : 0 });
                    cells.push({ cell: 'S' + line, value: item.leadershipPoint ? item.leadershipPoint.toFixed(2) : 0 });
                    cells.push({ cell: 'T' + line, value: item.leadershipCoefficient ? item.leadershipCoefficient.toFixed(2) : 0 });
                    cells.push({ cell: 'U' + line, value: item.rewardPoint });
                    cells.push({ cell: 'V' + line, value: item.penaltyPoint });
                    cells.push({ cell: 'W' + line, value: item.point ? item.point.toFixed(2) : 0 });
                    cells.push({ cell: 'X' + line, value: item.transformationType ? item.transformationType : '' });
                    cells.push({ cell: 'Y' + line, value: '' });
                    cells.push({ cell: 'Z' + line, value: item.resultBySystem ? item.resultBySystem : '' });
                    cells.push({ cell: 'AA' + line, value: item.result });
                    cells.push({ cell: 'AB' + line, value: item.note });
                    cells.push({ cell: 'AC' + line, value: item.ratedRatio1 });
                    cells.push({ cell: 'AD' + line, value: item.ratedRatio2 });
                    cells.push({ cell: 'AE' + line, value: item.ratedRatio3 });
                    cells.push({ cell: 'AF' + line, value: item.level ? item.level : '' });
                    cells.push({ cell: 'AG' + line, value: item.quotaPoint1 });
                    cells.push({ cell: 'AH' + line, value: item.quotaPoint2 });
                    cells.push({ cell: 'AI' + line, value: item.quotaPoint3 });
                    line++;

                    if (sheets[item.facultyCode]) {
                        const sheet = sheets[item.facultyCode];
                        sheet.cells.push({ cell: 'A' + sheet.line, value: item.shcc });
                        sheet.cells.push({ cell: 'B' + sheet.line, value: item.fullname });
                        sheet.cells.push({ cell: 'C' + sheet.line, value: item.facultyName ? item.facultyName : '' });
                        sheet.cells.push({ cell: 'D' + sheet.line, value: item.departmentName ? item.departmentName : '' });
                        sheet.cells.push({ cell: 'E' + sheet.line, value: item.currentLeadershipName ? item.currentLeadershipName : '' });
                        sheet.cells.push({ cell: 'F' + sheet.line, value: item.currentLeadershipName ? item.currentLeadershipCode : '' });
                        sheet.cells.push({ cell: 'G' + sheet.line, value: item.additionalIncomeCoefficient ? item.additionalIncomeCoefficient : '' });
                        sheet.cells.push({ cell: 'H' + sheet.line, value: item.leadershipName ? item.leadershipName + ` (${item.leadershipCode})` : item.currentLeadershipName ? item.currentLeadershipName : '' });
                        sheet.cells.push({ cell: 'I' + sheet.line, value: item.leadershipCode ? item.leadershipCode : (item.currentLeadershipCode ? item.currentLeadershipCode : '') });
                        sheet.cells.push({ cell: 'J' + sheet.line, value: item.quotaPoint1 });
                        sheet.cells.push({ cell: 'K' + sheet.line, value: item.professionalPoint1 });
                        sheet.cells.push({ cell: 'L' + sheet.line, value: item.quotaPoint2 });
                        sheet.cells.push({ cell: 'M' + sheet.line, value: item.professionalPoint2 });
                        sheet.cells.push({ cell: 'N' + sheet.line, value: item.quotaPoint3 });
                        sheet.cells.push({ cell: 'O' + sheet.line, value: item.professionalPoint3 });
                        sheet.cells.push({ cell: 'P' + sheet.line, value: item.professionalPoint ? item.professionalPoint.toFixed(2) : 0 });
                        sheet.cells.push({ cell: 'Q' + sheet.line, value: item.professionalCoefficient ? item.professionalCoefficient.toFixed(2) : 0 });
                        sheet.cells.push({ cell: 'R' + sheet.line, value: item.leadershipPoint ? item.leadershipPoint.toFixed(2) : 0 });
                        sheet.cells.push({ cell: 'S' + sheet.line, value: item.leadershipCoefficient ? item.leadershipCoefficient.toFixed(2) : 0 });
                        sheet.cells.push({ cell: 'T' + sheet.line, value: item.rewardPoint });
                        sheet.cells.push({ cell: 'U' + sheet.line, value: item.penaltyPoint });
                        sheet.cells.push({ cell: 'V' + sheet.line, value: item.point ? item.point.toFixed(2) : 0 });
                        sheet.cells.push({ cell: 'W' + sheet.line, value: item.transformationType ? item.transformationType : '' });
                        sheet.cells.push({ cell: 'X' + sheet.line, value: item.resultBySystem ? item.resultBySystem : '' });
                        sheet.cells.push({ cell: 'Y' + sheet.line, value: item.note });
                        sheet.line++;
                    }
                });

                Object.keys(sheets).forEach(key => {
                    let sheet = sheets[key],
                        columns = key == 'All' ? [
                            { header: 'SHCC', key: 'SHCC', width: 8, style: { font: { bold: true } } },
                            { header: 'Họ và Tên', key: 'Họ và Tên', width: 25, style: { font: { bold: true } } },
                            { header: 'Tên khoa', key: 'Tên khoa', width: 50, style: { font: { bold: true } } },
                            { header: 'Tên bộ môn', key: 'Tên bộ môn', width: 50, style: { font: { bold: true } } },
                            { header: 'Chức vụ quản lý', key: 'Chức vụ quản lý', width: 30, style: { font: { bold: true } } },
                            { header: 'Mã Chức vụ quản lý', key: 'Mã Chức vụ quản lý', width: 16, style: { font: { bold: true } } },
                            { header: 'Hệ số TNTT', key: 'Hệ số TNTT', width: 11, style: { font: { bold: true } } },
                            { header: 'Chức vụ được tính năm 2019', key: 'Chức vụ được tính năm 2019', width: 28, style: { font: { bold: true } } },
                            { header: 'Mã Chức vụ được tính năm 2019', key: 'Mã Chức vụ được tính năm 2019', width: 28, style: { font: { bold: true } } },
                            { header: 'Tỷ lệ định mức thực hiện công việc', key: 'Tỷ lệ định mức thực hiện công việc', width: 10, style: { font: { bold: true } } },
                            { header: 'Định mức NV1', key: 'Định mức NV1', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm NV1', key: 'Điểm NV1', width: 10, style: { font: { bold: true } } },
                            { header: 'Định mức NV2', key: 'Định mức NV2', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm NV2', key: 'Điểm NV2', width: 10, style: { font: { bold: true } } },
                            { header: 'Định mức NV3', key: 'Định mức NV3', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm NV3', key: 'Điểm NV3', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm chuyên môn', key: 'Điểm chuyên môn', width: 10, style: { font: { bold: true } } },
                            { header: 'Hệ số chuyên môn', key: 'Hệ số chuyên môn', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm lãnh đạo', key: 'Điểm lãnh đạo', width: 10, style: { font: { bold: true } } },
                            { header: 'Hệ số lãnh đạo', key: 'Hệ số lãnh đạo', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm thưởng', key: 'Điểm thưởng', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm trừ', key: 'Điểm trừ', width: 10, style: { font: { bold: true } } },
                            { header: 'Điểm tổng', key: 'Điểm tổng', width: 12, style: { font: { bold: true } } },
                            { header: 'Chuyển điểm', key: 'Chuyển tổng', width: 12, style: { font: { bold: true } } },
                            { header: 'Điểm đánh giá của hội đồng Đơn vị', key: 'Điểm đánh giá của hội đồng Đơn vị', width: 15, style: { font: { bold: true } } },
                            { header: 'Kết quả (Hệ thống tính)', key: 'Kết quả (Hệ thống tính)', width: 15, style: { font: { bold: true } } },
                            { header: 'Kết quả đánh giá, xếp loại năm 2019', key: 'Kết quả đánh giá, xếp loại năm 2019', width: 15, style: { font: { bold: true } } },
                            { header: 'Ghi chú', key: 'Ghi chú', width: 60, style: { font: { bold: true } } },
                            { header: 'Tỷ lệ định mức NV1', key: 'Tỷ lệ định mức NV1', width: 18, style: { font: { bold: true } } },
                            { header: 'Tỷ lệ định mức NV2', key: 'Tỷ lệ định mức NV2', width: 18, style: { font: { bold: true } } },
                            { header: 'Tỷ lệ định mức NV3', key: 'Tỷ lệ định mức NV3', width: 18, style: { font: { bold: true } } },
                            { header: 'Chức danh-Trình độ', key: 'Chức danh-Trình độ', width: 18, style: { font: { bold: true } } },
                            { header: 'Giờ chuẩn NV1', key: 'Giờ chuẩn NV1', width: 12, style: { font: { bold: true } } },
                            { header: 'Giờ chuẩn NV2', key: 'Giờ chuẩn NV2', width: 12, style: { font: { bold: true } } },
                            { header: 'Giờ chuẩn NV3', key: 'Giờ chuẩn NV3', width: 12, style: { font: { bold: true } } },
                        ] : [
                                { header: 'SHCC', key: 'SHCC', width: 8, style: { font: { bold: true } } },
                                { header: 'Họ và Tên', key: 'Họ và Tên', width: 25, style: { font: { bold: true } } },
                                { header: 'Tên khoa', key: 'Tên khoa', width: 50, style: { font: { bold: true } } },
                                { header: 'Tên bộ môn', key: 'Tên bộ môn', width: 50, style: { font: { bold: true } } },
                                { header: 'Chức vụ quản lý', key: 'Chức vụ quản lý', width: 30, style: { font: { bold: true } } },
                                { header: 'Mã Chức vụ quản lý', key: 'Mã Chức vụ quản lý', width: 16, style: { font: { bold: true } } },
                                { header: 'Hệ số TNTT', key: 'Hệ số TNTT', width: 11, style: { font: { bold: true } } },
                                { header: 'Chức vụ được tính năm 2019', key: 'Chức vụ được tính năm 2019', width: 28, style: { font: { bold: true } } },
                                { header: 'Mã Chức vụ được tính năm 2019', key: 'Mã Chức vụ được tính năm 2019', width: 28, style: { font: { bold: true } } },
                                { header: 'Định mức NV1', key: 'Định mức NV1', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm NV1', key: 'Điểm NV1', width: 10, style: { font: { bold: true } } },
                                { header: 'Định mức NV2', key: 'Định mức NV2', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm NV2', key: 'Điểm NV2', width: 10, style: { font: { bold: true } } },
                                { header: 'Định mức NV3', key: 'Định mức NV3', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm NV3', key: 'Điểm NV3', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm chuyên môn', key: 'Điểm chuyên môn', width: 10, style: { font: { bold: true } } },
                                { header: 'Hệ số chuyên môn', key: 'Hệ số chuyên môn', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm lãnh đạo', key: 'Điểm lãnh đạo', width: 10, style: { font: { bold: true } } },
                                { header: 'Hệ số lãnh đạo', key: 'Hệ số lãnh đạo', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm thưởng', key: 'Điểm thưởng', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm trừ', key: 'Điểm trừ', width: 10, style: { font: { bold: true } } },
                                { header: 'Điểm tổng', key: 'Điểm tổng', width: 12, style: { font: { bold: true } } },
                                { header: 'Chuyển điểm', key: 'Chuyển tổng', width: 12, style: { font: { bold: true } } },
                                { header: 'Kết quả (Hệ thống tính)', key: 'Kết quả (Hệ thống tính)', width: 15, style: { font: { bold: true } } },
                                { header: 'Ghi chú', key: 'Ghi chú', width: 60, style: { font: { bold: true } } },
                            ];

                    sheet.worksheet.columns = columns;
                    app.excel.write(sheet.worksheet, sheet.cells);
                });
                app.excel.attachment(workbook, res);
            }
        })
    });

    app.get('/api/year-point/personel/:shcc', (req, res) => app.model.yearPoint.get({ shcc: req.params.shcc }, (error, item) => {
        if (error || item == null) {
            res.send({ error: 'SHCC khong ton tai!' });
        } else {
            res.send({
                shcc: item.shcc,
                fullname: item.fullname,
                facultyName: item.facultyName ? item.facultyName : '',
                departmentName: item.departmentName ? item.departmentName : '',
                currentLeadershipName: item.currentLeadershipName ? item.currentLeadershipName : '',
                leadershipName: item.leadershipName ? item.leadershipName + ` (${item.leadershipCode})` : item.currentLeadershipName ? item.currentLeadershipName : '',
                ratedRatio: item.ratedRatio ? item.ratedRatio : '',
                quotaPoint1: item.quotaPoint1 ? item.quotaPoint1 : '',
                professionalPoint1: item.professionalPoint1 ? item.professionalPoint1 : '',
                quotaPoint2: item.quotaPoint2 ? item.quotaPoint2 : '',
                professionalPoint2: item.professionalPoint2 ? item.professionalPoint2 : '',
                quotaPoint3: item.quotaPoint3 ? item.quotaPoint3 : '',
                professionalPoint3: item.professionalPoint3 ? item.professionalPoint3 : '',
                professionalPoint: item.professionalPoint ? item.professionalPoint.toFixed(2) : 0,
                professionalCoefficient: item.professionalCoefficient ? item.professionalCoefficient.toFixed(2) : 0,
                leadershipPoint: item.leadershipPoint ? item.leadershipPoint.toFixed(2) : 0,
                leadershipCoefficient: item.leadershipCoefficient ? item.leadershipCoefficient.toFixed(2) : 0,
                additionalIncomeCoefficient: item.additionalIncomeCoefficient ? item.additionalIncomeCoefficient.toFixed(2) : 0,
                rewardPoint: item.rewardPoint ? item.rewardPoint : '',
                penaltyPoint: item.penaltyPoint ? item.penaltyPoint : '',
                point: item.point ? item.point.toFixed(2) : 0,
                result: item.result ? item.result : 'Đang cập nhật',
                note: item.note,
            });
        }
    }));

    app.get('/api/year-point/personel/html/:shcc', (req, res) => app.model.yearPoint.get({ shcc: req.params.shcc }, (error, item) => {
        if (error || item == null) {
            res.send({ error: 'SHCC khong ton tai!' });
        } else {
            res.render('yearPoint', {
                shcc: item.shcc,
                fullname: item.fullname,
                facultyName: item.facultyName ? item.facultyName : '',
                departmentName: item.departmentName ? item.departmentName : '',
                currentLeadershipName: item.currentLeadershipName ? item.currentLeadershipName : '',
                leadershipName: item.leadershipName ? item.leadershipName + ` (${item.leadershipCode})` : item.currentLeadershipName ? item.currentLeadershipName : '',
                ratedRatio: item.ratedRatio ? item.ratedRatio : '',
                quotaPoint1: item.quotaPoint1 ? item.quotaPoint1 : '',
                professionalPoint1: item.professionalPoint1 ? item.professionalPoint1 : '',
                quotaPoint2: item.quotaPoint2 ? item.quotaPoint2 : '',
                professionalPoint2: item.professionalPoint2 ? item.professionalPoint2 : '',
                quotaPoint3: item.quotaPoint3 ? item.quotaPoint3 : '',
                professionalPoint3: item.professionalPoint3 ? item.professionalPoint3 : '',
                professionalPoint: item.professionalPoint ? item.professionalPoint.toFixed(2) : 0,
                professionalCoefficient: item.professionalCoefficient ? item.professionalCoefficient.toFixed(2) : 0,
                leadershipPoint: item.leadershipPoint ? item.leadershipPoint.toFixed(2) : 0,
                leadershipCoefficient: item.leadershipCoefficient ? item.leadershipCoefficient.toFixed(2) : 0,
                additionalIncomeCoefficient: item.additionalIncomeCoefficient ? item.additionalIncomeCoefficient.toFixed(2) : 0,
                rewardPoint: item.rewardPoint ? item.rewardPoint : '',
                penaltyPoint: item.penaltyPoint ? item.penaltyPoint : '',
                point: item.point ? item.point.toFixed(2) : 0,
                result: item.result ? item.result : 'Đang cập nhật',
                note: item.note,
            });
        }
    }));
};