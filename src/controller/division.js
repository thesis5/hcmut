module.exports = app => {
    const menu = {
        parentMenu: { index: 2000, title: 'Cấu hình', icon: 'fa-cog' },
        menus: { 2050: { title: 'Các bộ phận của trường', link: '/user/division' } },
    };
    app.permission.add(
        { name: 'division:read', menu },
        { name: 'division:write', menu },
    );

    app.get('/user/division', app.permission.check('division:read'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/division/all', app.permission.check('division:read'), (req, res) =>
        app.model.division.getAll((error, items) => res.send({ error, items })));

    app.get('/api/division/item/:_id', app.permission.check('division:read'), (req, res) =>
        app.model.division.get(req.params._id, (error, item) => res.send({ error, item })));

    app.get('/staff/division/all', app.userType.isStaff, (req, res) => {
        app.model.division.getAll((error, items) => res.send({ error, items }));
    });

    app.post('/api/division', app.permission.check('division:write'), (req, res) =>
        app.model.division.create(req.body.division, (error, item) => res.send({ error, item })));

    app.put('/api/division', app.permission.check('division:write'), (req, res) => {
        app.model.division.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item }));
    });

    app.put('/api/division/swap', app.permission.check('division:write'), (req, res) => {
        const isMoveUp = req.body.isMoveUp.toString() == 'true';
        app.model.division.swapPriority(req.body._id, isMoveUp, error => res.send({ error }));
    });

    app.delete('/api/division', app.permission.check('division:write'), (req, res) =>
        app.model.division.delete(req.body._id, error => res.send({ error })));


    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/home/division/all', (req, res) => app.model.division.getAll((error, items) => res.send({ error, items })));

    app.get('/division/item/id/:divisionId', (req, res) => app.model.division.get(req.params.divisionId, (error, division) => {
        if (error || division == null) {
            res.send({ error: 'Error' });
        } else {
            app.model.user.getAll({ role: 'staff', divisions: req.params.divisionId }, (error, staffs) =>
                res.send({ error, division: app.clone(division, { staffs }) }));
        }
    }));


    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    app.createFolder(app.path.join(app.publicPath, '/img/division'));

    const uploadDivisionImage = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('Division:') && files.DivisionImage && files.DivisionImage.length > 0) {
            console.log('Hook: uploadDivisionImage => carousel image upload');
            app.uploadComponentImage(req, 'division', app.model.division.get, fields.userData[0].substring(9), files.DivisionImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadDivisionImage', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadDivisionImage(req, fields, files, params, done), done, 'component:write'));
};
