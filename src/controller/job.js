module.exports = app => {
    const menu = {
        parentMenu: { index: 7000, title: 'Việc làm', icon: 'fa-pagelines' },
        menus: {
            7001: { title: 'Danh mục', link: '/user/job/category' },
            7002: { title: 'Việc làm', link: '/user/job/list' },
            7003: { title: 'Chờ duyệt', link: '/user/job/draft' },
        },
    };
    app.permission.add(
        { name: 'job:read', menu },
        { name: 'job:write', menu },
        { name: 'job:registration', menu },
        { name: 'job:roller', menu },
        { name: 'job:import', menu },
        { name: 'job:export', menu },
        { name: 'job:draft', menu },
    );
    app.get('/user/job/category', app.permission.check('category:read'), app.templates.admin);
    app.get('/user/job/list', app.permission.check('job:read'), app.templates.admin);
    app.get('/user/job/edit/:_id', app.permission.check('job:read'), app.templates.admin);
    app.get('/user/job/registration/:_id', app.permission.check('job:read'), app.templates.admin);
    app.get('/user/job/draft', app.permission.check('job:draft'), app.templates.admin);
    app.get('/user/job/draft/edit/:_id', app.permission.check('job:draft'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/job/page/:pageNumber/:pageSize', app.permission.check('job:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.job.getPage(pageNumber, pageSize, {}, (error, page) => {
            const response = {};
            if (error || page == null) {
                response.error = 'Danh sách việc làm không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                response.page = app.clone(page, { list });
            }
            res.send(response);
        });
    });

    app.get('/api/draft/job/:userId', app.permission.check('job:read'), (req, res) => {
        userId = req.params.userId;
        app.model.draft.userGet('job', userId, (error, page) => {
            if (error) respone.error = 'Danh sách mẫu việc làm không sẵn sàng!';
            res.send(page);
        });
    });

    app.get('/api/draft-job/page/:pageNumber/:pageSize', app.permission.check('job:draft'), (req, res) => {
        const data = req.session.user;
        let condition = data.permissions.includes('job:write') ? { documentType: 'job' } : { documentType: 'job', editorId: data._id }
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.draft.getPage(pageNumber, pageSize, condition, (error, page) => {
            const respone = {};
            if (error || page == null) {
                respone.error = 'Danh sách việc làm không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                respone.page = app.clone(page, { list });
            }
            res.send(respone);
        });
    });

    app.post('/api/job/default', app.permission.check('job:write'), (req, res) =>
        app.model.job.create({ title: 'Việc làm', active: false }, (error, item) => res.send({ error, item })));

    app.delete('/api/job/delete', app.permission.check('job:write'), (req, res) => app.model.job.delete(req.body._id, error => res.send({ error })));

    app.delete('/api/draft-job', app.permission.check('job:draft'), (req, res) =>
        app.model.draft.delete(req.body._id, error => res.send({ error })));

    app.post('/api/job/draft', app.permission.check('job:draft'), (req, res) =>
        app.model.draft.create(req.body, (error, item) => res.send({ error, item })))

    app.put('/api/job/swap', app.permission.check('job:write'), (req, res) => {
        const isMoveUp = req.body.isMoveUp.toString() == 'true';
        app.model.job.swapPriority(req.body._id, isMoveUp, error => res.send({ error }));
    });

    app.put('/api/job/update', app.permission.check('job:write'), (req, res) =>
        app.model.job.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.get('/api/job/item/:jobId', app.permission.check('job:read'), (req, res) => app.model.category.getAll({ type: 'job', active: true }, (error, categories) => {
        if (error) {
            res.send({ error });
        } else {
            app.model.job.get(req.params.jobId, (error, item) => {
                res.send({ error, categories: categories.map(item => ({ id: item._id, text: item.title })), item });
            });
        }
    }));

    app.get('/api/draft-job/toJob/:draftId', app.permission.check('job:write'), (req, res) => {
        app.model.draft.toJob(req.params.draftId, (error, item) => res.send({ error, item }))
    })

    app.get('/api/job/item-question/:jobId', app.permission.check('job:read'), (req, res) => app.model.job.getAllQuestion(req.params.jobId, (error, item) => {
        res.send({ error, item });
    }));

    app.get('/api/draft-job/item/:jobId', app.permission.check('job:draft'), (req, res) => {
        app.model.category.getAll({ type: 'job', active: true }, (error, categories) => {
            if (error || categories == null) {
                res.send({ error: 'Lỗi khi lấy danh mục!' });
            } else {
                app.model.draft.get(req.params.jobId, (error, item) => {
                    res.send({ error, categories: categories.map(item => ({ id: item._id, text: item.title })), item });
                });
            }
        });
    });

    app.put('/api/draft-job', app.permission.check('job:draft'), (req, res) => app.model.draft.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/job/page/:pageNumber/:pageSize', (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            today = new Date(),
            user = req.session.user;

        const condition = {
            $or: [{ startPost: null }, { startPost: { $exists: false } }, { startPost: { $lte: today } },],
            $or: [{ stopPost: null }, { stopPost: { $exists: false } }, { stopPost: { $gte: today } },],
            active: true
        };
        if(!user) condition.isInternal= false;

        app.model.job.getPage(pageNumber, pageSize, condition, (error, page) => {
            const respone = {};
            if (error || page == null) {
                respone.error = 'Danh sách việc làm không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                respone.page = app.clone(page, { list });
            }
            res.send(respone);
        });
    });

    app.get('/job/page/:pageNumber/:pageSize/:categoryType', (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            today = new Date(),
            user = req.session.user;
        const condition = {
            categories: req.params.categoryType,
            $or: [{ startPost: null }, { startPost: { $exists: false } }, { startPost: { $lte: today } },],
            $or: [{ stopPost: null }, { stopPost: { $exists: false } }, { stopPost: { $gte: today } },],
            active: true
        };
        if(!user) condition.isInternal= false;

        app.model.job.getPage(pageNumber, pageSize, condition, (error, page) => {
            const respone = {};
            if (error || page == null) {
                respone.error = 'Danh sách việc làm không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                respone.page = app.clone(page, { list });
            }
            res.send(respone);
        });
    });

    const readJob = (req, res, error, item) => {
        if (item) {
            item.content = app.language.parse(req, item.content);
        }
        res.send({ error, item })
    };

    app.get('/job/item/id/:jobId', (req, res) =>
        app.model.job.readById(req.params.jobId, (error, item) => readJob(req, res, error, item)));
    app.get('/job/item/link/:jobLink', (req, res) =>
        app.model.job.readByLink(req.params.jobLink, (error, item) => readJob(req, res, error, item)));

    app.get('/job/item-question/id/:jobId', (req, res) => app.model.job.getAllQuestion(req.params.jobId, (error, item) => {
        res.send({ error, item });
    }));

    app.get('/job/item-question/link/:link', (req, res) => app.model.job.getAllQuestionByLink(req.params.link, (error, item) => {
        res.send({ error, item });
    }));

    app.put('/job/item/check-link', (req, res) => app.model.job.getByLink(req.body.link, (error, item) =>
        res.send({ error: error ? 'Lỗi hệ thống' : (item == null || item._id == req.body._id) ? null : 'Link không hợp lệ' })));


    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    app.createFolder(
        app.path.join(app.publicPath, '/img/draft'),
        app.path.join(app.publicPath, '/img/draft/job'),
        app.path.join(app.publicPath, '/img/job'),
        app.path.join(app.publicPath, '/img/draftJob')

    );

    const uploadJobCkEditor = (req, fields, files, params, done) => {
        if (files.upload && files.upload.length > 0 && fields.ckCsrfToken && params.Type == 'File' && params.category == 'job') {
            console.log('Hook: uploadJobCkEditor => ckEditor upload');

            const srcPath = files.upload[0].path;
            app.jimp.read(srcPath).then(image => {
                app.fs.unlinkSync(srcPath);

                if (image) {
                    if (image.bitmap.width > 1024) image.resize(1024, app.jimp.AUTO);
                    const url = '/img/job/' + app.path.basename(srcPath);
                    image.write(app.path.join(app.publicPath, url), error => {
                        done({ uploaded: error == null, url, error: { message: error ? 'Upload has errors!' : '' } });
                    });
                } else {
                    done({ uploaded: false, error: 'Upload has errors!' });
                }
            });
        } else {
            done();
        }
    };
    app.uploadHooks.add('uploadJobCkEditor', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadJobCkEditor(req, fields, files, params, done), done, 'job:write'));

    const uploadJobAvatar = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('job:') && files.JobImage && files.JobImage.length > 0) {
            console.log('Hook: uploadJobAvatar => job image upload');
            app.uploadComponentImage(req, 'job', app.model.job.get, fields.userData[0].substring(4), files.JobImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadJobAvatar', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadJobAvatar(req, fields, files, params, done), done, 'job:write'));

    const uploadJobDraftAvatar = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('job:') && files.JobDraftImage && files.JobDraftImage.length > 0) {
            console.log('Hook: uploadJobDraftAvatar => job draft image upload');
            app.uploadComponentImage(req, 'draftJob', app.model.draft.get, fields.userData[0].substring(4), files.JobDraftImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadJobDraftAvatar', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadJobDraftAvatar(req, fields, files, params, done), done, 'job:draft'));

};
