module.exports = app => {
    const responseError = (req, res) => {
        if (req.method.toLowerCase() === 'get') { // is get method
            if (req.originalUrl.startsWith('/api')) {
                res.send({ error: req.session.user ? 'request-permissions' : 'request-login' });
            } else {
                res.redirect(req.session.user ? '/request-permissions' : '/request-login');
            }
        } else {
            res.send({ error: `You don't have permission!` });
        }
    };

    const checkPermission = (reqField) => {
        return (req, res, next) => {
            if (req.session.user) {
                if (req.session.user.permissions && req.session.user.permissions.contains([req[reqField].permission])) {
                    next();
                } else {
                    responseError(req, res);
                }
            } else if (!req.params.permissions) {
                next();
            } else {
                responseError(req, res);
            }
        };
    };

    app.get('/api/answer/item/:permission/:answerId', checkPermission('params'), (req, res) =>
        app.model.answer.get(req.params.answerId, (error, item) => res.send({ error, item })));

    app.get('/api/answer-page/:postId/:permission/:field/:pageNumber/:pageSize', checkPermission('params'), (req, res) => {
        const postId = req.params.postId, field = req.params.field,
            pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.answer.getPage(pageNumber, pageSize, { postId, field }, (error, page) => res.send({
            error: error || page == null ? 'Danh sách câu trả lời không sẵn sàng!' : '',
            page
        }));
    });

    app.post('/api/answer', checkPermission('body'), (req, res) => {
        const body = req.body.newData;
        app.model.answer.get({ user: body.user, postId: body.postId, field: body.field }, (error, answer) => {
           if (error) {
               res.send({error});
           } else if (answer) {
               res.send({ exist: true });
           } else {
               app.model.answer.create(body, (error, item) => res.send({ error, item }));
           }
        });
    });

    app.post('/api/answer/import', checkPermission('body'), (req, res) => {
        const participants = req.session.participants ? req.session.participants : [], { postId, field, questions } = req.body;
        const createAnswer = (participants, index = 0) => {
            if (index === participants.length) {
                req.session.participants = null;
                res.send({error: null, success: true});
            } else {
                app.model.user.create(participants[index], (error, user) => {
                    if (user == null) {
                        res.send({error: 'Đã xảy ra lỗi', success: false});
                    } else {
                        if (error && error === 'Email bạn dùng đã được đăng ký!' && (!user.organizationId || user.organizationId === '')) {
                            user.organizationId = participants[index].organizationId;
                            user.save();
                        }
                        const answerData = {
                            user: user._id, postId, field, attendance: false,
                            record: (questions ? questions : []).map(question => ({
                                    questionId: question._id,
                                    answer: (question.typeName === 'choice' || question.typeName === 'multiChoice') ? question.typeValue[0] : (question.typeName === 'date' ? app.date.viDateFormat(new Date()) : question.defaultAnswer)
                                })
                            )
                        };
                        app.model.answer.create(answerData, (error, item) => {
                            if (error || !item) {
                                res.send({error: 'Đã xảy ra lỗi', success: false});
                            } else {
                                createAnswer(participants, index + 1);
                            }
                        });
                    }
                });
            }
        };
        createAnswer(participants);
    });

    app.get('/api/answer/export/:postType/:postId/:field/:permission', checkPermission('params'), (req, res) => {
        const { postType, postId, field } = req.params;
        app.model[postType].getQuestionForm({ _id: postId }, '_id ' + field, field,(error, post) => {
            if (!error && post) {
                app.model.answer.getAll({ postId, field }, (error, items) => {
                    if (error) {
                        res.send({error});
                    } else {
                        const workbook = app.excel.create(),
                            registrationQuestions = post[field],
                            worksheet = workbook.addWorksheet('RegistrationResult');
                        let cells = [
                            {cell: 'A1', value: '#', bold: true, border: '1234'},
                            {cell: 'B1', value: 'Thời gian đăng ký', bold: true, border: '1234'},
                            {cell: 'C1', value: 'Họ và tên lót', bold: true, border: '1234'},
                            {cell: 'D1', value: 'Tên', bold: true, border: '1234'},
                            {cell: 'E1', value: 'MSSV', bold: true, border: '1234'},
                            {cell: 'F1', value: 'Có mặt', bold: true, border: '1234'},
                        ];

                        worksheet.columns = [
                            {header: '#', key: 'number', width: 5},
                            {header: 'Thời gian đăng ký', key: 'text', width: 20},
                            {header: 'Họ và tên lót', key: 'text', width: 30},
                            {header: 'Tên', key: 'text', width: 10},
                            {header: 'MSSV', key: 'text', width: 10},
                            {header: 'Có mặt', key: 'text', width: 15},
                        ];

                        registrationQuestions.forEach((question, index) => {
                            cells.push({cell: app.excel.numberToExcelColumn(index + 7) + '1', value: question.title, bold: true, border: '1234'});
                        });

                        for (let index = 0; index <= items.length; index++) {
                            if (index == items.length) {
                                app.excel.write(worksheet, cells);
                                app.excel.attachment(workbook, res);
                            } else {
                                const user = items[index].user;
                                cells.push({ cell: 'A' + (index + 2), border: '1234', number: (index + 1) });
                                cells.push({ cell: 'B' + (index + 2), border: '1234',
                                    value: app.date.viDateFormat(items[index].answeredDate) + ' ' + app.date.viTimeFormat(items[index].answeredDate)
                                });
                                if (user && user != {}) {
                                    cells.push({ cell: 'C' + (index + 2), border: '1234', value: user.lastname });
                                    cells.push({ cell: 'D' + (index + 2), border: '1234', value: user.firstname });
                                    cells.push({ cell: 'E' + (index + 2), border: '1234', value: user.organizationId });
                                }

                                cells.push({ cell: 'F' + (index + 2), border: '1234', value: items[index].attendance ? 'x' : '' });
                                let answerValue = {};
                                items[index].record.forEach((record) => {
                                    answerValue[record.questionId] = record.answer;
                                });
                                for (let index2 = 0; index2 <= registrationQuestions.length; index2++) {
                                    if (index2 === registrationQuestions.length) {
                                        break;
                                    } else {
                                        cells.push({cell: app.excel.numberToExcelColumn(index2 + 7) + (index + 2), border: '1234', value: answerValue[registrationQuestions[index2]._id]});
                                    }
                                }
                            }
                        }
                    }
                });
            } else {
                res.send({error});
            }
        });
    });

    app.put('/api/answer', checkPermission('body'), (req, res) => {
        const changes = req.body.changes;
        if (changes.record && changes.record === 'empty') changes.record = [];
        app.model.answer.update(req.body._id, changes, (error, item) => res.send({ error, item }));
    });

    app.delete('/api/answer', checkPermission('body'), (req, res) =>
        app.model.answer.delete(req.body._id, (error) => res.send({ error })));

    // User =========================================================================================================================================
    app.post('/answer', app.permission.check(), (req, res) => {
        const { postType, newData } = req.body;
        const user = req.session.user;
        app.model[postType].get(newData.postId, (error, item) => {
            if (error || !item) {
                res.send({error: 'Invalid ' + postType + ' id!'});
            } else {
                const maxRegisterUsers = item.maxRegisterUsers ? item.maxRegisterUsers : -1;
                app.model.answer.count({postId: newData.postId, field: newData.field}, (error1, total) => {
                   if (error) {
                       res.send({error: 'Count registration has error!'});
                   } else if (maxRegisterUsers != -1 && total >= maxRegisterUsers) {
                       res.send({error: 'Enough number of participants!'});
                   } else {
                       app.model.answer.getCondition({ user: user._id, postId: newData.postId, field: newData.field }, (error, answer) => {
                           if (error) {
                               res.send({error});
                           } else if (answer) {
                               res.send({ exist: true });
                           } else {
                               app.model.answer.create(newData, (error, item) => res.send({ error, item }));
                           }
                       });
                   }
                });
            }
        });
    });

    app.put('/answer/edit', (req, res) =>
        app.model.answer.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.get('/answer/count/:postId/:field', (req, res) => {
        app.model.answer.count({postId: req.params.postId, field: req.params.field}, (error, total) => {
            res.send({error, total});
        })
    });

    // Hook upload files ---------------------------------------------------------------------------------------------------------------------------s
    const registrationImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'registrationImportData' && files.RegistrationImportData && files.RegistrationImportData.length > 0) {
            console.log('Hook: registrationImportData');
            app.importRegistration(req, files.RegistrationImportData[0].path, done);
        }
    };

    app.uploadHooks.add('registrationImportData', (req, fields, files, params, done) => registrationImportData(req, fields, files, params, done));
};
