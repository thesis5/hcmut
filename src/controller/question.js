module.exports = app => {
    const responseError = (req, res) => {
        if (req.method.toLowerCase() === 'get') { // is get method
            if (req.originalUrl.startsWith('/api')) {
                res.send({ error: req.session.user ? 'request-permissions' : 'request-login' });
            } else {
                res.redirect(req.session.user ? '/request-permissions' : '/request-login');
            }
        } else {
            res.send({ error: `You don't have permission!` });
        }
    };

    const checkPermission = () => {
        return (req, res, next) => {
            if (req.session.user) {
                if (req.session.user.permissions && req.session.user.permissions.contains([req.body.permission])) {
                    next();
                } else {
                    responseError(req, res);
                }
            } else if (!req.body.permissions) {
                next();
            } else {
                responseError(req, res);
            }
        };
    };

    app.get('/api/questions/:model/:field/:_id', (req, res) => {
        const { model, field, _id } = req.params;
        app.model[model].getQuestionForm(_id, '_id ' + field, field, (error, item) => {
            res.send({error, item});
        });
    });

    app.post('/api/question/:model/:field/:_id', checkPermission(), (req, res) => {
        const model = req.params.model, field = req.params.field, _id = req.params._id, data = req.body.data;
        app.model.question.create(data, (error, question) => {
            if (error || !question) {
                res.send({error});
            } else {
                app.model[model].pushQuestion({_id}, question._id, field, (error, item) => {
                    res.send({error, item});
                });
            }
        });
    });

    app.put('/api/question/:model/swap', checkPermission(), (req, res) => {
        const model = req.params.model, data = req.body.data, postId = req.body.postId;
        app.model[model].update(postId, data, (error, item) => {
            res.send({error, item});
        });
    });

    app.put('/api/question', checkPermission(), (req, res) => {
        const _id = req.body._id, data = req.body.data;
        app.model.question.update(_id, data, (error, question) => {
            res.send({error, question});
        });
    });

    app.delete('/api/question/:model/:field', checkPermission(), (req, res) => {
        const model = req.params.model, field = req.params.field, {data, postId, _id} = req.body;
        if (data[field] && data[field] == 'empty') data[field] = [];
        app.model[model].update(postId, data, (error, item) => {
            if (error) {
                res.send({error});
            } else {
                app.model.question.delete(_id, error => res.send({error}));
            }
        });
    });
};