module.exports = app => {
    app.get('/api/content/all', app.permission.check('component:read'), (req, res) =>
        app.model.content.getAll((error, items) => res.send({ error, items })));

    app.get('/api/content/item/:contentId', app.permission.check('component:read'), (req, res) =>
        app.model.content.get(req.params.contentId, (error, item) => res.send({ error, item })));

    app.post('/api/content', app.permission.check('component:write'), (req, res) =>
        app.model.content.create({ title: 'Tiêu đề', active: false }, (error, item) => res.send({ error, item })));

    app.put('/api/content', app.permission.check('component:write'), (req, res) =>
        app.model.content.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.delete('/api/content', app.permission.check('component:write'), (req, res) =>
        app.model.content.delete(req.body.id, error => res.send({ error })));


    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    app.createFolder(app.path.join(app.publicPath, '/img/content'));

    app.uploadHooks.add('uploadContentCkEditor', (req, fields, files, params, done) =>
        app.permission.has(req, () => app.uploadCkEditorImage('content', fields, files, params, done), done, 'component:write'));
};