module.exports = app => {
    const menu = {
        parentMenu: { index: 6000, title: 'Sự kiện', icon: 'fa-star' },
        menus: {
            6001: { title: 'Danh mục', link: '/user/event/category' },
            6002: { title: 'Sự kiện', link: '/user/event/list' },
            6003: { title: 'Chờ duyệt', link: '/user/event/draft' },
        },
    };
    app.permission.add(
        { name: 'event:read', menu },
        { name: 'event:write', menu },
        { name: 'event:registration', menu },
        { name: 'event:roller', menu },
        { name: 'event:import', menu },
        { name: 'event:export', menu },
        { name: 'event:draft', menu },
    );
    app.get('/user/event/category', app.permission.check('category:read'), app.templates.admin);
    app.get('/user/event/list', app.permission.check('event:read'), app.templates.admin);
    app.get('/user/event/edit/:_id', app.permission.check('event:read'), app.templates.admin);
    app.get('/user/event/registration/:_id', app.permission.check('event:read'), app.templates.admin);
    app.get('/user/event/draft', app.permission.check('event:draft'), app.templates.admin);
    app.get('/user/event/draft/edit/:_id', app.permission.check('event:draft'), app.templates.admin);

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/event/page/:pageNumber/:pageSize', app.permission.check('event:read'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.event.getPage(pageNumber, pageSize, {}, (error, page) => {
            const response = {};
            if (error || page == null) {
                response.error = 'Danh sách sự kiện không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                response.page = app.clone(page, { list });
            }
            res.send(response);
        });
    });

    app.put('/api/event/swap', app.permission.check('event:write'), (req, res) => {
        const isMoveUp = req.body.isMoveUp.toString() == 'true';
        app.model.event.swapPriority(req.body._id, isMoveUp, error => res.send({ error }));
    });

    app.get('/api/draft/event/:userId', app.permission.check('event:read'), (req, res) => {
        userId = req.params.userId;
        app.model.draft.userGet('event', userId, (error, page) => {
            if (error) respone.error = 'Danh sách mẫu sự kiện không sẵn sàng!';
            res.send(page);
        });
    });

    app.get('/api/draft-event/page/:pageNumber/:pageSize', app.permission.check('event:draft'), (req, res) => {
        const data = req.session.user, condition = data.permissions.includes('event:write') ? { documentType: 'event' } : { documentType: 'event', editorId: data._id };
        const pageNumber = parseInt(req.params.pageNumber), pageSize = parseInt(req.params.pageSize);
        app.model.draft.getPage(pageNumber, pageSize, condition, (error, page) => {
            const response = {};
            if (error || page == null) {
                response.error = 'Danh sách tin tức không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                response.page = app.clone(page, { list });
            }
            res.send(response);
        });
    });

    app.post('/api/event/default', app.permission.check('event:write'), (req, res) => app.model.event.create({ title: 'Sự kiện', active: false }, (error, item) => {
        res.send({ error, item })
    }));

    app.put('/api/event', app.permission.check('event:write'), (req, res) => {
        const changes = req.body.changes;
        if (changes && changes.questions && changes.questions === 'empty') changes.questions = [];
        app.model.event.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item }));
    });

    app.post('/api/event/draft', app.permission.check('event:draft'), (req, res) => app.model.draft.create(req.body, (error, item) => res.send({ error, item })))

    app.delete('/api/draft-event', app.permission.check('event:draft'), (req, res) => app.model.draft.delete(req.body._id, error => res.send({ error })));

    app.delete('/api/event', app.permission.check('event:write'), (req, res) => app.model.event.delete(req.body._id, error => res.send({ error })));

    app.get('/api/draft-event/toEvent/:eventId', app.permission.check('event:write'), (req, res) => app.model.draft.toEvent(req.params.eventId, (error, item) => res.send({ error, item })));

    app.put('/api/draft-event', app.permission.check('event:draft'), (req, res) =>
        app.model.draft.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));

    app.get('/api/event/item/:eventId', app.permission.check('event:read'), (req, res) => app.model.category.getAll({ type: 'event', active: true }, (error, categories) => {
        if (error || categories == null) {
            res.send({ error: 'Lỗi khi lấy danh mục!' });
        } else {
            app.model.event.get(req.params.eventId, (error, item) => {
                res.send({
                    error,
                    categories: categories.map(item => ({ id: item._id, text: item.title })),
                    item
                });
            });
        }
    }));
    app.get('/api/draft-event/item/:eventId', app.permission.check('event:draft'), (req, res) => {
        app.model.category.getAll({ type: 'event', active: true }, (error, categories) => {
            if (error || categories == null) {
                res.send({ error: 'Lỗi khi lấy danh mục!' });
            } else {
                app.model.draft.get(req.params.eventId, (error, item) => {
                    res.send({
                        error, categories: categories.map(item => ({ id: item._id, text: item.title })), item
                    });
                });
            }
        });
    });


    app.get('/api/event/item-question/:eventId', app.permission.check('event:read'), (req, res) => app.model.event.getAllQuestion(req.params.eventId, (error, item) => {
        res.send({ error, item });
    }));

    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/event/page/:pageNumber/:pageSize', (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            today = new Date(),
            user = req.session.user;
        const condition = {
            $or: [{ startPost: null }, { startPost: { $exists: false } }, { startPost: { $lte: today } },],
            $or: [{ stopPost: null }, { stopPost: { $exists: false } }, { stopPost: { $gte: today } },],
            active: true
        };
        if(!user) condition.isInternal= false;

        app.model.event.getPage(pageNumber, pageSize, condition, (error, page) => {
            const respone = {};
            if (error || page == null) {
                respone.error = 'Danh sách sự kiện không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                respone.page = app.clone(page, { list });
            }
            res.send(respone);
        });
    });

    app.get('/event/page/:pageNumber/:pageSize/:categoryType', (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            today = new Date(),
            user = req.session.user;
        const condition = {
            categories: req.params.categoryType,
            $or: [{ startPost: null }, { startPost: { $exists: false } }, { startPost: { $lte: today } },],
            $or: [{ stopPost: null }, { stopPost: { $exists: false } }, { stopPost: { $gte: today } },],
            active: true
        };
        if(!user) condition.isInternal= false;

        app.model.event.getPage(pageNumber, pageSize, condition, (error, page) => {
            const respone = {};
            if (error || page == null) {
                respone.error = 'Danh sách sự kiện không sẵn sàng!';
            } else {
                let list = page.list.map(item => app.clone(item, { content: null }));
                respone.page = app.clone(page, { list });
            }
            res.send(respone);
        });
    });

    const readEvent = (req, res, error, item) => {
        if (item) {
            item.content = app.language.parse(req, item.content);
        }
        res.send({ error, item })
    };
    app.get('/event/item/id/:eventId', (req, res) => app.model.event.readById(req.params.eventId, (error, item) => {
        readEvent(req, res, error, item)
    }));
    app.get('/event/item/link/:link', (req, res) => app.model.event.readByLink(req.params.link, (error, item) => {
        readEvent(req, res, error, item)
    }));

    app.get('/event/item-question/id/:eventId', (req, res) => app.model.event.getAllQuestion(req.params.eventId, (error, item) => {
        res.send({ error, item });
    }));

    app.get('/event/item-question/link/:link', (req, res) => app.model.event.getAllQuestionByLink(req.params.link, (error, item) => {
        res.send({ error, item });
    }));

    app.put('/event/item/check-link', (req, res) => app.model.event.getByLink(req.body.link, (error, item) => {
        res.send({
            error: error ? 'Lỗi hệ thống' : (item == null || item._id == req.body._id) ? null : 'Link không hợp lệ'
        });
    }));


    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    app.createFolder(
        app.path.join(app.publicPath, '/img/draft'),
        app.path.join(app.publicPath, '/img/draft/event'),
        app.path.join(app.publicPath, '/img/event'),
        app.path.join(app.publicPath, '/img/draftEvent')
    );

    const uploadEventCkEditor = (req, fields, files, params, done) => {
        if (files.upload && files.upload.length > 0 && fields.ckCsrfToken && params.Type == 'File' && params.category == 'event') {
            console.log('Hook: uploadEventCkEditor => ckEditor upload');

            const srcPath = files.upload[0].path;
            app.jimp.read(srcPath).then(image => {
                app.fs.unlinkSync(srcPath);

                if (image) {
                    if (image.bitmap.width > 1024) image.resize(1024, app.jimp.AUTO);
                    const url = '/img/event/' + app.path.basename(srcPath);
                    image.write(app.path.join(app.publicPath, url), error => {
                        done({ uploaded: error == null, url, error: { message: error ? 'Upload has errors!' : '' } });
                    });
                } else {
                    done({ uploaded: false, error: 'Upload has errors!' });
                }
            });
        } else {
            done();
        }
    };
    app.uploadHooks.add('uploadEventCkEditor', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadEventCkEditor(req, fields, files, params, done), done, 'event:write'));

    const uploadEventAvatar = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('event:') && files.EventImage && files.EventImage.length > 0) {
            console.log('Hook: uploadEventAvatar => event image upload');
            app.uploadComponentImage(req, 'event', app.model.event.get, fields.userData[0].substring(6), files.EventImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadEventAvatar', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadEventAvatar(req, fields, files, params, done), done, 'event:write'));
    const uploadEventDraftAvatar = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('event:') && files.EventDraftImage && files.EventDraftImage.length > 0) {
            console.log('Hook: uploadEventDraftAvatar => event draft image upload');
            app.uploadComponentImage(req, 'draftEvent', app.model.draft.get, fields.userData[0].substring(6), files.EventDraftImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadEventDraftAvatar', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadEventDraftAvatar(req, fields, files, params, done), done, 'event:draft'));
};
