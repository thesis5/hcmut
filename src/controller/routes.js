module.exports = app => {
    app.permission.add(
        { name: 'dashboard:standard', menu: { parentMenu: { index: 1000, title: 'Dashboard', icon: 'fa-dashboard', link: '/user/dashboard' } }, },
        { name: 'system:settings', menu: { parentMenu: { index: 2000, title: 'Cấu hình', icon: 'fa-cog' }, menus: { 2010: { title: 'Thông tin chung', link: '/user/settings' } } }, }
    );

    app.get('/user/dashboard', app.permission.check('dashboard:standard'), app.templates.admin);
    app.get('/user/settings', app.permission.check('system:settings'), app.templates.admin);

    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    ['/index.htm(l)?', '/contact(.htm(l)?)?', '/registered(.htm(l)?)?', '/404.htm(l)?', '/request-permissions(/:roleId?)', '/request-login',
        '/active-user/:userId', '/forgot-password/:userId/:userToken',
        '/news/item/:newsId', '/tintuc/:link',
        '/event/item/:eventId', '/sukien/:link', '/event/registration/item/:eventId', '/sukien/dangky/:link',
        '/job/item/:jobId', '/vieclam/:link', '/job/registration/item/:jobId', '/vieclam/dangky/:link',
        '/division/item/:_id', '/division/staff/:_id',
        '/personnel/:firstPartOfEmail',
    ].forEach(route => app.get(route, app.templates.home));


    // API ------------------------------------------------------------------------------------------------------------------------------------------
    app.put('/api/system', app.permission.check('system:settings'), (req, res) => {
        const changes = {};

        if (req.body.password) {
            changes.emailPassword = req.body.password;
            app.model.setting.set(changes, error => {
                if (error) {
                    res.send({ error: 'Lỗi khi cập nhật mật khẩu email!' });
                } else {
                    app.data.emailPassword = req.body.emailPassword;
                    res.send(app.data);
                }
            });
        } else {
            if (req.body.address != null || req.body.address == '') {
                changes.address = req.body.address.trim();
            }
            if (req.body.email && req.body.email != '') {
                changes.email = req.body.email.trim();
            }
            if (req.body.mobile != null || req.body.mobile == '') {
                changes.mobile = req.body.mobile.trim();
            }
            if (req.body.fax != null || req.body.fax == '') {
                changes.fax = req.body.fax.trim();
            }
            if (req.body.facebook != null || req.body.facebook == '') {
                changes.facebook = req.body.facebook.trim();
            }
            if (req.body.youtube != null || req.body.youtube == '') {
                changes.youtube = req.body.youtube.trim();
            }
            if (req.body.twitter != null || req.body.twitter == '') {
                changes.twitter = req.body.twitter.trim();
            }
            if (req.body.instagram != null || req.body.instagram == '') {
                changes.instagram = req.body.instagram.trim();
            }
            if (req.body.latitude != null || req.body.latitude == '') {
                changes.latitude = req.body.latitude.trim();
            }
            if (req.body.longitude != null || req.body.longitude == '') {
                changes.longitude = req.body.longitude.trim();
            }

            app.model.setting.set(changes, error => {
                if (error) {
                    res.send({ error: 'Lỗi khi cập nhật mật khẩu email!' });
                } else {
                    if (changes.email) {
                        app.data.email = changes.email;
                    }
                    app.data = app.clone(app.data, changes);
                    res.send(app.data);
                }
            });
        }
    });

    app.get('/api/state', (req, res) => {
        const data = app.clone(app.data);
        delete data.emailPassword;

        if (app.isDebug) data.isDebug = true;
        if (req.session.user) data.user = req.session.user;

        app.model.menu.getAll({ active: true }, (error, menus) => {
            if (menus) {
                data.menus = menus.slice();
                data.menus.forEach(menu => {
                    menu.content = '';
                    if (menu.submenus) {
                        menu.submenus.forEach(submenu => submenu.content = '');
                    }
                });
            }
            res.send(data);
        });
    });

    app.get('/api/menu', (req, res) => {
        let pathname = app.url.parse(req.headers.referer).pathname;
        if (pathname.length > 1 && pathname.endsWith('/')) pathname = pathname.substring(0, pathname.length - 1);
        const menu = app.menus[pathname];
        if (menu) {
            const menuComponents = [];
            const getComponent = (index, componentIds, components, done) => {
                if (index < componentIds.length) {
                    app.model.component.get(componentIds[index], (error, component) => {
                        if (error || component == null) {
                            getComponent(index + 1, componentIds, components, done);
                        } else {
                            component = app.clone(component);
                            component.components = [];
                            components.push(component);

                            const getNextComponent = view => {
                                if (view) component.view = view;
                                if (component.componentIds) {
                                    getComponent(0, component.componentIds, component.components, () =>
                                        getComponent(index + 1, componentIds, components, done));
                                } else {
                                    getComponent(index + 1, componentIds, components, done);
                                }
                            };

                            if (component.viewType && component.viewId) {
                                const viewType = component.viewType;
                                if (component.viewId && (viewType == 'carousel' || viewType == 'content' || viewType == 'news' || viewType == 'event' || viewType == 'job' || viewType == 'photoBooth')) {
                                    app.model[viewType].get(component.viewId, (error, item) => getNextComponent(item));
                                } else {
                                    getNextComponent();
                                }
                            } else {
                                getNextComponent();
                            }
                        }
                    });
                } else {
                    done();
                }
            };

            if (menu.component) {
                res.send(menu.component);
            } else if (menu.componentId) {
                getComponent(0, [menu.componentId], menuComponents, () => {
                    menu.component = menuComponents[0];
                    res.send(menu.component);
                });
            } else {
                res.send({ error: 'Menu không hợp lệ!' });
            }
        } else {
            res.send({ error: 'Link không hợp lệ!' });
        }
    });

    app.delete('/api/clear-session', app.permission.check(), (req, res) => {
        const sessionName = req.body.sessionName;
        req.session[sessionName] = null;
        res.send({});
    });


    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------s
    const uploadSettingImage = (req, fields, files, params, done) => {
        if (files.SettingImage && files.SettingImage.length > 0) {
            console.log('Hook: uploadSettingImage => ' + fields.userData);
            const srcPath = files.SettingImage[0].path;

            if (fields.userData == 'logo') {
                app.deleteImage(app.data.logo);
                let destPath = '/img/favicon' + app.path.extname(srcPath);
                app.fs.rename(srcPath, app.path.join(app.publicPath, destPath), error => {
                    if (error == null) {
                        destPath += '?t=' + (new Date().getTime()).toString().slice(-8);
                        app.model.setting.set({ logo: destPath }, error => {
                            if (error == null) app.data.logo = destPath;
                            done({ image: app.data.logo, error });
                        });
                    } else {
                        done({ error });
                    }
                });
            } else if (fields.userData == 'footer' && files.SettingImage && files.SettingImage.length > 0) {
                app.deleteImage(app.data.footer);
                let destPath = '/img/footer' + app.path.extname(srcPath);
                app.fs.rename(srcPath, app.path.join(app.publicPath, destPath), error => {
                    if (error == null) {
                        destPath += '?t=' + (new Date().getTime()).toString().slice(-8);
                        app.model.setting.set({ footer: destPath }, error => {
                            if (error == null) app.data.footer = destPath;
                            done({ image: app.data.footer, error });
                        });
                    } else {
                        done({ error });
                    }
                });
            } else if (fields.userData == 'map' && files.SettingImage && files.SettingImage.length > 0) {
                app.deleteImage(app.data.map);
                let destPath = '/img/map' + app.path.extname(srcPath);
                app.fs.rename(srcPath, app.path.join(app.publicPath, destPath), error => {
                    if (error == null) {
                        destPath += '?t=' + (new Date().getTime()).toString().slice(-8);
                        app.model.setting.set({ map: destPath }, error => {
                            if (error == null) app.data.map = destPath;
                            done({ image: app.data.map, error });
                        });
                    } else {
                        done({ error });
                    }
                });
            }
        }
    };
    app.uploadHooks.add('uploadSettingImage', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadSettingImage(req, fields, files, params, done), done, 'system:settings'));

    app.redirectToWebpackServer();
};
