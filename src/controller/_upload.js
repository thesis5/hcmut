module.exports = app => {

    app.adminUploadImage = (dataName, getItem, dataId, srcPath, req, res) => {
        if (dataId == 'new') {
            let imageLink = app.path.join('/img/draft', app.path.basename(srcPath)),
                sessionPath = app.path.join(app.publicPath, imageLink);
            app.fs.rename(srcPath, sessionPath, error => {
                if (error == null) req.session[dataName + 'Image'] = sessionPath;
                res.send({ error, image: imageLink });
            });
        } else {
            req.session[dataName + 'Image'] = null;
            if (getItem) {
                getItem(dataId, (error, dataItem) => {
                    if (error || dataItem == null) {
                        res.send({ error: 'Invalid Id!' });
                    } else {
                        app.deleteImage(dataItem.image);

                        dataItem.image = '/img/' + dataName + '/' + dataItem._id + app.path.extname(srcPath);
                        app.fs.rename(srcPath, app.path.join(app.publicPath, dataItem.image), error => {
                            if (error) {
                                res.send({ error });
                            } else {
                                dataItem.image += '?t=' + (new Date().getTime()).toString().slice(-8);
                                dataItem.save(error => {
                                    if (dataName == 'user') {
                                        dataItem = app.clone(dataItem, { password: '' });
                                        if (req.session.user && req.session.user._id == dataItem._id) {
                                            req.session.user.image = dataItem.image;
                                        }
                                    }

                                    if (error == null) app.io.emit(dataName + '-changed', dataItem);
                                    res.send({
                                        error,
                                        item: dataItem,
                                        image: dataItem.image,
                                    });
                                });
                            }
                        });
                    }
                });
            } else {
                const image = '/img/' + dataName + '/' + dataId + app.path.extname(srcPath);
                app.fs.rename(srcPath, app.path.join(app.publicPath, image), error => res.send({ error, image }));
            }
        }
    };

    // ============================================================================================
    // app.post('/admin/upload', (req, res) => {
    //     app.getUploadForm().parse(req, (error, fields, files) => {
    //         console.log('Upload', fields, files);
    //         if (error || fields == null || files == null) {
    //             res.send({ error });

    //         } else if (fields.userData == 'projectFile' && files.projectFile && files.projectFile.length > 0) {
    //             adminProjectFile(files.projectFile[0].path, req, res);
    //         } else if (fields.userData == 'publicationFile' && files.publicationFile && files.publicationFile.length > 0) {
    //             adminPublicationFile(files.publicationFile[0].path, req, res);

    //         } else {
    //             res.send({ error: 'Error' });
    //         }
    //     });
    // });

    app.createFolder(app.assetPath + '/photo-booth');
    app.createFolder(app.assetPath + '/photo-booth-thumbnail');
    app.createFolder(app.publicPath + '/photo-booth');
    app.post('/upload', (req, res) => {
        app.getUploadForm().parse(req, (error, fields, files) => {
            if (error) {
                res.send({ error });
            } else if (files.photo && files.photo.length > 0) {
                console.log('Upload image');
                app.tokenAuth.verify(fields.token[0], (error, decoded) => {
                    if (error || decoded == null) {
                        res.send({ error: 'Invalid token!' });
                    } else {
                        let photo = files.photo[0],
                            thumbnailFolder = app.path.join(app.assetPath, 'photo-booth-thumbnail', decoded._id),
                            destFolder = app.path.join(app.assetPath, 'photo-booth', decoded._id),
                            thumbnailPath = app.path.join(thumbnailFolder, photo.originalFilename),
                            destPath = app.path.join(destFolder, photo.originalFilename);
                        app.createFolder(thumbnailFolder);
                        app.createFolder(destFolder);

                        app.jimp.read(photo.path).then(thumbnailFile => {
                            if (thumbnailFile) {
                                thumbnailFile.resize(256, app.jimp.AUTO).write(thumbnailPath);
                                app.fs.rename(photo.path, destPath, error => {
                                    if (error == null) {
                                        app.io.emit('photo-booth-image-changed', 'add', decoded._id, photo.originalFilename);
                                    }
                                    res.send({ error });
                                });
                            } else {
                                res.send({ error: 'Create thumbnail has errors!' });
                            }
                        });
                    }
                });
            } else {
                res.send({
                    error: 'Error'
                });
            }
        });
    });
};
