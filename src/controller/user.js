module.exports = app => {
    const userMenu = {
        parentMenu: { index: 2000, title: 'Cấu hình', icon: 'fa-cog' },
        menus: {
            2060: { title: 'Người dùng', link: '/user/user' },
        },
    };
    app.permission.add(
        { name: 'user:read', menu: userMenu },
        { name: 'user:write', menu: userMenu },
    );

    app.get('/user/user', app.permission.check('user:read'), app.templates.admin);
    app.get('/user/staff/item/:staffId', app.permission.check('user:read'), app.templates.admin);

    app.get('/api/user-search/:email', app.permission.check('user:read'), (req, res) => app.model.user.get({ email: req.params.email }, (error, user) => {
        res.send({ error, user: user ? app.clone(user, { password: '', token: '', tokenDate: '' }) : null });
    }));

    // APIs -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/user/page/:pageNumber/:pageSize', app.permission.check('user:read'), (req, res) => {
        let pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize),
            condition = req.query.condition ? req.query.condition : {},
            pageCondition = {};
        try {
            if (condition.searchText) {
                const value = { $regex: `.*${condition.searchText}.*`, $options: 'i' };
                pageCondition['$or'] = [
                    { facebook: value },
                    { phoneNumber: value },
                    { organizationId: value },
                    { email: value },
                    { firstname: value },
                    { lastname: value },
                ];
            }
            if (condition.isStudent) {
                if (pageCondition['$or']) {
                    pageCondition['$or'].push({ isStudent: true });
                } else {
                    pageCondition['$or'] = { isStudent: true };
                }
            }
            if (condition.isStaff) {
                if (pageCondition['$or']) {
                    pageCondition['$or'].push({ isStaff: true });
                } else {
                    pageCondition['$or'] = { isStaff: true };
                }
            }
            app.model.user.getPage(pageNumber, pageSize, pageCondition, (error, page) => res.send({ error, page }));
        } catch (error) {
            res.send({ error });
        }
    });

    app.get('/api/user/all', app.permission.check('user:read'), (_, res) => app.model.user.getAll((error, items) => res.send({ error, items })));

    app.get('/api/staff/item/:_id', app.permission.check('user:read'), (req, res) => app.model.staff.getUser(req.params._id, (error, staff) => res.send({ error, staff })));

    app.post('/api/user', app.permission.check('user:write'), (req, res) => {
        const data = req.body.user;
        if (!app.isBKer(data.email)) {
            data.password = app.randomPassword(8);
        }
        if (data.divisions && data.divisions == 'empty') data.divisions = [];
        if (data.roles == 'empty') data.roles = [];
        app.model.user.create(data, (error, user) => {
            res.send({ error, user });

            if (user && !app.isBKer(user.email)) {
                app.model.setting.get(['emailCreateMemberByAdminTitle', 'emailCreateMemberByAdminText', 'emailCreateMemberByAdminHtml'], result => {
                    let url = req.protocol + '://' + req.get('host') + '/active-user/' + user._id,
                        mailTitle = result.emailCreateMemberByAdminTitle,
                        mailText = result.emailCreateMemberByAdminText.replaceAll('{name}', user.firstname + ' ' + user.lastname)
                            .replaceAll('{firstname}', user.firstname).replaceAll('{lastname}', user.lastname)
                            .replaceAll('{email}', user.email).replaceAll('{password}', data.password).replaceAll('{url}', url),
                        mailHtml = result.emailCreateMemberByAdminHtml.replaceAll('{name}', user.firstname + ' ' + user.lastname)
                            .replaceAll('{firstname}', user.firstname).replaceAll('{lastname}', user.lastname)
                            .replaceAll('{email}', user.email).replaceAll('{password}', data.password).replaceAll('{url}', url);
                    app.email.sendEmail(app.data.email, app.data.emailPassword, user.email, [], mailTitle, mailText, mailHtml, null);
                });
            }
        });
    });

    app.post('/api/multi-staff', app.permission.check('user:write'), (req, res) => {
        const staffs = req.session.staffs ? req.session.staffs : [];
        const errorList = [];
        let hasEmail = 0;
        for (let index = 0; index <= staffs.length; index++) {
            if (index < staffs.length) {
                const currentStaff = staffs[index];
                const email = currentStaff['E_mail'], lastname = currentStaff['HO'], firstname = currentStaff['TEN'], organizationId = currentStaff['shcc'];
                const createOrUpdateStaff = (userId) => app.model.staff.getByShcc(organizationId, (error, staff) => {
                    currentStaff.userId = userId;
                    if (error) {
                        res.send({ error });
                    } else if (staff) {
                        app.model.staff.update(userId, currentStaff);
                    } else {
                        app.model.staff.create(currentStaff);
                    }
                });

                if (email && email != '') {
                    hasEmail++;
                    app.model.user.get({ email }, (error, user) => {
                        if (error) {
                            res.send({ error });
                        } else if (user) {
                            user.lastname = lastname;
                            user.firstname = firstname;
                            user.save();
                            createOrUpdateStaff(user._id);
                        } else {
                            app.model.user.create({ email, firstname, lastname, active: true, isStaff: true }, (error, user) => {
                                if (error && user == null) {
                                    errorList.push(`Create ${currentStaff.lastname} ${currentStaff.firstname} has error\n`);
                                } else {
                                    user.organizationId = organizationId;
                                    user.isStaff = true;
                                    user.active = true;
                                    user.save((error, user) => {
                                        if (error) {
                                            errorList.push(`Save ${currentStaff.lastname} ${currentStaff.firstname} has error\n`);
                                        } else {
                                            createOrUpdateStaff(user._id);
                                        }
                                    })
                                }
                            });
                        }
                    });
                } else {
                    createOrUpdateStaff(null);
                }
            } else {
                res.send({ error: errorList });
            }
        }
    });

    app.put('/api/user', app.permission.check('user:write'), (req, res) => {
        const changes = req.body.changes;
        if (changes.divisions && changes.divisions == 'empty') changes.divisions = [];
        if (changes.roles && changes.roles == 'empty') changes.roles = [];

        app.model.role.get({ name: 'admin' }, (error, adminRole) => {
            if (error || adminRole == null) {
                res.send({ error: 'System has errors!' });
            } else {
                app.model.user.get(req.body._id, (error, user) => {
                    if (error || user == null) {
                        res.send({ error: 'System has errors!' });
                    } else {
                        if (user.email == app.defaultAdminEmail) {
                            changes.active = true;
                            changes.roles = [adminRole._id];
                            delete changes.email;
                        }

                        app.model.user.update(req.body._id, changes, (error, user) => {
                            if (error) {
                                res.send({ error });
                            } else {
                                if (changes.password) {
                                    app.model.setting.get(['emailNewPasswordTitle', 'emailNewPasswordText', 'emailNewPasswordHtml'], result => {
                                        let mailTitle = result.emailNewPasswordTitle,
                                            mailText = result.emailNewPasswordText.replaceAll('{name}', user.firstname + ' ' + user.lastname)
                                                .replaceAll('{firstname}', user.firstname).replaceAll('{lastname}', data.lastname)
                                                .replaceAll('{email}', user.email).replaceAll('{password}', data.password),
                                            mailHtml = result.emailNewPasswordHtml.replaceAll('{name}', user.firstname + ' ' + user.lastname)
                                                .replaceAll('{firstname}', user.firstname).replaceAll('{lastname}', data.lastname)
                                                .replaceAll('{email}', user.email).replaceAll('{password}', data.password);
                                        app.email.sendEmail(app.data.email, app.data.emailPassword, user.email, [], mailTitle, mailText, mailHtml, null);
                                    });
                                }

                                if (error) {
                                    res.send({ error });
                                } else {
                                    app.model.user.get(user._id, (error, user) => {
                                        user = app.clone(user, { password: '', default: user.email == app.defaultAdminEmail });
                                        app.io.emit('user-changed', user);
                                        res.send({ error, user });
                                    });
                                }
                            }
                        })
                    }
                });
            }
        });
    });

    app.put('/api/staff', app.permission.check('user:write'), (req, res) => {
        const { staffId, changes } = req.body;
        app.model.staff.update(staffId, changes, (error, staff) => {
            res.send({ error, staff });
        });
    });

    app.put('/api/profile', app.permission.check(), (req, res) => {
        const changes = req.body.changes, $unset = {};
        if (changes.birthday && changes.birthday == 'empty') {
            delete changes.birthday;
            $unset.birthday = '';
        }
        delete changes.roles;
        delete changes.email;
        delete changes.active;
        delete changes.isStudent;
        delete changes.isStaff;
        if (changes.password && req.session.user.email && app.email.isBKer(req.session.user.email)) delete changes.password;
        if (changes.divisions && changes.divisions == 'empty') changes.divisions = [];

        app.model.user.update(req.session.user._id, changes, $unset, (error, user) => {
            if (user) {
                req.session.user = user.clone();
            }
            res.send({ error, user: req.session.user });
        })
    });

    app.delete('/api/user', app.permission.check('user:write'), (req, res) => {
        app.model.user.delete(req.body._id, error => {
            app.model.staff.delete({ userId: req.body._id }, (error2) => {
                res.send({ error: error2 ? error2 : error });
            });
        })
    });

    app.delete('/api/user/clear-participants-session', app.permission.check(), (req, res) => {
        req.session.participants = null;
        res.send('OK');
    });


    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    // app.post('/register', (req, res) => app.registerUser(req, res, 'user'));
    app.post('/login', app.loginUser);
    app.post('/logout', app.logoutUser);

    //TODO: tạm thời dùng cho photoBooth
    app.post('/get_user_on_mobile', app.getUserOnMobile);
    app.post('/login_on_mobile', app.loginUserOnMobile);
    app.post('/logout_on_mobile', app.logoutUserOnMobile);


    /* app.post('/active-user/:userId', (req, res) => app.model.user.get({ _id: req.params.userId }, (error, user) => {
            if (error || user == null) {
                res.send({ message: 'Địa chỉ kích hoạt tài khoản không đúng!' });
            } else if (user.active) {
                res.send({ message: 'Bạn kích hoạt tài khoản đã được kích hoạt!' });
            } else if (user.token !== 'new') {
                res.send({ message: 'Bạn kích hoạt tài khoản đã được kích hoạt!' });
            } else {
                user.active = true;
                user.token = '';
                user.save(error => res.send({ message: error ? 'Quá trình kích hoạt tài khoản đã lỗi!' : 'Bạn đã kích hoạt tài khoản thành công!' }));
            }
        }));

        app.put('/forgot-password', app.isGuest, (req, res) => app.model.user.get({ email: req.body.email }, (error, user) => {
            if (error || user === null) {
                res.send({ error: 'Email không tồn tại!' });
            } else if (app.isBKer(req.body.email)) {
                res.send({ error: 'Vui lòng liên hệ Ban quản lý mạng!' });
            } else {
                user.token = app.getToken(8);
                user.tokenDate = new Date().getTime() + 24 * 60 * 60 * 1000;
                user.save(error => {
                    if (error) {
                        res.send({ error })
                    } else {
                        app.model.setting.get(['emailForgotPasswordTitle', 'emailForgotPasswordText', 'emailForgotPasswordHtml'], result => {
                            let url = (app.isDebug ? app.debugUrl : app.rootUrl) + '/forgot-password/' + user._id + '/' + user.token,
                                mailTitle = result.emailForgotPasswordTitle,
                                mailText = result.emailForgotPasswordText.replaceAll('{name}', user.firstname + ' ' + user.lastname)
                                    .replaceAll('{firstname}', user.firstname).replaceAll('{lastname}', data.lastname)
                                    .replaceAll('{email}', user.email).replaceAll('{url}', url),
                                mailHtml = result.emailForgotPasswordHtml.replaceAll('{name}', user.firstname + ' ' + user.lastname)
                                    .replaceAll('{firstname}', user.firstname).replaceAll('{lastname}', data.lastname)
                                    .replaceAll('{email}', user.email).replaceAll('{url}', url);
                            app.email.sendEmail(app.data.email, app.data.emailPassword, user.email, [], mailTitle, mailText, mailHtml, null);
                        });

                        res.send({ error: null });
                    }
                });
            }
        }));

        app.post('/forgot-password/:userId/:userToken', (req, res) => app.model.user.get({ _id: req.params.userId }, (error, user) => {
            if (error || user == null) {
                res.send({ error: 'Link không hợp lệ!' });
            } else {
                if (user.token === req.params.userToken) {
                    if (new Date().getTime() <= new Date(user.tokenDate).getTime()) {
                        res.send({});
                    } else {
                        res.send({ error: 'Sau 24 giờ, đường link đã mất hiệu lực!' });
                    }
                } else {
                    res.send({ error: 'Link không hợp lệ!' });
                }
            }
        }));

        app.put('/forgot-password/new-password', app.isGuest, (req, res) => app.model.user.get({ _id: req.body.userId }, (error, user) => {
            if (error || user == null) {
                res.send({ error: 'Mã số người dùng không hợp lệ!' });
            } else {
                if (user.token === req.body.token) {
                    user.password = app.model.user.hashPassword(req.body.password);
                    user.save(error => res.send({ error: error ? 'Doi mat khau bi loi!' : null }));
                } else {
                    res.send({ error: 'Đường link không hợp lệ!' });
                }
            }
        }));/**/

    // Home -----------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/user/personnel/item/:firstPartOfEmail', (req, res) => app.model.user.get({ email: req.params.firstPartOfEmail + '@hcmut.edu.vn' }, (error, user) => {
        const getOtherInfo = user => {
            app.model.project.getAll({ user: user._id }, (error, projects) => {
                if (error) {
                    res.send({ error: 'Error when get projects!' });
                } else {
                    app.model.publication.getAll({ user: user._id, active: true }, (error, publications) => {
                        if (error) {
                            res.send({ error: 'Error when get projects!' });
                        } else {
                            res.send({ item: app.clone(user, { password: '', projects, publications }) });
                        }
                    });
                }
            });
        };

        if (error) {
            res.send({ error: 'System has errors!' });
        } else if (user == null) {
            app.model.user.get({ email: req.params.firstPartOfEmail + '@oisp.edu.vn' }, (error, user) => {
                if (error || user == null || user.role != 'staff') {
                    res.send({ error: 'System has errors!' });
                } else {
                    getOtherInfo(user);
                }
            });
        } else if (user.role == 'staff') {
            getOtherInfo(user);
        } else {
            res.send({ error: 'User is not staff!' });
        }
    }));

    app.get('/api/user/personnel/all', (req, res) => app.model.user.getAll({ role: 'staff' }, (error, items) => res.send({ error, items })));
    app.get('/api/user/project-levels/all', (req, res) => app.model.projectLevel.getAll((error, items) => res.send({ error, items })));
    app.get('/api/user/publication-types/all', (req, res) => app.model.publicationType.getAll((error, items) => res.send({ error, items })));


    // Hook upload images ---------------------------------------------------------------------------------------------------------------------------
    app.createFolder(app.path.join(app.publicPath, '/img/user'));

    app.uploadHooks.add('uploadYourAvatar', (req, fields, files, params, done) => {
        if (req.session.user && fields.userData && fields.userData[0] == 'profile' && files.ProfileImage && files.ProfileImage.length > 0) {
            console.log('Hook: uploadYourAvatar => your avatar upload');
            app.uploadComponentImage(req, 'user', app.model.user.get, req.session.user._id, files.ProfileImage[0].path, done);
        }
    });

    const uploadUserAvatar = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0].startsWith('user:') && files.UserImage && files.UserImage.length > 0) {
            console.log('Hook: uploadUserAvatar => user avatar upload');
            app.uploadComponentImage(req, 'user', app.model.user.get, fields.userData[0].substring(5), files.UserImage[0].path, done);
        }
    };
    app.uploadHooks.add('uploadUserAvatar', (req, fields, files, params, done) =>
        app.permission.has(req, () => uploadUserAvatar(req, fields, files, params, done), done, 'user:write'));


    const staffImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'staffImportData' && files.StaffFile && files.StaffFile.length > 0) {
            console.log('Hook: staffImportData');
            app.uploadStaffFile(req, files.StaffFile[0].path, done);
        }
    };

    app.uploadHooks.add('staffImportData', (req, fields, files, params, done) =>
        app.permission.has(req, () => staffImportData(req, fields, files, params, done), done, 'user:write'));
};
