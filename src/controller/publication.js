module.exports = app => {
    app.permission.add(
        {
            name: 'system:publication',
            menu: {
                parentMenu: { index: 3000, title: 'Danh mục', icon: 'fa-list' },
                menus: { 3120: { title: 'Loại xuất bản', link: '/user/publication-type' } }
            },
        }
    );

    app.get('/user/publication-type', app.permission.check('system:publication'), app.templates.admin);
    app.get('/user/publication/:userId', app.permission.check('system:publication'), app.templates.admin);
    app.get('/user/publication', app.userType.isStaff, app.templates.admin);

    app.get('/api/publication-types/all', app.permission.check('system:publication'), (req, res) =>
        app.model.publicationType.getAll((error, items) => res.send({ error, items })));
    app.post('/api/publication-type', app.permission.check('system:publication'), (req, res) =>
        app.model.publicationType.create(req.body.publicationType, (error, item) => res.send({ error, item })));
    app.put('/api/publication-type', app.permission.check('system:publication'), (req, res) =>
        app.model.publicationType.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));
    app.delete('/api/publication-type', app.permission.check('system:publication'), (req, res) =>
        app.model.publicationType.delete(req.body._id, error => res.send({ error })));

    app.get('/api/publication/page/:userId/:pageNumber/:pageSize', app.permission.check('system:publication'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.publication.getPage(pageNumber, pageSize, { user: req.params.userId }, (error, page) => res.send({ error, page }));
    });
    app.post('/api/publication', app.permission.check('system:publication'), (req, res) => {
        const publication = req.body.publication;
        app.model.user.get({ _id: publication.user }, (error, user) => {
            if (error || user == null) {
                res.send({ error: 'Invalid user Id!' })
            } else {
                app.model.publication.create(publication, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.post('/api/publication/list', app.permission.check('system:publication'), (req, res) => app.model.user.get({ _id: req.body.userId }, (error, user) => {
        if (error || user == null) {
            res.send({ error: 'Error' });
        } else {
            let list = req.body.list,
                numberOfError = 0,
                solve = (list, index, done) => {
                    if (index < list.length) {
                        const item = list[index];
                        item.user = user._id;
                        item.active = true;
                        app.model.publication.create(item, (error, publication) => {
                            if (error || publication == null) numberOfError++;
                            solve(list, index + 1, done);
                        });
                    } else {
                        done();
                    }
                };

            solve(list ? list : [], 0, () => res.send({ numberOfError }));
        }
    }));

    app.put('/api/publication', app.permission.check('system:publication'), (req, res) => {
        const changes = req.body.changes;
        delete changes.user;
        app.model.publication.update(req.body._id, changes, (error, item) => res.send({ error, item }));
    });
    app.delete('/api/publication', app.permission.check('system:publication'), (req, res) => app.model.publication.delete(req.body._id, error => res.send({ error })));

    app.get('/staff/publication/page/:pageNumber/:pageSize', app.userType.isStaff, (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber), pageSize = parseInt(req.params.pageSize),
            staffId = req.session.user._id;
        app.model.publication.getPage(pageNumber, pageSize, { user: staffId }, (error, page) => res.send({ error, page }));
    });

    app.post('/staff/publication', app.userType.isStaff, (req, res) => {
        const publication = req.body.publication;
        publication.user = req.session.user._id;
        app.model.publication.create(publication, (error, item) => res.send({ error, item }));
    });

    app.post('/staff/publication/list', app.userType.isStaff, (req, res) => {
        const user = req.session.user;
        let list = req.body.list,
            numberOfError = 0,
            solve = (list, index, done) => {
                if (index < list.length) {
                    const item = list[index];
                    item.user = user._id;
                    item.active = true;
                    app.model.publication.create(item, (error, publication) => {
                        if (error || publication == null) numberOfError++;
                        solve(list, index + 1, done);
                    });
                } else {
                    done();
                }
            };
        solve(list ? list : [], 0, () => res.send({ numberOfError }));
    });

    app.put('/staff/publication', app.userType.isStaff, (req, res) => {
        const changes = req.body.changes;
        delete changes.user;
        app.model.publication.update(req.body._id, changes, (error, item) => res.send({ error, item }));
    });

    app.delete('/staff/publication', app.userType.isStaff, (req, res) => {
        app.model.publication.delete(req.body._id, error => res.send({ error }));
    });

    app.get('/staff/publication-types/all', app.userType.isStaff, (req, res) => {
        app.model.publicationType.getAll((error, items) => res.send({ error, items }));
    });

    // Hook upload files ---------------------------------------------------------------------------------------------------------------------------s
    const publicationImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'publicationImportData' && files.PublicationFile && files.PublicationFile.length > 0) {
            console.log('Hook: publicationImportData');
            app.uploadPublicationFile(req, files.PublicationFile[0].path, done);
        }
    };

    app.uploadHooks.add('publicationImportData', (req, fields, files, params, done) => publicationImportData(req, fields, files, params, done));
};