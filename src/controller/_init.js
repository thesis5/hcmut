module.exports = app => {
    app.data = {
        todayViews: 0,
        allViews: 0,
        logo: '/img/favicon.png',
        footer: '/img/footer.jpg',
        map: '/img/map.jpg',
        facebook: 'https://www.facebook.com/hcmut',
        youtube: '',
        twitter: '',
        instagram: '',
        latitude: 10.7744962,
        longitude: 106.6606518,
        email: app.email.from,
        emailPassword: app.email.password,
        mobile: '(08) 2214 6555',
        address: JSON.stringify({
            vi: 'Nhà B6 - Trường Đại học Bách Khoa - ĐHQG HCM | 268 Lý Thường Kiệt, P.14, Q.10',
            en: 'Block B6 - Ho Chi Minh City University of Technology | 268 Ly Thuong Kiet Street, District 10, Hochiminh City, Vietnam'
        }),
    };

    app.createFolder(
        app.assetPath,
        app.path.join(app.assetPath, '/upload'),
        app.path.join(app.publicPath, '/img/staff'),
    );

    app.getActivedRoles = done => app.model.role.getAll((error, items) => {
        const roles = [];
        if (error == null && items) {
            items.forEach(role => role.active && roles.push({ _id: role._id, name: role.name }));
        }
        app.data.roles = roles;
        done && done(roles);
    });
    app.getActivedRoles();

    // Count views ----------------------------------------------------------------------------------------------------------------------------------
    app.model.setting.init(app.data, () =>
        app.model.setting.get(['todayViews', 'allViews', 'logo', 'footer', 'map', 'facebook', 'youtube', 'twitter', 'instagram', 'latitude', 'longitude', 'email', 'emailPassword', 'mobile', 'address'], result => {
            app.data.todayViews = parseInt(result.todayViews);
            app.data.allViews = parseInt(result.allViews);
            app.data.logo = result.logo;
            app.data.footer = result.footer;
            app.data.map = result.map;
            app.data.facebook = result.facebook;
            app.data.youtube = result.youtube;
            app.data.twitter = result.twitter;
            app.data.instagram = result.instagram;
            app.data.latitude = result.latitude;
            app.data.longitude = result.longitude;
            app.data.email = result.email;
            app.data.emailPassword = result.emailPassword;
            app.data.mobile = result.mobile;
            app.data.address = result.address;
        }));

    app.model.user.count((error, numberOfUser) => {
        app.data.numberOfUser = error ? 0 : numberOfUser;

        app.model.news.count((error, numberOfNews) => {
            app.data.numberOfNews = error ? 0 : numberOfNews;

            app.model.event.count((error, numberOfEvent) => {
                app.data.numberOfEvent = error ? 0 : numberOfEvent;

                app.model.job.count((error, numberOfJob) => {
                    app.data.numberOfJob = error ? 0 : numberOfJob;
                });
            });
        });
    });

    const fiveMinuteJob = () => {
        const count = {
            todayViews: app.data.todayViews,
            allViews: app.data.allViews
        }
        app.io.emit('count', count);
        app.model.setting.set(count);
    };
    app.schedule('*/5 * * * *', fiveMinuteJob);

    app.schedule('0 0 * * *', () => app.data.todayViews = 0);

    // Menu -----------------------------------------------------------------------------------------------------------------------------------------
    app.model.menu.get({ link: '/' }, (error, menu) => {
        if (error) {
            console.error('Get menu by link has errors!');
        } else if (menu == null) {
            app.model.menu.create({
                active: true,
                title: 'home',
                link: '/',
            });
        }
    });

    // Email ----------------------------------------------------------------------------------------------------------------------------------------
    app.model.setting.init({
        emailRegisterMemberTitle: 'HCMUT: Chào mừng thành viên mới!',
        emailRegisterMemberText: 'Xin chào {name}, Trường đại học Bách Khoa (HCMUT) chào mừng bạn là thành viên mới. Trước khi bạn có thể đăng nhập, bạn vui lòng kích hoạt tài khoản bằng cách nhấp vào  {url}. Trân trọng, Trường đại học Bách Khoa (HCMUT), Website: ' + app.rootUrl + '',
        emailRegisterMemberHtml: 'Xin chào <b>{name}</b>,<br/><br/>' +
            'Trường đại học Bách Khoa (HCMUT) chào mừng bạn là thành viên mới. Trước khi bạn có thể đăng nhập, bạn vui lòng kích hoạt tài khoản bằng cách nhấp vào link <a href="{url}">{url}</a>.<br/><br/>' +
            'Trân trọng,<br/>' +
            'Trường đại học Bách Khoa (HCMUT)<br/>' +
            'Website: <a href="' + app.rootUrl + '">' + app.rootUrl + '</a>',
        emailCreateMemberByAdminTitle: 'HCMUT: Chào mừng thành viên mới!',
        emailCreateMemberByAdminText: 'Chào {name}, Tài khoản của bạn đã được tạo. Thông tin đăng nhập của bạn là: email: {email}. Mật khẩu: "{password}". Link kích hoạt: {url}. Trân trọng, HCMUT Admin.',
        emailCreateMemberByAdminHtml: 'Chào {name},<br/><br/>Tài khoản của bạn đã được tạo. Thông tin đăng nhập của bạn là: <br> - Email: {email}.<br> - Mật khẩu: "{password}".<br/> - Link kích hoạt: <a href="{url}">{url}</a>.<br/><br/>Trân trọng,<br/>HCMUT Admin.',
        emailNewPasswordTitle: 'HCMUT: Mật khẩu mới!',
        emailNewPasswordText: 'Chào {name}, Mật khẩu mới của bạn là "{password}". Trân trọng, HCMUT Admin.',
        emailNewPasswordHtml: 'Chào {name},<br/><br/>Mật khẩu mới của bạn là "<b>{password}</b>".<br/><br/>Trân trọng,<br/>HCMUT Admin.',
        emailForgotPasswordTitle: 'HCMUT: Quên mật khẩu!',
        emailForgotPasswordText: 'Chào {name}, Bạn vừa mới yếu cầu thay đổi mật khẩu tại trang web ' + app.rootUrl + '. ' +
            'Bạn dùng đường link bên dưới để thay đổi mật khẩu. ' +
            'Mật khẩu chỉ có hiệu lực trong 24 giờ kế tiếp. ' +
            'Link: {url}' +
            'Trân trọng, ' +
            'Trường đại học Bách Khoa (HCMUT)' +
            'Website: ' + app.rootUrl + '',
        emailForgotPasswordHtml: '<p><b>Chào {name}, </b><br/><br/>Bạn vừa mới yếu cầu thay đổi mật khẩu tại trang web <a href="' + app.rootUrl + '" target="_blank">' + app.rootUrl + '</a>. ' +
            'Bạn dùng đường link bên dưới để thay đổi mật khẩu. <b>Đường link bên dưới chỉ có hiệu lực trong 24 giờ tiếp theo.</b><br/>' +
            'Link: <a href="{url}">{url}</a><br/>' +
            'Trân trọng, <br/>' +
            'Trường đại học Bách Khoa (HCMUT)<br/>' +
            'Website: <a href="' + app.rootUrl + '" target="_blank">' + app.rootUrl + '</a></p>',
        emailContactTitle: 'HCMUT: Liên hệ',
        emailContactText: 'Chào bạn, HCMUT đã nhận tin nhắn của bạn. Cảm ơn bạn đã liên hệ với chúng tôi. Tiêu đề liên hệ là: "{title}". Thông điệp của bạn là: "{message}". HCMUT sẽ trả lời bạn sớm nhất. Trân trọng, HCMUT admin.',
        emailContactHtml: 'Chào bạn,<br/><br/>HCMUT đã nhận tin nhắn của bạn. Cảm ơn bạn đã liên hệ với chúng tôi.<br/>Tiêu đề liên hệ là: "{title}".<br/>Thông điệp của bạn là: "{message}".<br/>HCMUT sẽ trả lời bạn sớm nhất.<br/><br/>Trân trọng,<br/>HCMUT admin.',
    });

    // Upload ---------------------------------------------------------------------------------------------------------------------------------------
    // app.post('/upload', app.permission.check(), (req, res) => { // Unauthorize upload
    //     app.getUploadForm().parse(req, (error, fields, files) => {
    //         console.log('User Upload:', fields, files, req.query);

    //         if (error) {
    //             res.send({ error });
    //         } else {
    //         }
    //     });
    // });

    app.post('/user/upload', app.permission.check(), (req, res) => {
        app.getUploadForm().parse(req, (error, fields, files) => {
            console.log('User Upload:', fields, files, req.query);

            if (error) {
                res.send({ error });
            } else {
                let hasResponsed = false;
                app.uploadHooks.run(req, fields, files, req.query, data => {
                    if (hasResponsed == false) res.send(data);
                    hasResponsed = true;
                });
            }
        });
    });

    app.uploadComponentImage = (req, dataName, getItem, dataId, srcPath, sendResponse) => {
        if (dataId == 'new') {
            let imageLink = app.path.join('/img/draft', app.path.basename(srcPath)),
                sessionPath = app.path.join(app.publicPath, imageLink);
            app.fs.rename(srcPath, sessionPath, error => {
                if (error == null) req.session[dataName + 'Image'] = sessionPath;
                sendResponse({ error, image: imageLink });
            });
        } else {
            req.session[dataName + 'Image'] = null;
            if (getItem) {
                getItem(dataId, (error, dataItem) => {
                    if (error || dataItem == null) {
                        sendResponse({ error: 'Invalid Id!' });
                    } else {
                        app.deleteImage(dataItem.image);

                        dataItem.image = '/img/' + dataName + '/' + dataItem._id + app.path.extname(srcPath);
                        app.fs.rename(srcPath, app.path.join(app.publicPath, dataItem.image), error => {
                            if (error) {
                                sendResponse({ error });
                            } else {
                                dataItem.image += '?t=' + (new Date().getTime()).toString().slice(-8);
                                dataItem.save(error => {
                                    if (dataName == 'user') {
                                        dataItem = app.clone(dataItem, { password: '' });
                                        if (req.session.user && req.session.user._id == dataItem._id) {
                                            req.session.user.image = dataItem.image;
                                        }
                                    }

                                    if (error == null) app.io.emit(dataName + '-changed', dataItem);
                                    if (dataName == 'photoBooth') app.io.emit('cover-image', 'set', dataItem._id, dataItem.image);
                                    sendResponse({
                                        error,
                                        item: dataItem,
                                        image: dataItem.image,
                                    });
                                });
                            }
                        });
                    }
                });
            } else {
                const image = '/img/' + dataName + '/' + dataId + app.path.extname(srcPath);
                app.fs.rename(srcPath, app.path.join(app.publicPath, image), error => sendResponse({ error, image }));
            }
        }
    };

    app.uploadCkEditorImage = (category, fields, files, params, done) => {
        if (files.upload && files.upload.length > 0 && fields.ckCsrfToken && params.Type == 'File' && params.category == category) {
            console.log('Hook: uploadCkEditorImage => ckEditor upload');

            const srcPath = files.upload[0].path;
            app.jimp.read(srcPath).then(image => {
                app.fs.unlinkSync(srcPath);

                if (image) {
                    if (image.bitmap.width > 1024) image.resize(1024, app.jimp.AUTO);
                    const url = `/img/${category}/${app.path.basename(srcPath)}`;
                    image.write(app.path.join(app.publicPath, url), error => {
                        done({ uploaded: error == null, url, error: { message: error ? 'Upload has errors!' : '' } });
                    });
                } else {
                    done({ uploaded: false, error: 'Upload has errors!' });
                }
            });
        } else {
            done();
        }
    };

    app.uploadImageToBase64 = (srcPath, sendResponse) => {
        app.jimp.read(srcPath).then(image => image.getBuffer(app.jimp.MIME_PNG, (error, buffer) => {
            app.fs.unlinkSync(srcPath);

            sendResponse({
                uploaded: error == null,
                url: 'data:image/png;base64, ' + buffer.toString('base64'),
                error: { message: error ? 'Đăng hình bị lỗi!' : '' }
            });
        }));
    };

    app.uploadGuestImage = (req, getItem, dataId, srcPath, sendResponse) => {
        if (getItem) {
            getItem(dataId, (error, guest) => {
                if (error || guest == null) {
                    sendResponse({ error: 'Invalid Id!' });
                } else {
                    const hasImage = guest.image && guest.image != '';
                    guest.image =  `/photo-booth/${guest.photoBooth.toString()}/${guest._id.toString()}${app.path.extname(srcPath)}`;
                    guest.priority = new Date().getTime();
                    app.fs.rename(srcPath, app.path.join(app.publicPath, guest.image), error => {
                        if (error) {
                            sendResponse({ error });
                        } else {
                            guest.save(error => {
                                if (hasImage && guest.active) app.io.emit('guest-change', { newItem: guest, oldItem: guest });
                                else if (guest.active) app.io.emit('guest-change', { newItem: guest });
                                sendResponse({
                                    error,
                                    item: guest,
                                    image: guest.image,
                                });
                            });
                        }
                    });
                }
            });
        } else {
            sendResponse({ error: 'Invalid get function' });
        }

    };

    app.uploadGuestFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), guests = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ guests });
                    } else {
                        guests.push({ numberId: value[2], fullname: value[3], description: value[4], company: value[5] });
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.importRegistration = (req, srcPath, sendResponse) => {
        const workbook = app.excel.create();
        workbook.xlsx.readFile(srcPath).then(() => {
            const worksheet = workbook.getWorksheet(1);
            let index = 1, participants = [];
            while (true) {
                index++;
                let organizationId = worksheet.getCell('A' + index).value;
                if (organizationId) {
                    organizationId = organizationId.toString().trim();
                    const lastname = worksheet.getCell('B' + index).value.toString().trim();
                    const firstname = worksheet.getCell('C' + index).value.toString().trim();
                    const email = worksheet.getCell('D' + index).value.toString().trim();
                    participants.push({ lastname, firstname, email, organizationId, active: true });
                } else {
                    require('fs').unlinkSync(srcPath);
                    req.session.participants = participants;
                    sendResponse({ number: participants.length });
                    break;
                }
            }
        });
    };

    app.uploadProjectFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                app.model.projectLevel.getAll((error, items) => {
                    if (error || items == null) {
                        sendResponse({ error: 'Error' });
                    } else {
                        const projectLevel = {};
                        items.forEach(item => projectLevel[app.language.parse(req, item.level, true).vi] = item._id);

                        const worksheet = workbook.getWorksheet(1), list = [];

                        const solve = index => {
                            const cellA = worksheet.getCell('A' + index);
                            if (cellA && cellA.text) {
                                let cellB = worksheet.getCell('B' + index),
                                    cellC = worksheet.getCell('C' + index),
                                    cellD = worksheet.getCell('D' + index);
                                const item = { title: cellA.text.trim() };
                                if (cellB && cellB.text) {
                                    const arr = cellB.text.trim().split('-');
                                    if (arr.length >= 1) {
                                        try {
                                            item.startYear = parseInt(arr[0]);
                                        } catch (e) { }
                                    }
                                    if (arr.length >= 2) {
                                        try {
                                            item.endYear = parseInt(arr[1]);
                                        } catch (e) { }
                                    }
                                }
                                if (cellC && cellC.text) {
                                    item.isLeader = cellC.text.trim().toLowerCase() == 'x';
                                }
                                if (cellD && cellD.text) {
                                    item.levelText = cellD.text.trim();
                                    item.level = projectLevel[item.levelText] ? projectLevel[item.levelText] : null;
                                    list.push(item);
                                    solve(index + 1);
                                } else {
                                    list.push(item);
                                    solve(index + 1);
                                }
                            } else {
                                app.deleteFile(srcPath);
                                sendResponse({ list });
                            }
                        };
                        solve(2);
                    }
                });
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadPublicationFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                app.model.publicationType.getAll((error, items) => {
                    if (error || items == null) {
                        sendResponse({ error: 'Error' });
                    } else {
                        const publicationType = {};
                        items.forEach(item => publicationType[app.language.parse(req, item.type, true).vi] = item._id);
                        const worksheet = workbook.getWorksheet(1), list = [];

                        const solve = index => {
                            const cellA = worksheet.getCell('A' + index);
                            if (cellA && cellA.text) {
                                let cellB = worksheet.getCell('B' + index),
                                    cellC = worksheet.getCell('C' + index);
                                const item = { title: cellA.text.trim(), active: true };
                                if (cellB && cellB.value) {
                                    item.year = cellB.value;
                                }
                                if (cellC && cellC.text) {
                                    item.typeText = cellC.text.trim();
                                    item.type = publicationType[item.typeText] ? publicationType[item.typeText] : null;
                                    list.push(item);
                                    solve(index + 1);
                                } else {
                                    list.push(item);
                                    solve(index + 1);
                                }
                            } else {
                                app.deleteFile(srcPath);
                                sendResponse({ list });
                            }
                        };
                        solve(2);
                    }
                });
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    // app.uploadStaffFile = (req, srcPath, sendResponse) => {
    //     app.excel.readFile(srcPath, workbook => {
    //         if (workbook) {
    //             app.model.role.get({ default: true }, (error, defaultRole) => {
    //                 if (error || !defaultRole) {
    //                     sendResponse({ error: 'Error when get default role has occurred' });
    //                 } else {
    //                     const worksheet = workbook.getWorksheet(1), staffs = [];
    //                     let index = 0;
    //                     while (true) {
    //                         index++;
    //                         let organizationId = worksheet.getCell('A' + index).value;
    //                         if (organizationId) {
    //                             organizationId = organizationId.toString().trim();
    //                             const fullName = worksheet.getCell('B' + index).value.toString().trim(), lastSpace = fullName.lastIndexOf(' ');
    //                             const lastname = fullName.substring(0, lastSpace), firstname = fullName.substring(lastSpace + 1);
    //                             const email = worksheet.getCell('C' + index).value.toString().trim();
    //                             staffs.push({
    //                                 roles: [defaultRole._id], firstname, lastname, email, organizationId, isStaff: true, active: true
    //                             });
    //                         } else {
    //                             require('fs').unlinkSync(srcPath);
    //                             req.session.staffs = staffs;
    //                             sendResponse({ number: staffs.length });
    //                             break;
    //                         }
    //                     }
    //                 }
    //             });
    //         } else {
    //             sendResponse({ error: 'Error' });
    //         }
    //     });
    // };

    app.uploadStaffFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                app.model.role.get({ default: true }, (error, defaultRole) => {
                    if (error || !defaultRole) {
                        sendResponse({ error: 'Error when get default role has occurred' });
                    } else {
                        const worksheet = workbook.getWorksheet(1), staffs = [], totalRow = worksheet.lastRow.number;;
                        for (let index = 2; index <= totalRow + 1; index++) {
                            const value = worksheet.getRow(index).values;
                            if (value.length == 0 || index == totalRow + 1) {
                                req.session.staffs = staffs;
                                sendResponse({ number: staffs.length });
                                break;
                            } else {
                                const staffMapper = {};
                                ['NGHI', 'IS_NNGOAI', 'LOAI', 'shcc', 'HO', 'TEN', 'PHAI', 'NGAY_SINH', 'NGND', 'NGUT', 'NGAY_BD_CT', 'NGAY_VAO', 'NGAY_CBGD', 'NGAY_BC', 'NGAY_NGHI',
                                    'CHUCDANH_TRINHDO', 'NGACH', 'NGACH_MOI', 'CHUC_DANH', 'TRINH_DO', 'NUOC_NGOAI', 'GHI_CHU_IN', 'HESO_LG', 'BAC_LG', 'MOC_NANG_LG', 'NGAY_HUONG_LG',
                                    'TY_LE_VUOT_KHUNG', 'PCCV', 'NGAY_PCCV', 'MS_CVU', 'CHUC_VU_BCH_DANG_BO', 'CHUC_VU_BCH_CONG_DOAN', 'CHUC_VU_BCH_DOAN_TN', 'CHUYEN_NGANH', 'TDO_LLCT', 'NOI_DKHK', 'DC_HIENTAI',
                                    'D_THOAI', 'E_mail', 'Dan_toc', 'Ton_giao', 'DANG_VIEN', 'MS_BM', 'TEN_BM', 'MS_KHOA', 'TEN_KHOA', 'PHUC_LOI', 'GHI_CHU'].forEach((item, index) => {
                                        staffMapper[item] = value[index + 1];
                                    });
                                staffMapper['IS_NNGOAI'] = staffMapper['IS_NNGOAI'].result;
                                staffMapper['DANG_VIEN'] = staffMapper['DANG_VIEN'].result;
                                staffs.push(staffMapper);
                            }
                        }
                    }
                });
            } else {
                app.deleteFile(srcPath);
                sendResponse({ error: 'Error' });
            }
        });
    };
    app.uploadDsNhiemVu1CoiThiFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['stt', 'shcc', 'fullname', 'ngay', 'maMonHoc', 'monHoc', 'thoiLuong', 'kyThi', 'donViCongTac', 'vn1', 'ghiChu'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDs_nv2_pkhcnFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points, warnings, numberOfErrors });
                    } else {
                        const organizationId = value[2], fullName = value[3], shtt = value[4], cgcn = value[5], ldsx = value[6],
                            articleOfUniversity = value[7], articleOutUniversity = value[8], reward = value[9], elseAction = value[10], article = value[11], point = value[12], totalHours = value[13]
                        app.model.user.get({ organizationId }, (error, user) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            }
                            else {
                                const element = {};
                                ['stt', 'shcc', 'fullName', 'shtt', 'cgcn', 'ldsx', 'articleOfUniversity', 'articleOutUniversity', 'reward', 'elseAction', 'article', 'point', 'totalHours'].forEach((key, index) => {
                                    element[key] = value[index + 1];
                                });
                                points.push(element);
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };
    app.uploadYearPointFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points, warnings, numberOfErrors });
                    } else {
                        const organizationId = value[1], ratedRatio = value[2], professionalCoefficient = value[3], leadershipCoefficient = value[4],
                            leadershipPoint = value[5], professionalPoint = value[6], professionalPoint1 = value[7], professionalPoint2 = value[8], professionalPoint3 = value[9], rewardPoint = value[10], penaltyPoint = value[11]
                        app.model.user.get({ organizationId }, (error, user) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            } else if (!user) {
                                warnings.push(`MSCB: ${organizationId} không tồn tại!</br>`);
                                handleUpload(index + 1);
                            } else {
                                points.push({
                                    userId: user._id, ratedRatio, leadershipCoefficient, professionalCoefficient, leadershipPoint,
                                    professionalPoint1, professionalPoint2, professionalPoint3, professionalPoint, rewardPoint, penaltyPoint,
                                    userInfo: {
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        email: user.email,
                                        organizationId: user.organizationId
                                    }
                                });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadBonusPointFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points, warnings, numberOfErrors });
                    } else {
                        const organizationId = value[1], rewardPoint = value[2], penaltyPoint = value[3];
                        app.model.user.get({ organizationId }, (error, user) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            } else if (!user) {
                                warnings.push(`MSCB: ${organizationId} không tồn tại!</br>`);
                                handleUpload(index + 1);
                            } else {
                                points.push({
                                    userId: user._id, rewardPoint, penaltyPoint,
                                    userInfo: {
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        email: user.email,
                                        organizationId: user.organizationId
                                    }
                                });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadProfessionalPointFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points, warnings, numberOfErrors });
                    } else {
                        const organizationId = value[1], professionalPoint1 = value[2], professionalPoint2 = value[3], professionalPoint3 = value[4],
                            round1Point = value[5], round2Point = value[6], professionalPoint = value[7];
                        app.model.user.get({ organizationId }, (error, user) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            } else if (!user) {
                                warnings.push(`MSCB: ${organizationId} không tồn tại!</br>`);
                                handleUpload(index + 1);
                            } else {
                                points.push({
                                    userId: user._id, professionalPoint1, professionalPoint2, professionalPoint3,
                                    professionalPoint, round1Point, round2Point,
                                    userInfo: {
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        email: user.email,
                                        organizationId: user.organizationId
                                    }
                                });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };
    // app.uploads_nv1_pdtFile = (req, srcPath, sendResponse) => {
    //     app.excel.readFile(srcPath, workbook => {
    //         if (workbook) {
    //             const worksheet = workbook.getWorksheet(1), ds_nv1_dpt = [], totalRow = worksheet.lastRow.number;
    //             let numberOfErrors = 0, warnings = [];
    //             const handleUpload = (index = 2) => {
    //                 const value = worksheet.getRow(index).values;
    //                 if (value.length == 0 || index == totalRow + 1) {
    //                     sendResponse({ points, warnings, numberOfErrors });
    //                 } else {
    //                     const shcc = value[1], holot = value[2], ten = value[3], ma_bm = value[4],
    //                         ma_kh = value[5], cq182 = value[6], cq183 = value[7], cq191 = value[8],
    //                         gvcn182 = value[9], gvcn183 = value[10], gvcn191 = value[11],
    //                         dttx182 = value[12], dttx183 = value[13], dttx191 = value[14],
    //                         qtcn182 = value[15], qtcn183 = value[16], qtcn191 = value[17],
    //                         qt182 = value[18], qt183 = value[19], qt191 = value[20],
    //                         tonggiochuan =  value[21];
    //                         ds_nv1_dpt.push({
    //                             shcc, holot, ten, ma_bm, ma_kh,
    //                             cq182, cq183, cq191,
    //                             gvcn182, gvcn183, gvcn191,
    //                             dttx182, dttx183, dttx191,
    //                             qtcn182, qtcn183, qtcn191,
    //                             qt182, qt183, qt191,
    //                             tonggiochuan,
    //                         });
    //                     // app.model.user.get({ shcc }, (error, user) => {
    //                     //     if (error) {
    //                     //         numberOfErrors++;
    //                     //         handleUpload(index + 1);
    //                     //     } else if (!user) {
    //                     //         warnings.push(`shcc: ${organizationId} không tồn tại!</br>`);
    //                     //         handleUpload(index + 1);
    //                     //     } else {
    //                     //         points.push({
    //                     //             userId: user.shcc, holot, ten, ma_bm, ma_kh,
    //                     //             cq182, cq183, cq191,
    //                     //             gvcn182, gvcn183, gvcn191,
    //                     //             dttx182, dttx183, dttx191,
    //                     //             qtcn182, qtcn183, qtcn191,
    //                     //             qt182, qt183, qt191,
    //                     //             tonggiochuan,

    //                     //             userInfo: {
    //                     //                 firstname: user.firstname,
    //                     //                 lastname: user.lastname,
    //                     //                 email: user.email,
    //                     //                 organizationId: user.organizationId
    //                     //             }
    //                     //         });
    //                     //         handleUpload(index + 1);
    //                     //     }
    //                     // });
    //                 }
    //             };
    //             handleUpload();
    //         } else {
    //             sendResponse({ error: 'Error' });
    //         }
    //     });
    // };

    app.uploadPgsGsKhtnvFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), officers = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ officers, warnings, numberOfErrors });
                    } else {
                        const note = value[1], year = value[2], shcc = value[3];
                        app.model.dsPgsGsKhongHoanThanhNhiemVu.get({ shcc }, error => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            } else {
                                officers.push({ note, year, shcc });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadkq_dg_ngoaileFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), officers = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ officers, warnings, numberOfErrors });
                    } else {
                        const officerId = value[1], lastName = value[2], firstName = value[3], classification = value[4], kI = value[5];
                        app.model.kqDanhGiaNgoaiLe.get({ officerId }, (error, officer) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            }
                            else {
                                officers.push({
                                    officerId, lastName, firstName, classification, kI
                                });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadProfessionalPointFile2 = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points, warnings, numberOfErrors });
                    } else {
                        const organizationId = value[1], professionalPoint2 = value[2];
                        app.model.user.get({ organizationId }, (error, user) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            } else if (!user) {
                                warnings.push(`MSCB: ${organizationId} không tồn tại!</br>`);
                                handleUpload(index + 1);
                            } else {
                                points.push({
                                    userId: user._id, professionalPoint2,
                                    userInfo: {
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        email: user.email,
                                        organizationId: user.organizationId
                                    }
                                });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
            }
        });
    };


    app.uploadCategoryOfficeFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), categories = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ categories });
                    } else {
                        const element = {};
                        ['maQL', 'tenChucVu', 'heSoPCQL', 'phanNgachCBQL', 'maNhomQuanLy', 'maNhomQuanLyKhongGD', 'tyLeDinhMuc', 'kCM', 'kLD', 'kI', 'cap', 'chucVuDoanThe', 'note', 'year'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        categories.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadProfessionalPoint1File = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                let numberOfErrors = 0, warnings = [];
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points, warnings, numberOfErrors });
                    } else {
                        const organizationId = value[1], professionalPoint1 = value[2], professionalPoint2 = value[3], professionalPoint3 = value[4],
                            round1Point = value[5], round2Point = value[6], professionalPoint = value[7];
                        app.model.user.get({ organizationId }, (error, user) => {
                            if (error) {
                                numberOfErrors++;
                                handleUpload(index + 1);
                            } else if (!user) {
                                warnings.push(`MSCB: ${organizationId} không tồn tại!</br>`);
                                handleUpload(index + 1);
                            } else {
                                points.push({
                                    userId: user._id, professionalPoint1, professionalPoint2, professionalPoint3,
                                    professionalPoint, round1Point, round2Point,
                                    userInfo: {
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        email: user.email,
                                        organizationId: user.organizationId
                                    }
                                });
                                handleUpload(index + 1);
                            }
                        });
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsNv2TvPointFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'authorName', 'acceptedHour', 'curriculum', 'content', 'page', 'factor', 'firstFactor', 'currentExchange', 'note'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsChuaDGCBQLPointFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['nam', 'shcc', 'hoten', 'chucVu', 'tenBoMon', 'tenKhoa', 'hoTenCanBoDuocDanhGia', 'chucVuCanBoDuocDanhGia', 'ghiChu', 'soLanChuaDanhGia', 'diemTru', 'loai', 'nghi', 'diNN'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsGiamDMPointFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'nam', 'ghiChu', 'tyLeDinhMucNv1', 'tyLeDinhMucNv2', 'tyLeDinhMucNv3', 'lyDoGiamDinhMuc'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsDiemThuongFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'hoTen', 'diemThuong', 'boNhiemGsPgs', 'nhanBangTn', 'khenThuong'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsDiemTruFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'hoTen', 'diemTru', 'lyDo'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };
    app.uploadDsNghiThaiSanFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'thangKhongTinh', 'nam', 'ghiChu'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsKQDGNgoaiLeCBQLFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'tongDiemTN'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsKQDGNgoaiLeHCPVFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'tongDanhGia', 'xepLoai'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                app.deleteFile(srcPath);
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsTapSuFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        app.deleteFile(srcPath);
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'nam', 'nv1GioChuanDinhMuc', 'nv2GioLamViecDinhMuc', 'nv3GioLamViecDinhMuc', 'ghiChu'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                app.deleteFile(srcPath);
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadQtTapSuFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        app.deleteFile(srcPath);
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['SHCC', 'STT', 'SO_KYHIEU_QD_TAP_SU', 'NGAY_QD_TAP_SU', 'TU_NGAY', 'DEN_NGAY', 'TYLE_TAP_SU', 'NGAY_NHAP_HS'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                app.deleteFile(srcPath);
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsNv1PdtFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        app.deleteFile(srcPath);
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'ma_bm', 'ma_kh', 'cq182', 'cq183', 'cq191', 'gvcn182', 'gvcn191', 'dttx182', 'dttx183', 'dttx191', 'qtcn182', 'qtcn183', 'qtcn191', 'qt182', 'qt183', 'qt191', 'tonggiochuan'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsNv1PsdhFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 3) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        app.deleteFile(srcPath);
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'cq172', 'cq173', 'cq181', 'TONG_GIO_CHUAN', 'CHUYEN_DOI', 'ON_SDH', 'TONG_ALL'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                app.deleteFile(srcPath);
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsPhieuTinNhiemFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        app.deleteFile(srcPath);
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'chucVu', 'boMonPhong', 'donVi', 'tongPhieu', 'soPhieuBau', 'tiLeBoPhieu'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                app.deleteFile(srcPath);
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsCanboCongdoanFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'maQL', 'chucVu', 'toCDBM', 'ghiChu', 'nam'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDanhMucTyLeDinhMucFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 6) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        app.deleteFile(srcPath);
                        sendResponse({ points });
                    } else {
                        var isValid = true;
                        const element = {};
                        ['stt', 'chucDanh', 'soGioLamViec', 'soGioGiangDay', 'nghienCuuKhoaHoc', 'nhiemVuKhac', 'nam', 'ghiChu', 'tongTgLamViec'].forEach((key, index) => {
                            element[key] = value[index + 1];
                            if ((element[key] === null || element[key] === undefined) && ['soGioLamViec', 'soGioGiangDay', 'nghienCuuKhoaHoc', 'nhiemVuKhac'].includes(key)) {
                                isValid = false
                            }
                        });
                        if (isValid === true)
                            points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDsSdhTrongNuocFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            app.deleteFile(srcPath);
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), points = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ points });
                    } else {
                        const element = {};
                        ['shcc', 'nam', 'ho', 'ten', 'ghiChu'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        points.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };

    app.uploadDmChucVuFile = (req, srcPath, sendResponse) => {
        app.excel.readFile(srcPath, workbook => {
            if (workbook) {
                const worksheet = workbook.getWorksheet(1), chucVu = [], totalRow = worksheet.lastRow.number;
                const handleUpload = (index = 2) => {
                    const value = worksheet.getRow(index).values;
                    if (value.length == 0 || index == totalRow + 1) {
                        sendResponse({ chucVu });
                    } else {
                        const element = {};
                        ['maQL', 'tenChucVu', 'heSoPCQL', 'phanNgachCBQL', 'maNhomQuanLy', 'maNhomQuanLyKhongGD', 'tyLeDinhMuc', 'kCM', 'kLD', 'kI', 'cap', 'chucVuDoanThe', 'note', 'year'].forEach((key, index) => {
                            element[key] = value[index + 1];
                        });
                        chucVu.push(element);
                        handleUpload(index + 1);
                    }
                };
                handleUpload();
            } else {
                sendResponse({ error: 'Error' });
            }
        });
    };
};
