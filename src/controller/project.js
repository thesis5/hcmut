module.exports = app => {
    app.permission.add(
        {
            name: 'system:project',
            menu: {
                parentMenu: { index: 3000, title: 'Danh mục', icon: 'fa-list' },
                menus: { 2110: { title: 'Cấp quản lý đề tài', link: '/user/project-level' } }
            },
        }
    );

    app.get('/user/project-level', app.permission.check('system:project'), app.templates.admin);
    app.get('/user/project/:userId', app.permission.check('system:project'), app.templates.admin);
    app.get('/user/project', app.userType.isStaff, app.templates.admin);

    app.get('/api/project-levels/all', app.permission.check('system:project'), (req, res) =>
        app.model.projectLevel.getAll((error, items) => res.send({ error, items })));
    app.post('/api/project-level', app.permission.check('system:project'), (req, res) =>
        app.model.projectLevel.create(req.body.projectLevel, (error, item) => res.send({ error, item })));
    app.put('/api/project-level', app.permission.check('system:project'), (req, res) =>
        app.model.projectLevel.update(req.body._id, req.body.changes, (error, item) => res.send({ error, item })));
    app.delete('/api/project-level', app.permission.check('system:project'), (req, res) =>
        app.model.projectLevel.delete(req.body._id, error => res.send({ error })));

    app.get('/api/project/page/:userId/:pageNumber/:pageSize', app.permission.check('system:project'), (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber),
            pageSize = parseInt(req.params.pageSize);
        app.model.project.getPage(pageNumber, pageSize, { user: req.params.userId }, (error, page) => res.send({ error, page }));
    });

    app.post('/api/project', app.permission.check('system:project'), (req, res) => {
        const project = req.body.project;
        app.model.user.get({ _id: project.user }, (error, user) => {
            if (error || user == null) {
                res.send({ error: 'Invalid user Id!' })
            } else {
                app.model.project.create(project, (error, item) => res.send({ error, item }));
            }
        });
    });

    app.post('/api/project/list', app.permission.check('system:project'), (req, res) => app.model.user.get({ _id: req.body.userId }, (error, user) => {
        if (error || user == null) {
            res.send({ error: 'Error' });
        } else {
            let list = req.body.list ? req.body.list : [],
                numberOfError = 0,
                solve = index => {
                    if (index < list.length) {
                        const item = list[index];
                        item.user = user._id;
                        item.finished = true;
                        app.model.project.create(item, (error, project) => {
                            if (error || project == null) numberOfError++;
                            solve(index + 1);
                        });
                    } else {
                        res.send({ numberOfError });
                    }
                };
            solve(0);
        }
    }));

    app.put('/api/project', app.permission.check('system:project'), (req, res) => {
        const changes = req.body.changes;
        delete changes.user;
        app.model.project.update(req.body._id, changes, (error, item) => res.send({ error, item }));
    });

    app.delete('/api/project', app.permission.check('system:project'), (req, res) => app.model.project.delete(req.body._id, error => res.send({ error })));

    //Staff ------------------------------------------------------------------------------------------------------------
    app.get('/staff/project/page/:pageNumber/:pageSize', app.userType.isStaff, (req, res) => {
        const pageNumber = parseInt(req.params.pageNumber), pageSize = parseInt(req.params.pageSize),
            staffId = req.session.user._id;
        app.model.project.getPage(pageNumber, pageSize, { user: staffId }, (error, page) => res.send({ error, page }));
    });

    app.post('/staff/project', app.userType.isStaff, (req, res) => {
        const project = req.body.project;
        project.user = req.session.user._id;
        app.model.project.create(project, (error, item) => res.send({ error, item }));
    });

    app.post('/staff/project/list', app.userType.isStaff, (req, res) => {
        const user = req.session.user._id;
        let list = req.body.list ? req.body.list : [],
            numberOfError = 0,
            solve = index => {
                if (index < list.length) {
                    const item = list[index];
                    item.user = user;
                    item.finished = true;
                    app.model.project.create(item, (error, project) => {
                        if (error || project == null) numberOfError++;
                        solve(index + 1);
                    });
                } else {
                    res.send({ numberOfError });
                }
            };
        solve(0);
    });

    app.put('/staff/project', app.userType.isStaff, (req, res) => {
        const changes = req.body.changes;
        delete changes.user;
        app.model.project.update(req.body._id, changes, (error, item) => res.send({ error, item }));
    });

    app.delete('/staff/project', app.userType.isStaff, (req, res) => {
        app.model.project.delete(req.body._id, (error) => res.send({ error }));
    });

    app.get('/staff/project-levels/all', app.userType.isStaff, (req, res) => {
        app.model.projectLevel.getAll((error, items) => res.send({ error, items }));
    });

    // Hook upload files ---------------------------------------------------------------------------------------------------------------------------s
    const projectImportData = (req, fields, files, params, done) => {
        if (fields.userData && fields.userData[0] && fields.userData[0] == 'projectImportData' && files.ProjectFile && files.ProjectFile.length > 0) {
            console.log('Hook: projectImportData');
            app.uploadProjectFile(req, files.ProjectFile[0].path, done);
        }
    };

    app.uploadHooks.add('projectImportData', (req, fields, files, params, done) => projectImportData(req, fields, files, params, done));
};