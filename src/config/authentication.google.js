module.exports = (app) => {
    const passport = require('passport');
    const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

    const GoogleStrategyConfig = {
        clientID: '1073789668013-i6lc1494pdihl9tke4ihpci1vfo4aode.apps.googleusercontent.com',
        clientSecret: 'PwR3KOwukkSLXK6lFhitEzE4',
        callbackURL: '/auth/google/callback'
    };

    passport.use(new GoogleStrategy(GoogleStrategyConfig, (accessToken, refreshToken, profile, done) => {
        // Check no email
        if (profile.emails.length == 0) {
            return done(null, false, { 'loginMessage': 'Fail to login!' });
        }

        // Check wether you are BKer or not
        const email = profile.emails[0].value;
        if (!app.isBKer(email) && !(app.isDebug && email.endsWith('@gmail.com'))) {
            return done(null, false, { 'loginMessage': 'Fail to login!' });
        }

        // Get user info
        let firstname = 'firstname', lastname = 'lastname';
        if (profile && profile.name) {
            if (profile.name.givenName) firstname = profile.name.givenName;
            if (profile.name.familyName) lastname = profile.name.familyName;
        }

        // Return Google user
        app.model.user.get({ email }, (error, user) => {
            if (error) {
                done(error);
            } else if (user) {
                done(null, user);
            } else {
                app.model.user.create({ firstname, lastname, email, role: 'user', active: true }, done);
            }
        });
    }));

    // Configure Passport authenticated session persistence
    passport.serializeUser((user, done) => done(null, user._id));
    passport.deserializeUser((_id, done) => app.model.user.get({ _id }, done));

    // Initialize Passport and restore authentication state, if any, from the session.
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions

    // Make sure a user is logged in
    app.isLoggedIn = function (req, res, next) {
        if (req.isAuthenticated()) return next(); // If user is authenticated in the session, carry on
        res.redirect('/login'); // If they aren't redirect them to the home page
    };

    // Do Google login action
    app.get('/auth/google', passport.authenticate('google', { scope: ['email', 'profile'] }));
    // Do Google login callback action
    app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login-fail' }), (req, res) => {
        let permission = [];
        req.user.roles.forEach(role => {
            role.permission.forEach(subPermission => permission.indexOf(subPermission) == -1 && permission.push(subPermission));
        });
        req.session.user = req.user.clone();
        res.redirect('/user');
    });
};