module.exports = app => {
    let systemPermission = {};

    const checkPermissions = (req, res, next, permissions) => {
        if (req.session.user) {
            if (req.session.user.permissions && req.session.user.permissions.contains(permissions)) {
                next();
            } else if (permissions.length == 0) {
                next();
            } else {
                responseError(req, res);
            }
        } else {
            responseError(req, res);
        }
    };

    const getUserByRole = (req, role, done) => {
        app.model.user.get({ roles: role._id }, (error, user) => {
            if (error || user == null) {
                console.error('System has errors!', error);
                done('Error');
            } else {
                req.session.user = user.clone();
                done();
            }
        });
    };

    const responseError = (req, res) => {
        if (req.method.toLowerCase() === 'get') { // is get method
            if (req.originalUrl.startsWith('/api')) {
                res.send({ error: req.session.user ? 'request-permissions' : 'request-login' });
            } else {
                res.redirect(req.session.user ? '/request-permissions' : '/request-login');
            }
        } else {
            res.send({ error: `You don't have permission!` });
        }
    };

    const hasPermissions = (req, success, fail, permissions) => {
        if (req.session.user) {
            if (req.session.user.permissions && req.session.user.permissions.contains(permissions)) {
                success();
            } else {
                fail && fail();
            }
        } else if (permissions.length == 0) {
            success();
        } else {
            fail && fail();
        }
    };


    app.permission = {
        list: () => systemPermission,

        add: (...permissions) => {
            permissions.forEach(permission => {
                if (systemPermission[permission.name] == undefined) {
                    systemPermission[permission.name] = permission.menu;
                }
            });
        },

        check: (...permissions) => {
            return (req, res, next) => {
                if (app.isDebug && app.autoLogin && (req.session.user == null || req.session.user == undefined)) {
                    let cookieDebugRole = req.cookies.debugRole;
                    if (cookieDebugRole == null || cookieDebugRole == undefined) {
                        cookieDebugRole = 'admin';
                    }

                    app.model.role.get({ name: cookieDebugRole }, (error, role) => {
                        if (error || role == null) {
                            app.model.role.get({ name: 'admin' }, (error, role) => {
                                if (error || role == null) {
                                    console.error('System has errors!', error);
                                    res.send({ error: `System has errors!` });
                                } else {
                                    getUserByRole(req, role, error => error ? res.send({ error: `System has errors!` }) : checkPermissions(req, res, next, permissions));
                                }
                            });
                        } else {
                            getUserByRole(req, role, error => error ? res.send({ error: `System has errors!` }) : checkPermissions(req, res, next, permissions));
                        }
                    });
                } else {
                    checkPermissions(req, res, next, permissions);
                }
            };
        },

        has: (req, success, fail, ...permissions) => {
            if (typeof fail == 'string') {
                permissions.unshift(fail);
                fail = null;
            }
            if (app.isDebug && app.autoLogin && (req.session.user == null || req.session.user == undefined)) {
                let cookieDebugRole = req.cookies.debugRole;
                if (cookieDebugRole == null || cookieDebugRole == undefined) {
                    cookieDebugRole = 'admin';
                }
                app.model.role.get({ name: cookieDebugRole }, (error, role) => {
                    if (error || role == null) {
                        app.model.role.get({ name: 'admin' }, (error, role) => {
                            if (error || role == null) {
                                console.error('System has errors!', error);
                                fail && fail();
                            } else {
                                getUserByRole(req, role, error => error ? (fail && fail()) : hasPermissions(req, success, fail, permissions));
                            }
                        });
                    } else {
                        getUserByRole(req, role, error => error ? (fail && fail()) : hasPermissions(req, success, fail, permissions));
                    }
                });
            } else {
                hasPermissions(req, success, fail, permissions);
            }
        },
    };
};
