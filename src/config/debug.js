module.exports = app => {
    if (app.isDebug) {
        app.post('/api/debug/change-role', (req, res) => {
            const role = req.body.role;
            if (app.isDebug) {
                app.model.user.get({ roles: req.body.role }, (error, user) => {
                    if (error || user == null) {
                        res.send({ error });
                    } else {
                        req.session.user = user.clone();
                        console.log(` - Debug as ${user.roles.toString()}!`);
                        res.send({ user: req.session.user });
                    }
                });
            } else {
                res.send({ error: 'Not in debug mode!' });
            }
        });
    }
};