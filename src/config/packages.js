module.exports = (app, http, config) => {
    app.url = require('url');

    // Get information from html forms
    const bodyParser = require('body-parser');
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 100000 }));

    // Cryptography
    app.crypt = require('bcrypt-nodejs');
    app.getToken = length => Array(length).fill('~!@#$%^&*()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz').map(x => x[Math.floor(Math.random() * x.length)]).join('');
    // app.sha256 = require('crypto-js/sha256');

    // Configure session
    app.set('trust proxy', 1); // trust first proxy
    const session = require('express-session');
    const sessionOptions = {
        secret: '@KKPR*u_-_7RXeN^P5!z_2JhV-u5Dq=ZZt3hY5GwU38u?*GsTjcKSXfC24_NWM%^7!_x$z3-Ag&HtLMg!2Anpn84t$Tw9N&ucq9',
        key: 'banquanlymang',
        resave: false,
        saveUninitialized: true,
    };
    if (config && config.sessionRedisStore) {
        const redis = require('redis'),
            redisClient = redis.createClient(),
            redisStore = require('connect-redis')(session);
        redisClient.on('error', error => console.log('Redis error:', error));

        sessionOptions.store = new redisStore({ client: redisClient });
    }
    app.use(session(sessionOptions));

    // Read cookies (needed for auth)
    const cookieParser = require('cookie-parser');
    app.use(cookieParser());

    // Multi upload
    const multiparty = require('multiparty');
    app.getUploadForm = () => new multiparty.Form({ uploadDir: app.uploadPath });

    // Image processing library
    app.jimp = require('jimp');

    // Libraries
    require('./lib/fs')(app);
    require('./lib/string')(app);
    require('./lib/date')(app);
    require('./lib/array')(app);
    require('./lib/email')(app);
    require('./lib/excel')(app);
    require('./lib/schedule')(app);
    require('./lib/language')(app);
};