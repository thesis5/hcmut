module.exports = app => {
    const publicationTypeSchema = app.db.Schema({
        type: String
    });
    const publicationSchema = app.db.Schema({
        user: { type: app.db.Schema.Types.ObjectId, ref: 'User' },
        year: Number,
        title: String,
        type: { type: app.db.Schema.Types.ObjectId, ref: 'PublicationType' },
        active: Boolean
    });
    const publicationTypeModel = app.db.model('PublicationType', publicationTypeSchema);
    const publicationModel = app.db.model('Publication', publicationSchema);

    app.model.publicationType = {
        create: (data, done) => publicationTypeModel.create(data, done),

        getAll: (condition, done) => done ?
            publicationTypeModel.find(condition).sort({ type: +1 }).exec(done) :
            publicationTypeModel.find({}).sort({ type: +1 }).exec(condition),

        get: (_id, done) => publicationTypeModel.findById(_id, done),

        update: (_id, $set, $unset, done) => done ?
            publicationTypeModel.findOneAndUpdate({ _id }, { $set, $unset }, { new: true }, done) :
            publicationTypeModel.findOneAndUpdate({ _id }, { $set }, { new: true }, $unset),

        delete: (_id, done) => publicationTypeModel.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                app.model.publication.updateMany({}, { type: null });
                item.remove(done);
            }
        }),
    };

    app.model.publication = {
        create: (data, done) => publicationModel.create(data, done),

        getAll: (condition, done) => done ?
            publicationModel.find(condition).sort({ year: -1 }).exec(done) :
            publicationModel.find({}).sort({ year: -1 }).exec(condition),

        getPage: (pageNumber, pageSize, condition, done) => publicationModel.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                publicationModel.find(condition).sort({ year: -1 }).skip(skipNumber).limit(result.pageSize).exec((error, list) => {
                    result.list = list;
                    done(error, result);
                });
            }
        }),

        get: (_id, done) => publicationModel.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                done(null, item);
            }
        }),

        update: (_id, $set, $unset, done) => done ?
            publicationModel.findOneAndUpdate({ _id }, { $set, $unset }, { new: true }, done) :
            publicationModel.findOneAndUpdate({ _id }, { $set }, { new: true }, $unset),

        updateMany: (conditions, changes, done) => {
            const updates = {};
            Object.keys(changes).forEach(key => {
                const value = changes[key];
                if (value != null || value == '') {
                    if (updates.$set == null) updates.$set = {};
                    updates.$set[key] = value;
                } else {
                    if (updates.$unset == null) updates.$unset = {};
                    updates.$unset[key] = value;
                }
            });
            publicationModel.updateMany(conditions, updates, { new: true }, done)
        },

        delete: (_id, done) => publicationModel.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                item.remove(done);
            }
        }),

        deleteMany: (condition, done) => publicationModel.deleteMany(condition, (error) => done && done(error))
    };
};
