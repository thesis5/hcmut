module.exports = app => {
    const projectLevelSchema = app.db.Schema({
        level: String
    });
    const projectSchema = app.db.Schema({
        user: { type: app.db.Schema.Types.ObjectId, ref: 'User' },
        title: String,
        startYear: Number,
        endYear: Number,
        isLeader: Boolean,                                                  // Là chủ nhiệm
        level: { type: app.db.Schema.Types.ObjectId, ref: 'ProjectLevel' }, // Cấp quản lý đề tài
        finished: Boolean,                                                  // Đã nghiệm thu
    });

    const projectLevelModel = app.db.model('ProjectLevel', projectLevelSchema);
    const projectModel = app.db.model('Project', projectSchema);

    app.model.projectLevel = {
        create: (data, done) => projectLevelModel.create(data, done),

        getAll: (condition, done) => done ?
            projectLevelModel.find(condition).sort({ level: +1 }).exec(done) :
            projectLevelModel.find({}).sort({ level: +1 }).exec(condition),

        get: (_id, done) => projectLevelModel.findById(_id, done),
        getByLevel: (level, done) => projectLevelModel.findOne({ level }, done),

        update: (_id, $set, $unset, done) => done ?
            projectLevelModel.findOneAndUpdate({ _id }, { $set, $unset }, { new: true }, done) :
            projectLevelModel.findOneAndUpdate({ _id }, { $set }, { new: true }, $unset),

        delete: (_id, done) => projectLevelModel.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                app.model.project.updateMany({}, { level: null });
                item.remove(done);
            }
        }),
    };

    app.model.project = {
        create: (data, done) => projectModel.create(data, done),

        getAll: (condition, done) => done ?
            projectModel.find(condition).sort({ startYear: -1, endYear: -1 }).exec(done) :
            projectModel.find({}).sort({ startYear: -1, endYear: -1 }).exec(condition),

        getPage: (pageNumber, pageSize, condition, done) => projectModel.countDocuments(condition, (error, totalItem) => {
            if (error) {
                done(error);
            } else {
                let result = { totalItem, pageSize, pageTotal: Math.ceil(totalItem / pageSize) };
                result.pageNumber = pageNumber === -1 ? result.pageTotal : Math.min(pageNumber, result.pageTotal);
                const skipNumber = (result.pageNumber > 0 ? result.pageNumber - 1 : 0) * result.pageSize;
                projectModel.find(condition).sort({ startYear: -1, endYear: -1 }).skip(skipNumber).limit(result.pageSize).exec((error, list) => {
                    result.list = list;
                    done(error, result);
                });
            }
        }),

        get: (_id, done) => projectModel.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                done(null, item);
            }
        }),

        update: (_id, $set, $unset, done) => done ?
            projectModel.findOneAndUpdate({ _id }, { $set, $unset }, { new: true }, done) :
            projectModel.findOneAndUpdate({ _id }, { $set }, { new: true }, $unset),

        updateMany: (conditions, changes, done) => {
            const updates = {};
            Object.keys(changes).forEach(key => {
                const value = changes[key];
                if (value != null || value == '') {
                    if (updates.$set == null) updates.$set = {};
                    updates.$set[key] = value;
                } else {
                    if (updates.$unset == null) updates.$unset = {};
                    updates.$unset[key] = value;
                }
            });
            projectModel.updateMany(conditions, updates, { new: true }, done)
        },

        delete: (_id, done) => projectModel.findById(_id, (error, item) => {
            if (error) {
                done(error);
            } else if (item == null) {
                done('Invalid Id!');
            } else {
                item.remove(done);
            }
        }),

        deleteMany: (condition, done) => projectModel.deleteMany(condition, (error) => done && done(error))
    };
};
