module.exports = app => {
    const schema = app.db.Schema({
        userId: { type: app.db.Schema.Types.ObjectId, ref: 'User' }, // Tham chiếu đến cán bộ trong bản User (isStaff = true)
        NGHI: String,
        IS_NNGOAI: Number,
        LOAI: String,
        shcc: String,
        HO: String,
        TEN: String,
        PHAI: String,
        NGAY_SINH: String,
        NGND: String,
        NGUT: String,
        NGAY_BD_CT: String,
        NGAY_VAO: String,
        NGAY_CBGD: String,
        NGAY_BC: String,
        NGAY_NGHI: String,
        CHUCDANH_TRINHDO: String,
        NGACH: String,
        NGACH_MOI: String,
        CHUC_DANH: String,
        TRINH_DO: String,
        NUOC_NGOAI: String,
        GHI_CHU_IN: String,
        HESO_LG: Number,
        BAC_LG: String,
        MOC_NANG_LG: Date,
        NGAY_HUONG_LG: String,
        TY_LE_VUOT_KHUNG: Number,
        PCCV: Number,
        NGAY_PCCV: String,
        MS_CVU: String,
        CHUC_VU_BCH_DANG_BO: String,
        CHUC_VU_BCH_CONG_DOAN: String,
        CHUC_VU_BCH_DOAN_TN: String,
        CHUYEN_NGANH: String,
        TDO_LLCT: String,
        NOI_DKHK: String,
        DC_HIENTAI: String,
        D_THOAI: String,
        E_mail: String,
        Dan_toc: String,
        Ton_giao: String,
        DANG_VIEN: Number,
        MS_BM: String,
        TEN_BM: String,
        MS_KHOA: String,
        TEN_KHOA: String,
        PHUC_LOI: String,
        GHI_CHU: String
    });
    const model = app.db.model('Staff', schema);

    app.model.staff = {
        create: (data, done) => model.create(data, done),

        get: (condition, done) => typeof condition == 'string' ? model.findById(condition, done) : model.findOne(condition, done), // condition is _id => string

        getUser: (condition, done) => app.model.user.get(condition, (error, user) => {
            if (error) {
                done && done(error);
            } else if (!user) {
                done && done('Invalid user id!');
            } else {
                model.findOne({ userId: user._id }, (error, staffInfo) => {
                    if (error) {
                        done && done(error);
                    } else {
                        done && done(null, app.clone(user, { staffInfo: staffInfo ? staffInfo : {} }));
                    }
                })
            }
        }),

        getByShcc: (shcc, done) => model.findOne({ shcc }, (error, staff) => done(error, staff)),

        getPage: (pageNumber, pageSize, condition, done) => app.model.user.getPage(pageNumber, pageSize, condition, (error, page) => {
            if (error || page == null) {
                done('System has errors!');
            } else {
                const getPoint = (index) => {
                    if (index < page.list.length) {
                        model.findOne({ userId: page.list[index]._id }, (error, point) => {
                            page.list[index] = app.clone(page.list[index], { point: point ? point : {} });
                            getPoint(index + 1);
                        });
                    } else {
                        done(null, page)
                    }
                };
                getPoint(0);
            }
        }),

        getAll: (condition, done) => done ?
            model.find(condition).sort({ _id: -1 }).exec(done) :
            model.find({}).sort({ _id: -1 }).exec(condition),

        update: (userId, changes, done) => {
            model.findOne({ userId }, (error, staff) => {
                if (error) {
                    done && done(error);
                } else if (!staff) {
                    changes.userId = userId;
                    model.create(changes, done);
                } else {
                    delete changes.userId;
                    model.findOneAndUpdate({ _id: staff._id }, { $set: changes }, { new: true }, done);
                }
            })
        },

        delete: (condition, done) => model.findOne(condition, (error, item) => {
            if (error) {
                done && done(error);
            } else if (item == null) {
                done && done();
            } else {
                item.remove(done);
            }
        }),
    };
};
