const package = require('./package');
const express = require('express');
const app = express();
app.fs = require('fs');
app.path = require('path');
app.isDebug = !__dirname.startsWith('/var/www/');
const server = app.isDebug ?
    require('http').createServer(app) :
    require('https').createServer({
        key: app.fs.readFileSync('./asset/ssl/private.pem'),
        cert: app.fs.readFileSync('./asset/ssl/public.pem'),
    }, app);

// Variables ==================================================================
app.title = package.title;
app.port = package.port;
app.version = package.version;
app.description = package.description;
app.keywords = package.keywords ? JSON.stringify(package.keywords) : '';
app.autoLogin = package.autoLogin;
app.rootUrl = package.rootUrl;
app.mongodb = 'mongodb://localhost:27017/' + package.dbName;
app.email = package.email;
app.mailCc = ['hithanhtung@gmail.com'];
app.mailAdmin = ['thanhtung@hcmut.edu.vn'];
app.defaultAdminEmail = package.default.adminEmail;
app.defaultAdminPassword = package.default.adminPassword;
app.assetPath = app.path.join(__dirname, 'asset');
app.srcPath = app.path.join(__dirname, package.path.src);
app.viewPath = app.path.join(__dirname, package.path.view);
app.modelPath = app.path.join(__dirname, package.path.model);
app.controllerPath = app.path.join(__dirname, package.path.controller);
app.modulePath = app.path.join(__dirname, package.path.module);
app.publicPath = app.path.join(__dirname, package.path.public);
app.imagePath = app.path.join(package.path.public, 'img');
app.logPath = app.path.join(__dirname, package.path.log);
app.uploadPath = app.path.join(__dirname, package.path.upload);
app.faviconPath = app.path.join(__dirname, package.path.favicon);

// Configure ==================================================================
require('./src/config/common')(app);
require('./src/config/view')(app, express);
require('./src/config/packages')(app, server, package);
require('./src/config/database')(app);
require('./src/config/authentication')(app);
require('./src/config/permission')(app);
require('./src/config/debug')(app);
require('./src/config/authentication.cas')(app, package);
// require('./src/config/authentication.google')(app);
require('./src/config/io')(app, server);

app.templates = {
    home: (req, res) => app.createResponse(req, res, '/home.template'),
    admin: (req, res) => app.createResponse(req, res, '/admin.template'),
};

// Init =======================================================================
app.loadModels();
app.loadControllers();
app.loadModules();
app.setupAdmin();
app.createFolder(app.publicPath, app.uploadPath);
app.createDebugUser();

// Default route ==============================================================
app.get('/user', app.permission.check(), app.templates.admin);
app.get('*', (req, res, next) => {
    const path = req.path.endsWith('/') && req.path.length > 1 ? req.path.substring(0, req.path.length - 1) : req.path;
    app.model.menu.get({ link: path }, (error, menu) => {
        if (error || menu == null) {
            next();
        } else {
            app.createResponse(req, res, '/home.template');
        }
    });
});

require('./src/config/error')(app);

// Launch website =============================================================
app.keywords = (app.keywords && app.keywords.length > 2 ? app.keywords.substring(2, app.keywords.length - 2) : '').replaceAll('","', ', ');
app.debugUrl = 'http://localhost:' + app.port;
server.listen(app.port, () => console.log(` - ${app.title} on ${app.debugUrl}`));
